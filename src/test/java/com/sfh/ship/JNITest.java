/*
 * Copyright SINTEF FIsheries and Aquaqulture Use of this software and source
 * code is prohibited without explicit authorization
 */
package com.sfh.ship;

import com.sfh.jni.FhSim;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author aarsathe
 */
public class JNITest {

    /**
     * Rigorous Test :-)
     */
    @Test
    public void testAppLoadingWithoutVisualization() {
        performTest(false);
    }
    
    @Ignore
    @Test
    public void testAppLoadingWithVisualization() {
        performTest(true);
    }

    protected void performTest(boolean visualization) throws UnsatisfiedLinkError {
        runSimulation(setupEnvironment(visualization));
    }

    private void runSimulation(String jnipath) throws UnsatisfiedLinkError {
        String os = System.getProperty("os.name");
        if (os.startsWith("Mac")) {
            // Currently not able to run on OS X:
            return;
        } else {
            assert( FhSim.setupFromFile(jnipath + "..\\..\\input\\onemass.xml", "target/results.csv", "target/log.txt") );
            
            // step FhSim through 60seconds of simulation
            double T = 0;
            while( T <= 60){
                FhSim.simulate(0.1);
                T += 0.1;
            }

            FhSim.stop();
            FhSim.unload();
        }
    }

    protected String setupEnvironment(boolean visualization) {
        String userdir = System.getProperty("user.dir");
        String arch = System.getProperty("os.arch");
        String resources = userdir + "\\src\\main\\files\\ogre\\";
        String jnipath = userdir + "\\src\\main\\files\\bin\\";
        switch (arch) {
            case "x86":
                jnipath += "i386\\";
                resources += "i386\\";
                break;
            case "amd64":
                jnipath += "x64\\";
                resources += "x64\\";
                break;
            default:
                assert(false);
        }
        System.out.println(arch);
        FhSim.setResourcePath(resources);
        FhSim.setJNIPath(jnipath);
        FhSim.setVisualization(visualization);
        return jnipath;
    }
}
