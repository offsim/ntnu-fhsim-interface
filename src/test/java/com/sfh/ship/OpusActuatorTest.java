package com.sfh.ship;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.sfh.jni.FhSim;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test class which verifies that the first order dynamic model 
 * for RPM, pitch angle and azimuth angle works as advertised
 * 5*time constant will give in excess of 99% of the desired response
 * 
 * Each dt=timconstant will bring the variables 63% closer to the reference
 * value. 
 * 
 * The time constants in the java API are divided by 5 in order to make the 
 * response time constants conform to the notion that the time constant equals 
 * signal rise time. 
 * 
 * The limitations of the properties are also tested. When the desired value is 
 * larger than the limit, the dynamic will behave as if the desired value is the
 * limit value (ie, not possible to lower the response time by ordering more than 
 * the limit value. 
 * 
 * @author aarsathe
 */
public class OpusActuatorTest {
  
    private final double fraction = 1- 1/Math.E;
    public OpusActuatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
   
    /**
     * Test the response of the RPM to a step in reference from 0 to 200 RPM
     * at T=10 seconds
     */
    @Test
    public void actuatorRPM(){
        FhSim.setupFromFile("src/test/resources/inputfiles/actuator.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(10); 
        double [] rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1);       
        assert( rpm[0] == 0);
        
        FhSim.simulate(10); 
        rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1);
        double rpm_nom = rpm[0]/(200*fraction);
        assert( ( rpm_nom -1)<1e-3 );
        
        FhSim.simulate(10); 
        rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1); 
        rpm_nom = rpm[0]/( 200*fraction + (200-200*fraction)*fraction);
        assert( ( rpm_nom -1)<1e-3 );
        
        FhSim.simulate(35); 
        rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1); 
        rpm_nom = rpm[0]/(200);
        assert( ( rpm_nom -1)<1e-2 ); 
    }
    
    /**
     * Test the response of the RPM to a step in reference from 0 to 200 RPM
     * at T=180 seconds
     */
    @Test
    public void actuatorRPMNegative(){
        FhSim.setupFromFile("src/test/resources/inputfiles/actuator.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(180); 
        double [] rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1);       
        assert( (rpm[0]) <1e-2*200); // not exatcly zero as it is the continuation of the positive test
        
        FhSim.simulate(10); 
        rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1);
        double rpm_nom = rpm[0]/(-200*fraction);
        assert( ( rpm_nom -1)<1e-3 );
        
        FhSim.simulate(10); 
        rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1); 
        rpm_nom = rpm[0]/( -200*fraction + (-200+200*fraction)*fraction);
        assert( ( rpm_nom -1)<1e-3 );
        
        FhSim.simulate(35); 
        rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1); 
        rpm_nom = rpm[0]/(-200);
        assert( ( rpm_nom -1)<1e-2 ); 
    }
    
    /**
     * Test the response of the RPM to a step in reference from 0 to 300 RPM
     * at T=10 seconds, where the maximum RPM is 200
     */
    @Test
    public void actuatorRPMLimit(){
        FhSim.setupFromFile("src/test/resources/inputfiles/actuatorLimits.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(10); 
        double [] rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1);       
        assert( rpm[0] == 0);
        
        FhSim.simulate(10); 
        rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1);
        double rpm_nom = rpm[0]/(200*fraction);
        assert( ( rpm_nom -1)<1e-3 );
        
        FhSim.simulate(10); 
        rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1); 
        rpm_nom = rpm[0]/( 200*fraction + (200-200*fraction)*fraction);
        assert( ( rpm_nom -1)<1e-3 );
        
        FhSim.simulate(35); 
        rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1); 
        rpm_nom = rpm[0]/(200);
        assert( ( rpm_nom -1)<1e-2 ); 
    }
    
    /**
     * Test the response of the RPM to a step in reference from 0 to -300 RPM
     * at T=10 seconds, where the minimum RPM is -200
     */
    @Test
    public void actuatorRPMLimitNegative(){
        FhSim.setupFromFile("src/test/resources/inputfiles/actuatorLimits.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(180); 
        double [] rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1);       
        assert( (rpm[0]) <1e-2*200);
        
        FhSim.simulate(10); 
        rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1);
        double rpm_nom = rpm[0]/(-200*fraction);
        assert( ( rpm_nom -1)<1e-3 );
        
        FhSim.simulate(10); 
        rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1); 
        rpm_nom = rpm[0]/( -200*fraction + (-200+200*fraction)*fraction);
        assert( ( rpm_nom -1)<1e-3 );
        
        FhSim.simulate(35); 
        rpm = FhSim.getOutput("conMain0IO","RPM_FB" , 1); 
        rpm_nom = rpm[0]/(-200);
        assert( ( rpm_nom -1)<1e-2 ); 
    }

    
    /**
     * Test the response of the pitch angle to a step in reference from 0 to 20 degrees
     * at T=10 seconds
     */
    @Test
    public void actuatorPitch(){
        FhSim.setupFromFile("src/test/resources/inputfiles/actuator.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(10); 
        double [] pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1);       
        assert( pitch[0] == 0);
        
        FhSim.simulate(10); 
        pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1);
        double pitch_nom = 180/Math.PI*pitch[0]/(20*fraction);
        assert( ( pitch_nom -1)<1e-3 );
        
        FhSim.simulate(10); 
        pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1); 
        pitch_nom = 180/Math.PI*pitch[0]/( 20*fraction + (20-20*fraction)*fraction);
        assert( ( pitch_nom -1)<1e-3 );
        
        FhSim.simulate(30); 
        pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1); 
        pitch_nom = 180/Math.PI*pitch[0]/(20);
        assert( ( pitch_nom -1)<1e-2 );    
    }

    /**
     * Test the response of the pitch angle to a step in reference from 0 to -20 degrees
     * at T=180 seconds
     */
    @Test
    public void actuatorPitchNegative(){
        FhSim.setupFromFile("src/test/resources/inputfiles/actuator.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(180); 
        double [] pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1);       
        assert( (pitch[0]) < 1e-3); // not zero du to test beeing continuation of the positive simulation 
        
        FhSim.simulate(10); 
        pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1);
        double pitch_nom = 180/Math.PI*pitch[0]/(-20*fraction);
        assert( ( pitch_nom -1)<1e-3 );
        
        FhSim.simulate(10); 
        pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1); 
        pitch_nom = 180/Math.PI*pitch[0]/( -20*fraction + (-20+20*fraction)*fraction);
        assert( ( pitch_nom -1)<1e-3 );
        
        FhSim.simulate(30); 
        pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1); 
        pitch_nom = 180/Math.PI*pitch[0]/(-20);
        assert( ( pitch_nom -1)<1e-2 );    
    }

    /**
     * Test the response of the pitch angle to a step in reference from 0 to 30 degrees
     * at T=10 seconds with a limit of 20 degrees
     */
    @Test
    public void actuatorPitchLimit(){
        FhSim.setupFromFile("src/test/resources/inputfiles/actuatorLimits.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(10); 
        double [] pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1);       
        assert( pitch[0] == 0);
        
        FhSim.simulate(10); 
        pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1);
        double pitch_nom = 180/Math.PI*pitch[0]/(20*fraction);
        assert( ( pitch_nom -1)<1e-2 );
        
        FhSim.simulate(10); 
        pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1); 
        pitch_nom = 180/Math.PI*pitch[0]/( 20*fraction + (20-20*fraction)*fraction);
        assert( ( pitch_nom -1)<1e-2 );
        
        FhSim.simulate(30); 
        pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1); 
        pitch_nom = 180/Math.PI*pitch[0]/(20);
        assert( ( pitch_nom -1)<1e-2 );    
    }

    /**
     * Test the response of the pitch angle to a step in reference from 0 to -30 degrees
     * at T=180 seconds with a limit of -20 degrees
     */
    @Test
    public void actuatorPitchLimitNegative(){
        FhSim.setupFromFile("src/test/resources/inputfiles/actuatorLimits.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(180); 
        double [] pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1);       
        assert( (pitch[0]) < 1e-3);
        
        FhSim.simulate(10); 
        pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1);
        double pitch_nom = 180/Math.PI*pitch[0]/(-20*fraction);
        assert( ( pitch_nom -1)<1e-2 );
        
        FhSim.simulate(10); 
        pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1); 
        pitch_nom = 180/Math.PI*pitch[0]/( -20*fraction + (-20+20*fraction)*fraction);
        assert( ( pitch_nom -1)<1e-2 );
        
        FhSim.simulate(30); 
        pitch = FhSim.getOutput("conMain0IO","Pitch_FB" , 1); 
        pitch_nom = 180/Math.PI*pitch[0]/(-20);
        assert( ( pitch_nom -1)<1e-2 );    
    }
    
    
    /**
     * Test the response of the azimuth angle to a step in reference from 0 to 180 degrees
     * at T=10 seconds 
     */
    @Test
    public void actuatorAngle(){
        FhSim.setupFromFile("src/test/resources/inputfiles/actuator.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(10); 
        double [] angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1);       
        assert( angle[0] == 0);
        
        FhSim.simulate(10); 
        angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1);
        double angle_nom = 180/Math.PI*angle[0]/(180*fraction);
        assert( ( angle_nom -1)<1e-3 );
        
        FhSim.simulate(10); 
        angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1); 
        angle_nom = 180/Math.PI*angle[0]/( 180*fraction + (180-180*fraction)*fraction);
        assert( ( angle_nom -1)<1e-3 );
        
        FhSim.simulate(30); 
        angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1); 
        angle_nom = 180/Math.PI*angle[0]/(180);
        assert( ( angle_nom -1)<1e-2 );    
    }
    
    /**
     * Test the response of the azimuth angle to a step in reference from 0 to -180 degrees
     * at T=180 seconds 
     */
    @Test
    public void actuatorAngleNegative(){
        FhSim.setupFromFile("src/test/resources/inputfiles/actuator.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(180); 
        double [] angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1);       
        assert( (angle[0]) <1e-3);
        
        FhSim.simulate(10); 
        angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1);
        double angle_nom = 180/Math.PI*angle[0]/(-180*fraction);
        assert( ( angle_nom -1)<1e-3 ); // not zero due to continuation of positive test
        
        FhSim.simulate(10); 
        angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1); 
        angle_nom = 180/Math.PI*angle[0]/( -180*fraction + (-180+180*fraction)*fraction);
        assert( ( angle_nom -1)<1e-3 );
        
        FhSim.simulate(30); 
        angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1); 
        angle_nom = 180/Math.PI*angle[0]/(-180);
        assert( ( angle_nom -1)<1e-2 );    
    }    
    
    /**
     * Test the response of the azimuth angle to a step in reference from 0 to 180 degrees
     * at T=10 seconds with a limit of 90
     */
    @Test
    public void actuatorAngleLimit(){
        FhSim.setupFromFile("src/test/resources/inputfiles/actuatorLimits.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(10); 
        double [] angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1);       
        assert( angle[0] == 0);
        
        FhSim.simulate(10); 
        angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1);
        double angle_nom = 180/3.14*angle[0]/(90*fraction);
        assert( ( angle_nom -1)<1e-3 );
        
        FhSim.simulate(10); 
        angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1); 
        angle_nom = 180/3.14*angle[0]/( 90*fraction + (90-90*fraction)*fraction);
        assert( ( angle_nom -1)<1e-3 );
        
        FhSim.simulate(30); 
        angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1); 
        angle_nom = 180/3.14*angle[0]/(90);
        assert( ( angle_nom -1)<1e-2 );    
    }
    
    /**
     * Test the response of the azimuth angle to a step in reference from 0 to -180 degrees
     * at T=180 seconds with a limit of -90
     */
    @Test
    public void actuatorAngleLimitNegative(){
        FhSim.setupFromFile("src/test/resources/inputfiles/actuatorLimits.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(180); 
        double [] angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1);       
        assert( (angle[0]) <1e-3);
        
        FhSim.simulate(10); 
        angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1);
        double angle_nom = 180/Math.PI*angle[0]/(-90*fraction);
        assert( ( angle_nom -1)<1e-3 );
        
        FhSim.simulate(10); 
        angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1); 
        angle_nom = 180/Math.PI*angle[0]/( -90*fraction + (-90+90*fraction)*fraction);
        assert( ( angle_nom -1)<1e-3 );
        
        FhSim.simulate(30); 
        angle = FhSim.getOutput("conMain0IO","Angle_FB" , 1); 
        angle_nom = 180/Math.PI*angle[0]/(-90);
        assert( (angle_nom -1) <1e-2 );    
    }       
    
    
    @Before
    public void setUp() {
        String arch = System.getProperty("os.arch");
        String userdir = System.getProperty("user.dir");
        String resources = userdir + "\\src\\main\\files\\ogre\\";
        String jnipath = userdir + "\\src\\main\\files\\bin\\";
        
        switch (arch) {
            case "x86":
                jnipath += "i386\\";
                resources += "i386\\";
                break;
            case "amd64":
                jnipath += "x64\\";
                resources += "x64\\";
                break;
            default:
                assert(false);
        }    
        FhSim.setJNIPath(jnipath);
        FhSim.setResourcePath(resources);
        FhSim.setVisualization(false);
    }
    
    @After
    public void tearDown() {
        FhSim.stop();
        FhSim.unload();
    }
    
}
