
package com.sfh.ship;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 *
 * @author aarsathe
 */
public class FhSimBaseLibraryTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FhSimBaseLibraryTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FhSimBaseLibraryTest.class);
    }

    public static void addDir(String s) throws IOException {
        try {
            // This enables the java.library.path to be modified at runtime
            // From a Sun engineer at http://forums.sun.com/thread.jspa?threadID=707176
            //
            Field field = ClassLoader.class.getDeclaredField("usr_paths");
            field.setAccessible(true);
            String[] paths = (String[]) field.get(null);
            for (int i = 0; i < paths.length; i++) {
                if (s.equals(paths[i])) {
                    return;
                }
            }
            String[] tmp = new String[paths.length + 1];
            System.arraycopy(paths, 0, tmp, 0, paths.length);
            tmp[paths.length] = s;
            field.set(null, tmp);
            System.setProperty("java.library.path", System.getProperty("java.library.path") + File.pathSeparator + s);
        } catch (IllegalAccessException e) {
            throw new IOException("Failed to get permissions to set library path");
        } catch (NoSuchFieldException e) {
            throw new IOException("Failed to get field handle to set library path");
        }
    }

    /**
     * Tests the presence of the FhSim Dll, and successfull loading
     */
    public void testApp() throws IOException {
        String userdir = System.getProperty("user.dir");
        String arch = System.getProperty("os.arch");
        String jnipath = userdir + "\\src\\main\\files\\bin\\";
        switch (arch) {
            case "x86":
                jnipath += "i386\\";
                break;
            case "amd64":
                jnipath += "x64\\";
                break;
            default:
                assert(false);
        }               
        addDir(jnipath+"SimObjectLibraries\\");
        String os = System.getProperty("os.name");
        if (os.startsWith("Mac")) {
            return;
        }
        System.loadLibrary("FhSimBaseLibrary");
        
        System.loadLibrary("../OgreMain");
        System.loadLibrary("../OgreOverLay");
        System.loadLibrary("FhSimBaseLibraryVis");
    }
}
