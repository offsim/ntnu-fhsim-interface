
package com.sfh.ship;

import com.sfh.ship.report.customPropulsorPositionCompare;
import com.sfh.ship.vesselExamples.BaseExample;
import com.sfh.ship.vesselExamples.BaseExample.PropulsorActuatorDataPair;
import java.util.ArrayList;
import java.util.Collections;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 *
 * @author Pierre Major
 */
public class CustomPropulsorPositionCompareTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public CustomPropulsorPositionCompareTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(CustomPropulsorPositionCompareTest.class);
    }

    public  void testPortSideComesBeforeStarboard()  {
        ShipPoint port = new ShipPoint(0, -1);
        ShipPoint starboard = new ShipPoint(0, 1);
        
        customPropulsorPositionCompare comparator = new customPropulsorPositionCompare();
        assertEquals(-1,comparator.compare(port, starboard));     
    }

    
    public  void testBowComesBeforeStern()  {
        ShipPoint bow = new ShipPoint(1, 1);
        ShipPoint stern = new ShipPoint(-1, 1);
        
        customPropulsorPositionCompare comparator = new customPropulsorPositionCompare();
        assertEquals(-1,comparator.compare(bow, stern));    
    } 
    
    public  void testCollectionBowStern()  {
        ShipPoint bow = new ShipPoint(1, 1);
        ShipPoint stern = new ShipPoint(-1, 1);
        
         ArrayList<BaseExample.IShipPoint> propulsors = new ArrayList<BaseExample.IShipPoint>();
         
        propulsors.add(stern);
        propulsors.add(bow);
         
        Collections.sort(propulsors, new customPropulsorPositionCompare() );
        
        assertEquals(propulsors.get(0), bow);
        assertEquals(propulsors.get(1), stern);
        
    }
    
      public  void testCollectionPortStarboard()  {
        ShipPoint port = new ShipPoint(0, -1);
        ShipPoint starboard = new ShipPoint(0, 1);
        
         ArrayList<BaseExample.IShipPoint> propulsors = new ArrayList<>();
         propulsors.add(port);
         propulsors.add(starboard);
        
        Collections.sort(propulsors, new customPropulsorPositionCompare() );
        
        assertEquals(propulsors.get(0), port);
        assertEquals(propulsors.get(1), starboard);
        
    }
    /**
     * Simple implementation of the IShipPoint as opposed to BaseExample.PropulsorActuatorDataPair
     */
    private class ShipPoint implements BaseExample.IShipPoint{
        double x;
        double y;
        
        public ShipPoint(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
                
        @Override
        public double getX() {
            return x;
        }

        @Override
        public double getY() {
           return y;
        }

        @Override
        public double getZ() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getFormattedX() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getFormattedY() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getFormattedZ() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }
    
}
