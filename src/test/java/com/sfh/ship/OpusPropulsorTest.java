/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sfh.ship;

import com.sfh.jni.FhSim;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author aarsathe
 */
public class OpusPropulsorTest {
    
    private String input_file;
    public OpusPropulsorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Test
    public void setPropulsors(){
        FhSim.setupFromFile("src/test/resources/inputfiles/propulsor.xml","target/results.cvs","target/propulsorlog.txt");
        FhSim.simulate(10);
        
        double [] f_open = FhSim.getOutput("IO", "openForce", 3);
        double [] t_open = FhSim.getOutput("IO", "openTorque", 3);

        double [] f_ducted = FhSim.getOutput("IO", "ductedTorque", 3);
        double [] t_ducted = FhSim.getOutput("IO", "ductedTorque", 3);

        double [] f_tunnel = FhSim.getOutput("IO", "tunnelTorque", 3);
        double [] t_tunnel = FhSim.getOutput("IO", "tunnelTorque", 3);

        // before ramping rpm
        assert( f_open[0] == 0);
        assert( f_open[1] == 0);
        assert( f_open[2] == 0);
        
        assert( t_open[0] == 0);
        assert( t_open[1] == 0);
        assert( t_open[2] == 0);

        assert( f_tunnel[0] == 0);
        assert( f_tunnel[1] == 0);
        assert( f_tunnel[2] == 0);
        
        assert( t_tunnel[0] == 0);
        assert( t_tunnel[1] == 0);
        assert( t_tunnel[2] == 0);

        assert( f_ducted[0] == 0);
        assert( f_ducted[1] == 0);
        assert( f_ducted[2] == 0);
        
        assert( t_ducted[0] == 0);
        assert( t_ducted[1] == 0);
        assert( t_ducted[2] == 0);

        FhSim.simulate(5); // rpm ~= 0
        f_open = FhSim.getOutput("IO", "openForce", 3);
        t_open = FhSim.getOutput("IO", "openTorque", 3);
        
        f_ducted = FhSim.getOutput("IO", "ductedTorque", 3);
        t_ducted = FhSim.getOutput("IO", "ductedTorque", 3);

        f_tunnel = FhSim.getOutput("IO", "tunnelTorque", 3);
        t_tunnel = FhSim.getOutput("IO", "tunnelTorque", 3);

        assert( Math.abs(f_open[0]) < 1e-4);
        assert( f_open[1] == 0);
        assert( f_open[2] == 0);
        
        assert( Math.abs(t_open[0]) < 1e-4);
        assert( t_open[1] == 0);
        assert( t_open[2] == 0);
        
     /*   assert( f_tunnel[0] == 0);
        assert( f_tunnel[1] == 0);
        assert( f_tunnel[2] == 0);
        
        assert( t_tunnel[0] == 0);
        assert( t_tunnel[1] == 0);
        assert( t_tunnel[2] == 0);

        assert( f_ducted[0] == 0);
        assert( f_ducted[1] == 0);
        assert( f_ducted[2] == 0);
        
        assert( t_ducted[0] == 0);
        assert( t_ducted[1] == 0);
        assert( t_ducted[2] == 0);     
        
        FhSim.simulate(5); // rpm ~= 0
        f_open = FhSim.getOutput("IO", "openForce", 3);
        t_open = FhSim.getOutput("IO", "openTorque", 3);
        
        f_ducted = FhSim.getOutput("IO", "ductedTorque", 3);
        t_ducted = FhSim.getOutput("IO", "ductedTorque", 3);

        f_tunnel = FhSim.getOutput("IO", "tunnelTorque", 3);
        t_tunnel = FhSim.getOutput("IO", "tunnelTorque", 3);

        assert( f_open[0] < 0);
        assert( f_open[1] == 0);
        assert( f_open[2] == 0);
        
        assert( t_open[0] < 0);
        assert( t_open[1] == 0);
        assert( t_open[2] == 0);
        
        assert( f_tunnel[0] == 0);
        assert( f_tunnel[1] == 0);
        assert( f_tunnel[2] == 0);
        
        assert( t_tunnel[0] == 0);
        assert( t_tunnel[1] == 0);
        assert( t_tunnel[2] == 0);

        assert( f_ducted[0] == 0);
        assert( f_ducted[1] == 0);
        assert( f_ducted[2] == 0);
        
        assert( t_ducted[0] == 0);
        assert( t_ducted[1] == 0);
        assert( t_ducted[2] == 0);        

        FhSim.simulate(10);*/
        
    }
    
    @Before
    public void setUp() {
        String arch = System.getProperty("os.arch");
        String userdir = System.getProperty("user.dir");
        String resources = userdir + "\\src\\main\\files\\ogre\\";
        String jnipath = userdir + "\\src\\main\\files\\bin\\";
        
        switch (arch) {
            case "x86":
                jnipath += "i386\\";
                resources += "i386\\";
                break;
            case "amd64":
                jnipath += "x64\\";
                resources += "x64\\";
                break;
            default:
                assert(false);
        }    
        FhSim.setJNIPath(jnipath);
        FhSim.setResourcePath(resources);
        FhSim.setVisualization(false);
    }
    
    @After
    public void tearDown() {
        FhDynamicModel.reset();
    }
    
}
