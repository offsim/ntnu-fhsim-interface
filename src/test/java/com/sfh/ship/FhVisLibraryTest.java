/*
 * Copyright SINTEF FIsheries and Aquaqulture Use of this software and source
 * code is prohibited without explicit authorization
 */
package com.sfh.ship;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.junit.Ignore;

/**
 *
 * @author aarsathe
 */
@Ignore
public class FhVisLibraryTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FhVisLibraryTest(String testName) {
        super(testName);
    }

    public static void addDir(String s) throws IOException {
        try {
            // This enables the java.library.path to be modified at runtime
            // From a Sun engineer at http://forums.sun.com/thread.jspa?threadID=707176
            //
            Field field = ClassLoader.class.getDeclaredField("usr_paths");
            field.setAccessible(true);
            String[] paths = (String[]) field.get(null);
            for (int i = 0; i < paths.length; i++) {
                if (s.equals(paths[i])) {
                    return;
                }
            }
            String[] tmp = new String[paths.length + 1];
            System.arraycopy(paths, 0, tmp, 0, paths.length);
            tmp[paths.length] = s;
            field.set(null, tmp);
            System.setProperty("java.library.path", System.getProperty("java.library.path") + File.pathSeparator + s);
        } catch (IllegalAccessException e) {
            throw new IOException("Failed to get permissions to set library path");
        } catch (NoSuchFieldException e) {
            throw new IOException("Failed to get field handle to set library path");
        }
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FhVisLibraryTest.class);
    }

    /**
     * Tests loading of FhVis with ogre and dependencies (OIS, CG, and rendersystem)
     */
    public void VisualizationLibraries() throws IOException {
        String userdir = System.getProperty("user.dir");
        String arch = System.getProperty("os.arch");
        String jnipath = userdir + "\\src\\main\\files\\bin\\";
        switch (arch) {
            case "x86":
                jnipath += "i386\\";
                break;
            case "amd64":
                jnipath += "x64\\";
                break;
            default:
                assert(false);
        }               
        addDir(jnipath);
        
        String os = System.getProperty("os.name");
        if (os.startsWith("Mac")) {
            return;
        } else {
            System.loadLibrary("cg");
            System.loadLibrary("OgreMain");
            System.loadLibrary("OIS");
            System.loadLibrary("Plugin_CgProgramManager");
            System.loadLibrary("RenderSystem_GL");
            System.loadLibrary("fhVisDll");
        }
    }
}
