/*
 * Copyright SINTEF FIsheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

import com.sfh.ship.FhDynamicModel;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 *
 * @author aarsathe
 */
public class DynamicModelTest extends TestCase{
 
    public DynamicModelTest( String testName )
    {
	super(testName);
    }
    
    public static Test suite()
    {
	return new TestSuite( DynamicModelTest.class );
    }
    
    public void testApp()
    {
	FhDynamicModel myModel1 = FhDynamicModel.instance();
	FhDynamicModel myModel2 = FhDynamicModel.instance();
	
	assert( myModel1.equals(myModel2) );
	System.out.println("Singelton test OK");
	
	assert( myModel1.getObjectElement() != null );
	assert( myModel1.getInitializationElement() != null);
	assert( myModel1.getIntegrationElement() != null );
	assert( myModel1.getInterconnectionElement() != null);

	
    }
}
