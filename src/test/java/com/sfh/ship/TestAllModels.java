//package com.sfh.ship;
//
//import com.sfh.ship.vesselExamples.A122;
//import com.sfh.ship.vesselExamples.AX104;
//import com.sfh.ship.vesselExamples.H845;
//import com.sfh.ship.vesselExamples.H858;
//import com.sfh.ship.vesselExamples.MT6015;
//import com.sfh.ship.vesselExamples.MT6022;
//import com.sfh.ship.vesselExamples.NSG;
//import com.sfh.ship.vesselExamples.PX105;
//import com.sfh.ship.vesselExamples.UT712;
//import com.sfh.ship.vesselExamples.UT712_mod;
//import com.sfh.ship.vesselExamples.VS4412;
//import junit.framework.Test;
//import junit.framework.TestCase;
//import junit.framework.TestSuite;
//
///**
// *
// * @author Pierre Major
// * 
// * If those test fail, your Fhsim model repo is out of sync or you have made somthing wrong
// * @todo: prevent the test from relying on the models being in ../fhsim_models ...
// */
//public class TestAllModels extends TestCase {
//
//    /**
//     * Create the test case
//     *
//     * @param testName name of the test case
//     */
//    public TestAllModels(String testName) {
//        super(testName);
//    }
//
//    /**
//     * @return the suite of tests being tested
//     */
//    public static Test suite() {
//        return new TestSuite(TestAllModels.class);
//    }
//
//    public void testNSG() {
//        NSG NSGdata = new NSG();
//        NSGdata.configureShip();
//
//    }
//
//    public void testUT712() {
//
//        UT712 UT712data = new UT712();
//        UT712data.configureShip();
//
//        UT712_mod UT712_moddata = new UT712_mod();
//        UT712_moddata.configureShip();
//
//    }
//
//    public void testA122() {
//
//        A122 A122data = new A122();
//        A122data.configureShip();
//
//
//    }
//
//    public void testPX105() {
//
//        PX105 PX105data = new PX105();
//        PX105data.configureShip();
//
//    }
//
//    public void testMT6015() {
//
//        MT6015 MT6015data = new MT6015();
//        MT6015data.configureShip();
//
//    }
//
//    public void testMT6022() {
//
//        MT6022 MT6022data = new MT6022();
//        MT6022data.configureShip();
//
//    }
//
//    public void testH845() {
//
//        H845 H845data = new H845();
//        H845data.configureShip();
//
//    }
//
//    public void testH858() {
//
//        H858 H858data = new H858();
//        H858data.configureShip();
//
//    }
//
//    public void testAX104() {
//
//        AX104 AX104data = new AX104();
//        AX104data.configureShip();
//
//    }
//
//    public void testVS4412() {
//
//        VS4412 VS4412data = new VS4412();
//        VS4412data.configureShip();
//
//    }
//}
