/*
 * Copyright SINTEF Fisheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

import com.sfh.jni.FhSim;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
/**
 *  Singleton class to represent a FhSim model
 * 
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public class FhDynamicModel {
    
    
    /**
     * Data which is sufficient to recreate a simulation 
     */
    public class SavedModel{
        
        /**
         * Path to file with initial state
         */
        private String stateFile;
        
        /**
         * List of data configuration objects
         */
        private ArrayList<Object> dataObjects;
        
        /**
         * Simulation integration settings
         */
        private Element integratorData;
        
        /**
         * Number of waves in global wave field 
         */
        private int numberOfWAves = -1;
        
        /**
         * Store integration settings (internal use)
         * @param id integration settings element
         */
        public void setIntegratorData(Element id ){
            integratorData = id;
        }
        
        /**
         * Retrieve integration settings (internal use)
         * @return DOM element with integration settings
         */
        public Element getIntegratorData( ){
            return integratorData;
        }

        
        /**
         * Set the path to the sate file (initial sate of resumed simulation)
         * @param fileName String with path to state file
         */
        public void setStateFile(String fileName) {
            stateFile = fileName;
        }

        /**
         * Get the path to the sate file (initial sate of resumed simulation)
         * @return String with path to state file
         */
        public String getStateFile() {
            return stateFile;
        }

        /**
         * Set data objects (internal use)
         * @param DataList 
         */
        public  void setDataObjects(ArrayList<Object> DataList) {
            dataObjects = DataList;
        }
        
        /**
         * return list of configuration data objects (internal use)
         * @return list of configuration data
         */
        public ArrayList<Object> getDataObjects() {
            return dataObjects;
        }
        
        /**
         * Set the number of waves
         * @param wave number of waves
         */
        public void setNumberOfWaves(int wave){
            numberOfWAves = wave;
        }
        
        /**
         * Get the number of waves
         * @return number of waves, positive if set
         */
        public int getNumberOfWaves(){
            return numberOfWAves;
        }
    }

    /**
     * Recreate a a saved model. 
     * 
     * Recreates a saved model with the use of old configuration data and integrator settings. 
     * The saved data objects are used to recreate the Actors of the corresponding class. The
     * The state file saved in the model is used as initial state and the number of waves in 
     * in the simulation is also retained. 
     * 
     * MIND THE GAP: Java and x86 architectures have different endiannes, take care if manipulating initial state file. 
     * 
     * @param model saved model containing which contains configuration data and integrator settings
     * @sa SavedModel
     */
    public /*ArrayList<ActorIntf>*/ void recreateGlobalSettings(SavedModel model ){
        FhDynamicModel.reset();

        FhDynamicModel.instance().setResumeFromStateFile(true);
        FhDynamicModel.instance().setStateFile(model.getStateFile());
        FhDynamicModel.instance().setIntegrationElement( model.getIntegratorData() ); 
        
        int cores = Integer.parseInt(model.getIntegratorData().getAttribute("NumCores"));
        int method = Integer.parseInt(model.getIntegratorData().getAttribute("IntegratorMethod"));
        double dt = Double.parseDouble(model.getIntegratorData().getAttribute("stepsize"));
        
        String tstring = model.getIntegratorData().getAttribute("TOutput");
        String[] tokens = tstring.split(",");
        
        FhDynamicModel.instance().setIntegrationMethod( method );
        FhDynamicModel.instance().setStepSize(dt);
        FhDynamicModel.instance().setCpuCores(cores);
        FhDynamicModel.instance().setEndTime( Double.parseDouble(tokens[tokens.length-1]));
        
        
        FhDynamicModel.instance().setNumberOfWavesInFhSim(model.getNumberOfWaves());
        
        /*ArrayList<ActorIntf> actors = new ArrayList<ActorIntf>();
        
        for(Object o: model.getDataObjects()){
            if( o instanceof SFHActuatorPointData ){
                actors.add( ((SFHActuatorPointData)o).create() );
            }else if( o instanceof SFHEnvironmentData ){
                actors.add( ((SFHEnvironmentData)o).create() );
            }else if( o instanceof SFHEnvironmentData2 ){
                actors.add( ((SFHEnvironmentData2)o).create() );
            }else if( o instanceof SFHHullData ){
                actors.add( ((SFHHullData)o).create() );
            }else if( o instanceof SFHOpusPropulsorData ){
                actors.add( ((SFHOpusPropulsorData)o).create() );
            }else if( o instanceof SFHReferencePointData ){
                actors.add( ((SFHReferencePointData)o).create() );
            }else if( o instanceof SFHTrawlData ){
                actors.add( ((SFHTrawlData)o).create() );
            }else if( o instanceof SFHTrawlData2 ){
                actors.add( ((SFHTrawlData2)o).create() );
            }else if( o instanceof SFHSeismicRig2Data ){
                actors.add( ((SFHSeismicRig2Data)o).create() );
            }else if( o instanceof SFHSeismicRigData ){
                actors.add( ((SFHSeismicRigData)o).create() );
            }else{
                throw new RuntimeException("Unknown data class for object during model reconstruction!");
            }
                
        }
        
        return actors;*/
    }
    
    /**
     * Save a model 
     * 
     * Saves the model configuration data and the current state. The configuration
     * data is returned in the object, while the current state is saved to disk. 
     * 
     * The data in the returned class is sufficient to restore the simulation if
     * the initial state file is available. 
     * 
     * @param fileName path on disk where to store the current simulation state. 
     * @return a class containing the model data
     */
    public SavedModel saveModel( String fileName){
        
        SavedModel ret = new SavedModel();
        if (!Files.exists( Paths.get(fileName).getParent()) ){
            throw new RuntimeException("Error in state file path! " + Paths.get(fileName).getParent().toString() );
        }
        ret.setStateFile(fileName);
        ret.setDataObjects(DataList);
        ret.setIntegratorData(FhDynamicModel.instance().getIntegrationElement());
        ret.setNumberOfWaves(FhDynamicModel.instance().getNumberOfWavesInFhSim());
        FhSim.DumpStateVectorNative(fileName);
        
        return ret;
    }
    
    public int lincenseDuration(){
        return FhSim.GetLicenseDuration();
    }
    
    /** Logger used in this class. */
    private static final Logger logger = Logger.getLogger(FhDynamicModel.class.getName());

    public int getNumberOfWavesInFhSim() {
        return numberOfWavesInFhSim;
    }

    /**
     * Single location where number of waves in the simulation is stored. 
     * @param numberOfWavesInFhSim number of waves in use in fhsim. 
     */
    public void setNumberOfWavesInFhSim(int numberOfWavesInFhSim) {
        if(state != modelState.IDLE )
            throw new RuntimeException("numbver of waves update in non-IDLE state");
        this.numberOfWavesInFhSim = numberOfWavesInFhSim;
    }

    /**
     * Check if resume from state file is enabled.
     * 
     * Resume from state file can only be enabled prior to model initialization
     * @return true if an initial state file is to be loaded
     */
    public boolean isResumeFromStateFile() {
        return resumeFromStateFile;
    }

    /**
     * Enable/disable resume from state file 
     * 
     * Resume from state file can only be enabled prior to model initialization
     * @param resumeFromStateFile true if a state file should be resumed from, false to disable
     */
    public void setResumeFromStateFile(boolean resumeFromStateFile) {
        this.resumeFromStateFile = resumeFromStateFile;
    }

    /**
     * Return state file path
     * May be relative or absolute
     * @return path to state file
     */
    public String getStateFile() {
        return stateFile;
    }

    /**
     * Return state file path
     * May be relative or absolute
     * @param stateFile path to the state file to resume from
     */
    public void setStateFile(String stateFile) {
        this.stateFile = stateFile;
    }

    private enum modelState{
        IDLE,
        CONFIGURATION,
        RUNNING
    }
    
    private modelState state = modelState.IDLE;
    private int numberOfWavesInFhSim = -1;
    
    /** The DOM of the configuration file*/
    protected Document Model;	
    private Element BaseNode;	///< The root node of the model
    private double Tstart=0;	///< The start time og the simulation
    private double Tend=Double.POSITIVE_INFINITY;///< The end time of the simulation
    private double deltaToutput=.2;///< The frequency of outputs
    private double StepSize;	///< The major step size for the integrator
    private double HMax = 0.05; ///The biggest time step allowed in case of variable step size.
    private int	Method;		///< The integration method in use 0=Euler, 1=Ad-hoc fixed step, 2=RK45 (Dorman-Price), 3=Un-supported, 4=Heun
    private int cpuCores =2;    ///< Number of cores for FhSim threadpool
    private boolean resultLog = false; ///< Log output to file
    
    private boolean resumeFromStateFile = false;    ///< resume from state file flag
    private String stateFile = ""; ///< File path for state file to resume from
    
    /** is the simulation finished*/
    private boolean finished;
    private String logFile = "log.txt";
    private String resultsFile = "results.csv";

    private static boolean initialized;
    private static double currentTime;
    private static HashMap<Object,Boolean> UpdateMap;
    private static ArrayList<Object> DataList;
    private static HashMap<Object,Double>  LookaheadMap;
    
    private static boolean visualization = false;
    private static String jnipath = null;
    private static String resourcepath = null;

    private static FhDynamicModel my_model;
    
    /**
     * Retrieve the dynamic model
     * 
     * @return Reference to the singleton
     * @throws RuntimeException if unable to create the FhDinamicModel instance.
     */
    public synchronized static FhDynamicModel instance() 
    {
	if( my_model == null ) { 
            try {
                my_model = new FhDynamicModel();
		my_model.setDefaultValues();
                if (null == jnipath) {
                    my_model.setupLibraryAndResources();
                }
            } catch (    ParserConfigurationException | IOException ex) {
                throw new RuntimeException("Unable to create FhDynamicModel instance.", ex);
            }
	    currentTime = 0;
	    my_model.finished = false;
	}
	return my_model;
    }

    /**
     * Returns {@code true} if this is a 64 bit architecture, {@code false}
     * otherwise.
     *
     * @return {@code true} if this is a 64 bit architecture, {@code false}
     * otherwise.
     */
    private static boolean is64bitArch() {
        return System.getProperty("os.arch").contains("64");
    }

    /**
     * Extracts the correct resource bundle to a temp folder.
     *
     * @param tempFolder the temporary folder to extract the bundled file to.
     * @return the path of the extracted bundled file.
     * @throws IOException
     */
    private static Path extractBundle(Path tempFolder) throws IOException {
        ByteBuffer bb = ByteBuffer.allocateDirect(16 * 1024);

        InputStream stream = FhDynamicModel.class.getClassLoader().getResourceAsStream(String.format("/com/sfh/bundles/file-bundle-x%s.zip", is64bitArch() ? "64" : "86"));
        if (null == stream) {
            stream = FhDynamicModel.class.getClassLoader().getResourceAsStream(String.format("com/sfh/bundles/file-bundle-x%s.zip", is64bitArch() ? "64" : "86"));
        }
        Path zipFile = tempFolder.resolve("bundle.zip");

        try (
                WritableByteChannel output = Files.newByteChannel(zipFile, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
                ReadableByteChannel c = Channels.newChannel(stream);) {
            while (c.read(bb) != -1) {
                bb.flip();

                while (bb.hasRemaining()) {
                    output.write(bb);
                }

                bb.clear();
            }
        }
        
        return zipFile;
    }
    
    /**
     * Extract the bundle to the supplied folder, and optionally mark the files
     * to be deleted on exit.
     *
     * @param bundleFile the bundle to extract.
     * @param folder the folder to extract the bundle to.
     * @param deleteOnExit whether or not to mark the extracted files as
     * {@link File#deleteOnExit() delete on exit}.
     * @throws IOException
     */
    private static void extractBundleFile(Path bundleFile, Path folder, boolean deleteOnExit) throws IOException {
        ZipFile zip = new ZipFile(bundleFile.toFile());

        Enumeration<? extends ZipEntry> entries = zip.entries();
        // Allocate a 16kb buffer:
        ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);
        for (ZipEntry entry = entries.nextElement(); entries.hasMoreElements(); entry = entries.nextElement()) {

            // Ensure that all folders exists:
            Path path = folder.resolve(entry.getName());
            if(entry.isDirectory()){
                path.toFile().mkdirs();
                if (deleteOnExit) {
                    path.toFile().deleteOnExit();
                }
                continue;
            }
            try (
                    WritableByteChannel output = Files.newByteChannel(path, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
                    ReadableByteChannel input = Channels.newChannel(zip.getInputStream(entry));) {
                while (input.read(buffer) != -1) {
                    buffer.flip();

                    while (buffer.hasRemaining()) {
                        output.write(buffer);
                    }

                    buffer.clear();
                }
                if (deleteOnExit) {
                    path.toFile().deleteOnExit();
                }
            }
        }
    }

    /**
     * Extracts the bundled resources for the current platform and sets the JNI
     * and Resource path to use.
     *
     * @throws IOException
     */
    private void setupLibraryAndResources() throws IOException {
        // Create the temp folder:
        Path folder = Files.createTempDirectory("fhsim-temp");
        // Delete the temp folder on exit:
        folder.toFile().deleteOnExit();
        // Extract the bundle:
        Path bundleFile = extractBundle(folder);
        // And delete it on exit:
        bundleFile.toFile().deleteOnExit();
        // Extract the bundle file, and mark the contents as delete on exit:
        extractBundleFile(bundleFile, folder, true);
        
        // Set the JNI path:
        setJnipath(folder.resolve("jni").toString());
        // Set the resource path:
        setResourcepath(folder.resolve("res").toString());
    }

    /**
     * Resets the dynamic fhSim model. Must be called between invocations to fhSim
     */
    public synchronized static void reset()
    {
	if( my_model == null ){
	    return;
	}
//	if( FhSim.isRunning() ){
//	    FhSim.stop();
//	}
        FhSim.stop();
        FhSim.unload();
	initialized = false;
	my_model.finished = false;
	my_model = null;
        instance().setResumeFromStateFile(false);
        instance().setStateFile("");
    }
    
    /**
     * Register an object with the dynamic model
     * @param ref The object reference which is added to the model
     * @param data configuration data for the object ref
     */
    public synchronized void registerObject(Object ref, Object data)
    {
        if( initialized )
            throw new RuntimeException("Trying to register objects in an initialized model");
	//System.out.println("Registering: " + ref.toString() );
	
        UpdateMap.put(ref,false);
        if(! (data == null ) ){
            DataList.add(data);
        }
    }

    /**
     * Register an a specific object which has requirements on the time step
     * @param ref The reference to the new object in the model
     * @param maxStep The time which the model can advance according to this object
     * @param data Data object used to configure ref
     * @sa registerObject
     */
    public synchronized void registerObject(Object ref, double maxStep, Object data)
    {
        state = modelState.CONFIGURATION;
        if( initialized )
            throw new RuntimeException("Trying to register objects in an initialized model");

        double currentStepsize = Double.parseDouble(getIntegrationElement().getAttribute("stepsize"));
	
	if( currentStepsize == 0)
	{
	    //getIntegrationElement().setAttribute("HMax",Double.toString(maxStep));
	    getIntegrationElement().setAttribute("stepsize",Double.toString(maxStep));
	}
	else if(currentStepsize > maxStep) {
	    getIntegrationElement().setAttribute("stepsize",Double.toString(maxStep));
	}
	
	UpdateMap.put(ref,false);
        
        if(! (data == null ) ){
            DataList.add(data);
        }
    }
    
    /**
     * Test for finished integration, true if simulation time is greater or equal Tend
     * @return true if the simulation is finished
     * @sa setTend
     */
    public synchronized boolean isFinished()
    {
	return finished;
    }

    /**
     * Sets the file used as log file by FhSim.
     *
     * @param logFile the file used as log file.
     * @throws IOException if something failed during building of the path name.
     */
    public synchronized void setLogFile(File logFile) throws IOException {
        setLogFile(logFile.getCanonicalPath());
    }

    /**
     * Sets the file used as log file by FhSim.
     *
     * @param logFile the file used as log file.
     */
    public synchronized void setLogFile(String logFile) {
        this.logFile = logFile;
    }
    
    /**
     * Returns the path of the file used as log file (default:
     * &quot;{@literal log.txt}&quot;).
     *
     * @return the path of the file used as log file.
     */
    public String getLogFile(){
        return this.logFile;
    }

    /**
     * Sets the file used as result file by FhSim.
     *
     * @param resultsFile the file used as result file.
     * @throws IOException if something failed during building of the path name.
     */
    public synchronized void setResultsFile(File resultsFile) throws IOException {
        setResultsFile(resultsFile.getCanonicalPath());
    }

    /**
     * Sets the file used as result file by FhSim.
     *
     * @param resultsFile the file used as result file.
     */
    public synchronized void setResultsFile(String resultsFile) {
        this.resultsFile = resultsFile;
    }

    /**
     * Returns the path of the file used as results file (default:
     * &quot;{@literal results.cvs}&quot;).
     *
     * @return the path of the file used as results file.
     */
    public String getResultsFile() {
        return this.resultsFile;
    }

    /**
     * Register an update to a specific object. 
     * 
     * This method handles model initialization and stepping of the simulation in the time domain in a 
     * unified fashion. Each object which is registered by registerObject must register an update. 
     *
     * The model is initialized when all objects have registered the initial update. 
     * 
     * The model is then stepped by the minimum requested time step as the complete set of objects completes 
     * the update step.
     * @param ref The object which is updated
     * @param Lookahead The maximum advance step
     * @throws RuntimeException 
     */
    public synchronized void objectUpdated(Object ref, double Lookahead) throws RuntimeException
    {
	if( finished ) {
	    return;
	}

        if( ! UpdateMap.containsKey(ref) )
            throw new RuntimeException("Trying to register unknown object: " + ref + " as updated");
        
	UpdateMap.put(ref, Boolean.TRUE);
	LookaheadMap.put(ref,Lookahead);
	
	if( UpdateMap.containsValue(Boolean.FALSE) ) {
	    return;
	} 
        
        if (!initialized) {
            initialize();
        }
        
	for( Object obj: UpdateMap.keySet() )
	{
	    ActorIntf actor = (ActorIntf)obj;
	    actor.preTick();
	}

	double stepsize = Collections.min(LookaheadMap.values() );
	boolean status = FhSim.simulate(stepsize);
        
	currentTime += stepsize;
	
	// workaround for fhsim's dll which use 2 status codes to report three states
	finished = currentTime/Tend > 0.999;
	
	if(!status && !finished){
	    throw new RuntimeException("Simulation error! " + FhSim.getErrorString());	    
	}

	for( Object obj: UpdateMap.keySet() ){
	    ActorIntf actor = (ActorIntf)obj;
	    actor.postTick();
	}
	for( Object key: UpdateMap.keySet() ) {
	    UpdateMap.put(key,Boolean.FALSE);
	}
    }
        
    public synchronized void initialize() throws RuntimeException 
    {
    	if( finished ) {
	    return;
	}
        if( !initialized ) {
	    FhSim.stop();
            FhSim.setJNIPath(jnipath);
            FhSim.setResourcePath(resourcepath);
            FhSim.setVisualization(getVisualization());
	    initialized = FhSim.setupFromString(fhSimConfiguration(), this.resultsFile, this.logFile);
            state = modelState.RUNNING;
	}
	
	if( !initialized ) {
            state = modelState.IDLE;
	    throw new RuntimeException("Initialization failed " + FhSim.getErrorString());
	}
    }
    
    
    
    /**
     * Constructor, constructs the basic XML structure for en FhSim model
     * @throws ParserConfigurationException from DOM model creation
     */
    private FhDynamicModel() throws ParserConfigurationException
    {
	
	UpdateMap	= new HashMap<Object, Boolean>();
	LookaheadMap	= new HashMap<Object, Double>();
	DataList        = new ArrayList<Object>();
	
	Method	    = 2; // 1 - SFH stable, 2 - Dorman prince, 4 - heun
	StepSize    = 0.0; // 0.0 - variable time step
	
	DocumentBuilderFactory Factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder Builder = Factory.newDocumentBuilder();
	Model = Builder.newDocument();


	BaseNode = Model.createElement("Contents");
	Model.appendChild(BaseNode);
	
	Element Objects = Model.createElement("OBJECTS");
	BaseNode.appendChild( Objects );

	Element Connections = Model.createElement("INTERCONNECTIONS");
	Element Connection = Model.createElement("Connection");
	Connections.appendChild( Connection );
	BaseNode.appendChild( Connections );

    	Element Initialization = Model.createElement("INITIALIZATION");
    	Element InitialCondition = Model.createElement("InitialCondition");
	Initialization.appendChild( InitialCondition );
	BaseNode.appendChild( Initialization );

    	Element Integration = Model.createElement("INTEGRATION");
    	Element Engine = Model.createElement("Engine");
	Integration.appendChild( Engine );
	BaseNode.appendChild( Integration );
	
	setIntegrationMethod(Method);
	setStepSize(StepSize);	
        
        state = modelState.IDLE;
    }
    
    /**
     * Builds an FhSim configuration file from the contents of the DOM model in the singleton instance
     * @return The FhSim configuration file in a String
     */
    public synchronized String fhSimConfiguration()
    {
        setIntegrationMethod(Method);
        setStepSize(StepSize);
        setDeltaTime(deltaToutput);
        
        if(resumeFromStateFile){
            Element integrator = getIntegrationElement();
            integrator.setAttribute("InitialStatesFile", stateFile);
            /*Element  ic = getInitializationElement();
            Element ic_empty = Model.createElement("InitialCondition");
            NamedNodeMap attributes = ic.getAttributes();
            for( int i = 0; i < attributes.getLength(); i++){
                ic.removeAttribute(attributes.item(i).getNodeName());
            }*/
        }
        StringWriter outTxt = new StringWriter();
        try {
            //write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(Model);
            StreamResult result = new StreamResult(outTxt);
            StreamResult result2 = new StreamResult(new File("./testing.xml"));
            transformer.transform(source, result);
            transformer.transform(source, result2);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return outTxt.toString();
    }
    
    /**
     * Creates an element in the DOM model 
     * @param TagName The name of the element
     * @return Reference to the created element
     */
    public synchronized Element createElement(String TagName )
    {
	return Model.createElement(TagName);
    }

    /**
     * Create an element of type "Lib" in the model document
     * @return The created element
     */
    public synchronized Element createLibElement()
    {
	return Model.createElement("Lib");    
    }
    
    /**
     * Create a new simObject element in the model document
     * @param Name The name of the simObject
     * @param Type The simObject type 
     * @param Library The library name in which the simObject type is located
     * @return The created element
     */
    public synchronized Element createSimObjectElement(String Name, String Type, String Library)
    {
	Element newObject = Model.createElement("Lib");    
	
	newObject.setAttribute("Name", Name);
	newObject.setAttribute("LibName", Library);
	newObject.setAttribute("SimObject", Type);
	return newObject;
    }

    /**
     * Add the required fields in an external element in order to create a new input port in FhSim
     * @param element The element created as an "ExternalLinkStandard" 
     * @param portName The name of the new input port
     * @param portWidth the width of the new input port
     */
    public synchronized void addInputPortToExternalElement(Element element , String portName, int portWidth)
    {
	String type = element.getAttribute("SimObject");
	assert( "ExternalLinkStandard".equals(type) );
	String inputPorts = element.getAttribute("inputPortNames");
	if( "".equals(inputPorts) )
	{
	    inputPorts = portName;
	}
	else
	{
	    inputPorts += "," + portName;
	}
	
	element.setAttribute("inputPortNames", inputPorts);
	element.setAttribute("Size_"+portName, Integer.toString(portWidth));
    }
    
    /**
     * Add the required fields in an external element in order to create a new output port in FhSim
     * @param element The element created as "ExternalLinkStandard"
     * @param portName The name of the new output port
     * @param portValue The initial value of the output port 
     */
    public synchronized void addOutputPortToExternalElement(Element element , String portName, double []portValue)
    {
	String type = element.getAttribute("SimObject");
	assert( "ExternalLinkStandard".equals(type) );
	String outputPorts = element.getAttribute("outputPortNames");
	if( "".equals(outputPorts) )
	{
	    outputPorts = portName;
	}
	else
	{
	    outputPorts += "," + portName;
	}
	element.setAttribute("outputPortNames", outputPorts);
	
	String valueString = "";
	if(portValue.length > 1 ){
	    for( int i=0; i < portValue.length; i++)
	    {
		valueString += "," + Double.toString(portValue[i]);
	    }
	}
	else
	{
	    valueString = Double.toString(portValue[0]);
	}
	element.setAttribute("Initial_"+portName, valueString);
    }
    
    /**
     * Return the element which is the single OBJECTS element in the FhSim model file
     * @return Reference to the OBJECTS element
     */
    public synchronized Element getObjectElement()
    {
	return (Element)Model.getElementsByTagName("OBJECTS").item(0);
    }
    
    /**
     * Return a reference to the element which contains the initial conditions in the model file
     * @return The single initialization element
     */
    public synchronized Element getInitializationElement()
    {
	return (Element)Model.getElementsByTagName("InitialCondition").item(0);	
    }
    
    /**
     * Return a reference to the element which holds the interconnections in the model file
     * @return The single interconnection element 
     */
    public synchronized Element getInterconnectionElement()
    {
	return  (Element)Model.getElementsByTagName("Connection").item(0);
    }
    
    /**
     * Return a reference to the integration setting element in the model file
     * @return The single integration settings element 
     */
    public synchronized Element getIntegrationElement()
    {
	return  (Element)Model.getElementsByTagName("Engine").item(0);	
    }

    public synchronized void setIntegrationElement(Element integratorData) {
        
        NamedNodeMap attributes = integratorData.getAttributes();
        for( int i=0; i < attributes.getLength(); i++){
            getIntegrationElement().setAttribute( attributes.item(i).getNodeName(), attributes.item(i).getNodeValue() );
        }
    }
    
    
    /**
     * Sets default values for the model file
     */
    public synchronized void setDefaultValues()
    {
	Element engine = getIntegrationElement();
	
        setIntegrationMethod(2);//RK45 Dormand Prince
	setStepSize(0.0); //variable set size
        setHMax(5e-2);//biggest step size FhSim is allowed to use 20Hz
        engine.setAttribute("NumCores", "2");
	engine.setAttribute("AbsTol", "1e-5");
	engine.setAttribute("RelTol", "1e-3");
	engine.setAttribute("HMin", "1e-10");
	engine.setAttribute("TOutput", getTimeString());
	engine.setAttribute("FileOutput", "objects:ports");
	engine.setAttribute("UseRSSNormInsteadOfInfNorm","0");
    }
    
    /**
     * Returns the string representation of the start time, output interval and end time. For use while constructing the model file
     * @return The string representation of the simulation time and output rate
     */
    public synchronized String getTimeString()
    {
        String endStr;
        if( Double.isInfinite(Tend) )
            endStr = "Inf";
        else
            endStr = Double.toString(Tend);
        
        if( resultLog ){
            String logInterval;
            logInterval = Double.toString(Tstart)+":"+Double.toString(deltaToutput)+":"+endStr;
            return Double.toString(Tstart)+","+logInterval+","+endStr;
            //return Double.toString(Tstart)+","+endStr; // fix for seismic rig, something seems to crash when logging forces with interval
        }else{
            return Double.toString(Tstart)+","+endStr;
        }
        
    }
    
    /**
     * Set the start time of the simulation
     * @param T The initial time
     */
    public synchronized void setStartTime( double T)
    {
	Tstart = T;
	Element engine = getIntegrationElement();	
	engine.setAttribute("TOutput", getTimeString());
    }

    /**
     * Set the end time of the simulation
     * @param T The final time 
     */
    public synchronized void setEndTime( double T)
    {
	Tend = T;
	Element engine = getIntegrationElement();	
	engine.setAttribute("TOutput", getTimeString());
    }
    
    /**
     * Set the output update rate the global logging/output function 
     * @param T The time increment between outputs
     */
    public synchronized void setDeltaTime( double T)
    {
	deltaToutput = T;
	Element engine = getIntegrationElement();	
	engine.setAttribute("TOutput", getTimeString());
    }

    /**
     * Sets the step size of the integrator
     * @param size The major step size to use in the integrator
     */
    public synchronized final void setStepSize(double size)
    {
	StepSize = size;
	
	Element el = getIntegrationElement();
	el.setAttribute("stepsize", Double.toString(StepSize));
    }
    
    /**
     * Set the integration method 
     * The integration method in use 0=Euler, 1=Ad-hoc fixed step, 2=RK45 (Dorman-Price), 3=Un-supported, 4=Heun
     * @param method integer for integration method
     */
    public synchronized final void setIntegrationMethod(int method)
    {
	Method = method;
	Element el = getIntegrationElement();
	el.setAttribute("IntegratorMethod", Integer.toString(method));	
    }
           
    static
    {
	my_model = null;
    }

    /**
     * Get the path to the FhSimDll and Native part of the JNI
     * @return the previously set path
     */
    public String getJnipath() {
        return jnipath;
    }

    /**
     * Set the path to the FhSimDll and Native part of the JNI
     * @param jnipath the new path
     */
    private void setJnipath(String jnipath) {
        this.jnipath = jnipath;
    }

    /**
     * Get the path to the ogre configuration (ogre resource directory)
     * @return the previously set resource directory
     */
    public String getResourcepath() {
        return resourcepath;
    }
    
    /**
     * Set the path to the ogre configuration (ogre resource directory)
     * @param resourcepath the new resource directory
     */
    private void setResourcepath(String resourcepath) {
        this.resourcepath = resourcepath;
    }
    
    /**
     * Set the value of an FhSim port 
     * @param object The object which contains the port
     * @param port The name of the port
     * @param value The new value of the port
     */
    public void setFhSimPort(String object,String port, double [] value){
        FhSim.setInput(object, port, value);
    }

    
    /**
     * Retrieve the value of an FhSim output port
     * @param object The name of the object which contains the port
     * @param port The name of the port
     * @param width the width of the port (required by the FhSim JNI)
     * @return the value of the port
     */
    public double []getFhSimPort(String object, String port, int width){
        return FhSim.getOutput(object, port, width);
    }

    /**
     * Retrieve the value of an FhSim output port
     * @param object The index of the object which contains the port
     * @param port The index of the port on the object
     * @param width the width of the port (required by the FhSim JNI)
     * @return the value of the port
     * @see getFhSimObjectIndex#getObjectIndex
     * @see getFhSimObjectPortIndex#getObjectPortIndex
     */
    public double []getFhSimPortFromIndex(int object, int port, int width){
        return FhSim.getOutputWithIndex(object, port, width);
    }
    
    /**
     * Retrieve an index value used to refer to the FhSim SimObject 
     * Will reduce time spend on name lookup when retrieving data
     * @param name The name of the SimObject
     * @return an index which can be used to refer to the object -1 on error (unknown SimObject)
     * @see getFhSimPortFromIndex
     * @see #getObjectPortIndex
     * @see getFhSimObjectPortWidthIndex#getObjectPortWidthIndex
     */
    public int getObjectIndex(String name){
        return FhSim.getSimObjectIndex(name);
    }
    
    /**
     * Retrieve an index for a port on a named SimObject to be use later to replace
     * string lookups when retrieving data from the port 
     * @param name name of the object
     * @param port port name on the object
     * @return index to be used to retrieve output, -1 on error (unknown SimObject or port)
     * @see getFhSimPortFromIndex
     * @see #getObjectIndex
     * @see #getObjectPortWidthIndex
     */
    public int getObjectPortIndex(String name, String port){
        return FhSim.getSimObjectPortIndex(name, port);
    }
    
    /**
     * Retrieve the signal width from object and port indices
     * @param object object index
     * @param port port index
     * @return the width of the signal
     * @see getFhSimPortFromIndex
     * @see #getObjectIndex
     * @see getFhSimObjectPortIndex#getObjectPortIndex
     */
    public int getObjectPortWidthIndex(int object, int port){
        return FhSim.getOutputWidthWithIndex(object, port);
    }
    
    
    /**
     * Return the current simulation time
     * @return time in seconds
     */
    public double getCurrentTime(){
        return currentTime;
    }
    
    /**
     * @return the visualization
     */
    public boolean getVisualization() {
        return visualization;
    }

    /**
     * @param visualization the visualization to set
     */
    public void setVisualization(boolean visualization) {
        this.visualization = visualization;
    }
    
    Element getNamedObjectElement(String name){
    
        Element objects = getObjectElement();
        
        NodeList objs = objects.getChildNodes();
        for( int i=0; i < objs.getLength();i++){
            Element thisElement = (Element)objs.item(i);
            
            if( thisElement.getAttribute("Name").equals(name))
                return thisElement;
        }
        return null;
    }

    /**
     * Test if model is logging results to "results.csv"
     * @return true if results are logged
     */
    public boolean isResultLog() {
        return resultLog;
    }

    /**
     * Enable/disable result logging. True for result log, false for no logging
     * @param resultLog the flag for results logging
     */
    public void setResultLog(boolean resultLog) {
        this.resultLog = resultLog;
    }
    
    public void loadLibraryFromJNIfolder(String library_name){
        System.setProperty( "java.library.path", getJnipath() +"/" );
        Field fieldSysPath;
        try {
            fieldSysPath = ClassLoader.class.getDeclaredField( "sys_paths" );
            fieldSysPath.setAccessible( true );
            try {
                    fieldSysPath.set( null, null );
            } catch (IllegalArgumentException e) {
                    e.printStackTrace();
            } catch (IllegalAccessException e) {
                    e.printStackTrace();
            }
            System.loadLibrary(library_name);
        } catch (SecurityException e) {
                e.printStackTrace();
        } catch (NoSuchFieldException e) {
                e.printStackTrace();
        }
    }

    public int getCpuCores() {
        return Integer.parseInt(getIntegrationElement().getAttribute("NumCores"));
    }

    public void setCpuCores(int cpuCores) {
        Element el = getIntegrationElement();
        el.setAttribute("NumCores", cpuCores+"");
        this.cpuCores = cpuCores;
    }
    
    public double getHMax() {
    return HMax;
    }
    
    public void setHMax(double HMax) {
        this.HMax = HMax;
        Element engine = getIntegrationElement();
        engine.setAttribute("HMax", Double.toString(HMax));        
    }
    
    public static String getFhSimInterfaceVersion(String pomPath) {
        String version = null;
        Element el = null;
        try {
         File inputFile = new File(pomPath);
         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.parse(inputFile);
         doc.getDocumentElement().normalize();
         el = (Element) doc.getElementsByTagName("version").item(0);
      } catch (Exception e) {
         e.printStackTrace();
      }
    return el.getTextContent();
    }
}

