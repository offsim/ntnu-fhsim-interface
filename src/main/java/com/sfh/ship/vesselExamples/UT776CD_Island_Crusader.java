package com.sfh.ship.vesselExamples;

import com.sfh.ship.VeresFile;
import com.osc.wave.FlatSea;
import com.osc.wave.SingleWave;
import com.sfh.ship.ActorIntf;
import com.sfh.ship.FhDynamicModel;
import com.sfh.ship.SFHActuatorPointData;
import com.sfh.ship.SFHActuatorPointIntf;
import com.sfh.ship.SFHEnvironmentData;
import com.sfh.ship.SFHEnvironmentIntf;
import com.sfh.ship.SFHHullData;
import com.sfh.ship.SFHHullIntf;
import com.sfh.ship.SFHOpusPropulsorData;
import com.sfh.ship.SFHOpusPropulsorIntf;
import com.sfh.osc.spec.FhSimModelsFolder;
import com.sfh.osc.spec.OSCHullDataSpec;
import com.sfh.osc.spec.ReportSpec;
import java.util.ArrayList;

public class UT776CD_Island_Crusader extends BaseExample {

    private SFHHullData ut776cd;
    private SFHActuatorPointData conMain0, conMain1;
    private SFHActuatorPointData conThruster0, conThruster1;
    private SFHActuatorPointData conAzimuth0;

    private SFHOpusPropulsorData main0, main1;
    private SFHOpusPropulsorData thruster0, thruster1;
    private SFHOpusPropulsorData azimuth0;

    private SFHHullIntf _ut776cd;
    private SFHActuatorPointIntf _conMain0, _conMain1;
    private SFHActuatorPointIntf _conThruster0, _conThruster1;
    private SFHActuatorPointIntf _conAzimuth0;

    private SFHOpusPropulsorIntf _main0, _main1;
    private SFHOpusPropulsorIntf _thruster0, _thruster1;
    private SFHOpusPropulsorIntf _azimuth0;

    public static void main(String args[]) {
        //    FhDynamicModel.instance().setVisualization(false);
        UT776CD_Island_Crusader theTest = new UT776CD_Island_Crusader();
        theTest.execute();
    }

    @Override
    public void configureShip() {
        /*
         HULL
         */
        String folder = FhSimModelsFolder.getFolder();
        ut776cd = SFHHullData.loadFromXml(folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/vessel.xml");


        reportSpec = ReportSpec.loadFromXml(folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/report_spec.xml");
        oscSpec = OSCHullDataSpec.loadFromXml(folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/vessel_spec_" + loadCondition + ".xml");

   
        
        /*
         MAIN CONNECTIONS
         */
        conMain0 = SFHActuatorPointData.loadFromXml("conMain0", folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/starboardPodActuator.xml");
        conMain1 = SFHActuatorPointData.loadFromXml("conMain1", folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/portPodActuator.xml");

        /*
         THRUSTER CONNECTIONS
         */
        conThruster0 = SFHActuatorPointData.loadFromXml("conForeThruster0", folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/foreThruster0Actuator.xml");
        conThruster1 = SFHActuatorPointData.loadFromXml("conForeThruster1", folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/foreThruster1Actuator.xml");

        /*
         AZIMUTH CONNECTION
         */
        conAzimuth0 = SFHActuatorPointData.loadFromXml("conAzimuth0", folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/azimuthThrusterActuator.xml");

        /*
         MAIN PROPELLERS
         */
        main0 = SFHOpusPropulsorData.loadFromXml("main0", folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/propulsors/Main.xml");
        main1 = SFHOpusPropulsorData.loadFromXml("main1", folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/propulsors/Main.xml");

        /*
         THRUSTERS
         */
        thruster0 = SFHOpusPropulsorData.loadFromXml("forwardTunnel0", folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/propulsors/foreThruster.xml");
        thruster1 = SFHOpusPropulsorData.loadFromXml("forwardTunnel1", folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/propulsors/foreThruster.xml");

        /*
         AZIMUTH
         */
        azimuth0 = SFHOpusPropulsorData.loadFromXml("azimuth0", folder + "/OSCV_UT776CD_ISL/dynmodel/fhsim/propulsors/azimuthThruster.xml");

        /*
         CONNECT PROPULSORS TO ACTUATORS
         */
        main0.ConnectToReference(conMain0);
        main1.ConnectToReference(conMain1);

        thruster0.ConnectToReference(conThruster0);
        thruster1.ConnectToReference(conThruster1);

        azimuth0.ConnectToReference(conAzimuth0);

        /*
         ATTACH ACTUATORS TO HULL
         */
        ut776cd.AddReferencePoint(conMain0);
        ut776cd.AddReferencePoint(conMain1);

        ut776cd.AddReferencePoint(conThruster0);
        ut776cd.AddReferencePoint(conThruster1);

        ut776cd.AddReferencePoint(conAzimuth0);

        hyd = (new VeresFile(getHull().getModelData() + "/" + getHull().getModelFileStem())).hydFile;

    }

    @Override
    public void runTrials() {
        FhDynamicModel.instance().setVisualization(true);
        FhDynamicModel.instance().setResultLog(true);
        //runCrashStop(7.0);
        //thrusterTest(1, 0);
        //  runDPcrab(1);
        //propulsorTest();
        //lateralBalanceTest();
        //runMaxSpeed();
        //fullSpeed();
        //runStarboardTurningCirlceTrial();
        //runDPpirouette();
        //runDPcrab(1);
        //runDPcrab(-1);
        //   testWindDrift(30, Math.PI / 2);
        testWaveDrift(0, 0, 2, 8, true);
    }

    @Override
    public SFHHullData getHull() {
        return ut776cd;
    }

    @Override
    public ArrayList<BaseExample.PropulsorActuatorDataPair> getPropulsors() {
        ArrayList<BaseExample.PropulsorActuatorDataPair> retval = new ArrayList<BaseExample.PropulsorActuatorDataPair>();

        retval.add(new BaseExample.PropulsorActuatorDataPair(main0, conMain0));
        retval.add(new BaseExample.PropulsorActuatorDataPair(main1, conMain1));

        retval.add(new BaseExample.PropulsorActuatorDataPair(thruster0, conThruster0));
        retval.add(new BaseExample.PropulsorActuatorDataPair(thruster1, conThruster1));

        retval.add(new BaseExample.PropulsorActuatorDataPair(azimuth0, conAzimuth0));
        return retval;
    }

    @Override
    public ArrayList<ActorIntf> createActors() {
        ArrayList<ActorIntf> retval = new ArrayList<>();
        _ut776cd = ut776cd.create();
        _conMain0 = conMain0.create();
        _conMain1 = conMain1.create();
        _conThruster0 = conThruster0.create();
        _conThruster1 = conThruster1.create();

        _conAzimuth0 = conAzimuth0.create();

        _main0 = main0.create();
        _main1 = main1.create();
        _thruster0 = thruster0.create();
        _thruster1 = thruster1.create();
        _azimuth0 = azimuth0.create();

        _conMain0.setAvailablePower(3000e3);
        _conMain1.setAvailablePower(3000e3);

        _conThruster0.setAvailablePower(10 * 1300e3);
        _conThruster1.setAvailablePower(10 * 1300e3);
        _conAzimuth0.setAvailablePower(10 * 1200e3);

        retval.add(_ut776cd);
        retval.add(_conMain0);
        retval.add(_conMain1);
        retval.add(_conThruster0);
        retval.add(_conThruster1);
        retval.add(_conAzimuth0);

        retval.add(_main0);
        retval.add(_main1);
        retval.add(_thruster0);
        retval.add(_thruster1);
        retval.add(_azimuth0);
        return retval;
    }

    @Override
    public SFHHullIntf getHullIntf() {
        return _ut776cd;
    }

    @Override
    public ArrayList<SFHActuatorPointIntf> getMainActuatorsIntf() {
        ArrayList<SFHActuatorPointIntf> retval = new ArrayList<>();
        retval.add(_conMain0);
        retval.add(_conMain1);
        return retval;
    }

    @Override
    public ArrayList<SFHActuatorPointIntf> getMainRudderIntf() {
        ArrayList<SFHActuatorPointIntf> retval = new ArrayList<>();
        retval.add(_conMain0);
        retval.add(_conMain1);
        return retval;
    }

    public void fullSpeed() {
        //Marine Traffic average/max speed: 11.3/12.8 knots
        FlatSea waveModel = new FlatSea();

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> rudders = getMainRudderIntf();
        ArrayList<SFHActuatorPointIntf> propellers = getMainActuatorsIntf();

        FhDynamicModel.instance().setEndTime(900);
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            T += dT;

            for (SFHActuatorPointIntf a : propellers) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0);
            }

            double power = (_main0.getConsumedPower() + _main1.getConsumedPower()) / 1e3;

            System.out.println("speed:" + Math.round(_ut776cd.GetLocalVelocity()[0] / 0.5144 * 10) / 10.0 + "Kn @ " + Double.toString(power) + " kW");
        }
        FhDynamicModel.reset();
    }

    public void lateralBalanceTest() {

        configureShip();
        ut776cd.setSfh_test(true);
        FlatSea waveModel = new FlatSea();

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> propellers = getMainActuatorsIntf();
        ArrayList<SFHActuatorPointIntf> thrusters = getTransverseThrusterIntf();
        ArrayList<SFHActuatorPointIntf> azimuths = getRotatableTrhusterIntf();

        FhDynamicModel.instance().setEndTime(240);
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            T += dT;
            for (SFHActuatorPointIntf a : propellers) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0 / 2);
                //a.setCommandedRPMNormalized(1.0);
            }

            _conMain0.setComandedPitchNormalized(1.0);
            _conMain0.setCommandedRPMNormalized(1.0 / 1.8);

            _conMain1.setComandedPitchNormalized(1.0);
            _conMain1.setCommandedRPMNormalized(1.0);

            for (SFHActuatorPointIntf a : thrusters) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0);
            }

            for (SFHActuatorPointIntf a : azimuths) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0);
            }
        }

        double cm0_a = _main0.GetNominalThrust() / 1e3;
        double cm0_b = _main0.getConsumedPower() / 1e3;

        double cm1_a = _main1.GetNominalThrust() / 1e3;
        double cm1_b = _main1.getConsumedPower() / 1e3;

        double ca0_a = _azimuth0.GetActualTrhust() / 1e3;
        double ca0_b = _azimuth0.getConsumedPower() / 1e3;

        System.out.println("Main0: " + Double.toString(cm0_a) + " kN @ " + Double.toString(cm0_b) + " kW ");
        System.out.println("Main1: " + Double.toString(cm1_a) + " kN @ " + Double.toString(cm1_b) + " kW ");

        System.out.println("Azimuth0: " + Double.toString(ca0_a) + " kN @ " + Double.toString(ca0_b) + " kW ");

        FhDynamicModel.reset();
        ut776cd.setSfh_test(false);
    }

    public void propulsorTest() {

        //main bollard -   1880 kW / 225kN @ 134 RPM (prop)
        //main free - 2450 kW /276.8 kN @ 176.5 RPM (prop)
        //fwd tunnels  - 1300 kW / 222kN  @ 257 RPM (prop) max - 1.036 PD
        //azi  - 1200W / 203 kN @ 258 RPM (prop) ESTIMATED RPM!
        ut776cd.setSfh_test(true);
        FlatSea waveModel = new FlatSea();

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> propellers = getMainActuatorsIntf();
        ArrayList<SFHActuatorPointIntf> thrusters = getTransverseThrusterIntf();
        ArrayList<SFHActuatorPointIntf> azimuths = getRotatableTrhusterIntf();

        FhDynamicModel.instance().setEndTime(60);
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            T += dT;

            for (SFHActuatorPointIntf a : thrusters) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0);
            }

            for (SFHActuatorPointIntf a : azimuths) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0);
            }

            for (SFHActuatorPointIntf a : propellers) {
                a.setComandedPitchNormalized(1.0);
            }

            _conMain1.setCommandedRPM(146);
            _conMain0.setCommandedRPM(201);

        }

        double cm0_a = _main0.GetActualTrhust() / 1e3;
        double cm0_b = _main0.getConsumedPower() / 1e3;

        double cm1_a = _main1.GetActualTrhust() / 1e3;
        double cm1_b = _main1.getConsumedPower() / 1e3;

        double ca0_a = _azimuth0.GetActualTrhust() / 1e3;
        double ca0_b = _azimuth0.getConsumedPower() / 1e3;

        double ct0_a = _thruster0.GetActualTrhust() / 1e3;
        double ct0_b = _thruster0.getConsumedPower() / 1e3;

        double ct1_a = _thruster1.GetActualTrhust() / 1e3;
        double ct1_b = _thruster1.getConsumedPower() / 1e3;

        System.out.println("Main0: " + Double.toString(cm0_a) + " kN @ " + Double.toString(cm0_b) + " kW ");
        System.out.println("Main1: " + Double.toString(cm1_a) + " kN @ " + Double.toString(cm1_b) + " kW ");

        System.out.println("Azimuth0: " + Double.toString(ca0_a) + " kN @ " + Double.toString(ca0_b) + " kW ");

        System.out.println("Thruster0: " + Double.toString(ct0_a) + " kN @ " + Double.toString(ct0_b) + " kW ");
        System.out.println("Thruster1: " + Double.toString(ct1_a) + " kN @ " + Double.toString(ct1_b) + " kW ");


        /*
         FhDynamicModel.reset();
         _env = environment.create();

         actorList = createActors();
         actorList.add(_env);

         waveModel.addListener(_env);

         propellers = getMainActuatorsIntf();
         thrusters = getTransverseThrusterIntf();
         azimuths = getRotatableTrhusterIntf();
        
         FhDynamicModel.instance().setEndTime(60);
         T=0;
         dT=0.1;
         while(!FhDynamicModel.instance().isFinished() ){
         for(ActorIntf a: actorList) a.tick(dT);
         T += dT;
         for(SFHActuatorPointIntf a: propellers){
         a.setComandedPitchNormalized(1.0);
         a.setCommandedRPMNormalized(-1.0);
         } 

         for(SFHActuatorPointIntf a: thrusters){
         a.setComandedPitchNormalized(1.0);
         a.setCommandedRPMNormalized(-1.0);
         } 
        
         for(SFHActuatorPointIntf a: azimuths){
         a.setComandedPitchNormalized(-1.0);
         a.setCommandedRPMNormalized(1.0);
         } 
         }

         cm0_a = _main0.GetActualTrhust()/1e3;
         cm0_b = _main0.getConsumedPower()/1e3;
        
         cm1_a = _main1.GetActualTrhust()/1e3;
         cm1_b = _main1.getConsumedPower()/1e3;
        
         ca0_a = _azimuth0.GetActualTrhust()/1e3;
         ca0_b = _azimuth0.getConsumedPower()/1e3;
        
         ct0_a = _thruster0.GetActualTrhust()/1e3;
         ct0_b = _thruster0.getConsumedPower()/1e3;
        
         ct1_a = _thruster1.GetActualTrhust()/1e3;
         ct1_b = _thruster1.getConsumedPower()/1e3;
        
         System.out.println("Main0: " + Double.toString(cm0_a) + " kN @ " + Double.toString(cm0_b) +" kW " );
         System.out.println("Main1: " + Double.toString(cm1_a) + " kN @ " + Double.toString(cm1_b) +" kW " );
        
         System.out.println("Azimuth0: " + Double.toString(ca0_a) + " kN @ " + Double.toString(ca0_b) +" kW " );

         System.out.println("Thruster0: " + Double.toString(ct0_a) + " kN @ " + Double.toString(ct0_b) +" kW " );
         System.out.println("Thruster1: " + Double.toString(ct1_a) + " kN @ " + Double.toString(ct1_b) +" kW " );


        
        
         */
        FhDynamicModel.reset();
        ut776cd.setSfh_test(false);
    }

    @Override
    public ArrayList<SFHActuatorPointIntf> getTransverseThrusterIntf() {
        ArrayList<SFHActuatorPointIntf> retval = new ArrayList<>();
        retval.add(_conThruster0);
        retval.add(_conThruster1);
        return retval;
    }

    @Override
    public ArrayList<SFHActuatorPointIntf> getRotatableTrhusterIntf() {
        ArrayList<SFHActuatorPointIntf> retval = new ArrayList<>();
        retval.add(_conAzimuth0);
        retval.add(_conMain0);
        retval.add(_conMain1);
        return retval;
    }

    @Override
    public void setTurningCircleCommand(double direction) {
        setCruiseCommand();
        _conMain0.setCommandeAngle(90 * Math.signum(direction));
        _conMain1.setCommandeAngle(90 * Math.signum(direction));
    }

    @Override
    public void setCrashStopCommand() {
        _conMain0.setComandedPitchNormalized(1);
        _conMain1.setComandedPitchNormalized(1);

        _conMain0.setCommandedRPMNormalized(1);
        _conMain1.setCommandedRPMNormalized(1);

        _conMain0.setCommandeAngle(135 * 0.999);
        _conMain1.setCommandeAngle(-135 * 0.999);
    }

    @Override
    public void setCruiseCommand() {
        setCruiseCommand(1.0);
    }

    @Override
    public void setCruiseCommand(double percent) {
        _conMain0.setComandedPitchNormalized(1);
        _conMain1.setComandedPitchNormalized(1);

        if (percent > 0) {
            _conMain0.setCommandedRPMNormalized(percent);
            _conMain1.setCommandedRPMNormalized(percent);

            _conMain0.setCommandeAngleNormalized(0);
            _conMain1.setCommandeAngleNormalized(0);
        } else {
            _conMain0.setCommandedRPMNormalized(percent);
            _conMain1.setCommandedRPMNormalized(percent);

            _conMain0.setCommandeAngleNormalized(0.99999);
            _conMain1.setCommandeAngleNormalized(-0.99999);
        }
    }

    @Override
    public void setAllThrusterCommand(double percent) {
        setAllThrusterCommand(percent, false);
    }

    @Override
    public void setAllThrusterCommand(double percent, boolean noAzi) {
        _conMain0.setCommandedRPMNormalized(percent);
        _conMain1.setCommandedRPMNormalized(percent);

        _conMain0.setComandedPitchNormalized(percent);
        _conMain1.setComandedPitchNormalized(percent);

        _conThruster0.setComandedPitchNormalized(percent);
        _conThruster1.setComandedPitchNormalized(percent);

        _conThruster0.setCommandedRPMNormalized(percent);
        _conThruster1.setCommandedRPMNormalized(percent);

        _conAzimuth0.setComandedPitchNormalized(percent);
        _conAzimuth0.setCommandedRPMNormalized(percent);
        if (!noAzi) {
            _conMain0.setCommandeAngleNormalized(percent);
            _conMain1.setCommandeAngleNormalized(percent);
            _conAzimuth0.setCommandeAngleNormalized(percent);
        }
    }

    @Override
    public void setDPCrabCommand(double direction) {

        double moment = 0;

        moment += (_conThruster0.getPermanentLinearOffset()[0] - hyd.LCG) * thruster0.getThrustForPitch(conThruster0.getMaxPitch(), conThruster0.getMaxRPM(), 0);
        moment += (_conThruster1.getPermanentLinearOffset()[0] - hyd.LCG) * thruster1.getThrustForPitch(conThruster1.getMaxPitch(), conThruster1.getMaxRPM(), 0);
        moment += (_conAzimuth0.getPermanentLinearOffset()[0] - hyd.LCG) * azimuth0.getThrustForPitch(conAzimuth0.getMaxPitch(), conAzimuth0.getMaxRPM(), 0);

        _conThruster0.setCommandedRPMNormalized(1);
        _conThruster1.setCommandedRPMNormalized(1);
        _conAzimuth0.setCommandedRPMNormalized(1);

        _conThruster0.setComandedPitchNormalized(1 * direction);
        _conThruster1.setComandedPitchNormalized(1 * direction);
        _conAzimuth0.setComandedPitchNormalized(1);

        _conAzimuth0.setCommandeAngle(90 * direction);

        double thrustMain = (moment) / (hyd.LCG - _conMain0.getPermanentLinearOffset()[0]) / Math.sin(45 * Math.PI / 180);

        double[] cmd1 = main0.rpmPitchForThrust(thrustMain, 2100e3);
        double[] cmd2 = main0.rpmPitchForThrust(thrustMain / 2, 2100e3);

        if (cmd1 != null) {
            System.out.println("Balancing a moment of : " + moment / 1e3 + " kNm with main thruster force of : " + 1 / 1e3 * thrustMain * Math.sin(Math.PI / 4) * (hyd.LCG - _conMain0.getPermanentLinearOffset()[0]));
            _conMain0.setCommandeAngle(90 * direction);
            _conMain0.setCommandedRPM(cmd1[0]);
            _conMain0.setComandedPitch(cmd1[1]);
        } else if (cmd2 != null) {
            System.out.println("Balancing a moment of : " + moment / 1e3 + " kNm with main thruster force of : 2x" + 1 / 1e3 / 2 * thrustMain * Math.sin(Math.PI / 4) * (hyd.LCG - _conMain0.getPermanentLinearOffset()[0]));
            System.out.println("Balancing a moment of : " + moment / 1e3 + " kNm with main thruster force of : 2x" + 1 / 1e3 / 2 * thrustMain * Math.sin(Math.PI / 4));
            _conMain0.setCommandeAngle(45 * direction);
            _conMain0.setCommandedRPM(cmd2[0]);
            _conMain0.setComandedPitch(cmd2[1]);

            _conMain1.setCommandeAngle(135 * direction);
            _conMain1.setCommandedRPM(cmd2[0]);
            _conMain1.setComandedPitch(cmd2[1]);
        } else {
            _conMain0.setCommandeAngle(45 * direction);
            _conMain0.setCommandedRPMNormalized(1);
            _conMain0.setComandedPitchNormalized(1);

            _conMain1.setCommandeAngle(135 * direction);
            _conMain1.setCommandedRPMNormalized(1);
            _conMain1.setComandedPitchNormalized(1);
        }

    }

    @Override
    public void setDPPirCommand(double direction) {
        _conThruster0.setCommandedRPMNormalized(1);
        _conThruster1.setCommandedRPMNormalized(1);
        _conAzimuth0.setCommandedRPMNormalized(1);

        _conThruster0.setComandedPitchNormalized(1 * direction);
        _conThruster1.setComandedPitchNormalized(1 * direction);
        _conAzimuth0.setComandedPitchNormalized(1);

        _conAzimuth0.setCommandeAngle(90 * direction);

        _conMain0.setCommandedRPMNormalized(1);
        _conMain0.setComandedPitchNormalized(1);

        _conMain1.setCommandedRPMNormalized(1);
        _conMain1.setComandedPitchNormalized(1);

        _conMain0.setCommandeAngle(-45 * direction);
        _conMain1.setCommandeAngle(-135 * direction);

    }

    public void testWindDrift(double windSpeedInMeterPerSecond, double WindGoesToDirectionInRadian) {

        FlatSea waveModel = new FlatSea();

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        FhDynamicModel.instance().setEndTime(900);
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            T += dT;

            _env.SetWindDirection(WindGoesToDirectionInRadian);
            _env.SetWindSpeed(windSpeedInMeterPerSecond);
            double r_2 = _ut776cd.GetGlobalPosition()[0] * _ut776cd.GetGlobalPosition()[0] + _ut776cd.GetGlobalPosition()[1] * _ut776cd.GetGlobalPosition()[1];
            if ((T - (int) T) < .1) {
                System.out.println(Math.round(_ut776cd.GetGlobalOrientation()[2] * 180 / Math.PI) + "," + Math.round(Math.sqrt(r_2)));
            }

        }
        FhDynamicModel.reset();
    }

    public void testWaveDrift(double intialYawInDegrees, double waveComesFromDirectionInDregrees, double waveWaveHeightInMeter, double wavePeriodInSec, boolean veresWaveDrift) {

        double initialYaw = intialYawInDegrees; // in degrees
        double initialWaveDirection = (waveComesFromDirectionInDregrees) * Math.PI / 180; // in radians

        ut776cd.setInitialRollPitchYaw(new double[]{0, 0, initialYaw});
        ut776cd.setVeresWaveDrift(veresWaveDrift);

        double height = waveWaveHeightInMeter;

        double direction = initialWaveDirection;
        SingleWave waveModel = new SingleWave();
        waveModel.setWaveAmplitude(height);
        waveModel.setWaveFrequency(2 * Math.PI / wavePeriodInSec);
        waveModel.setWaveDirection(direction);

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        FhDynamicModel.instance().setEndTime(900);
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            T += dT;

            if ((T - (int) T) < .1) {
                System.out.println(Math.round(_ut776cd.GetGlobalPosition()[0]) + " , " + Math.round(_ut776cd.GetGlobalPosition()[1]) + ", " + Math.round(_ut776cd.GetGlobalOrientation()[2] * 180 / Math.PI));
            }

        }
        FhDynamicModel.reset();

    }

}
