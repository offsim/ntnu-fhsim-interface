/*
 * Copyright (C) 2004, 2019, Offshore Simulator Centre AS (OSC AS).
 * All rights reserved.
 * OSC AS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.sfh.ship.vesselExamples;

import com.sfh.ship.VeresFile;
import com.osc.wave.FlatSea;
import com.sfh.ship.ActorIntf;
import com.sfh.ship.FhDynamicModel;
import com.sfh.ship.SFHActuatorPointData;
import com.sfh.ship.SFHActuatorPointIntf;
import com.sfh.ship.SFHEnvironmentData;
import com.sfh.ship.SFHEnvironmentIntf;
import com.sfh.ship.SFHHullData;
import com.sfh.ship.SFHHullIntf;
import com.sfh.ship.SFHOpusPropulsorData;
import com.sfh.ship.SFHOpusPropulsorIntf;
import com.sfh.osc.spec.FhSimModelsFolder;
import com.sfh.osc.spec.OSCHullDataSpec;
import com.sfh.osc.spec.ReportSpec;
import java.util.ArrayList;

/**
 *
 * @author pierre
 */
public class PK410_NTNU_Gunnerus extends BaseExample {

    private SFHHullData gunnerus;
    private SFHActuatorPointData conMainPort, conMainStarboard;
    private SFHActuatorPointData conTunnel;

    private SFHOpusPropulsorData mainPort, mainStarboard;
    private SFHOpusPropulsorData tunnel;

    private SFHHullIntf _gunnerus;
    private SFHActuatorPointIntf _conMainPort, _conMainStarboard;
    private SFHActuatorPointIntf _conTunnel;

    private SFHOpusPropulsorIntf _mainPort, _mainStarboard;
    private SFHOpusPropulsorIntf _tunnel;

    public static void main(String args[]) {
        //    FhDynamicModel.instance().setVisualization(false);
        PK410_NTNU_Gunnerus theTest = new PK410_NTNU_Gunnerus();
        theTest.execute();
    }

    @Override
    public void configureShip() {
        /*
        HULL
         */
        String folder = FhSimModelsFolder.getFolder();
        gunnerus = SFHHullData.loadFromXml(folder + "/RV_PK410_2_NTNU_Gunnerus/dynmodel/fhsim/vessel.xml");
        reportSpec = ReportSpec.loadFromXml(folder + "/RV_PK410_2_NTNU_Gunnerus/dynmodel/fhsim/report_spec.xml");
        oscSpec  = OSCHullDataSpec.loadFromXml(folder + "/RV_PK410_2_NTNU_Gunnerus/dynmodel/fhsim/vessel_spec.xml");
        /*
        MAIN CONNECTIONS
         */
        conMainPort = SFHActuatorPointData.loadFromXml("conMainPort", folder + "/RV_PK410_2_NTNU_Gunnerus/dynmodel/fhsim/mainPortActuator.xml");
        conMainStarboard = SFHActuatorPointData.loadFromXml("conMainStarboard", folder + "/RV_PK410_2_NTNU_Gunnerus/dynmodel/fhsim/mainStarboardActuator.xml");
        conTunnel = SFHActuatorPointData.loadFromXml("conTunnel", folder + "/RV_PK410_2_NTNU_Gunnerus/dynmodel/fhsim/TunnelActuator.xml");

        /*
        MAIN PROPELLERS
         */
        mainPort = SFHOpusPropulsorData.loadFromXml("MainPort", folder + "/RV_PK410_2_NTNU_Gunnerus/dynmodel/fhsim/propulsors/main.xml");
        mainStarboard = SFHOpusPropulsorData.loadFromXml("MainStarboard", folder + "/RV_PK410_2_NTNU_Gunnerus/dynmodel/fhsim/propulsors/main.xml");

        /*
        THRUSTERS
         */
        tunnel = SFHOpusPropulsorData.loadFromXml("Tunnel", folder + "/RV_PK410_2_NTNU_Gunnerus/dynmodel/fhsim/propulsors/tunnel.xml");

        /*
        CONNECT PROPULSORS TO ACTUATORS
         */
        mainPort.ConnectToReference(conMainPort);
        mainStarboard.ConnectToReference(conMainStarboard);

        tunnel.ConnectToReference(conTunnel);

        /*
        ATTACH ACTUATORS TO HULL
         */
        gunnerus.AddReferencePoint(conMainPort);
        gunnerus.AddReferencePoint(conMainStarboard);
        gunnerus.AddReferencePoint(conTunnel);

        hyd = (new VeresFile(getHull().getModelData() + "/" + getHull().getModelFileStem())).hydFile;

    }

    @Override
    public void runTrials() {
        FhDynamicModel.instance().setVisualization(true);
        FhDynamicModel.instance().setResultLog(true);
        // thrusterTest();
        // runDPcrab(-1);
        //   runDPcrab(1);
        //runFullSpeedTrial();
        fullSpeed();

        //       FhDynamicModel.reset();
/*        runDPpirouette();
        FhDynamicModel.reset();
        runDPcrab(1);
        FhDynamicModel.reset();
        runDPcrab(-1);*/
        //runCrashStop(7.0);
        //   thrusterTest(1, 0);
        //   propulsorTest();
// lateralBalanceTest();
        //       fullSpeed();
        //  runStarboardTurningCirlceTrial();
    }

    @Override
    public SFHHullData getHull() {
        return gunnerus;
    }

    @Override
    public ArrayList<BaseExample.PropulsorActuatorDataPair> getPropulsors() {
        ArrayList<BaseExample.PropulsorActuatorDataPair> retval = new ArrayList<BaseExample.PropulsorActuatorDataPair>();

        retval.add(new BaseExample.PropulsorActuatorDataPair(mainPort, conMainPort));
        retval.add(new BaseExample.PropulsorActuatorDataPair(mainStarboard, conMainStarboard));

        retval.add(new BaseExample.PropulsorActuatorDataPair(tunnel, conTunnel));

        return retval;
    }

    @Override
    public ArrayList<ActorIntf> createActors() {
        ArrayList<ActorIntf> retval = new ArrayList<>();
        _gunnerus = gunnerus.create();
        _conMainPort = conMainPort.create();
        _conMainStarboard = conMainStarboard.create();
        _conTunnel = conTunnel.create();

        _mainPort = mainPort.create();
        _mainStarboard = mainStarboard.create();
        _tunnel = tunnel.create();

        _conMainPort.setAvailablePower(500e3);
        _conMainStarboard.setAvailablePower(500e3);
        _conTunnel.setAvailablePower(200e3);

        retval.add(_gunnerus);
        retval.add(_conMainPort);
        retval.add(_conMainStarboard);
        retval.add(_conTunnel);

        retval.add(_mainPort);
        retval.add(_mainStarboard);
        retval.add(_tunnel);

        return retval;
    }

    @Override
    public SFHHullIntf getHullIntf() {
        return _gunnerus;
    }

    @Override
    public ArrayList<SFHActuatorPointIntf> getMainActuatorsIntf() {
        ArrayList<SFHActuatorPointIntf> retval = new ArrayList<>();
        retval.add(_conMainPort);
        retval.add(_conMainStarboard);
        return retval;
    }

    @Override
    public ArrayList<SFHActuatorPointIntf> getMainRudderIntf() {
        ArrayList<SFHActuatorPointIntf> retval = new ArrayList<>();
        retval.add(_conMainPort);
        retval.add(_conMainStarboard);
        return retval;
    }

    public void fullSpeed() {
        //Marine Traffic average/max speed: 11.3/12.8 knots
        FlatSea waveModel = new FlatSea();

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> rudders = getMainRudderIntf();
        ArrayList<SFHActuatorPointIntf> propellers = getMainActuatorsIntf();

        FhDynamicModel.instance().setEndTime(900);
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            T += dT;

            for (SFHActuatorPointIntf a : propellers) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0);
            }

            for (SFHActuatorPointIntf a : rudders) {
                a.setCommandeAngle(0);

            }

            double power = (_mainPort.getConsumedPower() + _mainStarboard.getConsumedPower()) / 1e3;
            double speed = Math.sqrt(_gunnerus.GetLocalVelocity()[0] * _gunnerus.GetLocalVelocity()[0] + _gunnerus.GetLocalVelocity()[1] * _gunnerus.GetLocalVelocity()[1]) / 0.5144;

            System.out.println(String.format("surge speed: %.1f kn, total speed : %.1f , power %.1f kW ", _gunnerus.GetLocalVelocity()[0] / 0.5144 , speed, power ));

        }
        FhDynamicModel.reset();
    }

    public void thrusterTest() {

        configureShip();
        gunnerus.setSfh_test(true);
        FlatSea waveModel = new FlatSea();

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        setAllThrusterCommand(0.5, true);

        FhDynamicModel.instance().setEndTime(40);
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            T += dT;
        }

        double cm0_a = _mainPort.GetNominalThrust() / 1e3;
        double cm0_b = _mainPort.getConsumedPower() / 1e3;

        double cm1_a = _mainStarboard.GetNominalThrust() / 1e3;
        double cm1_b = _mainStarboard.getConsumedPower() / 1e3;

        double cmt_a = _tunnel.GetNominalThrust() / 1e3;
        double cmt_b = _tunnel.getConsumedPower() / 1e3;

        System.out.println("Port: " + Double.toString(cm0_a) + " kN @ " + Double.toString(cm0_b) + " kW ");
        System.out.println("Starboard: " + Double.toString(cm1_a) + " kN @ " + Double.toString(cm1_b) + " kW ");
        System.out.println("Tunnel: " + Double.toString(cmt_a) + " kN @ " + Double.toString(cmt_b) + " kW ");

        FhDynamicModel.reset();
        gunnerus.setSfh_test(false);
    }

    public void lateralBalanceTest() {

        configureShip();
        gunnerus.setSfh_test(true);
        FlatSea waveModel = new FlatSea();

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> propellers = getMainActuatorsIntf();
        ArrayList<SFHActuatorPointIntf> thrusters = getTransverseThrusterIntf();
        ArrayList<SFHActuatorPointIntf> azimuths = getRotatableTrhusterIntf();

        FhDynamicModel.instance().setEndTime(40);
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            T += dT;
            for (SFHActuatorPointIntf a : propellers) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0 / 2);
                //a.setCommandedRPMNormalized(1.0);
            }

            _conMainPort.setComandedPitchNormalized(1.0);
            _conMainPort.setCommandedRPMNormalized(1.0 / 1.8);

            _conMainStarboard.setComandedPitchNormalized(1.0);
            _conMainStarboard.setCommandedRPMNormalized(1.0);

            for (SFHActuatorPointIntf a : thrusters) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0);
            }

            for (SFHActuatorPointIntf a : azimuths) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0);
            }
        }

        double cm0_a = _mainPort.GetNominalThrust() / 1e3;
        double cm0_b = _mainPort.getConsumedPower() / 1e3;

        double cm1_a = _mainStarboard.GetNominalThrust() / 1e3;
        double cm1_b = _mainStarboard.getConsumedPower() / 1e3;

        double cmt_a = _tunnel.GetNominalThrust() / 1e3;
        double cmt_b = _tunnel.getConsumedPower() / 1e3;

        System.out.println("Main0: " + Double.toString(cm0_a) + " kN @ " + Double.toString(cm0_b) + " kW ");
        System.out.println("Main1: " + Double.toString(cm1_a) + " kN @ " + Double.toString(cm1_b) + " kW ");
        System.out.println("Tunnel: " + Double.toString(cmt_a) + " kN @ " + Double.toString(cmt_b) + " kW ");

        FhDynamicModel.reset();
        gunnerus.setSfh_test(false);
    }

    public void propulsorTest() {

        gunnerus.setSfh_test(true);
        FlatSea waveModel = new FlatSea();

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> propellers = getMainActuatorsIntf();
        ArrayList<SFHActuatorPointIntf> thrusters = getTransverseThrusterIntf();
        ArrayList<SFHActuatorPointIntf> azimuths = getRotatableTrhusterIntf();

        FhDynamicModel.instance().setEndTime(60);
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            T += dT;
            for (SFHActuatorPointIntf a : propellers) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0);
            }

            for (SFHActuatorPointIntf a : thrusters) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0);
            }

            for (SFHActuatorPointIntf a : azimuths) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(1.0);
            }
        }

        double cm0_a = _mainPort.GetActualTrhust() / 1e3;
        double cm0_b = _mainPort.getConsumedPower() / 1e3;

        double cm1_a = _mainStarboard.GetActualTrhust() / 1e3;
        double cm1_b = _mainStarboard.getConsumedPower() / 1e3;

        double ct0_a = _tunnel.GetActualTrhust() / 1e3;
        double ct0_b = _tunnel.getConsumedPower() / 1e3;

        System.out.println("MainPort: " + Double.toString(cm0_a) + " kN @ " + Double.toString(cm0_b) + " kW ");
        System.out.println("MainStarboard: " + Double.toString(cm1_a) + " kN @ " + Double.toString(cm1_b) + " kW ");
        System.out.println("Tunnel: " + Double.toString(ct0_a) + " kN @ " + Double.toString(ct0_b) + " kW ");

        FhDynamicModel.reset();
        _env = environment.create();

        actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        propellers = getMainActuatorsIntf();
        thrusters = getTransverseThrusterIntf();
        azimuths = getRotatableTrhusterIntf();

        FhDynamicModel.instance().setEndTime(60);
        T = 0;
        dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            T += dT;
            for (SFHActuatorPointIntf a : propellers) {
                a.setComandedPitchNormalized(1.0);
                a.setCommandedRPMNormalized(-1.0);
            }

            for (SFHActuatorPointIntf a : thrusters) {
                a.setComandedPitchNormalized(-1.0);
                a.setCommandedRPMNormalized(1.0);
            }

            for (SFHActuatorPointIntf a : azimuths) {
                a.setComandedPitchNormalized(-1.0);
                a.setCommandedRPMNormalized(1.0);
            }
        }

        cm0_a = _mainPort.GetActualTrhust() / 1e3;
        cm0_b = _mainPort.getConsumedPower() / 1e3;

        cm1_a = _mainStarboard.GetActualTrhust() / 1e3;
        cm1_b = _mainStarboard.getConsumedPower() / 1e3;

        ct0_a = _tunnel.GetActualTrhust() / 1e3;
        ct0_b = _tunnel.getConsumedPower() / 1e3;

        System.out.println("Main0: " + Double.toString(cm0_a) + " kN @ " + Double.toString(cm0_b) + " kW ");
        System.out.println("Main1: " + Double.toString(cm1_a) + " kN @ " + Double.toString(cm1_b) + " kW ");

        System.out.println("Tunnel: " + Double.toString(ct0_a) + " kN @ " + Double.toString(ct0_b) + " kW ");

        FhDynamicModel.reset();
        gunnerus.setSfh_test(false);
    }

    @Override
    public ArrayList<SFHActuatorPointIntf> getTransverseThrusterIntf() {
        ArrayList<SFHActuatorPointIntf> retval = new ArrayList<>();
        retval.add(_conTunnel);

        return retval;
    }

    @Override
    public ArrayList<SFHActuatorPointIntf> getRotatableTrhusterIntf() {
        ArrayList<SFHActuatorPointIntf> retval = new ArrayList<>();

        retval.add(_conMainPort);
        retval.add(_conMainStarboard);
        return retval;
    }

    @Override
    public void setTurningCircleCommand(double direction) {
        _conMainPort.setCommandeAngle(45 * Math.signum(direction));
        _conMainStarboard.setCommandeAngle(45 * Math.signum(direction));
    }

    @Override
    public void setCrashStopCommand() {
        _conMainPort.setComandedPitchNormalized(1);
        _conMainStarboard.setComandedPitchNormalized(1);

        _conMainPort.setCommandedRPMNormalized(1);
        _conMainStarboard.setCommandedRPMNormalized(1);

        _conMainPort.setCommandeAngle(180 * 0.9999);
        _conMainStarboard.setCommandeAngle(-180 * 0.9999);
    }

    @Override
    public void setCruiseCommand() {
        setCruiseCommand(0.9);
    }

    @Override
    public void setCruiseCommand(double percent) {
        _conMainPort.setComandedPitchNormalized(1);
        _conMainStarboard.setComandedPitchNormalized(1);

        if (percent > 0) {
            _conMainPort.setCommandedRPMNormalized(percent);
            _conMainStarboard.setCommandedRPMNormalized(percent);

            _conMainPort.setCommandeAngleNormalized(0);
            _conMainStarboard.setCommandeAngleNormalized(0);
        } else {
            _conMainPort.setCommandedRPMNormalized(percent);
            _conMainStarboard.setCommandedRPMNormalized(percent);

            _conMainPort.setCommandeAngleNormalized(0.99999);
            _conMainStarboard.setCommandeAngleNormalized(-0.99999);
        }
    }

    @Override
    public void setAllThrusterCommand(double percent) {
        setAllThrusterCommand(percent, false);
    }

    @Override
    public void setAllThrusterCommand(double percent, boolean noAzi) {
        _conMainPort.setCommandedRPMNormalized(percent);
        _conMainStarboard.setCommandedRPMNormalized(percent);

        if (!noAzi) {
            _conMainPort.setCommandeAngleNormalized(percent);
            _conMainStarboard.setCommandeAngleNormalized(percent);
            _conTunnel.setCommandeAngleNormalized(percent);
        }
        _conMainPort.setComandedPitchNormalized(1.0);
        _conMainStarboard.setComandedPitchNormalized(1.0);
        _conTunnel.setComandedPitchNormalized(percent);

        _conTunnel.setCommandedRPMNormalized(1.0);

    }

    @Override
    public void setDPCrabCommand(double direction) {

        double moment = 0;

        moment += (_conTunnel.getPermanentLinearOffset()[0] - hyd.LCG) * tunnel.getThrustForPitch(conTunnel.getMaxPitch(), conTunnel.getMaxRPM(), 0);
        moment += (_conMainPort.getPermanentLinearOffset()[0] - hyd.LCG) * mainPort.getThrustForPitch(conMainPort.getMaxPitch(), conMainPort.getMaxRPM(), 0);
//        moment += (_conMainStarboard.getPermanentLinearOffset()[0] - hyd.LCG) * mainStarboard.getThrustForPitch(conMainStarboard.getMaxPitch(), conMainStarboard.getMaxRPM(), 0);

        _conTunnel.setCommandedRPMNormalized(1);
        _conMainPort.setCommandedRPMNormalized(1);
        _conMainStarboard.setCommandedRPMNormalized(1);

        _conTunnel.setComandedPitchNormalized(1 * direction);
        _conMainPort.setComandedPitchNormalized(1 * direction);

        double thrustMain = (moment) / (hyd.LCG - _conMainPort.getPermanentLinearOffset()[0]) / Math.sin(45 * Math.PI / 180);

        double[] cmd1 = mainPort.rpmPitchForThrust(thrustMain, 2500e3);
        double[] cmd2 = mainPort.rpmPitchForThrust(thrustMain / 2, 2500e3);

        if (cmd1 != null) {
            System.out.println("Balancing a moment of : " + moment / 1e3 + " kNm with main thruster force of : " + 1 / 1e3 * thrustMain * Math.sin(Math.PI / 4) * (hyd.LCG - _conMainPort.getPermanentLinearOffset()[0]));
            _conMainPort.setCommandeAngle(90 * direction);
            _conMainPort.setCommandedRPM(cmd1[0]);
            _conMainPort.setComandedPitch(cmd1[1]);
        } else if (cmd2 != null) {
            System.out.println("Balancing a moment of : " + moment / 1e3 + " kNm with main thruster force of : 2x" + 1 / 1e3 / 2 * thrustMain * Math.sin(Math.PI / 4) * (hyd.LCG - _conMainPort.getPermanentLinearOffset()[0]));
            System.out.println("Balancing a moment of : " + moment / 1e3 + " kNm with main thruster force of : 2x" + 1 / 1e3 / 2 * thrustMain * Math.sin(Math.PI / 4));
            _conMainPort.setCommandeAngle(45 * direction);
            _conMainPort.setCommandedRPM(cmd2[0]);
            _conMainPort.setComandedPitch(cmd2[1]);

            _conMainStarboard.setCommandeAngle(135 * direction);
            _conMainStarboard.setCommandedRPM(cmd2[0]);
            _conMainStarboard.setComandedPitch(cmd2[1]);
        } else {
            _conMainPort.setCommandeAngle(45 * direction);
            _conMainPort.setCommandedRPMNormalized(1);
            _conMainPort.setComandedPitchNormalized(1);

            _conMainStarboard.setCommandeAngle(135 * direction);
            _conMainStarboard.setCommandedRPMNormalized(1);
            _conMainStarboard.setComandedPitchNormalized(1);
        }

    }

    @Override
    public void setDPPirCommand(double direction) {
        _conTunnel.setCommandedRPMNormalized(1);
        _conMainPort.setCommandedRPMNormalized(1);

        _conTunnel.setComandedPitchNormalized(1 * direction);
        _conMainPort.setComandedPitchNormalized(1 * direction);

        _conMainPort.setCommandedRPMNormalized(1);
        _conMainPort.setComandedPitchNormalized(1);

    }

    @Override
    public void recreateFromSavedModel(FhDynamicModel.SavedModel model) {
        throw new UnsupportedOperationException("Resume reconstruction of model not implemented");
    }

}
