/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.ship.vesselExamples;

import com.sfh.ship.VeresFile;
import com.osc.wave.FlatSea;
import com.osc.wave.ITTCTestWaveModel;
import com.osc.wave.SingleWave;
import com.sfh.osc.spec.OSCHullDataSpec;
import com.sfh.osc.spec.OSCModel;
import com.sfh.osc.spec.ReportSpec;
import com.sfh.ship.ActorIntf;
import com.sfh.ship.FhDynamicModel;
import com.sfh.ship.SFHActuatorPointData;
import com.sfh.ship.SFHActuatorPointIntf;
import com.sfh.ship.SFHEnvironmentData;
import com.sfh.ship.SFHEnvironmentIntf;
import com.sfh.ship.SFHHullData;
import com.sfh.ship.SFHHullIntf;
import com.sfh.ship.SFHOpusPropulsorData;
import com.sfh.ship.SFHOpusPropulsorIntf;
import com.sfh.ship.SFHReferencePointData;
import com.sfh.ship.SFHReferencePointIntf;
import com.sfh.ship.SFHSpringData;
import com.sfh.ship.SFHSpringIntf;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import org.w3c.dom.Element;

/**
 *
 * @author aarsathe
 */
public abstract class BaseExample {

    
    private VeresFile veresFile;
    protected String loadCondition;
    protected boolean gm_reduced;

    //All the default values are true;
    protected ReportSpec reportSpec = new ReportSpec();
    //All the specified values from OSC;
    protected OSCHullDataSpec oscSpec = new OSCHullDataSpec();
    protected OSCModel oscModel = new OSCModel();
    private double naturalRollPeriodInSec = 10;
    private int count = 0;

    public BaseExample() {
        this.loadCondition = "DWL";
        gm_reduced = false;
    }

    public BaseExample(String loadCondition) {
        this.loadCondition = loadCondition;
        gm_reduced = false;
    }

    public BaseExample(String wl_id, boolean reduce_gm) {
        this.loadCondition = wl_id;
        this.gm_reduced = reduce_gm;
    }

    public VeresFile getVeresFile() {

        if(veresFile == null)
            veresFile = new VeresFile(getHull().getModelData() + "/" + getHull().getModelFileStem());
        return veresFile;
    }

    public OSCHullDataSpec getOSCHullDataSpec() {
        return oscSpec;
    }

    public ReportSpec getReportSpec() {
        return reportSpec;
    }

    private final static String format(double value) {
        return String.format("%.2f", value);
    }

    public void setNaturalRollPeriod(double d) {
        this.naturalRollPeriodInSec = d;
    }

    public double getNatualRollPeriod() {
        return naturalRollPeriodInSec;
    }

    public double getOSCModelXRelAP() {
        return oscModel.getXPos();
    }

    public double getOSCModelYRelCL() {
        return oscModel.getYPos();
    }

    public double getOSCModelZRelBL() {
        return oscModel.getZPos();
    }

    public interface IShipPoint {

        public double getX();

        public double getY();

        public double getZ();

        public String getFormattedX();

        public String getFormattedY();

        public String getFormattedZ();
    }

    public class PropulsorActuatorDataPair implements IShipPoint {

        PropulsorActuatorDataPair(SFHOpusPropulsorData p, SFHActuatorPointData a) {
            propulsor = p;
            actuator = a;
        }
        public SFHOpusPropulsorData propulsor;
        public SFHActuatorPointData actuator;

        /**
         *
         * @return position relative to AP
         */
        @Override
        public double getX() {
            return actuator.getLinearOffset()[0];
        }

        /**
         *
         * @return position relative to CL
         */
        @Override
        public double getY() {
            return actuator.getLinearOffset()[1];
        }

        /**
         *
         * @return position relative to BL
         */
        @Override
        public double getZ() {
            return actuator.getLinearOffset()[2];
        }

        @Override
        public String getFormattedX() {
            return format(getX());
        }

        @Override
        public String getFormattedY() {
            return format(getY());
        }

        @Override
        public String getFormattedZ() {
            return format(getZ());
        }

        /**
         *
         * @param offset for example Lpp/2
         * @return x coordinate relative to AP-offset
         */
        public String getFormattedX(double offset) {
            return format(getX() - offset);
        }

        /**
         *
         * @param offset for example Design Waterline
         * @return z coordinated relative BL-offset
         */
        public String getFormattedZ(double offset) {
            return format(getZ() - offset);
        }

        public String getFormattedY(double offset) {
            return format(getY() - offset);
        }
    }

    public static class TimeSeries {

        public static ArrayList<Double> diffArrayList(ArrayList<Double> series) {
            ArrayList<Double> ret = new ArrayList<Double>();
            for (int i = 0; i < series.size() - 1; i++) {
                ret.add(series.get(i + 1) - series.get(i));
            }
            return ret;
        }

        public static ArrayList<Double> absArrayList(ArrayList<Double> series) {
            ArrayList<Double> ret = new ArrayList<Double>();
            for (int i = 0; i < series.size(); i++) {
                ret.add(Math.abs(series.get(i)));
            }
            return ret;
        }

        public static double avgArrayList(ArrayList<Double> series) {
            double ret = 0;
            for (int i = 0; i < series.size(); i++) {
                ret += series.get(i) / series.size();
            }
            return ret;
        }

        public TimeSeries() {
            x = new ArrayList<>();
            y = new ArrayList<>();
        }
        public ArrayList<Double> x;
        public ArrayList<Double> y;

        public ArrayList<ArrayList<Double>> getExtrema() {
            ArrayList<ArrayList<Double>> ret = new ArrayList<ArrayList<Double>>();
            ArrayList<Double> retX = new ArrayList<Double>();
            ArrayList<Double> retY = new ArrayList<Double>();

            retX.add(x.get(0));
            retY.add(y.get(0));

            for (int i = 1; i < (x.size() - 1); i++) {

                double val = y.get(i);
                double next = y.get(i + 1);
                double last = y.get(i - 1);

                if (val > next && val > last) {
                    retX.add(x.get(i));
                    retY.add(y.get(i));
                }
                if (val < next && val < last) {
                    retX.add(x.get(i));
                    retY.add(y.get(i));
                }
            }
            ret.add(retX);
            ret.add(retY);

            return ret;
        }

        public void scale(double s) {
            for (int i = 0; i < y.size(); i++) {
                y.set(i, y.get(i) * s);
            }

        }

        public void append(double x, double y) {
            this.x.add(x);
            this.y.add(y);
        }

        public double getX(int i) {
            assert (i < x.size());
            return x.get(i);
        }

        public double getY(int i) {
            assert (i < y.size());
            return y.get(i);
        }

        public double getLastX() {
            return x.get(x.size() - 1);
        }

        public double getLastY() {
            return y.get(y.size() - 1);
        }

        public int seriesLength() {
            return x.size();
        }

        public double[] toArrayX() {
            Double[] ret = new Double[x.size()];
            ret = x.toArray(ret);
            double[] ret2 = new double[ret.length];
            for (int i = 0; i < ret.length; i++) {
                ret2[i] = ret[i];
            }
            return ret2;
        }

        public double[] toArrayY() {
            Double[] ret = new Double[y.size()];
            ret = y.toArray(ret);
            double[] ret2 = new double[ret.length];
            for (int i = 0; i < ret.length; i++) {
                ret2[i] = ret[i];
            }
            return ret2;
        }

        public double maxY() {
            double retval = 0;
            for (int i = 0; i < y.size(); i++) {
                if (y.get(i) > retval) {
                    retval = y.get(i);
                }
            }
            return retval;
        }

        public double minY() {
            double retval = 0;
            for (int i = 0; i < y.size(); i++) {
                if (y.get(i) < retval) {
                    retval = y.get(i);
                }
            }
            return retval;
        }

        public double maxabsY() {
            double retval = 0;
            for (int i = 0; i < y.size(); i++) {
                if (Math.abs(y.get(i)) > retval) {
                    retval = Math.abs(y.get(i));
                }
            }
            return retval;
        }

        public double[] regionToArrayX(double startTime, double stopTime) {
            int start = 0;
            while (start < x.size() - 1 && x.get(start + 1) < startTime) {
                start++;
            }
            int stop = start;
            while (stop < x.size() - 1 && x.get(stop + 1) < stopTime) {
                stop++;
            }
            Double[] ret = new Double[x.size()];
            ret = x.toArray(ret);
            double[] ret2 = new double[stop - start];
            for (int i = start; i < stop; i++) {
                ret2[i - start] = ret[i] - ret[start];
            }
            return ret2;
        }

        public double[] regionToArrayY(double startTime, double stopTime) {
            int start = 0;
            while (start < x.size() - 1 && x.get(start + 1) < startTime) {
                start++;
            }
            int stop = start;
            while (stop < x.size() - 1 && x.get(stop + 1) < stopTime) {
                stop++;
            }
            Double[] ret = new Double[x.size()];
            ret = y.toArray(ret);
            double[] ret2 = new double[stop - start];
            for (int i = start; i < stop; i++) {
                ret2[i - start] = ret[i];
            }
            return ret2;
        }

    }

    public static class fullSpeed {

        public TimeSeries speed;
        public TimeSeries speed_knots;
        public TimeSeries power;
    }

    public class fullSpeed2 extends positionData {

        public class propulsorCommand {

            public propulsorCommand(double p, double r, double a, boolean isVSP) {
                pitch = p;
                rpm = r;
                angle = a;
                this.isVSP = isVSP;
            }
            public boolean isVSP = false;

            public double pitch = 0;
            public double rpm = 0;
            public double angle = 0;
        }
        private final HashMap<String, propulsorCommand> commanded = new HashMap<>();

        public void updateFromIntf(double T, SFHHullIntf hull, ArrayList<SFHActuatorPointIntf> intfList) {
            super.updateFromIntf(T, hull);
            for (SFHActuatorPointIntf i : intfList) {
                commanded.put(i.getName(), new propulsorCommand(i.getComandedPitch(), i.getCommandedRPM(), i.getCommandeAngle(), i.MaxPitch() < 1.01));
            }
        }

        public HashMap<String, propulsorCommand> getCommand() {
            return commanded;
        }
    }

    public static class positionData {

        public positionData() {
            b = 20;
            l = 100;

            theta = new TimeSeries();
            alfa = new TimeSeries();
            psi = new TimeSeries();
            n = new TimeSeries();
            e = new TimeSeries();
            d = new TimeSeries();

            u = new TimeSeries();
            v = new TimeSeries();
            r = new TimeSeries();
        }

        public void updateFromIntf(double T, SFHHullIntf hull) {

            n.append(T, hull.GetGlobalPosition()[0]);
            e.append(T, hull.GetGlobalPosition()[1]);
            d.append(T, hull.GetGlobalPosition()[2]);

            u.append(T, hull.GetLocalVelocity()[0]);
            v.append(T, hull.GetLocalVelocity()[1]);

            theta.append(T, hull.GetGlobalOrientation()[0]);
            alfa.append(T, hull.GetGlobalOrientation()[1]);
            psi.append(T, hull.GetGlobalOrientation()[2]);
            r.append(T, hull.GetLocalRotationVelocity()[2]);
        }

        public void writeStateToFile(PrintWriter out) {
            String line = String.format(Locale.US, "%e,%e,%e,%e,%e,%e,%e", n.getLastX(), n.getLastY(), e.getLastY(), d.getLastY(), theta.getLastY(), alfa.getLastY(), psi.getLastY());
            out.println(line);
        }

        public double b;
        public double l;
        public TimeSeries theta;
        public TimeSeries alfa;
        public TimeSeries psi;
        public TimeSeries n;
        public TimeSeries e;
        public TimeSeries d;

        public TimeSeries u;
        public TimeSeries v;
        public TimeSeries r;
    }

    public static class turingCircle extends positionData {

        public turingCircle() {
            super();
            delta = new TimeSeries();
        }

        public void updateFromIntf(double T, SFHHullIntf hull, SFHActuatorPointIntf rudder) {
            super.updateFromIntf(T, hull);
            delta.append(T, rudder.ActualAngle());
        }
        public TimeSeries delta;
    }

    public static class crashStop extends positionData {

        private int startIndex;

        public crashStop() {
            super();
            startIndex = -1;
        }

        public double getCrashStopDistance() {

            double s = psi.getY(startIndex);
            double v_n = Math.cos(s);
            double v_e = Math.sin(s);
            double dn = n.getLastY() - n.getY(startIndex);
            double de = e.getLastY() - e.getY(startIndex);

            return Math.abs(v_n * dn + v_e * de);
        }

        public double getCrashStopTime() {
            return n.getLastX() - n.getX(startIndex);
        }

        public int getStartIndex() {
            return startIndex;
        }

        public void setStartIndex() {
            this.startIndex = n.seriesLength();
        }
    }

    public static class thrusterResponse {

        public thrusterResponse() {
            thrust = new TimeSeries();
            power = new TimeSeries();
            rpm = new TimeSeries();
            pitch = new TimeSeries();
            angle = new TimeSeries();
        }

        public void updateFromIntf(double T, SFHActuatorPointIntf intf) {
            thrust.append(T, intf.ThrustFeedback());
            rpm.append(T, intf.ActualRPM());
            pitch.append(T, intf.ActualPitch());
            angle.append(T, intf.ActualAngle());
        }

        public TimeSeries thrust;
        public TimeSeries power;
        public TimeSeries rpm;
        public TimeSeries pitch;
        public TimeSeries angle;
    }

    public static class allThrusterResponse {

        public allThrusterResponse() {
            data = new HashMap<>();
        }
        public HashMap<String, thrusterResponse> data;
    }

    public abstract void runTrials();

    public abstract void configureShip();

    public abstract SFHHullData getHull();

    public abstract ArrayList<PropulsorActuatorDataPair> getPropulsors();

    public abstract ArrayList<ActorIntf> createActors();

    public abstract SFHHullIntf getHullIntf();

    public ArrayList<SFHOpusPropulsorIntf> getOpusPropulsors() {

        return null;
    }

    public abstract ArrayList<SFHActuatorPointIntf> getMainActuatorsIntf();

    public ArrayList<SFHActuatorPointIntf> getMainPropulsorsIntf() {

        return null;
    }

    ;
    
    public abstract ArrayList<SFHActuatorPointIntf> getMainRudderIntf();

    public abstract ArrayList<SFHActuatorPointIntf> getTransverseThrusterIntf();

    public abstract ArrayList<SFHActuatorPointIntf> getRotatableTrhusterIntf();

    public abstract void setTurningCircleCommand(double direction);

    public abstract void setCrashStopCommand();

    public abstract void setCruiseCommand();

    public abstract void setCruiseCommand(double precent);

    public abstract void setDPCrabCommand(double direction);

    public abstract void setDPPirCommand(double direction);

    public abstract void setAllThrusterCommand(double percent);

    public abstract void setAllThrusterCommand(double percent, boolean noAzi);

    //Run a pirouette with a rudder at max angle and its propeller max trhust
    public void setRudderPirCommand(double direction) {

        for (SFHActuatorPointIntf azi : getRotatableTrhusterIntf()) {
            azi.setComandedPitchNormalized(0);
            azi.setCommandedRPMNormalized(0);
            azi.setAvailablePower(0);
        }

        for (SFHActuatorPointIntf tunnel : getTransverseThrusterIntf()) {
            tunnel.setComandedPitchNormalized(0);
            tunnel.setCommandedRPMNormalized(0);
            tunnel.setAvailablePower(0);
        }

        ArrayList<SFHActuatorPointIntf> main = getMainActuatorsIntf();
        ArrayList<SFHActuatorPointIntf> rudders = getMainRudderIntf();
        if (rudders != null && rudders.size() == main.size() && rudders.size() == 2) {

            //TODO: IMPLEMENT DIRECTION
            main.get(0).setAvailablePower(11180 * 1e3);
            main.get(0).setComandedPitchNormalized(1);
            main.get(0).setCommandedRPMNormalized(1);
            rudders.get(0).setCommandeAngleNormalized(1);

            main.get(1).setAvailablePower(0);
            main.get(1).setComandedPitchNormalized(0);
            main.get(1).setCommandedRPMNormalized(0);
            rudders.get(1).setCommandeAngleNormalized(0);
        }

    }

    protected VeresFile.HydFile hyd = null;

    final public fullSpeed2 waveResponse(double height, double period, double direction) {
        FhDynamicModel.instance().setVisualization(false);
        FhDynamicModel.instance().setHMax(0.1);
        configureShip();
        double Lpp = hyd.Lpp;
        double cog_rel_waterline = hyd.Draught - hyd.VCG;

        SingleWave waveModel = new SingleWave();
        waveModel.setWaveAmplitude(height);
        waveModel.setWaveFrequency(2 * Math.PI / period);
        waveModel.setWaveDirection(direction);

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);
        SFHEnvironmentIntf _env = environment.create();

        //Attach mooring springs
        double springLength = Lpp * 10;
        double springStiffness = 40000;//This is chosen add max 1% to the heave stiffness for a typical ship.
        double[] mooringSternPort = new double[]{-(Lpp / 2.0 + springLength / Math.sqrt(2.0)), -springLength / Math.sqrt(2.0), cog_rel_waterline};
        double[] mooringSternStb = new double[]{-(Lpp / 2.0 + springLength / Math.sqrt(2.0)), +springLength / Math.sqrt(2.0), cog_rel_waterline};
        double[] mooringBowPort = new double[]{(Lpp / 2.0 + springLength / Math.sqrt(2.0)), -springLength / Math.sqrt(2.0), cog_rel_waterline};
        double[] mooringBowStb = new double[]{(Lpp / 2.0 + springLength / Math.sqrt(2.0)), +springLength / Math.sqrt(2.0), cog_rel_waterline};
        SFHSpringData springSternPort = new SFHSpringData(springStiffness, springLength, "springSternPort");
        SFHSpringData springSternStb = new SFHSpringData(springStiffness, springLength, "springSternStb");
        SFHSpringData springBowPort = new SFHSpringData(springStiffness, springLength, "springBowPort");
        SFHSpringData springBowStb = new SFHSpringData(springStiffness, springLength, "springBowStb");
        SFHReferencePointData conSpringSternPort = new SFHReferencePointData("conSpringSternPort", 0, 0, cog_rel_waterline);
        SFHReferencePointData conSpringSternStb = new SFHReferencePointData("conSpringSternStb", 0, 0, cog_rel_waterline);
        SFHReferencePointData conSpringBowPort = new SFHReferencePointData("conSpringBowPort", Lpp, 0, cog_rel_waterline);
        SFHReferencePointData conSpringBowStb = new SFHReferencePointData("conSpringBowStb", Lpp, 0, cog_rel_waterline);
        springSternPort.ConnectToReference(conSpringSternPort, SFHSpringData.End.A);
        springSternStb.ConnectToReference(conSpringSternStb, SFHSpringData.End.A);
        springBowPort.ConnectToReference(conSpringBowPort, SFHSpringData.End.A);
        springBowStb.ConnectToReference(conSpringBowStb, SFHSpringData.End.A);
        springSternPort.ConnectToFixedPoint(mooringSternPort, SFHSpringData.End.B);
        springSternStb.ConnectToFixedPoint(mooringSternStb, SFHSpringData.End.B);
        springBowPort.ConnectToFixedPoint(mooringBowPort, SFHSpringData.End.B);
        springBowStb.ConnectToFixedPoint(mooringBowStb, SFHSpringData.End.B);
        getHull().AddReferencePoint(conSpringSternPort);
        getHull().AddReferencePoint(conSpringSternStb);
        getHull().AddReferencePoint(conSpringBowPort);
        getHull().AddReferencePoint(conSpringBowStb);
        SFHSpringIntf _springSternPort = springSternPort.create();
        SFHSpringIntf _springSternStb = springSternStb.create();
        SFHSpringIntf _springBowPort = springBowPort.create();
        SFHSpringIntf _springBowStb = springBowStb.create();
        SFHReferencePointIntf _conSpringSternPort = conSpringSternPort.create();
        SFHReferencePointIntf _conSpringSternStb = conSpringSternStb.create();
        SFHReferencePointIntf _conSpringBowPort = conSpringBowPort.create();
        SFHReferencePointIntf _conSpringBowStb = conSpringBowStb.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);
        actorList.add(_conSpringSternPort);
        actorList.add(_conSpringSternStb);
        actorList.add(_conSpringBowPort);
        actorList.add(_conSpringBowStb);
        actorList.add(_springSternPort);
        actorList.add(_springSternStb);
        actorList.add(_springBowPort);
        actorList.add(_springBowStb);

        fullSpeed2 retval = new fullSpeed2();
        double dT = 0.1;
        FhDynamicModel.instance().setEndTime(60);
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            retval.updateFromIntf(FhDynamicModel.instance().getCurrentTime(), getHullIntf());
        }

        FhDynamicModel.reset();
        return retval;
    }

    final public fullSpeed runFullSpeedTrial() {
        FhDynamicModel.instance().setVisualization(false);
        configureShip();

        fullSpeed retval = new fullSpeed();

        retval.power = new TimeSeries();
        retval.speed = new TimeSeries();
        retval.speed_knots = new TimeSeries();

        for (int i = 0; i < 12; i++) {

            FlatSea waveModel = new FlatSea();

            FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());

            SFHEnvironmentData environment = new SFHEnvironmentData();

            environment.setInitialAmplitude(waveModel.getZeta_a());
            environment.setInitialFrequency(waveModel.getOmega());
            environment.setInitialNumber(waveModel.getWavenum());
            environment.setInitialDirection(waveModel.getPsi());
            environment.setInitialPhase(waveModel.getPhase());
            environment.setSeaDepth(80);

            SFHEnvironmentIntf _env = environment.create();
            ArrayList<ActorIntf> actorList = createActors();
            actorList.add(_env);

            waveModel.addListener(_env);

            if (i < 4) {
                FhDynamicModel.instance().setEndTime(200);
            } else if (i < 6) {
                FhDynamicModel.instance().setEndTime(400);
            } else {
                FhDynamicModel.instance().setEndTime(600);
            }

            setCruiseCommand((i + 1) / 12.0);

            double T = 0, dT = 0.1;
            while (!FhDynamicModel.instance().isFinished()) {
                for (ActorIntf a : actorList) {
                    a.tick(dT);
                }
                T += dT;
            }
            FhDynamicModel.reset();

            retval.speed.append(T, getHullIntf().GetLocalVelocity()[0]);
            retval.speed_knots.append(T, getHullIntf().GetLocalVelocity()[0] / 0.5144);

            double forcesum = 0;
            for (SFHActuatorPointIntf a : getMainActuatorsIntf()) {
                forcesum += a.getForceBody()[0];
            }
            retval.power.append(T, forcesum * getHullIntf().GetLocalVelocity()[0]);
        }
        return retval;
    }

    final public fullSpeed2 runDPcrab(double dir) {

        fullSpeed2 retval = new fullSpeed2();
        configureShip();

        SingleWave waveModel = new SingleWave();

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());
        //getHull().setSfh_test(true);
        //getHull().setOnly_sway(true);

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> tun = getTransverseThrusterIntf();
        ArrayList<SFHActuatorPointIntf> azi = getRotatableTrhusterIntf();
        ArrayList<SFHActuatorPointIntf> main = getMainActuatorsIntf();
        ArrayList<SFHActuatorPointIntf> rudder = getMainRudderIntf();

        for (SFHActuatorPointIntf i : main) {
            if (i.MaxAngle() == i.MinAngle()) {
                i.setAvailablePower(0);
            }
        }

        ArrayList<SFHActuatorPointIntf> propulsors = new ArrayList<SFHActuatorPointIntf>();
        propulsors.addAll(tun);
        propulsors.addAll(azi);

        setDPCrabCommand(dir);

        double dT = 0.1;
        FhDynamicModel.instance().setEndTime(5 * 60);
        double previousSway = -100;
        double change = 1000;
        double T = 0;
        while (!FhDynamicModel.instance().isFinished() && (change > 0.001)) {
            T += dT;
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            retval.updateFromIntf(FhDynamicModel.instance().getCurrentTime(), getHullIntf(), propulsors);
            change = getHullIntf().GetLocalVelocity()[1] == 0 ? change : Math.abs((previousSway - getHullIntf().GetLocalVelocity()[1]) / getHullIntf().GetLocalVelocity()[1]);
            previousSway = getHullIntf().GetLocalVelocity()[1];
        }

        getHull().setOnly_sway(false);
        getHull().setSfh_test(false);

        showVelocity(T);
        showThrusterForces();

        FhDynamicModel.reset();
        return retval;
    }

    public boolean show(double timeStep) {

        return show(1, timeStep);
    }

    public boolean show(double frequency, double timeStep) {

        return (count++) % (frequency / timeStep) == 0;
    }

    public void showConsummedPower(double T, double timeStep) {

        if (show(timeStep)) {
            StringBuilder sb = new StringBuilder();
            sb.append(String.format("%.1f", T));
            for (SFHOpusPropulsorIntf p : getOpusPropulsors()) {
                sb.append(String.format(" %.1f kW: ", p.getConsumedPower() / 1000));
            }

            System.out.println(sb.toString());
        }
    }

    public void showVelocity(double T) {
        System.out.println(String.format("End velocities after  %.1f", T));

        System.out.println(String.format("Terminal yaw velocity= %.1f deg/min ", getHullIntf().GetLocalRotationVelocity()[2] * 180.0 / Math.PI * 60.0));
        System.out.println(String.format("Terminal sway velocity= %.1f m/s %.1f knots ", getHullIntf().GetLocalVelocity()[1], getHullIntf().GetLocalVelocity()[1] / 0.5144));
        System.out.println(String.format("Terminal surge velocity= %.1f m/s %.1f knots", getHullIntf().GetLocalVelocity()[0], getHullIntf().GetLocalVelocity()[0] / 0.5144));
        System.out.println("");

    }

    public void showThrusterForces() {

        System.out.println("Thruster forces: ");

        ArrayList<SFHActuatorPointIntf> tun = getTransverseThrusterIntf();
        ArrayList<SFHActuatorPointIntf> azi = getRotatableTrhusterIntf();
        ArrayList<SFHActuatorPointIntf> main = getMainActuatorsIntf();
        ArrayList<SFHActuatorPointIntf> rudder = getMainRudderIntf();

        for (SFHActuatorPointIntf i : tun) {
            double f = i.ThrustFeedback();
            System.out.println(String.format("%s = %.1f kN", i.getName(), f / 1e3));
        }
        for (SFHActuatorPointIntf i : azi) {
            double f = i.ThrustFeedback();
            System.out.println(String.format("%s = %.1f kN", i.getName(), f / 1e3));
        }

        for (SFHActuatorPointIntf i : main) {
            double f = i.ThrustFeedback();
            System.out.println(String.format("%s = %.1f kN", i.getName(), f / 1e3));

        }

        if (rudder != null) {
            for (SFHActuatorPointIntf i : rudder) {
                double f = i.ThrustFeedback();
                System.out.println(String.format("%s = %.1f kN", i.getName(), f / 1e3));

            }
        }
        System.out.println("");
    }

    final public fullSpeed2 runDecay(double u, double v, double r) {
        fullSpeed2 retval = new fullSpeed2();
        configureShip();

        FlatSea waveModel = new FlatSea();

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        Element e = FhDynamicModel.instance().getInitializationElement();
        String velString = Double.toString(u) + "," + Double.toString(v) + ",0";
        String omgString = "0,0," + Double.toString(r);

        e.setAttribute(getHull().getName() + ".StateVelocity", velString);
        e.setAttribute(getHull().getName() + ".StateOmega", omgString);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> tun = getTransverseThrusterIntf();
        ArrayList<SFHActuatorPointIntf> azi = getRotatableTrhusterIntf();
        ArrayList<SFHActuatorPointIntf> main = getMainActuatorsIntf();

        ArrayList<SFHActuatorPointIntf> propulsors = new ArrayList<SFHActuatorPointIntf>();
        propulsors.addAll(tun);
        propulsors.addAll(azi);
        propulsors.addAll(main);

        for (SFHActuatorPointIntf i : propulsors) {
            i.setAvailablePower(0);
        }

        setAllThrusterCommand(0);

        boolean testSurge = false, testSway = false, testYaw = false;
        if (u > 0) {
            testSurge = true;
        }
        if (v > 0) {
            testSway = true;
        }
        if (r > 0) {
            testYaw = true;
        }
        double dT = 0.1, T = 0;
        FhDynamicModel.instance().setEndTime(10 * 60);
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            if (T > 5) {

                if (testSurge) {
                    if (getHullIntf().GetLocalVelocity()[0] < 0.05) {
                        break;
                    }
                }
                if (testSway) {
                    if (getHullIntf().GetLocalVelocity()[1] < 0.05) {
                        break;
                    }
                }

                if (testYaw) {
                    if (getHullIntf().GetLocalRotationVelocity()[2] < 0.01) {
                        break;
                    }
                }

            }
            retval.updateFromIntf(T, getHullIntf(), propulsors);
            T += dT;
        }
        System.out.println("decay time: " + T);
        FhDynamicModel.reset();
        return retval;
    }

    final public fullSpeed2 runDPpirouette() {
        fullSpeed2 retval = new fullSpeed2();
        //FhDynamicModel.instance().setVisualization(false);
        //FhDynamicModel.instance().setResultLog(true);
        configureShip();

//        ITTCTestWaveModel waveModel = new ITTCTestWaveModel();
        FlatSea waveModel = new FlatSea();
        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());
        //getHull().setSfh_test(true);
        //getHull().setOnly_yaw(true);

        SFHEnvironmentData environment = new SFHEnvironmentData();
        System.out.println("number of wave components " + waveModel.getNumberOfWaves());

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> tun = getTransverseThrusterIntf();
        ArrayList<SFHActuatorPointIntf> azi = getRotatableTrhusterIntf();
        ArrayList<SFHActuatorPointIntf> main = getMainActuatorsIntf();
        ArrayList<SFHActuatorPointIntf> rudders = getMainRudderIntf();

        for (SFHActuatorPointIntf i : main) {
            if (i.MaxAngle() == i.MinAngle()) {
                i.setAvailablePower(0);
            }
        }

        ArrayList<SFHActuatorPointIntf> propulsors = new ArrayList<SFHActuatorPointIntf>();
        propulsors.addAll(tun);
        propulsors.addAll(azi);

        setDPPirCommand(1);

        double dT = 0.1, T = 0;
        FhDynamicModel.instance().setEndTime(10 * 60);
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            retval.updateFromIntf(T, getHullIntf(), propulsors);
            T += dT;
        }

        getHull().setOnly_yaw(false);
        getHull().setSfh_test(false);

        showVelocity(T);
        showThrusterForces();

        FhDynamicModel.reset();
        return retval;
    }

    final public fullSpeed2 runRudderDPpirouette() {
        fullSpeed2 retval = new fullSpeed2();
        //FhDynamicModel.instance().setVisualization(false);
        //FhDynamicModel.instance().setResultLog(true);
        configureShip();

//        ITTCTestWaveModel waveModel = new ITTCTestWaveModel();
        FlatSea waveModel = new FlatSea();
        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());
        //getHull().setSfh_test(true);
        //getHull().setOnly_yaw(true);

        SFHEnvironmentData environment = new SFHEnvironmentData();
        System.out.println("number of wave components " + waveModel.getNumberOfWaves());

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> noUse = getTransverseThrusterIntf();
        noUse.addAll(getRotatableTrhusterIntf());

        for (SFHActuatorPointIntf a : noUse) {
            a.setAvailablePower(10);
        }

        ArrayList<SFHActuatorPointIntf> rudders = getMainRudderIntf();

        for (SFHActuatorPointIntf a : rudders) {

            a.setCommandeAngleNormalized(1);
        }
        ArrayList<SFHActuatorPointIntf> propellers = getMainActuatorsIntf();

        int count = 0;
        for (SFHActuatorPointIntf a : propellers) {
            if (count++ == 0) {

                a.setAvailablePower(11e6);
            } else {

                a.setAvailablePower(0);

            }
        }

        //  propellers.addAll(rudders);
        FhDynamicModel.instance().setEndTime(200);
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {

                a.tick(dT);

            }

            T += dT;

            count = 0;
            for (SFHActuatorPointIntf a : propellers) {
                if (count++ == 0) {
                    a.setComandedPitchNormalized(1.0);
                    a.setCommandedRPMNormalized(1.0);
                    a.setCommandeAngleNormalized(1);

                } else {
                    a.setComandedPitchNormalized(0.0);
                    a.setCommandedRPMNormalized(0.0);
                    a.setCommandeAngleNormalized(1);

                }
            }

            showConsummedPower(T, dT);
        }

        getHull().setOnly_yaw(false);
        getHull().setSfh_test(false);

        showVelocity(T);
        showThrusterForces();

        FhDynamicModel.reset();
        return retval;

    }

    final public positionData runPhaseTrial(int duration, double dT, double amplitude, double frequency, double dir, double phase, double cog_rel_waterline) {
        FhDynamicModel.instance().setVisualization(false);
        configureShip();
        positionData res = new positionData();
        SingleWave waveModel = new SingleWave();
        waveModel.setWaveAmplitude(amplitude);
        waveModel.setWaveFrequency(frequency);
        waveModel.setWaveDirection(dir);
        waveModel.setWavePhase(phase);

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());
        FhDynamicModel.instance().setHMax(dT);
        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(999999);

        getHull().setInitialNorthEastDown(new double[]{0, 0, cog_rel_waterline});
        getHull().setInitialRollPitchYaw(new double[]{0, 0, 0});
        SFHEnvironmentIntf _env = environment.create();
        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);
        waveModel.addListener(_env);

        FhDynamicModel.instance().setEndTime(duration);

        double T = 0;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            res.updateFromIntf(T, getHullIntf());
            T += dT;
        }
        FhDynamicModel.reset();

        return res;
    }

    final public positionData runCurrentTest(int duration, double dT, double currentDir, double currentVelocity) {
        FhDynamicModel.instance().setVisualization(false);
        configureShip();
        positionData res = new positionData();
        FlatSea waveModel = new FlatSea();
        double cog_rel_waterline = hyd.Draught - hyd.VCG;
        double dirRads = currentDir * Math.PI / 180.0;

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());
        FhDynamicModel.instance().setHMax(dT);
        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(999999);
        environment.setInitialCurrent(new double[]{0, 1, 0});
        getHull().setInitialNorthEastDown(new double[]{0, 0, cog_rel_waterline});
        getHull().setInitialRollPitchYaw(new double[]{0, 0, 0});
        SFHEnvironmentIntf _env = environment.create();
        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);
        waveModel.addListener(_env);

        FhDynamicModel.instance().setEndTime(duration);

        double T = 0;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            res.updateFromIntf(T, getHullIntf());
            _env.SetCurrentDirection(dirRads);
            _env.SetCurrentSpeed(currentVelocity);
            T += dT;
        }
        FhDynamicModel.reset();

        return res;
    }

    final public positionData runWindTest(int duration, double dT, double windDir, double windVelocity) {
        FhDynamicModel.instance().setVisualization(false);
        configureShip();
        positionData res = new positionData();
        FlatSea waveModel = new FlatSea();
        double cog_rel_waterline = hyd.Draught - hyd.VCG;
        double dirRads = windDir * Math.PI / 180.0;

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());
        FhDynamicModel.instance().setHMax(dT);
        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(999999);
        environment.setInitialCurrent(new double[]{0, 0, 0});
        getHull().setInitialNorthEastDown(new double[]{0, 0, cog_rel_waterline});
        getHull().setInitialRollPitchYaw(new double[]{0, 0, 0});
        SFHEnvironmentIntf _env = environment.create();
        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);
        waveModel.addListener(_env);

        FhDynamicModel.instance().setEndTime(duration);

        double T = 0;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            res.updateFromIntf(T, getHullIntf());
            _env.SetWindDirection(dirRads);
            _env.SetWindSpeed(windVelocity);
            T += dT;
        }
        FhDynamicModel.reset();

        return res;
    }

    final public positionData runWaveDriftTest(int duration, double dT, double dir, double amplitude, double period) {
        System.out.println(String.format("Run wave drift test for %d s direction %.1f degrees %.1f m amplitude and %.1f s period ", duration, dir, amplitude, period));
        FhDynamicModel.instance().setVisualization(false);
        configureShip();
        positionData res = new positionData();

        double cog_rel_waterline = hyd.Draught - hyd.VCG;
        double dirRads = dir * Math.PI / 180.0;
        double omega = 2 * Math.PI / period;

        SingleWave waveModel = new SingleWave();
        waveModel.setWaveAmplitude(amplitude);
        waveModel.setWaveFrequency(omega);
        waveModel.setWaveDirection(dirRads);

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());
        FhDynamicModel.instance().setHMax(dT);
        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(999999);

        environment.setInitialCurrent(new double[]{0, 0, 0});
        getHull().setInitialNorthEastDown(new double[]{0, 0, cog_rel_waterline});
        getHull().setInitialRollPitchYaw(new double[]{0, 0, 0});
        getHull().setVeresWaveDrift(true);

        SFHEnvironmentIntf _env = environment.create();
        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);
        waveModel.addListener(_env);

        FhDynamicModel.instance().setEndTime(duration);

        double T = 0;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            res.updateFromIntf(T, getHullIntf());
            T += dT;
        }
        FhDynamicModel.reset();

        return res;
    }

    final public allThrusterResponse runThrusterReponseTrial(double percent) {
        return runThrusterReponseTrial(percent, false);
    }

    final public allThrusterResponse runThrusterReponseTrial(double percent, boolean noAzi) {
        FhDynamicModel.instance().setVisualization(false);
        FhDynamicModel.instance().setResultLog(true);
        configureShip();

        FlatSea waveModel = new FlatSea();

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());
        getHull().setSfh_test(true);

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        allThrusterResponse retval = new allThrusterResponse();

        FhDynamicModel.instance().setEndTime(260);

        ArrayList<SFHActuatorPointIntf> main = getMainActuatorsIntf();
        ArrayList<SFHActuatorPointIntf> tun = getTransverseThrusterIntf();
        ArrayList<SFHActuatorPointIntf> azi = getRotatableTrhusterIntf();
        ArrayList<SFHActuatorPointIntf> rud = getMainRudderIntf();

        boolean has_rudders = false;

        for (SFHActuatorPointIntf i : main) {
            retval.data.put(i.getName(), new thrusterResponse());
        }
        for (SFHActuatorPointIntf i : tun) {
            retval.data.put(i.getName(), new thrusterResponse());
        }
        for (SFHActuatorPointIntf i : azi) {
            retval.data.put(i.getName(), new thrusterResponse());
        }
        for (SFHActuatorPointIntf i : rud) {
            if (!retval.data.containsKey(i) && !noAzi) {
                retval.data.put(i.getName(), new thrusterResponse());
                has_rudders = true;
            }
        }

        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }

            if (T < 100) {
                setAllThrusterCommand(0);
            } else {
                setAllThrusterCommand(percent, noAzi);
                if (has_rudders) {
                    for (SFHActuatorPointIntf i : rud) {
                        i.setCommandeAngleNormalized(percent);
                    }
                }
            }

            if (T > 70) {
                for (SFHActuatorPointIntf i : main) {
                    retval.data.get(i.getName()).updateFromIntf(T, i);
                }
                for (SFHActuatorPointIntf i : tun) {
                    retval.data.get(i.getName()).updateFromIntf(T, i);
                }
                for (SFHActuatorPointIntf i : azi) {
                    retval.data.get(i.getName()).updateFromIntf(T, i);
                }
                for (SFHActuatorPointIntf i : rud) {
                    if (has_rudders) {
                        retval.data.get(i.getName()).updateFromIntf(T, i);
                    }
                }
            }
            T += dT;
        }
        FhDynamicModel.reset();

        return retval;
    }

    public ArrayList<ArrayList<ArrayList<Double>>> runRAO(double dir, double frequency, double amplitude) {

        FhDynamicModel.instance().setHMax(0.1);
        FhDynamicModel.instance().setEndTime(1000);
        FhDynamicModel.instance().setVisualization(false);
        FhDynamicModel.instance().setResultLog(false);

        double Lpp = hyd.Lpp;
        double cog_rel_waterline = hyd.Draught - hyd.VCG;

        fullSpeed2 res = new fullSpeed2();

        SingleWave waveModel = new SingleWave();

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());
        SFHEnvironmentData environment = new SFHEnvironmentData();
        environment.setInitialAmplitude(new double[]{amplitude});
        environment.setInitialFrequency(new double[]{frequency});
        environment.setInitialNumber(new double[]{(frequency * frequency) / 9.81});
        environment.setInitialDirection(new double[]{dir});
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(999999);
        waveModel.setWaveAmplitude(amplitude);
        waveModel.setWaveDirection(dir);
        waveModel.setWaveFrequency(frequency);

        configureShip();
        getHull().setVeresWaveDrift(false);
        getHull().setInitialNorthEastDown(new double[]{0, 0, cog_rel_waterline});

        //Attach mooring springs
        double springLength = Lpp * 10;
        double springStiffness = 40000;//This is chosen add max 1% to the heave stiffness for a typical ship.
        double[] mooringSternPort = new double[]{-(Lpp / 2.0 + springLength / Math.sqrt(2.0)), -springLength / Math.sqrt(2.0), cog_rel_waterline};
        double[] mooringSternStb = new double[]{-(Lpp / 2.0 + springLength / Math.sqrt(2.0)), +springLength / Math.sqrt(2.0), cog_rel_waterline};
        double[] mooringBowPort = new double[]{(Lpp / 2.0 + springLength / Math.sqrt(2.0)), -springLength / Math.sqrt(2.0), cog_rel_waterline};
        double[] mooringBowStb = new double[]{(Lpp / 2.0 + springLength / Math.sqrt(2.0)), +springLength / Math.sqrt(2.0), cog_rel_waterline};
        SFHSpringData springSternPort = new SFHSpringData(springStiffness, springLength, "springSternPort");
        SFHSpringData springSternStb = new SFHSpringData(springStiffness, springLength, "springSternStb");
        SFHSpringData springBowPort = new SFHSpringData(springStiffness, springLength, "springBowPort");
        SFHSpringData springBowStb = new SFHSpringData(springStiffness, springLength, "springBowStb");
        SFHReferencePointData conSpringSternPort = new SFHReferencePointData("conSpringSternPort", 0, 0, cog_rel_waterline);
        SFHReferencePointData conSpringSternStb = new SFHReferencePointData("conSpringSternStb", 0, 0, cog_rel_waterline);
        SFHReferencePointData conSpringBowPort = new SFHReferencePointData("conSpringBowPort", Lpp, 0, cog_rel_waterline);
        SFHReferencePointData conSpringBowStb = new SFHReferencePointData("conSpringBowStb", Lpp, 0, cog_rel_waterline);
        springSternPort.ConnectToReference(conSpringSternPort, SFHSpringData.End.A);
        springSternStb.ConnectToReference(conSpringSternStb, SFHSpringData.End.A);
        springBowPort.ConnectToReference(conSpringBowPort, SFHSpringData.End.A);
        springBowStb.ConnectToReference(conSpringBowStb, SFHSpringData.End.A);
        springSternPort.ConnectToFixedPoint(mooringSternPort, SFHSpringData.End.B);
        springSternStb.ConnectToFixedPoint(mooringSternStb, SFHSpringData.End.B);
        springBowPort.ConnectToFixedPoint(mooringBowPort, SFHSpringData.End.B);
        springBowStb.ConnectToFixedPoint(mooringBowStb, SFHSpringData.End.B);
        getHull().AddReferencePoint(conSpringSternPort);
        getHull().AddReferencePoint(conSpringSternStb);
        getHull().AddReferencePoint(conSpringBowPort);
        getHull().AddReferencePoint(conSpringBowStb);
        SFHSpringIntf _springSternPort = springSternPort.create();
        SFHSpringIntf _springSternStb = springSternStb.create();
        SFHSpringIntf _springBowPort = springBowPort.create();
        SFHSpringIntf _springBowStb = springBowStb.create();
        SFHReferencePointIntf _conSpringSternPort = conSpringSternPort.create();
        SFHReferencePointIntf _conSpringSternStb = conSpringSternStb.create();
        SFHReferencePointIntf _conSpringBowPort = conSpringBowPort.create();
        SFHReferencePointIntf _conSpringBowStb = conSpringBowStb.create();

        SFHEnvironmentIntf _env = environment.create();
        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_conSpringSternPort);
        actorList.add(_conSpringSternStb);
        actorList.add(_conSpringBowPort);
        actorList.add(_conSpringBowStb);
        actorList.add(_springSternPort);
        actorList.add(_springSternStb);
        actorList.add(_springBowPort);
        actorList.add(_springBowStb);
        actorList.add(_env);
        waveModel.addListener(_env);

        try (PrintWriter out = new PrintWriter(new FileWriter(String.format("RAOTrialSpring%2.1f.csv", amplitude), false))) {

            double T = 0, dT = 0.1;
            while (!FhDynamicModel.instance().isFinished()) {
                for (ActorIntf a : actorList) {
                    a.tick(dT);
                }
                if (T > 700) {//Filter out any transient effects
                    res.updateFromIntf(T, getHullIntf());
                    //res.writeStateToFile(out);
                }
                T += dT;
            }
        } catch (Exception e) {
        }
        FhDynamicModel.reset();

        ArrayList<ArrayList<ArrayList<Double>>> ret = new ArrayList<ArrayList<ArrayList<Double>>>();
        ArrayList<ArrayList<Double>> n = res.n.getExtrema();
        ret.add(n);
        ArrayList<ArrayList<Double>> e = res.e.getExtrema();
        ret.add(e);
        ArrayList<ArrayList<Double>> d = res.d.getExtrema();
        ret.add(d);
        ArrayList<ArrayList<Double>> t = res.theta.getExtrema();
        ret.add(t);
        ArrayList<ArrayList<Double>> a = res.alfa.getExtrema();
        ret.add(a);
        ArrayList<ArrayList<Double>> q = res.psi.getExtrema();
        ret.add(q);
        return ret;
    }

    public double[] calculateRAO(ArrayList<ArrayList<ArrayList<Double>>> peaks) {
        double[] ret = new double[6];

        ArrayList<ArrayList<Double>> n = peaks.get(0);
        ArrayList<ArrayList<Double>> e = peaks.get(1);
        ArrayList<ArrayList<Double>> d = peaks.get(2);
        ArrayList<ArrayList<Double>> t = peaks.get(3);
        ArrayList<ArrayList<Double>> a = peaks.get(4);
        ArrayList<ArrayList<Double>> q = peaks.get(5);

        ret[0] = 0;
        double norm = 0;
        for (int i = 0; i < n.get(1).size() / 2; i += 2) {
            norm += 1.0;
            ret[0] += (n.get(1).get(i) - n.get(1).get(i + 1)) / 2;
        }
        if (norm > 0) {
            ret[0] /= norm;
        }
        ret[0] = Math.abs(ret[0]);

        ret[1] = 0;
        norm = 0;
        for (int i = 0; i < e.get(1).size() / 2; i += 2) {
            norm += 1.0;
            ret[1] += (e.get(1).get(i) - e.get(1).get(i + 1)) / 2;
        }
        ret[1] /= norm;
        ret[1] = Math.abs(ret[1]);

        ret[2] = 0;
        norm = 0;
        for (int i = 0; i < d.get(1).size() / 2; i += 2) {
            norm += 1.0;
            ret[2] += ((d.get(1).get(i)) - (d.get(1).get(i + 1))) / 2;
        }
        if (norm > 0) {
            ret[2] /= norm;
        }
        ret[2] = Math.abs(ret[2]);

        ret[3] = 0;
        norm = 0;
        for (int i = 0; i < t.get(1).size() / 2; i += 2) {
            norm += 1.0;
            ret[3] += (t.get(1).get(i) - t.get(1).get(i + 1)) / 2;
        }
        ret[3] /= norm;
        ret[3] = Math.abs(ret[3]) * 180.0 / Math.PI;

        ret[4] = 0;
        norm = 0;
        for (int i = 0; i < a.get(1).size() / 2; i += 2) {
            norm += 1.0;
            ret[4] += (a.get(1).get(i) - a.get(1).get(i + 1)) / 2;
        }
        ret[4] /= norm;
        ret[4] = Math.abs(ret[4]) * 180.0 / Math.PI;

        ret[5] = 0;
        norm = 0;
        for (int i = 0; i < q.get(1).size() / 2; i += 2) {
            norm += 1.0;
            ret[5] += (q.get(1).get(i) - q.get(1).get(i + 1)) / 2;
        }
        ret[5] /= norm;
        ret[5] = Math.abs(ret[5]) * 180.0 / Math.PI;

        return ret;
    }

    // Calculate the phase lag of the motion relative to the waves in degrees [-180,180].
    public double[] calculatePhase(ArrayList<ArrayList<ArrayList<Double>>> peaks, double omega) {
        double ret[] = new double[6];
        double sum[] = new double[]{0, 0, 0, 0, 0, 0};
        double tMotionPeak, tWavePeak, phaseDiff;
        int n;
        int nSamples[] = new int[]{0, 0, 0, 0, 0, 0};
        for (int dir = 0; dir < 6; dir++) {
            for (int i = 1; i < peaks.get(dir).get(0).size() - 1; i++) {
                if (peaks.get(dir).get(1).get(i) > peaks.get(dir).get(1).get(i - 1)
                        && peaks.get(dir).get(1).get(i) > peaks.get(dir).get(1).get(i + 1)) {
                    tMotionPeak = peaks.get(dir).get(0).get(i);
                    n = (int) Math.floor(omega * tMotionPeak / (2 * Math.PI) - 3.0 / 4.0);
                    tWavePeak = (2 * Math.PI / omega) * (n + 3.0 / 4.0);
                    phaseDiff = (tMotionPeak - tWavePeak) * omega * (180.0 / Math.PI);
                    nSamples[dir]++;
                    sum[dir] += phaseDiff;
                }
            }
        }

        for (int i = 0; i < 6; i++) {
            ret[i] = sum[i] / nSamples[i];
            if (ret[i] > 180) {
                ret[i] = ret[i] - 360;
            }
        }
        return ret;
    }

    public positionData runStabilityCase(int duration, double samplingTime, double cog_rel_waterline) {

        positionData res = new positionData();
        configureShip();
        FhDynamicModel.instance().setVisualization(false);

        ITTCTestWaveModel waveModel = new ITTCTestWaveModel();

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());
        SFHEnvironmentData environment = new SFHEnvironmentData();
        FhDynamicModel.instance().setHMax(2);

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        getHull().setInitialNorthEastDown(new double[]{0, 0, cog_rel_waterline});
        getHull().setInitialRollPitchYaw(new double[]{0, 0, Math.PI / 4});

        SFHEnvironmentIntf _env = environment.create();
        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);
        setCruiseCommand(1.0);
        waveModel.addListener(_env);

        FhDynamicModel.instance().setEndTime(duration);
        double progress = 0;

        try (FileWriter fw = new FileWriter("states.csv");
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {

            double T = 0, dT = samplingTime;
            while (!FhDynamicModel.instance().isFinished()) {
                for (ActorIntf a : actorList) {
                    try {
                        a.tick(dT);
                    } catch (RuntimeException ex) {
                        System.out.println("CRASH!\n");
                        return res;
                    }
                }
                res.updateFromIntf(T, getHullIntf());
                res.writeStateToFile(out);
                T += dT;
                progress = 100 * T / ((double) duration);
                System.out.println(String.format("%4.2f%% \n", progress));
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FhDynamicModel.reset();

        return res;
    }

    public fullSpeed2 runHeelDecay() {

        fullSpeed2 res = new fullSpeed2();
        configureShip();

        FlatSea waveModel = new FlatSea();

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        getHull().setInitialRollPitchYaw(new double[]{getReportSpec().getHeelDecayStartAngleDegrees() * Math.PI / 180.0, 0, 0});

        SFHEnvironmentIntf _env = environment.create();
        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        double T = 0, dT = 0.01;
        FhDynamicModel.instance().setEndTime(getReportSpec().getHeelDecayEndTimeSeconds());
        FhDynamicModel.instance().setStepSize(dT);

        ArrayList<Double> heel_history = new ArrayList<Double>();
        ArrayList<Double> time_history = new ArrayList<Double>();;

        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            res.updateFromIntf(T, getHullIntf());
            T += dT;
        }
        FhDynamicModel.reset();

        /*ArrayList<Double> extrema_v = new ArrayList<Double>();
        ArrayList<Double> extrema_t = new ArrayList<Double>();
        
        extrema_t.add(res.theta.x.get(0));
        extrema_v.add(res.theta.y.get(0));
        for( int i=1; i < (res.theta.x.size()-1); i++){
                
            double val = res.theta.y.get(i);
            double next = res.theta.y.get(i+1);
            double last = res.theta.y.get(i-1);

            if( Math.abs(val)>Math.abs(next) && Math.abs(val)>Math.abs(last) ){
                extrema_t.add(res.theta.x.get(i));
                extrema_v.add(res.theta.y.get(i));
            }
        }
        
        for(int i=0; i < extrema_t.size(); i++){
            System.out.println("Extrema: " + extrema_v.get(i)*180.0/Math.PI + " At: " + extrema_t.get(i));
        }
        for(int i=0; i < extrema_t.size()-1; i++){
            
            double decrement = Math.log( Math.abs(extrema_v.get(i))/Math.abs(extrema_v.get(i+1)))/(extrema_t.get(i+1)-extrema_t.get(i) );
            System.out.println("Decrement " +i + " = " + decrement);
        }*/
        return res;
    }

    /**
     * Runs a heel test by apply a load at an arm length and return the
     * corresponding heel angle
     *
     * @param load load in kg
     * @param arm arm in m
     * @return heel angle in radians and velocity vector in 6D
     */
    public double[] runHeel(double load, double arm) {

        configureShip();

        FlatSea waveModel = new FlatSea();

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());
        FhDynamicModel.instance().setIntegrationMethod(0);
        FhDynamicModel.instance().setStepSize(0.01);

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHReferencePointData heel_force = new SFHActuatorPointData("heel_force", 0, arm, 0, SFHReferencePointIntf.Reference.CG);
        heel_force.setForceFrom(SFHReferencePointIntf.ForceSource.External);
        heel_force.setIsForceCalculator(true, SFHReferencePointIntf.ForceSource.External);

        getHull().AddReferencePoint(heel_force, true);

        SFHReferencePointIntf _heel = heel_force.create();
        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);
        actorList.add(_heel);

        waveModel.addListener(_env);

        FhDynamicModel.instance().setEndTime(1200);
        _heel.setExternalForceInput(new double[]{0, 0, 0});
        double[] force = new double[]{0, 0, load};

        double T = 0, dT = 0.01;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            if (T > 60) {
                _heel.setExternalForceInput(force);
            }
            T += dT;
        }
        double heel = getHullIntf().GetGlobalOrientation()[0];
        FhDynamicModel.reset();

        double[] heelTestResult = new double[7];
        heelTestResult[0] = heel;
        heelTestResult[1] = getHullIntf().GetGlobalVelocity()[0];
        heelTestResult[2] = getHullIntf().GetGlobalVelocity()[1];
        heelTestResult[3] = getHullIntf().GetGlobalVelocity()[2];
        heelTestResult[4] = getHullIntf().GetGlobalRotationVelocity()[0];
        heelTestResult[5] = getHullIntf().GetGlobalRotationVelocity()[1];
        heelTestResult[6] = getHullIntf().GetGlobalRotationVelocity()[2];

        return heelTestResult;
    }

    public crashStop runCrashStop(double startSpeed) {
        crashStop retval = new crashStop();

        //FhDynamicModel.instance().setVisualization(false);
        configureShip();

        FlatSea waveModel = new FlatSea();

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        getHull().setInitialSurgeVelocity(startSpeed);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> rudders = getMainRudderIntf();
        ArrayList<SFHActuatorPointIntf> propellers = getMainActuatorsIntf();

        for (SFHActuatorPointIntf a : propellers) {
            a.setComandedPitchNormalized(0.85);
            a.setCommandedRPMNormalized(0.85);
        }

        FhDynamicModel.instance().setEndTime(50 * 60 + 1);

        boolean trigger = true;
        double[] posStart = null, posEnd = null;
        int counter = 0;
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }

            if (T > 60) {
                setCrashStopCommand();
                if (trigger) {
                    posStart = getHullIntf().GetGlobalPosition();
                    trigger = false;
                    retval.setStartIndex();
                }

                if (getHullIntf().GetLocalVelocity()[0] < 0) {
                    posEnd = getHullIntf().GetGlobalPosition();
                    break;
                }
            }

            int index = (int) (Math.floor(T));

            if (index > counter) {
                retval.updateFromIntf(T, getHullIntf());
                counter++;
            }

            T += dT;
        }
        if (posEnd != null && posStart != null) {
            System.out.println("Crash stop distance: " + Double.toString(Math.sqrt(Math.pow(posEnd[0] - posStart[0], 2.0) + Math.pow(posEnd[1] - posStart[1], 2.0))));
        }

        FhDynamicModel.reset();
        return retval;
    }

    public fullSpeed2 runMaxSpeed() {
        fullSpeed2 retval = new fullSpeed2();

        //FhDynamicModel.instance().setVisualization(false);
        configureShip();

        FlatSea waveModel = new FlatSea();

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        ArrayList<SFHActuatorPointIntf> propulsors = getMainActuatorsIntf();

        waveModel.addListener(_env);

        setCruiseCommand(1.0);
        FhDynamicModel.instance().setEndTime(15 * 60 + 1);
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            retval.updateFromIntf(T, getHullIntf(), propulsors);
            T += dT;

        }

        FhDynamicModel.reset();
        return retval;
    }

    public fullSpeed2 runMaxSpeedReverse() {
        fullSpeed2 retval = new fullSpeed2();

        //FhDynamicModel.instance().setVisualization(false);
        configureShip();

        FlatSea waveModel = new FlatSea();

        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        getHull().setInitialSurgeVelocity(0);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> propulsors = getMainActuatorsIntf();
        propulsors.addAll(getRotatableTrhusterIntf());

        setCrashStopCommand();
        FhDynamicModel.instance().setEndTime(5 * 60 + 1);
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            retval.updateFromIntf(T, getHullIntf(), propulsors);
            T += dT;
        }

        FhDynamicModel.reset();
        return retval;
    }

    final public turingCircle runPortTurningCirlceTrial() {

        return runTuriningTrial(14 * 0.5144, -1);
    }

    final public turingCircle runStarboardTurningCirlceTrial() {
        return runTuriningTrial(14 * 0.5144, 1);
    }

    final public turingCircle runPortAccelereatedTurningTrial() {
        return runTuriningTrial(0, -1);
    }

    final public turingCircle runStarboardAccelereatedTurningTrial() {
        return runTuriningTrial(0, 1);
    }

    public turingCircle runTuriningTrial(double startSpeed, int direction) {
        FhDynamicModel.instance().setVisualization(false);
        configureShip();

        turingCircle retval = new turingCircle();

        FlatSea waveModel = new FlatSea();
        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        getHull().setInitialSurgeVelocity(startSpeed);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        ArrayList<SFHActuatorPointIntf> rudders = getMainRudderIntf();
        ArrayList<SFHActuatorPointIntf> propellers = getMainActuatorsIntf();

        for (SFHActuatorPointIntf a : propellers) {
            a.setComandedPitchNormalized(1.0);
            a.setCommandedRPMNormalized(1.0);
        }

        FhDynamicModel.instance().setEndTime(500);

        double startTime = 60;
        if (startSpeed < 0.1) {
            startTime = 0;
            setCruiseCommand(0.5);
        }

        int counter = 0;
        double T = 0, dT = 0.1;
        while (!FhDynamicModel.instance().isFinished()) {
            for (ActorIntf a : actorList) {
                a.tick(dT);
            }

            if (T > startTime) {
                setTurningCircleCommand(direction);
            } else {
                for (SFHActuatorPointIntf a : rudders) {
                    a.setCommandeAngle((2 * getHullIntf().GetGlobalOrientation()[2] + getHullIntf().GetLocalRotationVelocity()[2]) * 180 / Math.PI);
                }
            }

            int index = (int) (Math.floor(T));

            if (index > counter) {
                retval.updateFromIntf(T, getHullIntf(), getMainRudderIntf().get(0));
                counter++;
            }

            T += dT;
        }
        FhDynamicModel.reset();

        return retval;
    }

    public ArrayList<thrusterResponse> thrusterTest(double start_cmd, double stop_cmd) {
        FhDynamicModel.instance().setVisualization(false);
        configureShip();
        getHull().setSfh_test(true);

        FlatSea waveModel = new FlatSea();
        FhDynamicModel.instance().setNumberOfWavesInFhSim(waveModel.getNumberOfWaves());

        SFHEnvironmentData environment = new SFHEnvironmentData();

        environment.setInitialAmplitude(waveModel.getZeta_a());
        environment.setInitialFrequency(waveModel.getOmega());
        environment.setInitialNumber(waveModel.getWavenum());
        environment.setInitialDirection(waveModel.getPsi());
        environment.setInitialPhase(waveModel.getPhase());
        environment.setSeaDepth(80);

        SFHEnvironmentIntf _env = environment.create();

        ArrayList<ActorIntf> actorList = createActors();
        actorList.add(_env);

        waveModel.addListener(_env);

        double pitchCommand = 1;
        double angleCommand = 0.0;
        double rpmCommand = 1;

        double T = 0, dT = 0.1;
        FhDynamicModel.instance().setEndTime(250);
        while (!FhDynamicModel.instance().isFinished()) {

            if (T > 150 && T < 150.11) {
                pitchCommand = -1.0;
                angleCommand = 1.0;
                rpmCommand = -1.0;

                for (ActorIntf a : actorList) {
                    if (a instanceof SFHActuatorPointIntf) {
                        double[] Thrust = ((SFHActuatorPointIntf) a).getForceBody();
                        String name = ((SFHActuatorPointIntf) a).getName();
                        double t = Math.sqrt(Thrust[0] * Thrust[0] + Thrust[1] * Thrust[1] + Thrust[2] * Thrust[2]);

                        System.out.println("Name: " + name + " thrust: " + Double.toString(t / 1000.0) + " kN");
                    }
                }

            }
            for (ActorIntf a : actorList) {
                if (a instanceof SFHActuatorPointIntf) {
                    ((SFHActuatorPointIntf) a).setComandedPitchNormalized(pitchCommand);
                    ((SFHActuatorPointIntf) a).setCommandeAngleNormalized(angleCommand);
                    ((SFHActuatorPointIntf) a).setCommandedRPMNormalized(rpmCommand);
                }
            }

            for (ActorIntf a : actorList) {
                a.tick(dT);
            }
            T += dT;
        }
        for (ActorIntf a : actorList) {
            if (a instanceof SFHActuatorPointIntf) {
                double[] Thrust = ((SFHActuatorPointIntf) a).getForceBody();
                String name = ((SFHActuatorPointIntf) a).getName();
                double t = Math.sqrt(Thrust[0] * Thrust[0] + Thrust[1] * Thrust[1] + Thrust[2] * Thrust[2]);

                System.out.println("Name: " + name + " thrust: " + Double.toString(t / 1000.0) + " kN");
            }
        }

        return null;
    }

    public void execute() {
        configureShip();
        runTrials();
    }

    public void recreateFromSavedModel(FhDynamicModel.SavedModel model) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
