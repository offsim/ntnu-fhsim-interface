/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.ship.vesselExamples;

/**
 * Utility functions for generating reports containing VERES data
 *
 * @author Karl Gunnar Aarsæther
 */
public class VERESFileUtil {

//    /**
//     * REpresentation of the contents of a VERES hyd file (hydrostatic data)
//     */
//    public static final class HydFile {
//
//        /**
//         * Length between perpendiculars
//         */
//        public double Lpp;
//        /**
//         * Vessel breadth
//         */
//        public double Breadth;
//        /**
//         * Draft at midship
//         */
//        public double Draught;
//        /**
//         * Sinkage
//         */
//        public double Sinkage;
//        /**
//         * Trim angle (positive aft)
//         */
//        public double Trim;
//        /**
//         * Displacement in KG
//         */
//        public double Displacement;
//        /**
//         * Distance from keel to buoancy center
//         */
//        public double KB;
//        /**
//         * Distance from keel to center of gravity
//         */
//        public double VCG;
//        /**
//         * Distance from aft perpendicular to buoancy center
//         */
//        public double LCB;
//        /**
//         * Distance from aft perpendicular to center of gravity
//         */
//        public double LCG;
//        /**
//         * Block coefficient
//         */
//        public double Cb;
//        /**
//         * Waterline coefficient
//         */
//        public double Cw;
//        /**
//         * Prismatic coefficient
//         */
//        public double Cp;
//        /**
//         * Center section coefficient
//         */
//        public double Cm;
//        /**
//         * Metacentric height in longship direction
//         */
//        public double GMl;
//        /**
//         * Metacentric height in transverse direction
//         */
//        public double GMt;
//        /**
//         * Roll radius of gyration
//         */
//        public double r44;
//        /**
//         * Pitch radius of gyration
//         */
//        public double r55;
//        /**
//         * Yaw radius of gyration
//         */
//        public double r66;
//        /**
//         * Roll-yaw coupled motion gyration radius
//         */
//        public double r46;
//    }

//    /**
//     * Class representing a VERES Mgf (geometry) file
//     */
//    public static final class MgfFile {
//
//        /**
//         * Class representing a single ship section
//         */
//        public static final class Section {
//
//            public double x;    ///< Location of section relative to aft perpendicular
//            public double[] y; ///< Point offsets (from centerline)
//            public double[] z; ///< Point offsets (from baseline)
//        }
//        public ArrayList<Section> hull;
//
//        /**
//         *
//         * @param cgXrelAP x position of the
//         * @param Lpp
//         * @return
//         */
//        public double transformFhsimCGXtoOSCModelOrigo(double cgXrelAP, double Lpp) {
//
//            double sappo = getSAPPO(Lpp);
//            double loa = getLAO();
//            return (sappo + cgXrelAP) - loa / 2;
//        }
//
//        /**
//         *
//         *
//         */
//        public void showZMinMax() {
//
//            double max = Double.NEGATIVE_INFINITY;
//
//            if (hull != null) {
//
//                for (int section = 0; section < hull.size(); section++) {
//
//                    for (int i = 0; i < hull.get(section).z.length; i++) {
//
//                        if (max < hull.get(section).z[i]) {
//                            max = hull.get(section).z[i];
//
//                        }
//                    }
//                }
//
//                double min = Double.POSITIVE_INFINITY;
//
//                if (hull != null) {
//
//                    for (int section = 0; section < hull.size(); section++) {
//                        for (int i = 0; i < hull.get(section).z.length; i++) {
//
//                            if (min > hull.get(section).z[i]) {
//                                min = hull.get(section).z[i];
//
//                            }
//                        }
//
//                    }
//                }
//                System.out.println("z min " + min + " z max  + " + max);
//
//            }
//        }
//
//        /**
//         *
//         * @return Length overall in x axis in m
//         */
//        public double getLAO() {
//
//            double max = Double.NEGATIVE_INFINITY;
//
//            if (hull != null) {
//
//                for (int section = 0; section < hull.size(); section++) {
//
//                    if (max < hull.get(section).x) {
//                        max = hull.get(section).x;
//
//                    }
//                }
//            }
//
//            double min = Double.POSITIVE_INFINITY;
//
//            if (hull != null) {
//
//                for (int section = 0; section < hull.size(); section++) {
//
//                    if (min > hull.get(section).x) {
//                        min = hull.get(section).x;
//
//                    }
//                }
//            }
//
//            return max - min;
//
//        }
//
//        /**
//         *
//         * Stern to Aft perpendicular offset
//         *
//         * @return offset between Stern and Aft Perpendicular in x axis in m
//         */
//        public double getSAPPO(double Lpp) {
//            /*
//                From Veres Manual:
//                The vessel descripton is given in a local coordinate system to preserve compatbility with previous versions of
//                the program. The user is provided with a certain degree of freedom in choosing the vertical position of this
//                local coordinate system. The origin of the local coordinate system is located at Lpp/2. The z-axis is pointing
//                upwards, and the x-axis is pointing towards the stern, and is parallel to the baseline. The vertical position of
//                the origin may be taken arbitrarily, and its posi􀆟on relative to the base line may be specified manually in the
//                user interface. To enhance flexibility, the user may also specify sinkage and trim relative to the waterline given
//                by the vessel draught. A positive trim angle implies that the draught is increased at the stern and reduced at
//                the bow. Further, the sinkage and trim are specified rela􀆟ve to the local coordinate system, at Lpp/2.
//             */
//
//            double max = Double.NEGATIVE_INFINITY;
//            double sappo = 0;
//
//            if (hull != null) {
//
//                for (int section = 0; section < hull.size(); section++) {
//
//                    if (max < hull.get(section).x) {
//                        max = hull.get(section).x;
//
//                    }
//                }
//
//                sappo = Math.abs(max) - Lpp / 2;
//            }
//
//            return sappo;
//
//        }
//    }


    /**
     * Evaluate an array of coefficients as a fourier series
     *
     * @param d array of coefficients of size 2*N+1, 0th element is dc component
     * the next N elements are the cosine components and the following N the
     * sine components
     * @param _b the angle of the series
     * @return fourier series sum
     */
    public static double eval(double[] d, double _b) {
        double retval = d[0];
        for (int j = 1; j < ((d.length - 1) / 2 + 1); j++) {
            retval += d[j] * Math.cos(j * _b);
        }
        for (int j = ((d.length - 1) / 2 + 1); j < d.length; j++) {
            retval += d[j] * Math.sin((j - (d.length - 1) / 2) * _b);
        }
        return retval;
    }

}
