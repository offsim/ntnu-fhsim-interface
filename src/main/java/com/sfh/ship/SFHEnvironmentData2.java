/*
 * Copyright SINTEF FIsheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

import java.util.ArrayList;

/**
 * Parameters and initial values for SimBa compliant environment object
 * 
 * @sa SFHEnvironmentIntf2
 * 
 * @author karl.gunnar.aarsather@sintef.no
 */
public class SFHEnvironmentData2 extends SFHEnvironmentData{
    
    /**
     * Find an instance if SFHEnvironmentData2 in the Object list "list" which matches
     * the name "Name", cast the object to SFHEnvironmentData2  and return. 
     * 
     * Used to recreate a model for a collection of Object references really pointing to
     * configuration data classes
     * @param Name The name of the object
     * @param list list of Object references to configuration data classes
     * @return an instance of SFHEnvironmentData2 with name=Name in the list, null otherwise
     */
    public static SFHEnvironmentData2 findByNameInObjectList(String Name, ArrayList<Object> list){
        for( Object o: list){
            if( o instanceof SFHEnvironmentData2)
                if( ((SFHEnvironmentData2)o).getName().equals(Name)){
                    return (SFHEnvironmentData2)o;
                }            
        }
        return null;
    }
    
    /**
     * Initial North/East position for interpolation point A
     */
    private double [] positionA = { 1, 1};
    
    /**
     * Initial North/East position for interpolation point B
     */
    private double [] positionB = {-1,-1};
    
    /**
     * Initial shallow and deep fluid densities for interpolation point A
     */
    private double [] rhoA = {1025,1025};
    /**
     * Initial shallow and deep fluid densities for interpolation point B
     */
    private double [] rhoB = {1025,1025};
    
    /**
     * Current vector in shallow region in point A
     */
    private double [] currentShallowA = {0,0,0};
    /**
     * Current vector in shallow region in point B
     */
    private double [] currentShallowB = {0,0,0};
    
    /**
     * Current vector in deep region in point A
     */
    private double [] currentDeepA = {0,0,0};

    /**
     * Current vector in deep region in point A
     */
    private double [] currentDeepB = {0,0,0};

    /**
     * Vertical height of shallow current/density region in point A
     */
    private double depthShallowA=10;
    /**
     * Vertical height of shallow current/density region in point B
     */
    private double depthShallowB=10;

    /**
     * Inverse of transition length between current/density regions in point A
     */
    private double transitionLayerA=1;
    /**
     * Inverse of transition length between current/density regions in point B
     */
    private double transitionLayerB=1;

    
    /**
     * Initial North/East position for interpolation point A
     * @return the positionA
     */
    public double[] getPositionA() {
        return positionA.clone();
    }

    /**
     * Initial North/East position for interpolation point A
     * @param positionA the new north/east position
     */
    public void setPositionA(double[] positionA) {
        this.positionA = positionA.clone();
    }

    /**
     * Initial North/East position for interpolation point B
     * @return the north east position
     */
    public double[] getPositionB() {
        return positionB.clone();
    }

    /**
     * Initial North/East position for interpolation point B
     * @param positionB the new north east position
     */
    public void setPositionB(double[] positionB) {
        this.positionB = positionB.clone();
    }

    /**
     * Initial shallow and deep fluid densities for interpolation point A
     * @return the rhoA
     */
    public double[] getRhoA() {
        return rhoA.clone();
    }

    /**
     * Initial shallow and deep fluid densities for interpolation point A
     * @param rhoA the rhoA to set
     */
    public void setRhoA(double[] rhoA) {
        this.rhoA = rhoA.clone();
    }

    /**
     * Initial shallow and deep fluid densities for interpolation point B
     * @return the rhoB
     */
    public double[] getRhoB() {
        return rhoB.clone();
    }

    /**
     * Initial shallow and deep fluid densities for interpolation point B
     * @param rhoB the rhoB to set
     */
    public void setRhoB(double[] rhoB) {
        this.rhoB = rhoB.clone();
    }

    /**
     * Current vector in shallow region in point A
     * @return the currentShallowA
     */
    public double[] getCurrentShallowA() {
        return currentShallowA.clone();
    }

    /**
     * Current vector in shallow region in point A
     * @param currentShallowA the currentShallowA to set
     */
    public void setCurrentShallowA(double[] currentShallowA) {
        this.currentShallowA = currentShallowA.clone();
    }

    /**
     * Current vector in shallow region in point B
     * @return the currentShallowB
     */
    public double[] getCurrentShallowB() {
        return currentShallowB.clone();
    }

    /**
     * Current vector in shallow region in point B
     * @param currentShallowB the currentShallowB to set
     */
    public void setCurrentShallowB(double[] currentShallowB) {
        this.currentShallowB = currentShallowB.clone();
    }

    /**
     * Current vector in deep region in point A
     * @return the currentDeepA
     */
    public double[] getCurrentDeepA() {
        return currentDeepA.clone();
    }

    /**
     * Current vector in deep region in point A
     * @param currentDeepA the currentDeepA to set
     */
    public void setCurrentDeepA(double[] currentDeepA) {
        this.currentDeepA = currentDeepA.clone();
    }

    /**
     * Current vector in deep region in point A
     * @return the currentDeepB
     */
    public double[] getCurrentDeepB() {
        return currentDeepB.clone();
    }

    /**
     * Current vector in deep region in point A
     * @param currentDeepB the currentDeepB to set
     */
    public void setCurrentDeepB(double[] currentDeepB) {
        this.currentDeepB = currentDeepB.clone();
    }

    /**
     * Vertical height of shallow current/density region in point A
     * @return the depthShallowA
     */
    public double getDepthShallowA() {
        return depthShallowA;
    }

    /**
     * Vertical height of shallow current/density region in point A
     * @param depthShallowA the depthShallowA to set
     */
    public void setDepthShallowA(double depthShallowA) {
        this.depthShallowA = depthShallowA;
    }

    /**
     * Vertical height of shallow current/density region in point B
     * @return the depthShallowB
     */
    public double getDepthShallowB() {
        return depthShallowB;
    }

    /**
     * Vertical height of shallow current/density region in point B
     * @param depthShallowB the depthShallowB to set
     */
    public void setDepthShallowB(double depthShallowB) {
        this.depthShallowB = depthShallowB;
    }

    /**
     * Inverse of transition length between current/density regions in point A
     * @return the transitionLayerA
     */
    public double getTransitionLayerA() {
        return transitionLayerA;
    }

    /**
     * Inverse of transition length between current/density regions in point A
     * @param transitionLayerA the transitionLayerA to set
     */
    public void setTransitionLayerA(double transitionLayerA) {
        this.transitionLayerA = transitionLayerA;
    }

    /**
     * Inverse of transition length between current/density regions in point B
     * @return the transitionLayerB
     */
    public double getTransitionLayerB() {
        return transitionLayerB;
    }

    /**
     * Inverse of transition length between current/density regions in point B
     * @param transitionLayerB the transitionLayerB to set
     */
    public void setTransitionLayerB(double transitionLayerB) {
        this.transitionLayerB = transitionLayerB;
    }
    
  
    
}
