/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sfh.ship.report;

import com.sfh.ship.vesselExamples.BaseExample;
import java.util.Comparator;

/**
 *
 * @author aarsathe
 */
public class customPropulsorPositionCompare implements Comparator<BaseExample.IShipPoint> {
    @Override
    public int compare(BaseExample.IShipPoint o1, BaseExample.IShipPoint o2) {
          if( o1.getX() == o2.getX())
            return o1.getY() > o2.getY() ? 1 : -1;
        return o1.getX() > o2.getX() ? -1 : 1;
    }
}
