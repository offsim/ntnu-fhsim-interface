/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sfh.ship.report;

import com.itextpdf.awt.DefaultFontMapper;
import com.itextpdf.text.Font;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.sfh.ship.vesselExamples.BaseExample.positionData;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYBoxAnnotation;
import org.jfree.chart.annotations.XYPointerAnnotation;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleEdge;

/**
 *
 * @author aarsathe
 */
public class MultiPlotCellEvent implements PdfPCellEvent{

    /**
     * @return the disabledString
     */
    public String getDisabledString() {
        return disabledString;
    }

    /**
     * @param disabledString the disabledString to set
     */
    public void setDisabledString(String disabledString) {
        disabledPlot = true;
        this.disabledString = disabledString;
    }

    private class shipPlot{
        public XYSeries data;
        public XYSeries data2;
        public double l;
        public double b;
    }
    
    private class shipAnnotation{
        public String string;
        public double x;
        public double y;
        public double p;
    }
    
    public MultiPlotCellEvent(){
        series = new ArrayList<>();
        colors = new ArrayList<>();
        shipSeries = new ArrayList<>();
        annotations = new ArrayList<>();
    }
    
    public void addPlot(String n,double[] x,double[] y){
        addPlot(n,x,y,Color.BLACK);
    }
    
    public void addAnnotation(String str, double x, double y){ 
        addAnnotation(str, x, y,0);
    }

    public void addAnnotation(String str, double x, double y, double p){
        shipAnnotation n = new shipAnnotation();
        n.string = str;
        n.x=x;
        n.y=y;
        n.p=p;
        annotations.add(n);
    }
    
    public void addPlot(String n,double[] x,double[] y, Color c){
        XYSeries tmp = new XYSeries(n,false);
        for( int i=0; i < x.length; i++)
            tmp.add(x[i], y[i]);
        series.add(tmp);
        colors.add(c);
    }

    public void addShipPlot(String n,double l, double b, positionData d  ){
        shipPlot p = new shipPlot();
        p.data =  new XYSeries(n,false);
        p.data2 = new XYSeries(n,false);
        for( int i=0; i < d.n.seriesLength(); i += 20){
            p.data.add( d.e.getY(i), d.n.getY(i));
            p.data2.add( d.psi.getX(i), -d.psi.getY(i) - Math.PI*3/2);
        }
        p.data.add(  d.e.getLastY(), d.n.getLastY() );
        p.data2.add( d.psi.getLastX(), -d.psi.getLastY() - Math.PI*3/2);
        
        p.l = l;
        p.b = b;
        shipSeries.add(p);
    }
    
    private XYSeries shipAt(double x, double y,double psi, double l,double b){
        XYSeries ret = new XYSeries("ss",false);
    
// x' = x cos f - y sin f
// y' = y cos f + x sin f
   
        double x0 =    -l/2.0 * Math.cos(psi) - (-b/2.0) * Math.sin(psi);
        double x1 =    -l/2.0 * Math.cos(psi) - ( b/2.0) * Math.sin(psi);
        double x2 = 0.8*l/2.0 * Math.cos(psi) - ( b/2.0) * Math.sin(psi);
        double x3 = 1.0*l/2.0 * Math.cos(psi) - ( 0.0)   * Math.sin(psi);
        double x4 = 0.8*l/2.0 * Math.cos(psi) - (-b/2.0) * Math.sin(psi);

        double y0 =    -l/2.0 * Math.sin(psi) + (-b/2.0) * Math.cos(psi);
        double y1 =    -l/2.0 * Math.sin(psi) + ( b/2.0) * Math.cos(psi);
        double y2 = 0.8*l/2.0 * Math.sin(psi) + ( b/2.0) * Math.cos(psi);
        double y3 = 1.0*l/2.0 * Math.sin(psi) + ( 0.0)   * Math.cos(psi);
        double y4 = 0.8*l/2.0 * Math.sin(psi) + (-b/2.0) * Math.cos(psi);
        
        ret.add(x+x0,y+y0);
        ret.add(x+x1,y+y1);
        ret.add(x+x2,y+y2);
        ret.add(x+x3,y+y3);
        ret.add(x+x4,y+y4);
        ret.add(x+x0,y+y0);
        return ret;
    }
    
    public void MakePlot( String title, String xlabel, String ylabel){
        MakePlot( title, xlabel, ylabel, false);
    }
    public void MakePlot( String title, String xlabel, String ylabel, boolean legend){
        dataset = new XYSeriesCollection();
        for( XYSeries s: series)
            dataset.addSeries(s);
        //for( int i=0; i < shipSeries.size(); i++){
        //    dataset.addSeries( shipSeries.get(i).data );
        //    colors.add(Color.BLACK);
        //}

        for( int i=0; i < shipSeries.size(); i++){
            for( int j=0; j < shipSeries.get(i).data.getItemCount(); j ++ ){
                double _x = shipSeries.get(i).data.getX(j).floatValue();
                double _y = shipSeries.get(i).data.getY(j).floatValue();
                double _psi = shipSeries.get(i).data2.getY(j).floatValue();
                dataset.addSeries(  shipAt(  _x, _y, _psi, shipSeries.get(i).l, shipSeries.get(i).b)  );
                colors.add(Color.RED);
            }
        }
        
        chart = ChartFactory.createXYLineChart(title, xlabel, ylabel, dataset, PlotOrientation.VERTICAL,legend,false,false );
        
        
        XYPlot plot = (XYPlot) chart.getPlot();

        if( shipSeries.size() > 0 ){
            double upperRange = plot.getRangeAxis().getRange().getUpperBound();
            double lowerRange = plot.getRangeAxis().getRange().getLowerBound();
        
            double upperDomain = plot.getDomainAxis().getRange().getUpperBound();
            double lowerDomain = plot.getDomainAxis().getRange().getLowerBound();
        
            double scale;
            if( (upperRange - lowerRange) > (upperDomain-lowerDomain) ){
                scale = (upperRange - lowerRange)/(upperDomain-lowerDomain);
                upperDomain *= scale;
                lowerDomain *= scale;
            }else{
                scale = (upperDomain-lowerDomain)/(upperRange - lowerRange);
                upperRange *= scale;
                lowerRange *= scale;           
            }
            
            plot.getRangeAxis().setUpperBound(upperRange);
            plot.getRangeAxis().setLowerBound(lowerRange);
            plot.getDomainAxis().setUpperBound(upperDomain);
            plot.getDomainAxis().setLowerBound(lowerDomain);
        }
    
        for( int i=0; i < dataset.getSeriesCount();i++)
            plot.getRenderer().setSeriesPaint(i, colors.get(i));

        for( shipAnnotation item: annotations){
            XYTextAnnotation anot =  new XYTextAnnotation(item.string, item.x, item.y);
            double xrange = plot.getDomainAxis().getUpperBound()-plot.getDomainAxis().getLowerBound();
            anot.setX(0.75*xrange);
            plot.addAnnotation(anot);
        }
        if(disabledPlot){
        
            double xl = plot.getRangeAxis().getLowerBound();
            double xu = plot.getRangeAxis().getUpperBound();

            double yl = plot.getDomainAxis().getLowerBound();
            double yu = plot.getDomainAxis().getUpperBound();
            
            XYBoxAnnotation disable = new XYBoxAnnotation(xl, yl, xu, yu,new BasicStroke(1),Color.DARK_GRAY,Color.LIGHT_GRAY);
            XYTextAnnotation textAnnotaion = new XYTextAnnotation(getDisabledString(), (xl+xu)/2.0, (yl+yu)/2.0);
            
            textAnnotaion.setPaint(Color.darkGray);
            
            plot.addAnnotation(disable);
            plot.addAnnotation(textAnnotaion);
        }
        
        
        if( legend ){
            LegendTitle legendTitle = chart.getLegend();
            legendTitle.setPosition(RectangleEdge.RIGHT);
        }
        
        // trick to change the default font of the chart
        chart.setTitle(new TextTitle(title, new java.awt.Font("Serif", Font.BOLD, 12)));
        chart.setBackgroundPaint(Color.white);
        chart.setBorderPaint(Color.black);
        chart.setBorderStroke(new BasicStroke(1));
        chart.setBorderVisible(false);
    }    

    
    private XYSeriesCollection dataset;
    private ArrayList<XYSeries> series;
    private ArrayList<Color> colors;
    private ArrayList<shipPlot> shipSeries;
    private ArrayList<shipAnnotation> annotations;
    private JFreeChart chart;
    
    private boolean disabledPlot=false;
    private String disabledString="Disabled";
    @Override
    public void cellLayout(PdfPCell ppc, Rectangle rctngl, PdfContentByte[] pcbs) {
 
        float width = rctngl.getWidth()-5;
        float height = rctngl.getHeight()-5;
        
        // get the direct pdf content
        PdfContentByte dc = pcbs[PdfPTable.BACKGROUNDCANVAS];//docWriter.getDirectContent();
 
        // get a pdf template from the direct content
        PdfTemplate tp = dc.createTemplate(width, height);
 
        // create an AWT renderer from the pdf template
        Graphics2D g2 = tp.createGraphics(width, height, new DefaultFontMapper() );
        Rectangle2D r2D = new Rectangle2D.Double(0,0, width,height);
        chart.draw(g2,r2D,null);
        g2.dispose();
 
        // add the rendered pdf template to the direct content
        // you will have to play around with this because the chart is absolutely positioned.
        // 38 is just a typical left margin
        // docWriter.getVerticalPosition(true) will approximate the position that the content above the chart ended
        dc.addTemplate(tp, rctngl.getLeft()+2.5f, rctngl.getTop()-height-2.5f);        
    }    
}
