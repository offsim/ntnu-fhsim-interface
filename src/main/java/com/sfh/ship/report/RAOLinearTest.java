/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.ship.report;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sfh.ship.vesselExamples.PK410_NTNU_Gunnerus;
import com.sfh.ship.vesselExamples.BaseExample;
import com.sfh.ship.vesselExamples.VERESFileUtil;
import com.sfh.ship.VeresFile;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tobias
 */
public class RAOLinearTest {
    
    private static double period = 9;
    private static double dir = Math.PI / 2;
    private static String vesselName = "PK410";
    private static String filename = "RAOLinearityReport.pdf";
            
    public static void main(String args[]) throws FileNotFoundException, DocumentException {
        PK410_NTNU_Gunnerus data = new PK410_NTNU_Gunnerus();
        data.configureShip();
      
     
        VeresFile.HydFile f = (new VeresFile(data.getHull().getModelData() + "/" + data.getHull().getModelFileStem())).hydFile;
     
        
        PdfPCell plot;
        PdfPTable page;
        Document document = new Document(PageSize.A4);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
        document.open();
        
        double cog_rel_waterline = f.Draught-f.VCG;
        double Lpp = f.Lpp;
        
        double[] waveAmplitudes = {0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10};
        int numAmplitudes = 20;
        double[] response;
        double[] RAO = new double[numAmplitudes]; 
        ArrayList<ArrayList<ArrayList<Double>>> peaks;
        
        for (int i = 0; i < numAmplitudes; i++) {
         peaks = data.runRAO(dir,2*Math.PI / period, waveAmplitudes[i]);
         response = data.calculateRAO(peaks);
         RAO[i] = response[3]/waveAmplitudes[i];
        }
        
        plot = createPlots(RAO, waveAmplitudes, numAmplitudes);
        page = createPage(plot, data);
        document.newPage();
        document.add(page);
        document.close();
    }
    
     private static PdfPTable createPage(PdfPCell content, BaseExample data) {
        String dateString = (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).format(new Date());
        String version = data.getHull().getVersionString();
        PdfPTable page = new PdfPTable(1);
        page.setWidthPercentage(100.0f);

        PdfPCell header = new PdfPCell();
        header.setFixedHeight(70);

        ReportPageHeader evt = new ReportPageHeader();
        evt.setSectionTitle("Roll Dynamics linearity check");
        evt.setShipTitle(vesselName);
        header.setCellEvent(evt);

        PdfPCell footer = new PdfPCell();
        footer.setFixedHeight(25);
        ReportPageFooter evt2 = new ReportPageFooter();
        evt2.setDate(dateString);
        evt2.setPage(1);
        evt2.setVersion(version);
        footer.setCellEvent(evt2);

        content.setFixedHeight(640);

        page.addCell(header);
        page.addCell(content);
        page.addCell(footer);

        return page;
    }

    private static PdfPCell createPlots(double[] RAO, double[] waveAmp, int numAmplitudes) {
       
        PdfPTable info = new PdfPTable(1);
        PdfPCell c = new PdfPCell();
        MultiPlotCellEvent n = new MultiPlotCellEvent();
        n.addPlot("RAO [deg/m]",waveAmp,RAO);
        n.addPlot("RAO for 1m wave", new double[] {waveAmp[0], waveAmp[numAmplitudes-1]}, new double[] {RAO[1], RAO[1]}, Color.BLUE);
        n.MakePlot("", "Wave amplitude [m]", "RAO [deg/m]", true);
        c.setCellEvent(n);
        c.setFixedHeight(500.0f);
        info.addCell(c);

        return new PdfPCell(info);
    }
}
