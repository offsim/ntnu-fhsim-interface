package com.sfh.ship.report;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sfh.ship.vesselExamples.BaseExample;
import com.sfh.ship.VeresFile;
import com.sfh.ship.vesselExamples.PK410_NTNU_Gunnerus;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Tobias
 * 
 * */

public class StabilityReport {
    
    private static int caseDuration = 15000;
    private static int numberOfRuns = 1;
    private static double samplingTime = 1.0;
    private static String filename = "StabilityReport.pdf";
    private static String vesselName = "PK 410 Gunnerus";

    public double getSamplingTime() {
        return samplingTime;
    }

    public void setSamplingTime(double samplingTime) {
        this.samplingTime = samplingTime;
    }

    public int getCaseDuration() {
        return caseDuration;
    }

    public void setCaseDuration(int caseDuration) {
        this.caseDuration = caseDuration;
    }

    public int getNumberOfRuns() {
        return numberOfRuns;
    }

    public void setNumberOfRuns(int numberOfRuns) {
        this.numberOfRuns = numberOfRuns;
    }

    public static void main(String args[]) throws FileNotFoundException, DocumentException {
        
        PK410_NTNU_Gunnerus data = new PK410_NTNU_Gunnerus();
        data.configureShip();
        VeresFile.HydFile f = (new VeresFile(data.getHull().getModelData() + "/" + data.getHull().getModelFileStem())).hydFile;
     
        BaseExample.positionData posData;
        ArrayList<PdfPCell> plots;
        PdfPTable page;
       
        Document document = new Document(PageSize.A4);        
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
        document.open();

         int pageNum = 1;
        
        for (int i = 0; i < numberOfRuns; i++){
           posData = data.runStabilityCase(caseDuration,samplingTime,f.Draught-f.VCG);
           plots = createPlots(posData);
           for (PdfPCell plot : plots) {
           page = createPage(plot, pageNum,data);
           document.newPage();
           document.add(page);
           pageNum++;
           }
        }
        
        document.close();
    }
    
    private static PdfPTable createPage(PdfPCell content, int pageNumber, BaseExample data) {
        String dateString = (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).format(new Date());
        String version = data.getHull().getVersionString();
        PdfPTable pageOne = new PdfPTable(1);
        pageOne.setWidthPercentage(100.0f);

        PdfPCell header = new PdfPCell();
        header.setFixedHeight(70);
        
        ReportPageHeader evt = new ReportPageHeader();
        evt.setSectionTitle("FhSim shipmodels stability test");
        evt.setShipTitle(vesselName);
        header.setCellEvent(evt);        
        
        PdfPCell footer = new PdfPCell();
        footer.setFixedHeight(25);
        ReportPageFooter evt2 = new ReportPageFooter();
        evt2.setDate(dateString);
        evt2.setPage(pageNumber);
        evt2.setVersion(version);
        footer.setCellEvent(evt2);
        
        content.setFixedHeight(640);
        
        pageOne.addCell( header );
        pageOne.addCell( content );
        pageOne.addCell( footer );
        
        return pageOne;
    }
    
   private static ArrayList<PdfPCell> createPlots(BaseExample.positionData posData) {
       Color [] colors = new Color[]{Color.BLACK,Color.BLUE,Color.GREEN,Color.ORANGE,Color.RED,Color.YELLOW};
       ArrayList<PdfPCell> ret = new ArrayList<PdfPCell>(); 

            PdfPTable info = new PdfPTable(1);           
            PdfPCell c = new PdfPCell();
            MultiPlotCellEvent n = new MultiPlotCellEvent();
            n.addPlot("Surge [m]",posData.n.toArrayX(),posData.n.toArrayY());       
            n.MakePlot("Surge Motion", "Time [s]", "Surge [m]",true);
            c.setCellEvent(n);
            c.setFixedHeight(500.0f);
            info.addCell(c);
            ret.add(new PdfPCell(info));
            
            info = new PdfPTable(1);
            c = new PdfPCell();
            n = new MultiPlotCellEvent();
            n.addPlot("Sway [m]",posData.e.toArrayX(),posData.e.toArrayY());       
            n.MakePlot("Sway Motion", "Time [s]", "Sway [m]",true);
            c.setCellEvent(n);
            c.setFixedHeight(500.0f);
            info.addCell(c);
            ret.add(new PdfPCell(info));
            
            info = new PdfPTable(1);
            c = new PdfPCell();
            n = new MultiPlotCellEvent();
            n.addPlot("Heave [m]",posData.d.toArrayX(),posData.d.toArrayY());       
            n.MakePlot("Heave Motion", "Time [s]", "Heave [m]",true);
            c.setCellEvent(n);
            c.setFixedHeight(500.0f);
            info.addCell(c);
            ret.add(new PdfPCell(info));
            
            info = new PdfPTable(1);
            c = new PdfPCell();
            n = new MultiPlotCellEvent();
            n.addPlot("Roll [rad]",posData.theta.toArrayX(),posData.theta.toArrayY());       
            n.MakePlot("Roll motion", "Time [s]", "Roll [rad]",true);
            c.setCellEvent(n);
            c.setFixedHeight(500.0f);
            info.addCell(c);
            ret.add(new PdfPCell(info));
            
            info = new PdfPTable(1);
            c = new PdfPCell();
            n = new MultiPlotCellEvent();
            n.addPlot("Pitch [rad]",posData.alfa.toArrayX(),posData.alfa.toArrayY());       
            n.MakePlot("Pitch Motion", "Time [s]", "Pitch [rad]",true);
            c.setCellEvent(n);
            c.setFixedHeight(500.0f);
            info.addCell(c);
            ret.add(new PdfPCell(info));
            
            info = new PdfPTable(1);
            c = new PdfPCell();
            n = new MultiPlotCellEvent();
            n.addPlot("Yaw [rad]",posData.psi.toArrayX(),posData.psi.toArrayY());       
            n.MakePlot("Yaw Motion", "Time [s]", "Yaw [rad]",true);
            c.setCellEvent(n);
            c.setFixedHeight(500.0f);
            info.addCell(c);                      
            ret.add(new PdfPCell(info));
   
    return ret;
    }
}