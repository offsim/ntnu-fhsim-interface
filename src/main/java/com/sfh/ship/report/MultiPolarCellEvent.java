/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sfh.ship.report;

import com.itextpdf.awt.DefaultFontMapper;
import com.itextpdf.text.Font;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PolarPlot;
import org.jfree.chart.renderer.DefaultPolarItemRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleEdge;

/**
 *
 * @author aarsathe
 */
public class MultiPolarCellEvent implements PdfPCellEvent{

    private boolean legend = false;
    private JFreeChart chart;
    private ArrayList<XYSeries> series = new ArrayList<XYSeries>();
    private ArrayList<Color> colors = new ArrayList<Color>();
    private XYSeriesCollection dataset;

    public void addPlot(String n,double[] x,double[] y, Color c){
        XYSeries tmp = new XYSeries(n,false);
        for( int i=0; i < x.length; i++)
            tmp.add(x[i], y[i]);
        series.add(tmp);
        colors.add(c);
    }

    public void MakePlot( String title, boolean legend){
        dataset = new XYSeriesCollection();
        for( XYSeries s: series)
            dataset.addSeries(s);
        

        chart = ChartFactory.createPolarChart(title, dataset, legend,false,false );
        final PolarPlot plot = (PolarPlot) chart.getPlot();
        final DefaultPolarItemRenderer renderer = (DefaultPolarItemRenderer) plot.getRenderer();
        renderer.setSeriesFilled(2, true);        
               
        if( legend ){
            LegendTitle legendTitle = chart.getLegend();
            legendTitle.setPosition(RectangleEdge.RIGHT);
        }

        
        chart.setTitle(new TextTitle(title, new java.awt.Font("Serif", Font.BOLD, 12)));
        chart.setBackgroundPaint(Color.white);
        chart.setBorderPaint(Color.black);
        chart.setBorderStroke(new BasicStroke(1));
        chart.setBorderVisible(false);
        
    }    

    
    
    @Override
    public void cellLayout(PdfPCell ppc, Rectangle rctngl, PdfContentByte[] pcbs) { 
        float width = rctngl.getWidth()-5;
        float height = rctngl.getHeight()-5;
        
        // get the direct pdf content
        PdfContentByte dc = pcbs[PdfPTable.BACKGROUNDCANVAS];//docWriter.getDirectContent();
 
        // get a pdf template from the direct content
        PdfTemplate tp = dc.createTemplate(width, height);
 
        // create an AWT renderer from the pdf template
        Graphics2D g2 = tp.createGraphics(width, height, new DefaultFontMapper() );
        Rectangle2D r2D = new Rectangle2D.Double(0,0, width,height);
        chart.draw(g2,r2D,null);
        g2.dispose();
 
        // add the rendered pdf template to the direct content
        // you will have to play around with this because the chart is absolutely positioned.
        // 38 is just a typical left margin
        // docWriter.getVerticalPosition(true) will approximate the position that the content above the chart ended
        dc.addTemplate(tp, rctngl.getLeft()+2.5f, rctngl.getTop()-height-2.5f);        
    }        
}
