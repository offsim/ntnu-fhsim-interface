/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.ship.report;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.sfh.ship.VeresFile;
import com.sfh.ship.VeresFile.HydFile;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aarsathe
 */
public class LinearHydrostatics implements PdfPCellEvent {

    private HydFile stabilityData = null;
    
    public LinearHydrostatics(VeresFile.HydFile f){
        stabilityData = f;
    }
    
    @Override
    public void cellLayout(PdfPCell ppc, Rectangle rect, PdfContentByte[] canvas) {
        
        PdfContentByte cb = canvas[PdfPTable.BACKGROUNDCANVAS];
        //cb.roundRectangle(rect.getLeft() + 1.5f, rect.getBottom() + 1.5f, rect.getWidth() - 3, rect.getHeight() - 3, 4);

        float left = rect.getLeft();
        float bottom = rect.getBottom();
        float width = rect.getWidth();
        float height = rect.getHeight();
             
        float vert_margin = 0.1f*width;
        float hor_margin = 0.2f*width;

        
        float start0 = left + vert_margin;
        float end0 = left + vert_margin + 0.2f*width;
        
        float start1 = end0;
        float end1 = left + vert_margin + (0.2f+0.35f)*width;

        float start2 = end1;
        float end2 = left + vert_margin + (0.2f+2*0.35f)*width;

        
        float waterline_x_start = start0;
        float waterline_x_end = end1 + 0.25f*(end2-start2);

        
        float hull_start_x = start1;
        float hull_end_x = end1;
        
        
        float hull_center_x = 0.5f*(hull_end_x+hull_start_x);
        
        float hull_y_top = bottom+ 0.85f*height;
        float hull_y_bot = bottom+0.35f*height;
        
        float waterline_mark_center = waterline_x_start + 0.1f*(waterline_x_end - waterline_x_start);

        float KM = (float)(stabilityData.VCG + stabilityData.GMt ); 
         
        float scale = (hull_y_top-hull_y_bot)/KM;

        float waterline_y = bottom + 0.35f*height+scale*(float)stabilityData.Draught;
        
        
        // draw waterline
        cb.moveTo(waterline_x_start, waterline_y);
        cb.lineTo(waterline_x_end, waterline_y);
        
        // triangle
        cb.moveTo(waterline_mark_center-8, waterline_y+11);
        cb.lineTo(waterline_mark_center+8, waterline_y+11);
        cb.lineTo(waterline_mark_center+0, waterline_y+2);
        cb.lineTo(waterline_mark_center-8, waterline_y+11);
        
        // water surface line marks 
        cb.moveTo(waterline_mark_center-8, waterline_y-2);
        cb.lineTo(waterline_mark_center+8, waterline_y-2);

        cb.moveTo(waterline_mark_center-4, waterline_y-4);
        cb.lineTo(waterline_mark_center+4, waterline_y-4);
        
        cb.moveTo(waterline_mark_center-1, waterline_y-6);
        cb.lineTo(waterline_mark_center+1, waterline_y-6);
        
        cb.stroke();
        
        // draw hull
        cb.moveTo(hull_start_x, hull_y_top-20);
        cb.lineTo(hull_start_x, hull_y_top - 0.85f*(hull_y_top-hull_y_bot) );
        cb.curveTo(hull_start_x,hull_y_bot, left + 1.15f*(hull_start_x-left),hull_y_bot);
        cb.lineTo(hull_start_x - 0.85f*(hull_start_x - hull_end_x) , hull_y_bot);
        cb.curveTo(hull_end_x,hull_y_bot,hull_end_x, hull_y_top - 0.85f*(hull_y_top-hull_y_bot));
        cb.lineTo(hull_end_x,hull_y_top-20);
        cb.stroke();
        
        // draw centerline
        cb.moveTo(hull_center_x, hull_y_top);
        cb.lineTo(hull_center_x, hull_y_bot);
        cb.stroke();
        
        
        // draw points 
        
        BaseFont bf = null;
        try {
            bf = BaseFont.createFont();
        } catch (DocumentException ex) {
            Logger.getLogger(LinearHydrostatics.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LinearHydrostatics.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        cb.moveTo(hull_center_x, hull_y_bot);
        cb.setFontAndSize(bf, 10);

        // K
        cb.circle(hull_center_x, hull_y_bot, 5);
        cb.setColorFill(BaseColor.BLACK);
        cb.beginText();
        cb.setTextMatrix(hull_center_x+4, hull_y_bot+4);
        cb.showText("Keel: 0m");
        cb.endText();
        
        // G
        cb.circle(hull_center_x, hull_y_bot + scale*(float)stabilityData.VCG, 5);
        cb.setColorFill(BaseColor.BLACK);
        cb.beginText();
        cb.setTextMatrix(hull_center_x+4, hull_y_bot + scale*(float)stabilityData.VCG+4);
        cb.showText("G: " + stabilityData.VCG + "m");
        cb.endText();

        // B
        cb.circle(hull_center_x, hull_y_bot + scale*(float)stabilityData.KB, 5);
        cb.setColorFill(BaseColor.BLACK);
        cb.beginText();
        cb.setTextMatrix(hull_center_x+4, hull_y_bot + scale*(float)stabilityData.KB+4);
        cb.showText("B: " + stabilityData.KB + "m");
        cb.endText();

        // M
        cb.circle(hull_center_x, hull_y_top, 5);
        cb.setColorFill(BaseColor.BLACK);
        cb.beginText();
        cb.setTextMatrix(hull_center_x+4, hull_y_top+4);
        cb.showText("M: " + Math.round( (stabilityData.VCG + stabilityData.GMt)*100)/100.0 + "m");
        cb.endText();
        
        cb.setColorFill(BaseColor.BLACK);
        
    }
    
}
