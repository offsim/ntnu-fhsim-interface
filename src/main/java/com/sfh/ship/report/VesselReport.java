/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.ship.report;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sfh.ship.FhDynamicModel;
import com.sfh.ship.SFHActuatorPointData;
import com.sfh.ship.SFHOpusPropulsorData;
import com.sfh.ship.vesselExamples.BaseExample;
import com.sfh.ship.vesselExamples.BaseExample.allThrusterResponse;
import com.sfh.ship.vesselExamples.BaseExample.crashStop;
import com.sfh.ship.vesselExamples.BaseExample.fullSpeed;
import com.sfh.ship.vesselExamples.BaseExample.fullSpeed2;
import com.sfh.ship.vesselExamples.BaseExample.positionData;
import com.sfh.ship.vesselExamples.BaseExample.turingCircle;
import com.sfh.ship.vesselExamples.UT776CD_Island_Crusader;

import com.sfh.ship.vesselExamples.VERESFileUtil;


import com.sfh.ship.VeresFile;
import com.sfh.ship.VeresFile.Section;
import com.sfh.ship.vesselExamples.PK410_NTNU_Gunnerus;
import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
//import org.jfree.chart.plot.PlotOrientation;

/**
 *
 * @author aarsathe
 */
public class VesselReport {

    private final static String format(double value) {
        return String.format(Locale.ENGLISH, "%.2f", value);
    }

    static class thrusterPerformanceCollection {

        /*        public thrusterPerformanceCollection(){
         t20 = new allThrusterResponse();
         t40 = new allThrusterResponse();
         t6 0 = new allThrusterResponse();
         t80 = new allThrusterResponse();
         t100 = new allThrusterResponse();
         }*/
        public void runThrusterTrials(BaseExample ex) {
            t20 = ex.runThrusterReponseTrial(0.2);
            t40 = ex.runThrusterReponseTrial(0.4);
            t60 = ex.runThrusterReponseTrial(0.6);
            t80 = ex.runThrusterReponseTrial(0.8);
            t100 = ex.runThrusterReponseTrial(1.0);
            t50_thrust = ex.runThrusterReponseTrial(0.5, true);
            t100_thrust = ex.runThrusterReponseTrial(1.0, true);
            t100_thrust_n = ex.runThrusterReponseTrial(-1.0, true);
        }
        public allThrusterResponse t20;
        public allThrusterResponse t40;
        public allThrusterResponse t60;
        public allThrusterResponse t80;
        public allThrusterResponse t100;
        public allThrusterResponse t50_thrust;
        public allThrusterResponse t100_thrust;
        public allThrusterResponse t100_thrust_n;
    }
    private static int pageNo;
    private static String vesselName;
    private static String sectionName;
    private static String dateString;
    private static String interfaceVersion;

    public static void main(String args[]) throws FileNotFoundException, DocumentException {
     
        UT776CD_Island_Crusader UT776CDdata = new UT776CD_Island_Crusader();
        UT776CDdata.configureShip();
        
        PK410_NTNU_Gunnerus gunnerus = new PK410_NTNU_Gunnerus();
        gunnerus.configureShip();
        
        
        createHullReport(gunnerus, "RV_PK410_NTNU_Gunnerus.pdf", "PK410 - NTNU Gunnerus");
        
//     
        //    createHullReport(UT776CDdata, "OSCV_UT776CD_ISL.pdf", "UT776CD - Island Crown");
    
    }

    private static void createHullReport(BaseExample data, String saveAs, String name) throws FileNotFoundException, DocumentException {

        Document document = new Document(PageSize.A4);
        String date = (new SimpleDateFormat("yyyyMMddHHmm_")).format(new Date());
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(date + saveAs));
        document.open();

        pageNo = 1;
        vesselName = name;
        System.out.println(name);
        sectionName = "Vessel data";
        dateString = (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).format(new Date());
        interfaceVersion = FhDynamicModel.getFhSimInterfaceVersion("pom.xml");

        if (data.getReportSpec().isRunVeres()) {
            System.out.println("VERES INFO");
            appendToDocument(creatPageTable(createVeresInfo(data), data.getHull().getVersionString()), document);
        }
        if (data.getReportSpec().isRunDampingInfo()) {
            System.out.println("DAMPING INFO");
            appendToDocument(creatPageTable(createDampingData(data), data.getHull().getVersionString()), document);
        }
        if (data.getReportSpec().isRunStability()) {
            System.out.println("STABILITY INFO");
            appendToDocument(creatPageTable(createStabilityData(data), data.getHull().getVersionString()), document);
        }
        if (data.getReportSpec().isRunDriftInfo()) {
            sectionName = "Simulated Trial data";
            System.out.println("CURRENT TEST");
            appendToDocument(creatPageTable(createCurrentData(data), data.getHull().getVersionString()), document);
            System.out.println("WIND TEST");
            appendToDocument(creatPageTable(createWindData(data), data.getHull().getVersionString()), document);
            System.out.println("WAVE DRIFT TEST");
            appendToDocument(creatPageTable(createWaveDriftData(data), data.getHull().getVersionString()), document);
        }
        if (data.getReportSpec().isRunFullSpeed()) {
            System.out.println("FULL SPEED ");
            appendToDocument(creatPageTable(createFullSpeedData(data), data.getHull().getVersionString()), document);
        }
        if (data.getReportSpec().isRunWheelingHousePoster()) {
            System.out.println("TURNING CIRCLE");
            appendToDocument(creatPageTable(createTurningCircleData(data), data.getHull().getVersionString()), document);
        }
        if (data.getReportSpec().isRunCrashStop()) {
            System.out.println("CRASH STOP");
            appendToDocument(creatPageTable(createCrashStopData(data), data.getHull().getVersionString()), document);
        }
        if (data.getReportSpec().isRunHeelDecay()) {
            System.out.println("ROLL DECAY INFO");
            appendToDocument(creatPageTable(createRollDecayData(data), data.getHull().getVersionString()), document);
        }
        if (data.getReportSpec().isRunRAO()) {
            System.out.println("RAO INFO");

            ArrayList<PdfPCell> rao = null;
            try {
                rao = createRAOData(data);
            } catch (InterruptedException ex) {
                Logger.getLogger(VesselReport.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (PdfPCell r : rao) {
                appendToDocument(creatPageTable(r, data.getHull().getVersionString()), document);
            }
        }
        if (data.getReportSpec().isRunPolar()) {
            ArrayList<PdfPCell> polar = null;
            try {
                polar = createPolarWavePlot(data);
            } catch (InterruptedException ex) {
                Logger.getLogger(VesselReport.class.getName()).log(Level.SEVERE, null, ex);
            }

            for (PdfPCell p : polar) {
                appendToDocument(creatPageTable(p, data.getHull().getVersionString()), document);
            }
        }
        if (data.getReportSpec().isRunPropulsorInfo()) {
            thrusterPerformanceCollection thruster_data = new thrusterPerformanceCollection();
            thruster_data.runThrusterTrials(data);

            VeresFile veres = new VeresFile(data.getHull().getModelData() + "/" + data.getHull().getModelFileStem());

            sectionName = String.format("Propulsors (Stern-AP= %.1fm)", veres.getSAPPO());
            appendToDocument(creatPageTable(createPopulsorInfo(data, thruster_data), data.getHull().getVersionString()), document);

            ArrayList<BaseExample.PropulsorActuatorDataPair> propulsors = data.getPropulsors();
            Collections.sort(propulsors, new customPropulsorPositionCompare());
            sectionName = "Propulsors Information";

            for (BaseExample.PropulsorActuatorDataPair p : propulsors) {
                try {

                    System.out.println("page for " + p.propulsor.getName());
                    appendToDocument(creatPageTable(createPropulsorDetail(p, thruster_data), data.getHull().getVersionString()), document);
                    appendToDocument(creatPageTable(createPropulsorResponsePlot(p, thruster_data), data.getHull().getVersionString()), document);

                } catch (Exception e) {
                    System.err.println(e.getMessage());
                    for (StackTraceElement s : e.getStackTrace()) {
                        System.out.println(s.toString());
                    }
                    System.out.println(e.getMessage());
                }
            }
        }
//        if (data.getReportSpec().isRunSensorInfo()) {
//            sectionName = "Sensor Info";
//            appendToDocument(creatPageTable(createOSCSensorInfo(data), data.getHull().getVersionString()), document);
//        }
        document.close();
    }

    //uses the peak period in rolll to fine have a higher resolution around the peak
    //if might be completely off for pitch, heave
    private static ArrayList<PdfPCell> createRAOData(BaseExample data) throws InterruptedException {

        double amp = 0.01;
        double k, omega;
        VeresFile veres = new VeresFile(data.getHull().getModelData() + "/" + data.getHull().getModelFileStem());
        VeresFile.HydFile f = veres.hydFile;

        double[] periods = new double[]{20.0, 16.0, 14.0, 13.0, 10.0, 9.0, 8.0, 7.5, 7.0, 6.5, 6.25, 6.0, 5.5, 5.25, 4.5, 4.0, 3.0};
        if (data.getReportSpec().isRunExtraRAOPeriods()) {
            createRollDecayData(data);
            double peak = data.getNatualRollPeriod();
            ArrayList<Double> periodss = new ArrayList<>();
            for (double p = peak - 1; p <= peak + 1; p += data.getReportSpec().getRaoPeriodStep()) {
                periodss.add(p);
            }
            for (double p = 3; p <= 20; p++) {
                if (!periodss.contains(p)) {
                    periodss.add(p);
                }
            }

            periodss.sort(new Comparator<Double>() {
                @Override
                public int compare(Double o1, Double o2) {
                    return o1 > o2 ? -1 : 1;
                }
            });
            periods = new double[periodss.size()];
            for (int i = 0; i < periods.length; i++) {
                periods[i] = periodss.get(i);
            }
        }

        double[] directions = new double[]{0, 1. / 4 * Math.PI, Math.PI / 2.0, 3.0 / 4.0 * Math.PI, Math.PI};
        double[][][] raores = new double[6][][];
        double[][] stern = new double[6][periods.length];
        double[][] beam = new double[6][periods.length];
        ArrayList<ArrayList<ArrayList<Double>>> peaks;
        double[] phase;
        for (int i = 2; i < 5; i++) {
            raores[i] = new double[directions.length][];
        }
        for (int di = 0; di < directions.length; di++) {
            for (int i = 2; i < 5; i++) {
                raores[i][di] = new double[periods.length];
            }
        }
        for (int di = 0; di < directions.length; di++) {
            for (int fi = 0; fi < periods.length; fi++) {
                System.out.println("direction " + directions[di] + " period " + periods[fi]);
                omega = 2.0 * Math.PI / periods[fi];
                peaks = data.runRAO(directions[di], omega, amp);
                double rao[] = data.calculateRAO(peaks);
                k = omega * omega / 9.81;
                raores[2][di][fi] = rao[2] / amp;
                raores[3][di][fi] = rao[3] / (amp * 180 * k / Math.PI);
                raores[4][di][fi] = rao[4] / (amp * 180 * k / Math.PI);
                if (directions[di] == 0.0) {
                    phase = data.calculatePhase(peaks, omega);
                    for (int dof = 0; dof < 6; dof++) {
                        stern[dof][fi] = phase[dof];
                    }
                }

                if (directions[di] == Math.PI / 2.0) {
                    phase = data.calculatePhase(peaks, omega);
                    for (int dof = 0; dof < 6; dof++) {
                        beam[dof][fi] = phase[dof];
                    }
                }
            }
        }

        // for all dof (minus surge, sway and yaw)
        // make plot of RAO response-vs-freq for 5 direction (45 deg increment)
        Color[] colors = new Color[]{Color.BLACK, Color.BLUE, Color.GREEN, Color.ORANGE, Color.RED, Color.YELLOW};

        ArrayList<PdfPCell> ret = new ArrayList<PdfPCell>();
        for (int i = 2; i < 5; i++) {
            PdfPTable info = new PdfPTable(1);

            PdfPCell c2 = new PdfPCell();
            MultiPlotCellEvent n2 = new MultiPlotCellEvent();
            for (int di = 0; di < directions.length; di++) {
                n2.addPlot("" + Math.round(directions[di] * 180 / Math.PI * 10) / 10, periods, raores[i][di], colors[di]);
            }
            String title = "";
            String unit = "";

            if (i == 2) {
                title = "Heave";
                unit = "[-]";
            }
            if (i == 3) {
                title = "Roll";
                unit = "[-]";
            }

            if (i == 4) {
                title = "Pitch";
                unit = "[-]";
            }

            n2.MakePlot(title, "Wave period in [s]", "RAO " + title + unit, true);
            c2.setCellEvent(n2);
            c2.setFixedHeight(500.0f);
            info.addCell(c2);
            ret.add(new PdfPCell(info));
        }

//        PdfPTable tableStern = new PdfPTable(1);
//        PdfPCell cellStern = new PdfPCell();
//        MultiPlotCellEvent eventStern = new MultiPlotCellEvent();
//        PdfPTable tableBeam = new PdfPTable(1);
//        PdfPCell cellBeam = new PdfPCell();
//        MultiPlotCellEvent eventBeam = new MultiPlotCellEvent();
//
//        eventStern.addPlot("Heave", periods, stern[2], colors[0]);
//        eventStern.addPlot("Pitch", periods, stern[4], colors[1]);
//        eventStern.addPlot("Heave long wave  limit", new double[]{periods[0], periods[periods.length - 1]}, new double[]{0, 0}, Color.RED);
//        eventStern.addPlot("Pitch long wave  limit", new double[]{periods[0], periods[periods.length - 1]}, new double[]{-90, -90}, Color.ORANGE);
//        eventStern.MakePlot("Phase plot in stern waves", "Wave period in [s]", "Phase lag of motion relative to wave elevation [deg]", true);
//        cellStern.setCellEvent(eventStern);
//        cellStern.setFixedHeight(500.0f);
//        tableStern.addCell(cellStern);
//
//        eventBeam.addPlot("Heave", periods, beam[2], colors[0]);
//        eventBeam.addPlot("Roll", periods, beam[3], colors[1]);
//        eventBeam.addPlot("Heave long wave  limit", new double[]{periods[0], periods[periods.length - 1]}, new double[]{0, 0}, Color.RED);
//        eventBeam.addPlot("Roll long wave  limit", new double[]{periods[0], periods[periods.length - 1]}, new double[]{90, 90}, Color.ORANGE);
//        eventBeam.MakePlot("Phase plot in beam waves", "Wave period in [s]", "Phase lag of motion relative to wave elevation [deg]", true);
//        cellBeam.setCellEvent(eventBeam);
//        cellBeam.setFixedHeight(500.0f);
//        tableBeam.addCell(cellBeam);
//
//        ret.add(new PdfPCell(tableStern));
//        ret.add(new PdfPCell(tableBeam));
        return ret;
    }

    private static PdfPCell createCurrentData(BaseExample data) {

        double[] dirs = {0, 45, 90, 135, 180};
        double currentVelocity = 1.0;
        int duration = 300;
        double dT = 5;
        Color[] colors = new Color[]{Color.BLACK, Color.BLUE, Color.GREEN, Color.ORANGE, Color.RED};
        ArrayList<positionData> res = new ArrayList<positionData>();
        for (int i = 0; i < 5; i++) {
            res.add(data.runCurrentTest(duration, dT, dirs[i], currentVelocity));
        }

        int nSamples = res.get(0).n.seriesLength();
        double[] speedInCurrentDir = new double[nSamples];
        double[] u, v, yaw;
        double dirRad, northVel, eastVel;

        PdfPTable info = new PdfPTable(1);
        PdfPCell c2 = new PdfPCell();
        MultiPlotCellEvent n2 = new MultiPlotCellEvent();
        for (int i = 0; i < 5; i++) {
            u = res.get(i).u.toArrayY();
            v = res.get(i).v.toArrayY();
            yaw = res.get(i).r.toArrayY();
            dirRad = dirs[i] * Math.PI / 180;
            for (int j = 0; j < nSamples; j++) {
                northVel = u[j] * Math.cos(yaw[j]) + v[j] * Math.sin(yaw[j]);
                eastVel = u[j] * Math.sin(yaw[j]) + v[j] * Math.cos(yaw[j]);
                speedInCurrentDir[j] = (northVel * Math.cos(dirRad) + eastVel * Math.sin(dirRad));
            }
            n2.addPlot("Current heading " + dirs[i] + " deg", res.get(0).n.toArrayX(), speedInCurrentDir, colors[i]);
        }

        String title = "Current reponse test";
        n2.MakePlot(title, "Time [s]", "Speed in current direction, normalized on current velocity", true);
        c2.setCellEvent(n2);
        c2.setFixedHeight(500.0f);
        info.addCell(c2);

        return new PdfPCell(info);
    }

    private static PdfPCell createWindData(BaseExample data) {

        double[] dirs = {0, 45, 90, 135, 180};
        double windSpeed = 20.0;
        int duration = 100;
        double dT = 1;
        Color[] colors = new Color[]{Color.BLACK, Color.BLUE, Color.GREEN, Color.ORANGE, Color.RED};
        ArrayList<positionData> res = new ArrayList<positionData>();
        for (int i = 0; i < 5; i++) {
            res.add(data.runWindTest(duration, dT, dirs[i], windSpeed));
        }

        double[] north, east;
        PdfPTable info = new PdfPTable(1);
        PdfPCell c2 = new PdfPCell();
        MultiPlotCellEvent n2 = new MultiPlotCellEvent();
        for (int i = 0; i < 5; i++) {
            north = res.get(i).n.toArrayY();
            east = res.get(i).e.toArrayY();
            n2.addPlot("Wind heading " + dirs[i] + " deg", east, north, colors[i]);
        }

        String title = "Wind response test (20 m/s wind for 100s)";
        n2.MakePlot(title, "East position [m]", "North position [m]", true);
        c2.setCellEvent(n2);
        c2.setFixedHeight(500.0f);
        info.addCell(c2);

        return new PdfPCell(info);
    }

    private static PdfPCell createWaveDriftData(BaseExample data) {

        double[] dirs = {0, 45, 90, 135, 180};
        double waveAmplitude = 1;//meter
        double wavePeriod = 8;//seconds
        int duration = 600;
        double dT = 1;
        Color[] colors = new Color[]{Color.BLACK, Color.BLUE, Color.GREEN, Color.ORANGE, Color.RED};
        ArrayList<positionData> res = new ArrayList<positionData>();
        for (int i = 0; i < 5; i++) {
            res.add(data.runWaveDriftTest(duration, dT, dirs[i], waveAmplitude, wavePeriod));
        }

        double[] north, east;
        PdfPTable info = new PdfPTable(1);
        PdfPCell c2 = new PdfPCell();
        MultiPlotCellEvent n2 = new MultiPlotCellEvent();
        for (int i = 0; i < 5; i++) {
            north = res.get(i).n.toArrayY();
            east = res.get(i).e.toArrayY();
            n2.addPlot("Waves heading " + dirs[i] + " deg", east, north, colors[i]);
        }

        String title = "Wave drift test (1m waves, 8s period, 100s duration)";
        n2.MakePlot(title, "East position [m]", "North position [m]", true);
        c2.setCellEvent(n2);
        c2.setFixedHeight(500.0f);
        info.addCell(c2);

        return new PdfPCell(info);
    }

    private static PdfPCell createRollDecayData(BaseExample data) {
        PdfPTable info = new PdfPTable(2);

        fullSpeed2 heel = data.runHeelDecay();
        heel.theta.scale(180.0 / Math.PI);
        ArrayList<ArrayList<Double>> extrema = heel.theta.getExtrema();

        double avgPeriod = BaseExample.TimeSeries.avgArrayList(BaseExample.TimeSeries.diffArrayList(extrema.get(0)));
        System.out.println("Natural period in roll: " + 2 * avgPeriod);

        PdfPCell c2 = new PdfPCell();
        MultiPlotCellEvent n2 = new MultiPlotCellEvent();
        heel.theta.scale(180 / 3.1416);
        n2.addPlot("heel angle", heel.theta.toArrayX(), heel.theta.toArrayY());
        n2.MakePlot(String.format("Heel decay test from %.1f degrees", data.getReportSpec().getHeelDecayStartAngleDegrees()), "Time in seconds", "Degrees heel");
        c2.setCellEvent(n2);
        c2.setFixedHeight(500.0f);
        c2.setColspan(2);

        info.addCell(c2);

        PdfPCell t = new PdfPCell(new Phrase("Natural period in roll [s]"));
        t.setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        info.addCell(t);
        info.addCell(new PdfPCell(new Phrase("" + format(2 * avgPeriod) + "/" + data.getOSCHullDataSpec().getRollPeriod_s())));
        data.setNaturalRollPeriod(2 * avgPeriod);

        double Inertia = 0;

        try {
            String path = data.getHull().getModelData() + "\\" + data.getHull().getModelFileStem() + ".identcache0";
            Path abspath = Paths.get(System.getProperty("user.dir"), path);
            RandomAccessFile rFile = new RandomAccessFile(abspath.toString(), "r");
            rFile.seek(rFile.length() - 15 * Double.SIZE / Byte.SIZE);
            Inertia = Double.longBitsToDouble(Long.reverseBytes(rFile.readLong()));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VesselReport.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VesselReport.class.getName()).log(Level.SEVERE, null, ex);
        }

        String path = data.getHull().getModelData() + "\\" + data.getHull().getModelFileStem() + ".re7";
        Path abspath = Paths.get(System.getProperty("user.dir"), path);
        double[][] m = VeresFile.getInertiaMatrix(new ByteArrayInputStream(VeresFile.readFileAsString(abspath.toString()).getBytes()));

        PdfPCell t2 = new PdfPCell(new Phrase("Inertia in roll [kg*m^2]"));
        t2.setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        info.addCell(t2);
        info.addCell(new PdfPCell(new Phrase(format(Inertia + m[3][3]))));

        PdfPCell r44 = new PdfPCell(new Phrase("Roll radius of gyration  r44 [m] / % Breadth"));
        r44.setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        info.addCell(r44);

        Font font = new Font();
        font.setColor(data.getVeresFile().hydFile.r44 < data.getVeresFile().hydFile.Breadth / 3 ? BaseColor.RED : BaseColor.BLACK);
        double percBreadth = 100 * data.getVeresFile().hydFile.r44 / data.getVeresFile().hydFile.Breadth;
        info.addCell(new PdfPCell(new Phrase(String.format("%.1f - %.1f %%", data.getVeresFile().hydFile.r44, percBreadth), font)));

        return new PdfPCell(info);
    }

    private static PdfPCell createStabilityData(BaseExample data) {

        double weight_in_Tonnes = 2000;
        VeresFile veres = new VeresFile(data.getHull().getModelData() + "/" + data.getHull().getModelFileStem());
        VeresFile.HydFile f = veres.hydFile;
        double[] runHeelResults = data.runHeel(weight_in_Tonnes * 1000 * 9.81, f.Breadth / 2.0);;
        double heel_angle = runHeelResults[0];

        double endVelocity = Math.sqrt(runHeelResults[1] * runHeelResults[1]
                + runHeelResults[2] * runHeelResults[2]
                + runHeelResults[3] * runHeelResults[3]
        );

        double[] x = new double[]{0, heel_angle * 180 / Math.PI};
        double[] y = new double[]{0, weight_in_Tonnes * f.Breadth / 2.0};

        PdfPTable info = new PdfPTable(2);
        PdfPTable info2 = new PdfPTable(2);

        PdfPCell c1 = new PdfPCell();
        LinearHydrostatics n1 = new LinearHydrostatics(f);
        c1.setCellEvent(n1);
        c1.setFixedHeight(400.0f);
        c1.setColspan(2);
        info2.addCell(c1);
        PdfPCell c = new PdfPCell(new Phrase("c [Nm/deg]"));
        c.setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        info2.addCell(c);
        double restoring_coefficient = weight_in_Tonnes * 1000 * 9.81 * f.Breadth / 2.0 / (heel_angle * 180 / Math.PI);
        info2.addCell(new PdfPCell(new Phrase(String.format("%.1f", restoring_coefficient))));

        PdfPCell kg = new PdfPCell(new Phrase("KG [m]"));
        kg.setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        info2.addCell(kg);
        info2.addCell(new PdfPCell(new Phrase("" + f.VCG)));

        PdfPCell gm = new PdfPCell(new Phrase("GM [m]"));
        gm.setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        info2.addCell(gm);
        info2.addCell(new PdfPCell(new Phrase("" + f.GMt)));

        PdfPCell velocity = new PdfPCell(new Phrase("End velocity [m/s]"));
        velocity.setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        info2.addCell(velocity);
        Font font = new Font();
        font.setColor(endVelocity > .2 ? BaseColor.RED : BaseColor.BLACK);
        info2.addCell(new PdfPCell(new Phrase(String.format("%.1f", endVelocity), font)));

        PdfPCell yaw = new PdfPCell(new Phrase("End yaw velocity [RPM]"));
        yaw.setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        info2.addCell(yaw);
        info2.addCell(new PdfPCell(new Phrase(String.format("%.1f", runHeelResults[6] * 60 / (2 * Math.PI)))));

        PdfPCell c2 = new PdfPCell();
        MultiPlotCellEvent n2 = new MultiPlotCellEvent();
        n2.addPlot("heel angle", x, y);
        n2.MakePlot("Simulated restoring moment for heel angle\n" + weight_in_Tonnes + " tonnes downward force at " + Math.round(f.Breadth / 2.0 * 100.0f) / 100.0f + "m arm", "Degrees heel", "Moment in tonnes*meter");
        c2.setCellEvent(n2);
        c2.setFixedHeight(400.0f);

        info.addCell(new PdfPCell(info2));
        info.addCell(c2);

        PdfPCell empty = new PdfPCell(new Phrase(""));
        empty.setColspan(2);
        info.addCell(empty);

        return new PdfPCell(info);
    }

    private static PdfPCell createTurningCircleData(BaseExample data) {

        VeresFile veres = new VeresFile(data.getHull().getModelData() + "/" + data.getHull().getModelFileStem());
        VeresFile.HydFile f = veres.hydFile;

        double b = f.Breadth;
        double l = f.Lpp;

        PdfPTable info = new PdfPTable(5);

        PdfPCell plot = new PdfPCell();
        turingCircle port = data.runPortTurningCirlceTrial();
        turingCircle stbd = data.runStarboardTurningCirlceTrial();

        MultiPlotCellEvent event = new MultiPlotCellEvent();

        event.addShipPlot("port", l, b, port);
        event.addShipPlot("starboard", l, b, stbd);
        event.addPlot("port", port.e.toArrayY(), port.n.toArrayY(), Color.RED);
        event.addPlot("starboard", stbd.e.toArrayY(), stbd.n.toArrayY(), Color.GREEN);
        event.MakePlot("Turning circle", "East [m]", "North  [m]");
        plot.setCellEvent(event);
        plot.setColspan(3);
        plot.setFixedHeight(300.0f);

        PdfPTable infoPortCircle = new PdfPTable(2);
        PdfPTable infoStbdCircle = new PdfPTable(2);

        long advPort = Math.round(findAdvance(port));
        long trfPort = Math.round(findTransfer(port));
        long tdPort = Math.round(findTacticalDiameter(port));

        int start_port = findTurnStart(port);
        double speed_port = port.u.getY(start_port);

        infoPortCircle.addCell(new Phrase("Advance [m]"));
        infoPortCircle.addCell(new Phrase(Long.toString(advPort)));
        infoPortCircle.addCell(new Phrase("Transfer [m]"));
        infoPortCircle.addCell(new Phrase(Long.toString(trfPort)));
        infoPortCircle.addCell(new Phrase("Tactical diameter [m]"));
        infoPortCircle.addCell(new Phrase(Long.toString(tdPort)));
        infoPortCircle.addCell(new Phrase("Speed at wheel-over [Kn]"));
        infoPortCircle.addCell(new Phrase(format(speed_port / 0.5144)));
        infoPortCircle.addCell(new Phrase("Rudder angle [deg]"));
        infoPortCircle.addCell(new Phrase(format(port.delta.getLastY())));
        infoPortCircle.addCell(new Phrase("Outward heel angle [deg]"));
        infoPortCircle.addCell(new Phrase(format(port.theta.maxY() * 180 / Math.PI)));
        infoPortCircle.addCell(new Phrase("Inward heel angle [deg]"));
        infoPortCircle.addCell(new Phrase(format(port.theta.minY() * 180 / Math.PI)));

        long advStbd = Math.round(findAdvance(stbd));
        long trfStbd = Math.round(findTransfer(stbd));
        long tdStbd = Math.round(findTacticalDiameter(stbd));

        int start_stbd = findTurnStart(stbd);
        double speed_stbd = port.u.getY(start_stbd);

        infoStbdCircle.addCell(new Phrase("Advance [m]"));
        infoStbdCircle.addCell(new Phrase(Long.toString(advStbd)));
        infoStbdCircle.addCell(new Phrase("Transfer [m]"));
        infoStbdCircle.addCell(new Phrase(Long.toString(trfStbd)));
        infoStbdCircle.addCell(new Phrase("Tactical diameter [m]"));
        infoStbdCircle.addCell(new Phrase(Long.toString(tdStbd)));
        infoStbdCircle.addCell(new Phrase("Speed at wheel-over [Kn]"));
        infoStbdCircle.addCell(new Phrase(format(speed_stbd / 0.5144)));
        infoStbdCircle.addCell(new Phrase("Rudder angle [deg]"));
        infoStbdCircle.addCell(new Phrase(format(stbd.delta.getLastY())));
        infoStbdCircle.addCell(new Phrase("Outward heel angle [deg]"));
        infoStbdCircle.addCell(new Phrase(format(stbd.theta.minY() * 180 / Math.PI)));
        infoStbdCircle.addCell(new Phrase("Inward heel angle [deg]"));
        infoStbdCircle.addCell(new Phrase(format(stbd.theta.maxY() * 180 / Math.PI)));

        info.addCell(new PdfPCell(infoPortCircle));
        info.addCell(plot);
        info.addCell(new PdfPCell(infoStbdCircle));

        plot = new PdfPCell();
        port = data.runPortAccelereatedTurningTrial();
        stbd = data.runStarboardAccelereatedTurningTrial();

        event = new MultiPlotCellEvent();
        //event.addShipPlot("port", l, b, port);
        // event.addShipPlot("starboard", l, b, stbd);
        event.addPlot("port", port.e.toArrayY(), port.n.toArrayY(), Color.RED);
        event.addPlot("starboard", stbd.e.toArrayY(), stbd.n.toArrayY(), Color.GREEN);
        event.MakePlot("Turning circle From Zero Speed", "East [m]", "North  [m]");
        plot.setCellEvent(event);
        plot.setColspan(3);
        plot.setFixedHeight(300.0f);

        infoPortCircle = new PdfPTable(2);
        infoStbdCircle = new PdfPTable(2);

        advPort = Math.round(findAdvance(port));
        trfPort = Math.round(findTransfer(port));
        tdPort = Math.round(findTacticalDiameter(port));

        infoPortCircle.addCell(new Phrase("Advance [m]"));
        infoPortCircle.addCell(new Phrase(Long.toString(advPort)));
        infoPortCircle.addCell(new Phrase("Transfer [m]"));
        infoPortCircle.addCell(new Phrase(Long.toString(trfPort)));
        infoPortCircle.addCell(new Phrase("Tactical diameter [m]"));
        infoPortCircle.addCell(new Phrase(Long.toString(tdPort)));
        infoPortCircle.addCell(new Phrase("Rudder angle [deg]"));
        infoPortCircle.addCell(new Phrase(format(port.delta.getLastY())));
        infoPortCircle.addCell(new Phrase("Outward heel angle [deg]"));
        infoPortCircle.addCell(new Phrase(format(port.theta.maxY() * 180 / Math.PI)));
        infoPortCircle.addCell(new Phrase("Inward heel angle [deg]"));
        infoPortCircle.addCell(new Phrase(format(port.theta.minY() * 180 / Math.PI)));

        advStbd = Math.round(findAdvance(stbd));
        trfStbd = Math.round(findTransfer(stbd));
        tdStbd = Math.round(findTacticalDiameter(stbd));

        infoStbdCircle.addCell(new Phrase("Advance [m]"));
        infoStbdCircle.addCell(new Phrase(Long.toString(advStbd)));
        infoStbdCircle.addCell(new Phrase("Transfer [m]"));
        infoStbdCircle.addCell(new Phrase(Long.toString(trfStbd)));
        infoStbdCircle.addCell(new Phrase("Tactical diameter [m]"));
        infoStbdCircle.addCell(new Phrase(Long.toString(tdStbd)));
        infoStbdCircle.addCell(new Phrase("Rudder angle [deg]"));
        infoStbdCircle.addCell(new Phrase(format(stbd.delta.getLastY())));
        infoStbdCircle.addCell(new Phrase("Outward heel angle [deg]"));
        infoStbdCircle.addCell(new Phrase(format(stbd.theta.minY() * 180 / Math.PI)));
        infoStbdCircle.addCell(new Phrase("Inward heel angle [deg]"));
        infoStbdCircle.addCell(new Phrase(format(stbd.theta.maxY() * 180 / Math.PI)));

        info.addCell(new PdfPCell(infoPortCircle));
        info.addCell(plot);
        info.addCell(new PdfPCell(infoStbdCircle));

        return new PdfPCell(info);
    }

    private static PdfPCell createCrashStopData(BaseExample data) {

        VeresFile.HydFile f = data.getVeresFile().hydFile;

        double b = f.Breadth;
        double l = f.Lpp;

        PdfPTable info = new PdfPTable(2);
        PdfPTable info2 = new PdfPTable(4);
        PdfPTable info3 = new PdfPTable(4);
        PdfPTable info4 = new PdfPTable(4);
        PdfPTable info5 = new PdfPTable(4);
        PdfPTable info6 = new PdfPTable(4);

        System.out.println("Run max speed trial");
        fullSpeed2 fullForward = data.runMaxSpeed();
        System.out.println("Run max speed reverse trial");
        fullSpeed2 fullBackward = data.runMaxSpeedReverse();
        System.out.println("Run crash stop trial");
        crashStop d = data.runCrashStop(6.0);

        System.out.println("Run DP crab trial ");
        fullSpeed2 fullSide = data.runDPcrab(1);
        System.out.println("Run DP piruette");
        fullSpeed2 fullYaw = data.runDPpirouette();

        System.out.println("Run decay surge");
        fullSpeed2 decaySurge = data.runDecay(2.5 * 0.5144, 0, 0);
        System.out.println("Run decay sway");
        fullSpeed2 decaySway = data.runDecay(0, 2.5 * 0.5144, 0);
        System.out.println("Run decay yaw");
        fullSpeed2 decayYaw = data.runDecay(0, 0, 60 / 60 * Math.PI / 180);

        double decayTimeSurge = Math.round(decaySurge.d.getLastX() * 100) / 100.0;
        double decayTimeSway = Math.round(decaySway.d.getLastX() * 100) / 100.0;
        double decayTimeYaw = Math.round(decayYaw.d.getLastX() * 100) / 100.0;

        PdfPCell[] cellsDecayKey = new PdfPCell[4];
        PdfPCell[] cellsDecayVal = new PdfPCell[4];
        cellsDecayKey[0] = new PdfPCell(new Phrase("Decay time to 5% of initial value from:"));
        cellsDecayKey[0].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsDecayKey[1] = new PdfPCell(new Phrase("Surge 2.5 Knots"));
        cellsDecayKey[1].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsDecayKey[2] = new PdfPCell(new Phrase("Sway 2.5 Knots"));
        cellsDecayKey[2].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsDecayKey[3] = new PdfPCell(new Phrase("Yaw 60 deg/min"));
        cellsDecayKey[3].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));

        cellsDecayVal[0] = new PdfPCell(new Phrase(""));
        cellsDecayVal[1] = new PdfPCell(new Phrase("" + decayTimeSurge + " seconds"));
        cellsDecayVal[2] = new PdfPCell(new Phrase("" + decayTimeSway + " seconds"));
        cellsDecayVal[3] = new PdfPCell(new Phrase("" + decayTimeYaw + " seconds"));

        for (int i = 0; i < 4; i++) {
            info6.addCell(cellsDecayKey[i]);
        }
        for (int i = 0; i < 4; i++) {
            info6.addCell(cellsDecayVal[i]);
        }

        PdfPCell key0 = new PdfPCell(new Phrase("Full speed forward: " + format(fullForward.u.getLastY() / 0.5144) + " Knots"));
        key0.setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        key0.setColspan(2);

        PdfPCell[] cellsForward = new PdfPCell[fullForward.getCommand().size() * 4 + 4];
        cellsForward[0] = new PdfPCell(new Phrase("Name"));
        cellsForward[0].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsForward[1] = new PdfPCell(new Phrase("RPM"));
        cellsForward[1].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsForward[2] = new PdfPCell(new Phrase("Pitch"));
        cellsForward[2].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsForward[3] = new PdfPCell(new Phrase("Azimuth"));
        cellsForward[3].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        int counter = 4;
        for (Map.Entry<String, fullSpeed2.propulsorCommand> p : fullForward.getCommand().entrySet()) {

            cellsForward[counter + 0] = new PdfPCell(new Phrase(p.getKey()));
            cellsForward[counter + 0].setBackgroundColor(BaseColor.LIGHT_GRAY);
            cellsForward[counter + 1] = new PdfPCell(new Phrase(Double.toString(p.getValue().rpm)));
            if (p.getValue().isVSP) {
                cellsForward[counter + 2] = new PdfPCell(new Phrase(format(p.getValue().pitch) + " [-]"));
            } else {
                cellsForward[counter + 2] = new PdfPCell(new Phrase(format(p.getValue().pitch) + " [deg]"));
            }

            cellsForward[counter + 3] = new PdfPCell(new Phrase(Double.toString(p.getValue().angle) + " [deg]"));
            counter += 4;
        }

        PdfPCell key1 = new PdfPCell(new Phrase("Full speed backward: " + format(fullBackward.u.getLastY() / 0.5144) + " Knots    -    Crash stop distance/time: " + format(d.getCrashStopDistance()) + " m / " + format(d.getCrashStopTime() / 60) + " min"));
        key1.setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        key1.setColspan(2);

        PdfPCell[] cellsReverse = new PdfPCell[fullBackward.getCommand().size() * 4 + 4];
        cellsReverse[0] = new PdfPCell(new Phrase("Name"));
        cellsReverse[0].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsReverse[1] = new PdfPCell(new Phrase("RPM"));
        cellsReverse[1].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsReverse[2] = new PdfPCell(new Phrase("Pitch"));
        cellsReverse[2].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsReverse[3] = new PdfPCell(new Phrase("Azimuth"));
        cellsReverse[3].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));

        counter = 4;
        for (Map.Entry<String, fullSpeed2.propulsorCommand> p : fullBackward.getCommand().entrySet()) {

            cellsReverse[counter + 0] = new PdfPCell(new Phrase(p.getKey()));
            cellsReverse[counter + 0].setBackgroundColor(BaseColor.LIGHT_GRAY);
            cellsReverse[counter + 1] = new PdfPCell(new Phrase(Double.toString(p.getValue().rpm)));
            if (p.getValue().isVSP) {
                cellsReverse[counter + 2] = new PdfPCell(new Phrase(format(p.getValue().pitch) + " [-]"));
            } else {
                cellsReverse[counter + 2] = new PdfPCell(new Phrase(format(p.getValue().pitch) + " [deg]"));
            }
            cellsReverse[counter + 3] = new PdfPCell(new Phrase(Double.toString(p.getValue().angle) + " [deg]"));
            counter += 4;
        }

        PdfPCell key00 = new PdfPCell(new Phrase("DP Crab maneuver sideways speed: " + format(fullSide.v.getLastY() / 0.5144) + " Knots"));
        key00.setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        key00.setColspan(2);

        PdfPCell[] cellsDPCrab = new PdfPCell[fullSide.getCommand().size() * 4 + 4];
        cellsDPCrab[0] = new PdfPCell(new Phrase("Name"));
        cellsDPCrab[0].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsDPCrab[1] = new PdfPCell(new Phrase("RPM"));
        cellsDPCrab[1].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsDPCrab[2] = new PdfPCell(new Phrase("Pitch"));
        cellsDPCrab[2].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsDPCrab[3] = new PdfPCell(new Phrase("Azimuth"));
        cellsDPCrab[3].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        counter = 4;
        for (Map.Entry<String, fullSpeed2.propulsorCommand> p : fullSide.getCommand().entrySet()) {
            cellsDPCrab[counter + 0] = new PdfPCell(new Phrase(p.getKey()));
            cellsDPCrab[counter + 0].setBackgroundColor(BaseColor.LIGHT_GRAY);
            cellsDPCrab[counter + 1] = new PdfPCell(new Phrase(Double.toString(p.getValue().rpm)));
            if (p.getValue().isVSP) {
                cellsDPCrab[counter + 2] = new PdfPCell(new Phrase(format(p.getValue().pitch) + " [-]"));
            } else {
                cellsDPCrab[counter + 2] = new PdfPCell(new Phrase(format(p.getValue().pitch) + " [deg]"));
            }
            cellsDPCrab[counter + 3] = new PdfPCell(new Phrase(Double.toString(p.getValue().angle) + "[deg]"));
            counter += 4;
        }

        PdfPCell key01 = new PdfPCell(new Phrase("DP Pirouette maneuver yaw speed: " + format(fullYaw.r.getLastY() * 180 / Math.PI * 60) + " Deg/min"));
        key01.setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        key01.setColspan(2);

        PdfPCell[] cellsDPPir = new PdfPCell[fullYaw.getCommand().size() * 4 + 4];
        cellsDPPir[0] = new PdfPCell(new Phrase("Name"));
        cellsDPPir[0].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsDPPir[1] = new PdfPCell(new Phrase("RPM"));
        cellsDPPir[1].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsDPPir[2] = new PdfPCell(new Phrase("Pitch"));
        cellsDPPir[2].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        cellsDPPir[3] = new PdfPCell(new Phrase("Azimuth"));
        cellsDPPir[3].setBackgroundColor(new BaseColor(216 / 255.0f, 208f / 255, 199f / 255));
        counter = 4;
        for (Map.Entry<String, fullSpeed2.propulsorCommand> p : fullYaw.getCommand().entrySet()) {
            cellsDPPir[counter + 0] = new PdfPCell(new Phrase(p.getKey()));
            cellsDPPir[counter + 0].setBackgroundColor(BaseColor.LIGHT_GRAY);
            cellsDPPir[counter + 1] = new PdfPCell(new Phrase(Double.toString(p.getValue().rpm)));
            if (p.getValue().isVSP) {
                cellsDPPir[counter + 2] = new PdfPCell(new Phrase(format(p.getValue().pitch) + " [-]"));
            } else {
                cellsDPPir[counter + 2] = new PdfPCell(new Phrase(format(p.getValue().pitch) + " [deg]"));
            }
            cellsDPPir[counter + 3] = new PdfPCell(new Phrase(Double.toString(p.getValue().angle) + " [deg]"));
            counter += 4;
        }

        //PdfPCell plot = new PdfPCell();
        int startIndex = d.getStartIndex();
        //double startX = d.n.getY(startIndex);
        //double startY = d.e.getY(startIndex);

        //double stopX = d.n.getLastY();
        //double stopY = d.e.getLastY();
        //MultiPlotCellEvent event = new MultiPlotCellEvent();
        //event.addShipPlot("Crash stop", l,b,d);
        //event.addPlot("starboard", d.e.toArrayY(), d.n.toArrayY(),Color.BLACK);
        //event.MakePlot("Crash Stop", "East [m]", "North  [m]");
        //event.addAnnotation("start",startX ,startY );
        //event.addAnnotation("stop",stopX ,stopY );
        //plot.setCellEvent(event);
        //plot.setFixedHeight(300.0f);
        //plot.setColspan(1);
        info.addCell(key0);
        for (int i = 0; i < cellsForward.length; i++) {
            info2.addCell(cellsForward[i]);
        }
        PdfPCell info2Cell = new PdfPCell(info2);
        info2Cell.setColspan(2);
        info.addCell(info2Cell);

        info.addCell(key1);
        for (int i = 0; i < cellsReverse.length; i++) {
            info3.addCell(cellsReverse[i]);
        }
        PdfPCell info3Cell = new PdfPCell(info3);
        info3Cell.setColspan(2);
        info.addCell(info3Cell);

        info.addCell(key00);
        for (int i = 0; i < cellsDPCrab.length; i++) {
            info4.addCell(cellsDPCrab[i]);
        }
        PdfPCell info4Cell = new PdfPCell(info4);
        info4Cell.setColspan(2);
        info.addCell(info4Cell);

        info.addCell(key01);
        for (int i = 0; i < cellsDPPir.length; i++) {
            info5.addCell(cellsDPPir[i]);
        }
        PdfPCell info5Cell = new PdfPCell(info5);
        info5Cell.setColspan(2);
        info.addCell(info5Cell);

        PdfPCell info6Cell = new PdfPCell(info6);
        info6Cell.setColspan(2);
        info.addCell(info6Cell);

        return new PdfPCell(info);
    }

    private static /*ArrayList<*/ PdfPCell/*>*/ createFullSpeedData(BaseExample data) {
        ArrayList<PdfPCell> retval = new ArrayList<PdfPCell>();
        PdfPTable info = new PdfPTable(1);
        PdfPCell plot = new PdfPCell();

        fullSpeed d = data.runFullSpeedTrial();

        double[] dd = d.power.toArrayY();
        for (int i = 0; i < dd.length; i++) {
            dd[i] /= 1e6;
        }

        PlotCellEvent plotEvent = new PlotCellEvent(d.speed_knots.toArrayY(), dd, "Speed vs power", "Speed in knots", "Power [MW]");
        plot.setCellEvent(plotEvent);
        plot.setFixedHeight(300.0f);
        info.addCell(plot);

        PdfPTable info2 = new PdfPTable(1);
        PdfPCell plot2 = new PdfPCell();

        fullSpeed2 d2 = data.runMaxSpeed();
        double[] xx = d2.u.toArrayX();
        double[] yy = d2.u.toArrayY();
        double maxSpeed995 = yy[yy.length - 1] * .995; //99.5% of max speed
        int index = 0;
        for (int i = 0; i < xx.length; i++) {
            if (yy[i] >= maxSpeed995) {
                index = i;
                break;
            }
        }

        int maxIndex = (int) (index / 0.8); //99.5% of max speed should be at 80% of the graph
        maxIndex = maxIndex < xx.length - 1 ? maxIndex : xx.length - 1;
        double[] xxx = new double[maxIndex + 1];
        double[] yyy = new double[maxIndex + 1];

        for (int i = 0; i < xxx.length; i++) {
            xxx[i] = xx[i];
            yyy[i] = yy[i] / 0.5144;
        }

        PlotCellEvent plotEvent2 = new PlotCellEvent(xxx, yyy, "Acceleration to max speed", "Time in seconds", "Speed in knots");
        plot.setCellEvent(plotEvent2);
//        plot.setFixedHeight(200.0f);
        info.addCell(plot);

        retval.add(new PdfPCell(info));
        retval.add(new PdfPCell(info2));

        return new PdfPCell(info);
    }

    private static PdfPCell createDampingData(BaseExample data) {
        PdfPTable info = new PdfPTable(2);

        PdfPTable infoDP = new PdfPTable(4);
        infoDP.addCell(new Phrase(""));
        infoDP.addCell(new Phrase("Surge"));
        infoDP.addCell(new Phrase("Sway"));
        infoDP.addCell(new Phrase("Yaw"));

        double[] dp = data.getHull().getDpDampingSurge();
        infoDP.addCell(new Phrase("Surge"));
        infoDP.addCell(new Phrase(String.format("%s", dp[0])));
        infoDP.addCell(new Phrase(String.format("%s", dp[1])));
        infoDP.addCell(new Phrase(String.format("%s", dp[2])));

        dp = data.getHull().getDpDampingSway();
        infoDP.addCell(new Phrase("Sway"));
        infoDP.addCell(new Phrase(String.format("%s", dp[0])));
        infoDP.addCell(new Phrase(String.format("%s", dp[1])));
        infoDP.addCell(new Phrase(String.format("%s", dp[2])));

        dp = data.getHull().getDpDampingYaw();
        infoDP.addCell(new Phrase("Yaw"));
        infoDP.addCell(new Phrase(String.format("%s", dp[0])));
        infoDP.addCell(new Phrase(String.format("%s", dp[1])));
        infoDP.addCell(new Phrase(String.format("%s", dp[2])));

        PdfPCell header = new PdfPCell(new Phrase("Dynamic positioning model"));
        header.setColspan(2);
        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
        info.addCell(header);
        info.addCell(new Phrase("Damping matrix in DP"));
        info.addCell(new PdfPCell(infoDP));

        header = new PdfPCell(new Phrase("Maneuver model"));
        header.setColspan(2);
        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
        info.addCell(header);

        PdfPTable infoManInertia = new PdfPTable(4);

        infoManInertia.addCell(new Phrase(""));
        infoManInertia.addCell(new Phrase("Surge"));
        infoManInertia.addCell(new Phrase("Sway"));
        infoManInertia.addCell(new Phrase("Yaw"));

        infoManInertia.addCell(new Phrase("Surge"));
        infoManInertia.addCell(new Phrase(String.format("%s", data.getHull().getXud_man())));
        infoManInertia.addCell(new Phrase("0"));
        infoManInertia.addCell(new Phrase("0"));

        infoManInertia.addCell(new Phrase("Sway"));
        infoManInertia.addCell(new Phrase("0"));
        infoManInertia.addCell(new Phrase(String.format("%s", data.getHull().getManYvd())));
        infoManInertia.addCell(new Phrase("0"));

        infoManInertia.addCell(new Phrase("Yaw"));
        infoManInertia.addCell(new Phrase("0"));
        infoManInertia.addCell(new Phrase("0"));
        infoManInertia.addCell(new Phrase(String.format("%s", data.getHull().getManNrd())));

        info.addCell(new Phrase("Inertia matrix for maneouver model"));
        info.addCell(new PdfPCell(infoManInertia));

        PdfPTable infoMan = new PdfPTable(2);
        infoMan.addCell(new Phrase("Xrr"));
        infoMan.addCell(new Phrase(String.format("%s", data.getHull().getManXrr())));
        infoMan.addCell(new Phrase("Xvr"));
        infoMan.addCell(new Phrase(String.format("%s", data.getHull().getXvr_man())));
        infoMan.addCell(new Phrase("Xvv"));
        infoMan.addCell(new Phrase(String.format("%s", data.getHull().getXvv_man())));
        infoMan.addCell(new Phrase("Xvvvv"));
        infoMan.addCell(new Phrase(String.format("%s", data.getHull().getXvvvv_man())));

        infoMan.addCell(new Phrase("Yv"));
        infoMan.addCell(new Phrase(String.format("%s", data.getHull().getManYv())));
        infoMan.addCell(new Phrase("Yr"));
        infoMan.addCell(new Phrase(String.format("%s", data.getHull().getYr_man())));

        infoMan.addCell(new Phrase("Nv"));
        infoMan.addCell(new Phrase(String.format("%s", data.getHull().getManNv())));
        infoMan.addCell(new Phrase("Nr"));
        infoMan.addCell(new Phrase(String.format("%s", data.getHull().getManNr())));

        info.addCell(new Phrase("Coefficients for maneuver model"));
        info.addCell(new PdfPCell(infoMan));

        header = new PdfPCell(new Phrase("Wind model"));
        header.setColspan(2);
        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
        info.addCell(header);
        info.addCell(new Phrase("Wind frontal projected area"));
        info.addCell(new Phrase(String.format("%s", data.getHull().getWindFrontalArea())));
        info.addCell(new Phrase("Wind lateral projected area"));
        info.addCell(new Phrase(String.format("%s", data.getHull().getWindLateralArea())));
        info.addCell(new Phrase("Pressure center height"));
        info.addCell(new Phrase(String.format("%s", data.getHull().getWindPressureCenterHeight())));
        info.addCell(new Phrase("Wind charactheristic vessel length"));
        info.addCell(new Phrase(String.format("%s", data.getHull().getWindCharacteristicLength())));

        PdfPCell windPlot = new PdfPCell();
        MultiPlotCellEvent windPlotEvent = new MultiPlotCellEvent();

        double[] psi = new double[91];
        double[] x = new double[91];
        double[] y = new double[91];
        double[] n = new double[91];

        double[] X = new double[1 + 2 * data.getHull().getWindAx().length];
        double[] Y = new double[X.length];
        double[] N = new double[X.length];

        X[0] = data.getHull().getWindA0x();
        Y[0] = data.getHull().getWindA0y();
        N[0] = data.getHull().getWindA0n();
        System.arraycopy(data.getHull().getWindAx(), 0, X, 1, data.getHull().getWindAx().length);
        System.arraycopy(data.getHull().getWindAy(), 0, Y, 1, data.getHull().getWindAy().length);
        System.arraycopy(data.getHull().getWindAn(), 0, N, 1, data.getHull().getWindAn().length);
        System.arraycopy(data.getHull().getWindBx(), 0, X, 1 + data.getHull().getWindAx().length, data.getHull().getWindBx().length);
        System.arraycopy(data.getHull().getWindBy(), 0, Y, 1 + data.getHull().getWindAy().length, data.getHull().getWindBy().length);
        System.arraycopy(data.getHull().getWindBn(), 0, N, 1 + data.getHull().getWindAn().length, data.getHull().getWindBn().length);

        for (int i = 0; i < 91; i++) {
            psi[i] = 2 * i * Math.PI / 180.0;
            x[i] = VERESFileUtil.eval(X, psi[i]);
            y[i] = VERESFileUtil.eval(Y, psi[i]);
            n[i] = 10 * VERESFileUtil.eval(N, psi[i]);
            psi[i] *= 180.0 / Math.PI;
        }

        windPlotEvent.addPlot("CX (Surge)", psi, x, Color.RED);
        windPlotEvent.addPlot("CY (Sway)", psi, y, Color.GREEN);
        windPlotEvent.addPlot("10*CN (Yaw)", psi, n, Color.BLUE);
        windPlotEvent.MakePlot("Wind coefficients", "Wind angle from bow", "[-]", true);
        windPlot.setCellEvent(windPlotEvent);
        windPlot.setFixedHeight(220.0f);
        windPlot.setColspan(2);

        info.addCell(windPlot);
        info.completeRow();
        return new PdfPCell(info);
    }

    private static PdfPCell createVeresInfo(BaseExample data) {

        VeresFile.HydFile f = data.getVeresFile().hydFile;

        PdfPTable info = new PdfPTable(2);
        info.addCell(new Phrase("Stability Booklet Case"));
        info.addCell(new Phrase(String.format("%s", data.getOSCHullDataSpec().getStabilityBookletCase())));
        info.addCell(new Phrase("Length between perpendiculars [m]"));
        info.addCell(new Phrase(String.format("%s / %s", f.Lpp, data.getOSCHullDataSpec().getLPP_m())));
        info.addCell(new Phrase("Bredth [m]"));
        info.addCell(new Phrase(String.format("%s/ %s", f.Breadth, data.getOSCHullDataSpec().getBredth_m())));
        info.addCell(new Phrase("App relative OSC Model Origo [m]"));
        info.addCell(new Phrase(String.format("%.2f", data.getVeresFile().getAPPrelOSCModelOrigo())));
        info.addCell(new Phrase("App relative To Stern [m]"));
        info.addCell(new Phrase(String.format("%.2f", data.getVeresFile().getSAPPO())));

        info.addCell(new Phrase("Fpp relative OSC Model Origo [m]"));
        info.addCell(new Phrase(String.format("%.2f", data.getVeresFile().getFPPrelOSCModelOrigo())));
        info.addCell(new Phrase("Draught [m]"));
        info.addCell(new Phrase(String.format("%s/ %s", f.Draught, data.getOSCHullDataSpec().getDraught_m())));
//        info.addCell(new Phrase("Design Draught (DWL) [m]"));
//        info.addCell(new Phrase(String.format("%s", data.getHull().getDWL())));
        info.addCell(new Phrase("Block coefficient (Cb) [-]"));
        info.addCell(new Phrase(String.format("%s", f.Cb)));
        info.addCell(new Phrase("Displacement [tonnes]"));
        info.addCell(new Phrase(String.format("%s/ %s", f.Displacement / 1000.0, data.getOSCHullDataSpec().getDisplacement_t())));
        info.addCell(new Phrase("Longitudinal COG relative AP [m]"));
        info.addCell(new Phrase(String.format("%s/ %s", f.LCG, data.getOSCHullDataSpec().getGRelAP_m())));
        info.addCell(new Phrase("Longitudinal COG relative OSC Origo [m]"));
        info.addCell(new Phrase(String.format("%.2f", data.getVeresFile().getCGXrelOSCModelOrigo())));
        info.addCell(new Phrase("Vertical center of gravity relative BL [m]"));
        info.addCell(new Phrase(String.format("%s/ %s", f.VCG, data.getOSCHullDataSpec().getGRelBL_m())));
        info.addCell(new Phrase("Vertical center of buoyancy relative BL [m]"));
        info.addCell(new Phrase(String.format("%s/ %s", f.KB, data.getOSCHullDataSpec().getBRelBL_m())));
        info.addCell(new Phrase("Metacentric Height [m]"));
        info.addCell(new Phrase(String.format("%s/ %s", f.GMt, data.getOSCHullDataSpec().getMetacentricHeight_m())));

        if (Math.abs(data.getVeresFile().getMgfZMin()) > 0.1) {
            Font font = new Font();
            font.setColor(BaseColor.RED);
            info.addCell(new Phrase("Min Z in Mfg [m]", font));
            info.addCell(new Phrase(String.format("%s0.2f", data.getVeresFile().getMgfZMin())));
        }

        PdfPCell padding = new PdfPCell();
        padding.setColspan(2);
        padding.setFixedHeight(20);
        info.addCell(padding);

        MultiPlotCellEvent evt = new MultiPlotCellEvent();
        int counter = 0;
        for (Section s : data.getVeresFile().hull) {
            evt.addPlot("Section " + counter, s.y, s.z);
            counter++;
        }

        double[] bx = {-0.55 * f.Breadth, 0.55 * f.Breadth};
        double[] ty = {f.Draught, f.Draught};
        evt.addPlot("Waterline", bx, ty, Color.RED);

        evt.MakePlot("Hull layout", "Displacement from centerline [m]", "Height over keel[m]");
        PdfPCell plotCell = new PdfPCell();
        plotCell.setColspan(2);
        plotCell.setCellEvent(evt);
        plotCell.setFixedHeight(240.0f);
        info.addCell(plotCell);
        padding = new PdfPCell();
        padding.setColspan(2);
        padding.setFixedHeight(5);
        //info.addCell(padding);        
        return new PdfPCell(info);
    }

    private static ArrayList<PdfPCell> createPolarWavePlot(BaseExample base) throws InterruptedException {

        PdfPCell rollResponseCell = new PdfPCell();
        rollResponseCell.setFixedHeight(100.0f);

        PdfPCell pitchResponseCell = new PdfPCell();
        pitchResponseCell.setFixedHeight(100.0f);

        double[] hh = {1, 3, 5, 8};
        double[] tt = {6, 12, 12, 12};
        MultiPolarCellEvent rollResponsePlot = new MultiPolarCellEvent();
        MultiPolarCellEvent pitchResponsePlot = new MultiPolarCellEvent();

        for (int i = 0; i < hh.length; i++) {
            double h = hh[i];
            double t = tt[i];
            System.out.println("WAVE CASE " + h + "m " + t + "s");
            ArrayList<Double> theta = new ArrayList<Double>();
            ArrayList<Double> alfa = new ArrayList<Double>();
            ArrayList<Double> angle = new ArrayList<Double>();
            for (double d = 0; d < 2 * Math.PI; d += 10 * Math.PI / 180) {
                System.out.println("DIRECTION: " + d * 180 / Math.PI + "deg");
                fullSpeed2 res = base.waveResponse(h / 2, t, d);
                Thread.sleep(1000);
                theta.add(res.theta.maxabsY());
                alfa.add(res.alfa.maxabsY());
                angle.add(d);
            }
            double[] x = new double[angle.size()];
            double[] y = new double[angle.size()];
            double[] z = new double[angle.size()];
            for (int ii = 0; ii < angle.size(); ii++) {
                x[ii] = angle.get(ii) * 180 / Math.PI;
                y[ii] = theta.get(ii) * 180 / Math.PI;
                z[ii] = alfa.get(ii) * 180 / Math.PI;
            }
            rollResponsePlot.addPlot("h=" + h + "m,t=" + t + "s", x, y, Color.red);
            pitchResponsePlot.addPlot("h=" + h + "m,t=" + t + "s", x, z, Color.red);
        }
        rollResponsePlot.MakePlot("Roll angle response in swell", true);
        rollResponseCell.setCellEvent(rollResponsePlot);

        pitchResponsePlot.MakePlot("Pitch angle response in swell", true);
        pitchResponseCell.setCellEvent(pitchResponsePlot);

        ArrayList<PdfPCell> ret = new ArrayList<PdfPCell>();
        ret.add(new PdfPCell(rollResponseCell));
        ret.add(new PdfPCell(pitchResponseCell));
        return ret;
    }

    private static PdfPCell createPropulsorResponsePlot(BaseExample.PropulsorActuatorDataPair propulsorAssemply, thrusterPerformanceCollection perf) {
        PdfPTable info = new PdfPTable(2);

        SFHOpusPropulsorData dataPropulsor = propulsorAssemply.propulsor;
        SFHActuatorPointData dataActuator = propulsorAssemply.actuator;

        PdfPCell titleCell = new PdfPCell(new Phrase("Name - " + dataPropulsor.getName()));
        titleCell.setColspan(2);
        titleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);

        PdfPCell subtitleCell0 = new PdfPCell(new Phrase("Propeller data"));
        subtitleCell0.setColspan(2);
        subtitleCell0.setBackgroundColor(BaseColor.LIGHT_GRAY);

        PdfPCell rpmResponseCell = new PdfPCell();
        MultiPlotCellEvent rpmResponsePlot = new MultiPlotCellEvent();

        double responseTimeMaxChangeAZI = (dataActuator.getMaxAngle() /*- dataActuator.getMinAngle()*/) / dataActuator.getMaxChangeRateAzimuth();
        double responseTimeMaxChangeRPM = (dataActuator.getMaxRPM()/*- dataActuator.getMinRPM()*/) / dataActuator.getMaxChangeRateRPM();
        double responseTimeMaxChangePITCH = (dataActuator.getMaxPitch()/*- dataActuator.getMinPitch()*/) / dataActuator.getMaxChangeRatePitch();

        if (responseTimeMaxChangePITCH < dataActuator.getTimeConstantPitch() * 10) {
            responseTimeMaxChangePITCH = dataActuator.getTimeConstantPitch() * 10;
        }
        if (responseTimeMaxChangeRPM < dataActuator.getTimeConstantRPM() * 10) {
            responseTimeMaxChangeRPM = dataActuator.getTimeConstantRPM() * 10;
        }
        if (responseTimeMaxChangeAZI < dataActuator.getAngularMotorTimeConstant()[2] * 10) {
            responseTimeMaxChangeAZI = dataActuator.getAngularMotorTimeConstant()[2] * 10;
        }

        rpmResponseCell.setFixedHeight(200.0f);

        rpmResponseCell.setCellEvent(rpmResponsePlot);
        rpmResponseCell.setColspan(2);
        System.out.println("Thruster/actuator " + dataPropulsor.getName() + "/" + dataActuator.getName());
        BaseExample.thrusterResponse t20 = perf.t20.data.get(dataActuator.getName());
        BaseExample.thrusterResponse t40 = perf.t40.data.get(dataActuator.getName());
        BaseExample.thrusterResponse t60 = perf.t60.data.get(dataActuator.getName());
        BaseExample.thrusterResponse t80 = perf.t80.data.get(dataActuator.getName());
        BaseExample.thrusterResponse t100 = perf.t100.data.get(dataActuator.getName());

        long index = 100 + Math.round(1.2 * responseTimeMaxChangeRPM);
        if (dataActuator.getMaxRPM() != dataActuator.getMinRPM()) {
            rpmResponsePlot.addPlot("response to 20%", t20.rpm.regionToArrayX(100, index), t20.rpm.regionToArrayY(100, index), Color.RED);
            rpmResponsePlot.addPlot("response to 40%", t40.rpm.regionToArrayX(100, index), t40.rpm.regionToArrayY(100, index), Color.GREEN);
            rpmResponsePlot.addPlot("response to 60%", t60.rpm.regionToArrayX(100, index), t60.rpm.regionToArrayY(100, index), Color.BLUE);
            rpmResponsePlot.addPlot("response to 80%", t80.rpm.regionToArrayX(100, index), t80.rpm.regionToArrayY(100, index), Color.YELLOW);
            rpmResponsePlot.addPlot("response to 100%", t100.rpm.regionToArrayX(100, index), t100.rpm.regionToArrayY(100, index), Color.MAGENTA);
        } else {
            rpmResponsePlot.setDisabledString("Fixed RPM propulsor");
        }

        rpmResponsePlot.MakePlot("Response of thruster", "Time in seconds", "RPM", true);

        PdfPCell pitchResponseCell = new PdfPCell();
        MultiPlotCellEvent pitchResponsePlot = new MultiPlotCellEvent();

        pitchResponseCell.setFixedHeight(200.0f);

        pitchResponseCell.setCellEvent(pitchResponsePlot);
        pitchResponseCell.setColspan(2);

        index = 100 + Math.round(1.2 * responseTimeMaxChangePITCH);
        if (dataActuator.getMaxPitch() != dataActuator.getMinPitch()) {
            pitchResponsePlot.addPlot("response to 20%", t20.pitch.regionToArrayX(100, index), t20.pitch.regionToArrayY(100, index), Color.RED);
            pitchResponsePlot.addPlot("response to 40%", t40.pitch.regionToArrayX(100, index), t40.pitch.regionToArrayY(100, index), Color.GREEN);
            pitchResponsePlot.addPlot("response to 60%", t60.pitch.regionToArrayX(100, index), t60.pitch.regionToArrayY(100, index), Color.BLUE);
            pitchResponsePlot.addPlot("response to 80%", t80.pitch.regionToArrayX(100, index), t80.pitch.regionToArrayY(100, index), Color.YELLOW);
            pitchResponsePlot.addPlot("response to 100%", t100.pitch.regionToArrayX(100, index), t100.pitch.regionToArrayY(100, index), Color.MAGENTA);
        } else {
            pitchResponsePlot.setDisabledString("Fixed pitch propulsor");
        }
        pitchResponsePlot.MakePlot("Response of thruster", "Time in seconds", "Pitch [Deg]", true);

        PdfPCell angleResponseCell = new PdfPCell();
        MultiPlotCellEvent angleResponsePlot = new MultiPlotCellEvent();

        angleResponseCell.setFixedHeight(200.0f);

        angleResponseCell.setCellEvent(angleResponsePlot);
        angleResponseCell.setColspan(2);

        index = 100 + Math.round(1.2 * responseTimeMaxChangeAZI);

        if (dataActuator.getMaxAngle() != dataActuator.getMinAngle()) {
            angleResponsePlot.addPlot("response to 20%", t20.angle.regionToArrayX(100, index), t20.angle.regionToArrayY(100, index), Color.RED);
            angleResponsePlot.addPlot("response to 40%", t40.angle.regionToArrayX(100, index), t40.angle.regionToArrayY(100, index), Color.GREEN);
            angleResponsePlot.addPlot("response to 60%", t60.angle.regionToArrayX(100, index), t60.angle.regionToArrayY(100, index), Color.BLUE);
            angleResponsePlot.addPlot("response to 80%", t80.angle.regionToArrayX(100, index), t80.angle.regionToArrayY(100, index), Color.YELLOW);
            angleResponsePlot.addPlot("response to 100%", t100.angle.regionToArrayX(100, index), t100.angle.regionToArrayY(100, index), Color.MAGENTA);
        } else {
            angleResponsePlot.setDisabledString("Fixed orientation propulsor");
        }
        if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Rudder) {
            angleResponsePlot.MakePlot("Response of thruster", "Time in seconds", "Angle [Deg]", true);
        } else {
            angleResponsePlot.MakePlot("Response of thruster", "Time in seconds", "Azimuth [Deg]", true);
        }

        info.addCell(titleCell);
        info.addCell(subtitleCell0);
        info.addCell(rpmResponseCell);
        info.addCell(pitchResponseCell);
        info.addCell(angleResponseCell);

        return new PdfPCell(info);
    }

    private static PdfPCell createPropulsorDetail(BaseExample.PropulsorActuatorDataPair propulsorAssemply, thrusterPerformanceCollection perf) {
        PdfPTable info = new PdfPTable(2);

        SFHOpusPropulsorData dataPropulsor = propulsorAssemply.propulsor;
        SFHActuatorPointData dataActuator = propulsorAssemply.actuator;

        boolean isFixedPitch = dataActuator.getMaxPitch() == dataActuator.getMinPitch();
        boolean isFixedRPM = dataActuator.getMaxRPM() == dataActuator.getMinRPM();
        boolean isFixedAzi = dataActuator.getMaxAngle() == dataActuator.getMinAngle();

        double actualMaxPitch = 0;
        double actualMinPitch = 0;
        double actualMaxRPM = 0;
        double actualMinRPM = 0;
        if (dataPropulsor.getType() != SFHOpusPropulsorData.PropulsorType.Rudder) {
            actualMaxPitch = perf.t100_thrust.data.get(dataActuator.getName()).pitch.getLastY();
            actualMinPitch = perf.t100_thrust_n.data.get(dataActuator.getName()).pitch.getLastY();
            actualMaxRPM = perf.t100_thrust.data.get(dataActuator.getName()).rpm.getLastY();
            actualMinRPM = perf.t100_thrust_n.data.get(dataActuator.getName()).rpm.getLastY();
        }

        /* Fix for propulsors where max and min RPM is the same, but angle used to reverse thrust, we want zero rpm for propeller thrust calculations*/
        if ((dataActuator.getMaxPitch() == dataActuator.getMinPitch()) && (dataActuator.getMaxAngle() != dataActuator.getMinAngle())) {
            actualMinRPM = 0;
        }

        PdfPCell titleCell = new PdfPCell(new Phrase("Name - " + dataPropulsor.getName()));
        titleCell.setColspan(2);
        titleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);

        PdfPCell subtitleCell0 = new PdfPCell(new Phrase("Propeller data"));
        subtitleCell0.setColspan(2);
        subtitleCell0.setBackgroundColor(BaseColor.LIGHT_GRAY);

        PdfPCell[][] values = null;
        PdfPCell[][] values2 = null;

        values = new PdfPCell[7][2];
        if (dataPropulsor.getType() != SFHOpusPropulsorData.PropulsorType.Rudder && dataPropulsor.getType() != SFHOpusPropulsorData.PropulsorType.VSP) {
            values[0][0] = new PdfPCell(new Phrase("Diameter [m]"));
            values[0][1] = new PdfPCell(new Phrase(String.format("%s", dataPropulsor.getDiameter())));
            values[1][0] = new PdfPCell(new Phrase("Ventilation Type [-]"));
            if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Open) {
                values[1][1] = new PdfPCell(new Phrase("Open"));
            } else if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Ducted) {
                values[1][1] = new PdfPCell(new Phrase("Ducted"));
            } else if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Tunnel) {
                values[1][1] = new PdfPCell(new Phrase("Tunnel"));
            }
            values[2][0] = new PdfPCell(new Phrase("Parameter set [-]"));
            values[2][1] = new PdfPCell(new Phrase(dataPropulsor.getParameterString()));

        } else if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Rudder) {
            values[0][0] = new PdfPCell(new Phrase("Height [m]"));
            values[0][1] = new PdfPCell(new Phrase(String.format("%s", dataPropulsor.getHeight())));
            values[1][0] = new PdfPCell(new Phrase("Chord [m]"));
            values[1][1] = new PdfPCell(new Phrase(String.format("%s", dataPropulsor.getChord())));
            values[2][0] = new PdfPCell(new Phrase("Parameter set [-]"));
            values[2][1] = new PdfPCell(new Phrase(dataPropulsor.getParameterString()));
        } else {
            values[0][0] = new PdfPCell(new Phrase("Diameter [m]"));
            values[0][1] = new PdfPCell(new Phrase(String.format("%s", dataPropulsor.getDiameter())));
            values[1][0] = new PdfPCell(new Phrase("Height [m]"));
            values[1][1] = new PdfPCell(new Phrase(String.format("%s", dataPropulsor.getHeight())));
            values[2][0] = new PdfPCell(new Phrase("Parameter set ID[-]"));
            values[2][1] = new PdfPCell(new Phrase("VSP"));
        }
        values[3][0] = new PdfPCell(new Phrase("Propulsion type:"));
        String propertiesString = "RPM:";
        if (isFixedRPM) {
            propertiesString += "Fixed";
        } else {
            propertiesString += "Variable";
        }
        propertiesString += "  Pitch:";
        if (isFixedPitch) {
            propertiesString += "Fixed";
        } else {
            propertiesString += "Variable";
        }
        propertiesString += "  Angle:";
        if (isFixedAzi) {
            propertiesString += "Fixed";
        } else {
            propertiesString += "Variable";
        }

        values[3][1] = new PdfPCell(new Phrase(propertiesString));

        values[4][0] = new PdfPCell(new Phrase("Torque limit at bollard (propulsor revolution):"));
        String torqueString = "-";
        if (dataActuator.getTorqueLimit() > 0) {
            torqueString = dataActuator.getTorqueLimit() / 1e3 + " kNm";
        }
        values[4][1] = new PdfPCell(new Phrase(torqueString));

        values[5][0] = new PdfPCell(new Phrase("Technical Name"));
        values[5][1] = new PdfPCell(new Phrase(dataPropulsor.getTechnicalName()));

        values[6][0] = new PdfPCell(new Phrase("File Name"));
        values[6][1] = new PdfPCell(new Phrase(dataPropulsor.getFileName()));

        double responseTimeTimeConstantAZI = dataActuator.getAngularMotorTimeConstant()[2] * 5.0;
        double responseTimeMaxChangeAZI = (dataActuator.getMaxAngle() /*- dataActuator.getMinAngle()*/) / dataActuator.getMaxChangeRateAzimuth();

        double responseTimeTimeConstantRPM = dataActuator.getTimeConstantRPM() * 5.0;
        double responseTimeMaxChangeRPM = (dataActuator.getMaxRPM()/*- dataActuator.getMinRPM()*/) / dataActuator.getMaxChangeRateRPM();

        if (responseTimeMaxChangeRPM > 0 && dataActuator.getMinRPM() >= 0) {
            responseTimeMaxChangeRPM = (dataActuator.getMaxRPM() - dataActuator.getMinRPM()) / dataActuator.getMaxChangeRateRPM();
        }

        double responseTimeTimeConstantPITCH = dataActuator.getTimeConstantPitch() * 5.0;
        double responseTimeMaxChangePITCH = (dataActuator.getMaxPitch()/*- dataActuator.getMinPitch()*/) / dataActuator.getMaxChangeRatePitch();

        PdfPCell subtitleCell1 = new PdfPCell(new Phrase("Actuator data"));
        subtitleCell1.setColspan(2);
        subtitleCell1.setBackgroundColor(BaseColor.LIGHT_GRAY);

        int numberOfRows = 1;
        int indexCorrection = 0;

        if (!isFixedAzi) {
            numberOfRows += 4;
        }

        if (!isFixedPitch) {
            numberOfRows += 4;
        } else if (dataPropulsor.getType() != SFHOpusPropulsorData.PropulsorType.Rudder) {
            numberOfRows += 1;
        }

        if (!isFixedRPM) {
            numberOfRows += 5;
        } else if (dataPropulsor.getType() != SFHOpusPropulsorData.PropulsorType.Rudder) {
            numberOfRows += 2;
        }

        values2 = new PdfPCell[numberOfRows][2];
        values2[0][0] = new PdfPCell(new Phrase("Mounting angle (x,y,z) [deg]"));
        values2[0][1] = new PdfPCell(new Phrase(String.format("%s", dataActuator.getAngularOffset()[0]) + "," + String.format("%s", dataActuator.getAngularOffset()[1]) + "," + String.format("%s", dataActuator.getAngularOffset()[2])));

        if (!isFixedAzi) {
            String typeStr = "azimuth";
            String TypeStr = "Azimuth";
            if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Rudder) {
                typeStr = "rudder";
                TypeStr = "Rudder";
            }
            values2[1][0] = new PdfPCell(new Phrase("Min " + typeStr + " angle [deg]"));
            values2[2][0] = new PdfPCell(new Phrase("Max " + typeStr + " angle [deg]"));
            values2[3][0] = new PdfPCell(new Phrase(TypeStr + " angle zero to max response time [second]"));
            values2[4][0] = new PdfPCell(new Phrase(TypeStr + " max change rate [deg/second]"));

            values2[1][1] = new PdfPCell(new Phrase(format(dataActuator.getMinAngle())));
            values2[2][1] = new PdfPCell(new Phrase(format(dataActuator.getMaxAngle())));
            values2[3][1] = new PdfPCell(new Phrase(format(responseTimeMaxChangeAZI)));
            values2[4][1] = new PdfPCell(new Phrase(format(dataActuator.getMaxChangeRateAzimuth())));
        } else {
            indexCorrection += 4;
        }

        if (!isFixedRPM) {
            values2[5 - indexCorrection][0] = new PdfPCell(new Phrase("Min propulsor rotational speed [RPM]"));
            values2[5 - indexCorrection][1] = new PdfPCell(new Phrase(format(dataActuator.getMinRPM())));
            values2[6 - indexCorrection][0] = new PdfPCell(new Phrase("Max propulsor rotational speed [RPM]"));
            values2[6 - indexCorrection][1] = new PdfPCell(new Phrase(format(dataActuator.getMaxRPM())));
            values2[7 - indexCorrection][0] = new PdfPCell(new Phrase("Propulsor RPM zero to max response time [second]"));
            values2[7 - indexCorrection][1] = new PdfPCell(new Phrase(format(responseTimeMaxChangeRPM)));
            values2[8 - indexCorrection][0] = new PdfPCell(new Phrase("Propulsor RPM max change rate [RPM/second]"));
            values2[8 - indexCorrection][1] = new PdfPCell(new Phrase(format(dataActuator.getMaxChangeRateRPM())));
            values2[9 - indexCorrection][0] = new PdfPCell(new Phrase("Rotation speed to engine speed ratio (gear ratio) [-]"));
            values2[9 - indexCorrection][1] = new PdfPCell(new Phrase(format(dataActuator.getGearRatio())));
        } else if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Rudder) {
            indexCorrection += 5;
        } else {
            values2[5 - indexCorrection][0] = new PdfPCell(new Phrase("Propulsor rotational speed [RPM]"));
            values2[5 - indexCorrection][1] = new PdfPCell(new Phrase(format(dataActuator.getMaxRPM())));
            values2[6 - indexCorrection][0] = new PdfPCell(new Phrase("Rotation speed to engine speed ratio (gear ratio) [-]"));
            values2[6 - indexCorrection][1] = new PdfPCell(new Phrase(format(dataActuator.getGearRatio())));
            indexCorrection += 3;
        }

        if (!isFixedPitch) {
            if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.VSP) {
                values2[10 - indexCorrection][0] = new PdfPCell(new Phrase("Min pitch [-]"));
            } else {
                values2[10 - indexCorrection][0] = new PdfPCell(new Phrase("Min pitch angle [deg]"));
            }

            values2[10 - indexCorrection][1] = new PdfPCell(new Phrase(String.format("%s", dataActuator.getMinPitch())));
            if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.VSP) {
                values2[11 - indexCorrection][0] = new PdfPCell(new Phrase("Max pitch [-]"));
            } else {
                values2[11 - indexCorrection][0] = new PdfPCell(new Phrase("Max pitch angle [deg]"));
            }
            values2[11 - indexCorrection][0] = new PdfPCell(new Phrase("Max pitch angle [deg]"));
            values2[11 - indexCorrection][1] = new PdfPCell(new Phrase(String.format("%s", dataActuator.getMaxPitch())));
            values2[12 - indexCorrection][0] = new PdfPCell(new Phrase("Pitch angle zero to max response time [seconds]"));
            values2[12 - indexCorrection][1] = new PdfPCell(new Phrase(String.format("%.1f", responseTimeMaxChangePITCH)));
            values2[13 - indexCorrection][0] = new PdfPCell(new Phrase("Pitch angle max change rate [deg/second]"));
            values2[13 - indexCorrection][1] = new PdfPCell(new Phrase(String.format("%s", dataActuator.getMaxChangeRatePitch())));
        } else if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Rudder) {
            indexCorrection += 4;
        } else {
            if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.VSP) {
                values2[10 - indexCorrection][0] = new PdfPCell(new Phrase("Pitch [-]"));
            } else {
                values2[10 - indexCorrection][0] = new PdfPCell(new Phrase("Pitch angle [deg]"));
            }
            values2[10 - indexCorrection][1] = new PdfPCell(new Phrase(String.format("%s", dataActuator.getMinPitch())));
            indexCorrection += 3;
        }
        PdfPCell headerCell = new PdfPCell(new Phrase("Characteristic at bollard condition"));
        headerCell.setColspan(2);

        if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.VSP) {
            headerCell = new PdfPCell(new Phrase("Characteristic at  RPM = 60"));
            headerCell.setColspan(2);
        } else if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Rudder) {
            headerCell = new PdfPCell(new Phrase("Rudder lift and drag characteristics"));
            headerCell.setColspan(2);
        } else {
            if (dataActuator.getMinPitch() == dataActuator.getMaxPitch()) {
                headerCell = new PdfPCell(new Phrase("Characteristic at bollard condition pitch angle = " + String.format("%.1f", dataActuator.getMaxPitch()) + "deg,    max/min RPM = " + Math.round(actualMaxRPM * 100.0) / 100.0 + "/" + Math.round(actualMinRPM * 100.0) / 100.0));
                headerCell.setColspan(2);
            } else {
                headerCell = new PdfPCell(new Phrase("Characteristic at bollard condition RPM = " + dataActuator.getMaxRPM() + "    max/min pitch = " + Math.round(actualMaxPitch * 100.0) / 100.0 + "/" + Math.round(actualMinPitch * 100.0) / 100.0 + " deg"));
                headerCell.setColspan(2);
            }

        }
        headerCell.setBackgroundColor(BaseColor.LIGHT_GRAY);

        PdfPCell ctCell = new PdfPCell();
        PdfPCell cqCell = new PdfPCell();

        ArrayList<Double> x = new ArrayList<Double>();
        ArrayList<Double> y0 = new ArrayList<Double>();
        ArrayList<Double> y1 = new ArrayList<Double>();

        String xlabel = "";

        double torqueLimit = dataActuator.getTorqueLimit();
        double[] rpmPitch = null;
        double[] x_t = null;
        double[] y_t = null;
        double[] yq_t = null;
        if (torqueLimit > 0) {
            rpmPitch = dataPropulsor.rpmPitchForTorque(torqueLimit, 1e10);
            x_t = new double[3];
            y_t = new double[3];
            yq_t = new double[3];
        }

        if (dataPropulsor.getType() != SFHOpusPropulsorData.PropulsorType.Rudder) {
            if (dataActuator.getMinPitch() == dataActuator.getMaxPitch()) {
                double minRPM = actualMinRPM;//dataActuator.getMinRPM();
                double maxRPM = actualMaxRPM;//dataActuator.getMaxRPM();

                if (rpmPitch != null) {
                    maxRPM = rpmPitch[0];
                }

                for (double a = minRPM; a <= maxRPM * 1.025; a += (maxRPM - minRPM) / 100.0) {

                    x.add(a);
                    if (rpmPitch != null) {
                        y0.add(dataPropulsor.getThrustForPitch(rpmPitch[1], a) / 1e3);
                        y1.add(dataPropulsor.getTorqueForPitch(rpmPitch[1], a) / 1e3 * a * 2.0 * Math.PI / 60.0);
                    } else {
                        y0.add(dataPropulsor.getThrustForPitch(dataActuator.getMaxPitch(), a) / 1e3);
                        y1.add(dataPropulsor.getTorqueForPitch(dataActuator.getMaxPitch(), a) / 1e3 * a * 2.0 * Math.PI / 60.0);
                    }
                }
                xlabel = "RPM";
                if (y_t != null) {
                    x_t[0] = rpmPitch[0];
                    x_t[1] = rpmPitch[0];
                    x_t[2] = 0;
                    y_t[0] = 0;
                    y_t[1] = dataPropulsor.getThrustForPitch(rpmPitch[1], rpmPitch[0]) / 1e3;
                    y_t[2] = y_t[1];
                    yq_t[0] = 0;
                    yq_t[1] = dataPropulsor.getTorqueForPitch(rpmPitch[1], rpmPitch[0]) / 1e3 * rpmPitch[0] * 2.0 * Math.PI / 60.0;
                    yq_t[2] = yq_t[1];
                }
            } else {
                double minP = actualMinPitch;//dataActuator.getMinPitch();
                double maxP = actualMaxPitch;//dataActuator.getMaxPitch();

                if (rpmPitch != null) {
                    maxP = rpmPitch[1];
                }

                for (double a = minP; a <= maxP * 1.025; a += (maxP - minP) / 100.0) {
                    x.add(a);
                    if (rpmPitch != null) {
                        y0.add(dataPropulsor.getThrustForPitch(a, rpmPitch[0]) / 1e3);
                        y1.add(dataPropulsor.getTorqueForPitch(a, rpmPitch[0]) / 1e3 * dataActuator.getMaxRPM() * 2.0 * Math.PI / 60.0);
                    } else {
                        y0.add(dataPropulsor.getThrustForPitch(a, dataActuator.getMaxRPM()) / 1e3);
                        y1.add(dataPropulsor.getTorqueForPitch(a, dataActuator.getMaxRPM()) / 1e3 * dataActuator.getMaxRPM() * 2.0 * Math.PI / 60.0);
                    }
                }
                xlabel = "Pitch angle [deg]";
                if (x_t != null) {
                    x_t[0] = rpmPitch[1];
                    x_t[1] = rpmPitch[1];
                    x_t[2] = 0;
                    y_t[1] = dataPropulsor.getThrustForPitch(rpmPitch[1], rpmPitch[0]) / 1e3;
                    y_t[2] = y_t[1];
                    yq_t[0] = 0;
                    yq_t[1] = dataPropulsor.getTorqueForPitch(rpmPitch[1], dataActuator.getMaxRPM()) / 1e3 * rpmPitch[0] * 2.0 * Math.PI / 60.0;
                    yq_t[2] = yq_t[1];
                }
            }
        } else {
            for (double a = 0; a < dataActuator.getMaxAngle(); a += 0.1) {
                x.add(a);
                y0.add(dataPropulsor.getPropellerThrustScaling() * dataPropulsor.getLiftForAngle(a));
                y1.add(dataPropulsor.getPropellerThrustScaling() * dataPropulsor.getDragForAngle(a));
            }
        }

        double[] _x = new double[x.size()];
        double[] _y = new double[y0.size()];
        double[] _yq = new double[y1.size()];

        for (int i = 0; i < x.size(); i++) {
            _x[i] = x.get(i);
            _y[i] = y0.get(i);
            _yq[i] = y1.get(i);
        }

        MultiPlotCellEvent ct = new MultiPlotCellEvent();
        MultiPlotCellEvent cq = new MultiPlotCellEvent();
        ct.addPlot("Thrust", _x, _y, Color.RED);
        cq.addPlot("Power", _x, _yq, Color.RED);

        if (dataPropulsor.getType() != SFHOpusPropulsorData.PropulsorType.Rudder) {
            if (dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.VSP) {
                cq.MakePlot("Power", "Normalized pitch angle [-]", "[kW]");
                ct.MakePlot("Thrust", "Normalized pitch angle [-]", "[kN]");
                //ct = new MultiPlotCellEvent(_x, _y,"Thrust","normalized pitch angle [-]","[kN]",false);
                //cq = new PlotCellEvent(_x, _yq,"Power","normalized pitch angle [-]","[kW]");
            } else {

                if (x_t != null) {
                    ct.addPlot("Thrust at torque limit", x_t, y_t, Color.BLUE);
                    cq.addPlot("Torque at torque limit", x_t, yq_t, Color.BLUE);
                    ct.addAnnotation("Torque\nlimit", x_t[1], y_t[1], Math.PI * (1 - 0.06));
                    cq.addAnnotation("Torque\nlimit", x_t[1], yq_t[1], Math.PI * (1 - 0.06));
                }
                cq.MakePlot("Power", xlabel, "[kW]");
                ct.MakePlot("Thrust", xlabel, "[kN]");

            }
        } else {
            ct.MakePlot("Lift coefficient - Cl", "Angle [deg]", "[-]");
            cq.MakePlot("Drag coefficient - Cd", "Angle [deg]", "[-]");
        }

        ctCell.setCellEvent(ct);
        cqCell.setCellEvent(cq);

        info.addCell(titleCell);
        info.addCell(subtitleCell0);
        for (PdfPCell[] value : values) {
            info.addCell(value[0]);
            info.addCell(value[1]);
        }
        info.addCell(subtitleCell1);
        for (PdfPCell[] value : values2) {
            info.addCell(value[0]);
            info.addCell(value[1]);
        }
        info.addCell(headerCell);
        info.addCell(ctCell);
        info.addCell(cqCell);

        return new PdfPCell(info);
    }

    private static PdfPCell createPopulsorInfo(BaseExample data, thrusterPerformanceCollection perf) {

        ArrayList<BaseExample.PropulsorActuatorDataPair> propulsors = data.getPropulsors();

        Collections.sort(propulsors, new customPropulsorPositionCompare());

        PdfPTable info = new PdfPTable(1);

        PdfPCell layout = new PdfPCell(); // layout
        PdfPTable listTable = new PdfPTable(6); // layout
        float[] columnWidths = new float[]{5f, 25f, 25f, 28f, 28f, 25f};
        try {
            listTable.setWidths(columnWidths);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        PropulsorLayout n = new PropulsorLayout();

        VeresFile.HydFile f = data.getVeresFile().hydFile;

        n.setLpp((float) f.Lpp);
        n.setB((float) f.Breadth);
        n.setT((float) f.Draught);

        for (BaseExample.PropulsorActuatorDataPair p : propulsors) {
            float x = Math.round((float) p.actuator.getLinearOffset()[0] * 10) / 10.0f;
            float y = Math.round((float) p.actuator.getLinearOffset()[1] * 10) / 10.0f;
            float z = Math.round((float) p.actuator.getLinearOffset()[2] * 10) / 10.0f;
            n.addPropulsor(p.propulsor.getName(), x, y, z, p.propulsor.getType());
        }
        layout.setCellEvent(n);

        layout.setFixedHeight(220.0f);

        createPropulsorListHeader(listTable);
        for (int i = 0; i < propulsors.size(); i++) {
            createPropulsorListElement(listTable, propulsors.get(i), i + 1, perf, data);
        }
        listTable.completeRow();
        info.addCell(layout);
        info.addCell(new PdfPCell(listTable));

        return new PdfPCell(info);
    }

    private static void createPropulsorListHeader(PdfPTable table) {

        PdfPCell no = new PdfPCell(new Phrase("No"));
        PdfPCell na = new PdfPCell(new Phrase("Name"));
        PdfPCell ty = new PdfPCell(new Phrase("Ventilation Type"));
        PdfPCell pa = new PdfPCell(new Phrase("Position AP/CL/BL (NED)"));
        PdfPCell pr = new PdfPCell(new Phrase("Position/\nOSC Origo (NED)"));
        PdfPCell t50_t100 = new PdfPCell(new Phrase("-100/50/100[kN]"));

        no.setBackgroundColor(BaseColor.LIGHT_GRAY);
        na.setBackgroundColor(BaseColor.LIGHT_GRAY);
        ty.setBackgroundColor(BaseColor.LIGHT_GRAY);
        pa.setBackgroundColor(BaseColor.LIGHT_GRAY);
        pr.setBackgroundColor(BaseColor.LIGHT_GRAY);
        t50_t100.setBackgroundColor(BaseColor.LIGHT_GRAY);

        table.addCell(no);
        table.addCell(na);
        table.addCell(ty);
        table.addCell(pa);
        table.addCell(pr);
        table.addCell(t50_t100);
    }

    private static void createPropulsorListElement(PdfPTable table, BaseExample.PropulsorActuatorDataPair p, int i, thrusterPerformanceCollection perf, BaseExample data) {
        PdfPCell no = new PdfPCell(new Phrase(Integer.toString(i)));
        PdfPCell na = new PdfPCell(new Phrase(p.propulsor.getName()));

        String typeString = "";
        if (null != p.propulsor.getType()) {
            switch (p.propulsor.getType()) {
                case Ducted:
                    typeString = "Ducted propeller";
                    break;
                case Tunnel:
                    typeString = "Tunnel propeller";
                    break;
                case Open:
                    typeString = "Open propeller";
                    break;
                case Rudder:
                    typeString = "Rudder";
                    break;
                case VSP:
                    typeString = "Cycliodial propeller (Voith type)";
                    break;
                default:
                    break;
            }
        }

        String posString = p.getFormattedX() + "/" + p.getFormattedY() + "/" + p.getFormattedZ();
        //Hard coding the position of the current Model Origo relative to AP/CL/BL
        String oscPosString = p.getFormattedX(data.getOSCModelXRelAP()) + "/" + p.getFormattedY(data.getOSCModelYRelCL()) + "/" + p.getFormattedZ(data.getOSCModelZRelBL());

        PdfPCell ty = new PdfPCell(new Phrase(typeString));
        PdfPCell pa = new PdfPCell(new Phrase(posString));
        PdfPCell prr = new PdfPCell(new Phrase(oscPosString));

        PdfPCell t50_t100;
        if (p.propulsor.getType() != SFHOpusPropulsorData.PropulsorType.Rudder) {

            double _t100 = perf.t100_thrust.data.get(p.actuator.getName()).thrust.getLastY();
            double _t50 = perf.t50_thrust.data.get(p.actuator.getName()).thrust.getLastY();
            double _t100n = perf.t100_thrust_n.data.get(p.actuator.getName()).thrust.getLastY();

            t50_t100 = new PdfPCell(new Phrase("-" + Math.round(_t100n / 1e3) + " / " + Math.round(_t50 / 1e3) + " / " + Math.round(_t100 / 1e3)));
        } else {
            t50_t100 = new PdfPCell(new Phrase("N/A"));
        }

        table.addCell(no);
        table.addCell(na);
        table.addCell(ty);
        table.addCell(pa);
        table.addCell(prr);
        table.addCell(t50_t100);

    }

    /**
     * OSC specific configuration, make sure it is up-to-date
     *
     * @param data
     * @return
     */
    private static PdfPCell createOSCSensorInfo(BaseExample data) {

        VeresFile veres = new VeresFile(data.getHull().getModelData() + "/" + data.getHull().getModelFileStem());
        VeresFile.HydFile f = veres.hydFile;

        ArrayList<Sensor> sensors = new ArrayList<>();
        sensors.add(new Sensor("GPS 1", 63.68, 3.22, -50.40, data.getHull().getDWL(), f.Lpp / 2));
        sensors.add(new Sensor("GPS 2", 63.68, -3.22, -50.40, data.getHull().getDWL(), f.Lpp / 2));
        //Coordinates are irrelevant for OSC, but not for RR
        sensors.add(new Sensor("Wind 1", 38.425, 3.22, -46.15, data.getHull().getDWL(), f.Lpp / 2));
        sensors.add(new Sensor("Wind 2", 38.425, 3.22, -46.15, data.getHull().getDWL(), f.Lpp / 2));
        sensors.add(new Sensor("Wind 3", 38.425, 3.22, -46.15, data.getHull().getDWL(), f.Lpp / 2));
        //Coordinates from ProcmanConfig.xml
        sensors.add(new Sensor("Cyscan", 55.20, -1.0, -13.27, data.getHull().getDWL(), f.Lpp / 2, true));

        sensors.add(new Sensor("MRU 1"));
        sensors.add(new Sensor("MRU 2"));
        sensors.add(new Sensor("MRU 3"));
        sensors.add(new Sensor("Gyro 1"));
        sensors.add(new Sensor("Gyro 2"));
        sensors.add(new Sensor("Gyro 3"));
        PdfPTable info = new PdfPTable(1);

        PdfPCell layout = new PdfPCell(); // layout
        PdfPTable listTable = new PdfPTable(3); // layout

        float[] columnWidths = new float[]{10f, 50f, 50f};
        try {
            listTable.setWidths(columnWidths);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        createSensorsListHeader(listTable);
        for (int i = 0; i < sensors.size(); i++) {
            createSensorListElement(listTable, sensors.get(i));
        }
        listTable.completeRow();
        info.addCell(layout);
        info.addCell(new PdfPCell(listTable));

        return new PdfPCell(info);
    }

    private static void createSensorsListHeader(PdfPTable table) {

        PdfPCell na = new PdfPCell(new Phrase("Name"));
        PdfPCell pa = new PdfPCell(new Phrase("Position AP/CL/BL"));
        PdfPCell pr = new PdfPCell(new Phrase("Position/\nMidpoint"));

        na.setBackgroundColor(BaseColor.LIGHT_GRAY);
        pa.setBackgroundColor(BaseColor.LIGHT_GRAY);
        pr.setBackgroundColor(BaseColor.LIGHT_GRAY);

        table.addCell(na);
        table.addCell(pa);
        table.addCell(pr);
    }

    private static void createSensorListElement(PdfPTable table, Sensor sensor) {

        PdfPCell na = new PdfPCell(new Phrase(sensor.getName()));

        PdfPCell pa = new PdfPCell(new Phrase(sensor.getPosition()));
        PdfPCell prr = new PdfPCell(new Phrase(sensor.getPositionRelativeMP()));

        table.addCell(na);
        table.addCell(pa);
        table.addCell(prr);

    }

    private static void appendToDocument(PdfPTable pageTable, Document document) throws DocumentException {
        document.newPage();
        document.add(pageTable);
    }

    private static PdfPTable creatPageTable(PdfPCell content, String version) {
        PdfPTable pageOne = new PdfPTable(1);
        pageOne.setWidthPercentage(100.0f);

        PdfPCell header = new PdfPCell();
        header.setFixedHeight(70);

        ReportPageHeader evt = new ReportPageHeader();
        evt.setSectionTitle(sectionName);
        evt.setShipTitle(vesselName);
        header.setCellEvent(evt);

        PdfPCell footer = new PdfPCell();
        footer.setFixedHeight(25);
        ReportPageFooter evt2 = new ReportPageFooter();
        evt2.setDate(dateString);
        evt2.setPage(pageNo);
        evt2.setVersion(version);
        evt2.setInterfaceVersion(interfaceVersion);
        footer.setCellEvent(evt2);

        content.setFixedHeight(640);

        pageOne.addCell(header);
        pageOne.addCell(content);
        pageOne.addCell(footer);

        pageNo++;

        return pageOne;
    }

    static double findTransfer(turingCircle data) {
        int start_index = findTurnStart(data);
        double psi_s = data.psi.toArrayY()[start_index];
        double start_n = data.n.toArrayY()[start_index];
        double start_e = data.e.toArrayY()[start_index];

        double advance = 0;
        double transfer = 0;

        for (int i = start_index + 10; i < data.psi.seriesLength(); i++) {

            double diff_angle = Math.atan2(Math.sin(data.psi.getY(i) - psi_s), Math.cos(data.psi.getY(i) - psi_s));

            if (Math.abs(diff_angle) > Math.PI / 2.0) {

                double dn = data.n.getY(i) - start_n;
                double de = data.e.getY(i) - start_e;

                advance = dn * Math.cos(psi_s) + de * Math.sin(psi_s);
                transfer = Math.sqrt((dn * dn + de * de) - advance * advance);
                break;
            }
        }
        return transfer;
    }

    static double findAdvance(turingCircle data) {
        int start_index = findTurnStart(data);
        double psi_s = data.psi.getY(start_index);
        double start_n = data.n.getY(start_index);
        double start_e = data.e.getY(start_index);

        double advance = 0;

        for (int i = start_index; i < data.psi.seriesLength(); i++) {

            double diff_angle = Math.atan2(Math.sin(data.psi.getY(i) - psi_s), Math.cos(data.psi.getY(i) - psi_s));

            if (Math.abs(diff_angle) > Math.PI / 2.0) {

                double dn = data.n.getY(i) - start_n;
                double de = data.e.getY(i) - start_e;

                advance = dn * Math.cos(psi_s) + de * Math.sin(psi_s);
                break;
            }
        }
        return advance;
    }

    static double findTacticalDiameter(turingCircle data) {
        int start_index = findTurnStart(data);
        double psi_s = data.psi.getY(start_index);
        double start_n = data.n.getY(start_index);
        double start_e = data.e.getY(start_index);

        double advance = 0;
        double transfer = 0;

        double sign = 0;
        boolean trigger = false;
        for (int i = start_index; i < data.psi.seriesLength(); i++) {

            double diff_angle = Math.atan2(Math.sin(data.psi.getY(i) - psi_s), Math.cos(data.psi.getY(i) - psi_s));

            if (!trigger && Math.abs(diff_angle) > Math.PI / 2) {
                sign = Math.signum(diff_angle);
                trigger = true;
            }

            if ((Math.signum(diff_angle) != sign) && trigger) {

                double dn = data.n.getY(i) - start_n;
                double de = data.e.getY(i) - start_e;

                System.out.println("Start point: " + start_n + "," + start_e + " turn 180deg: " + data.n.getY(i) + "," + data.e.getY(i));

                advance = dn * Math.cos(psi_s) + de * Math.sin(psi_s);
                transfer = Math.sqrt((dn * dn + de * de) - advance * advance);
                break;
            }
        }
        return transfer;
    }

    static int findTurnStart(turingCircle data) {
        int i = 0;
        for (i = 0; i < data.delta.seriesLength(); i++) {
            if (Math.abs(data.delta.getY(i)) >= 9) {
                return i;
            }
        }
        return -1;
    }

    private static class Sensor implements BaseExample.IShipPoint {

        String name;
        double x_rel_ap;
        double y_rel_cl;
        double z_rel_bl;

        double dwl;
        double mp_rel_ap;

        boolean validValues;

        public Sensor() {

        }

        public Sensor(String name, double x_rel_ap, double y_rel_cl, double z_rel_bl, double dwl, double mp_rel_ap) {
            this.name = name;
            this.x_rel_ap = x_rel_ap;
            this.y_rel_cl = y_rel_cl;
            this.z_rel_bl = z_rel_bl;
            this.dwl = dwl;
            this.mp_rel_ap = mp_rel_ap;
            validValues = true;
        }

        /**
         *
         * @param name of the sensor
         * @param x
         * @param y
         * @param z
         * @param dwl design waterline
         * @param mp_rel_ap position of the MP relative to AP along the x axis,
         * (Lpp/2)
         * @param relMP if the coordinates are given relative the MP
         */
        public Sensor(String name, double x, double y, double z, double dwl, double mp_rel_ap, boolean relMP) {
            this.name = name;
            this.x_rel_ap = relMP ? x + mp_rel_ap : x;
            this.y_rel_cl = y;
            this.z_rel_bl = relMP ? z - dwl : z;
            this.dwl = dwl;
            this.mp_rel_ap = mp_rel_ap;
            validValues = true;
        }

        public Sensor(String name) {
            this.name = name;
            validValues = false;
        }

        @Override
        public double getX() {
            return x_rel_ap;
        }

        @Override
        public double getY() {
            return y_rel_cl;
        }

        @Override
        public double getZ() {
            return z_rel_bl;

        }

        @Override
        public String getFormattedX() {
            return format(getX());
        }

        @Override
        public String getFormattedY() {
            return format(getY());
        }

        @Override
        public String getFormattedZ() {
            return format(getZ());
        }

        public String getFormattedXRelMP() {
            return format(getX() - mp_rel_ap);
        }

        public String getFormattedZRelMP() {
            return format(getZ() + dwl);
        }

        public String getName() {
            return name;
        }

        public String getPosition() {
            if (validValues) {
                return this.getFormattedX() + "/" + this.getFormattedY() + "/" + this.getFormattedZ();
            }
            return "NA";
        }

        public String getPositionRelativeMP() {
            if (validValues) {
                return this.getFormattedXRelMP() + "/" + this.getFormattedY() + "/" + this.getFormattedZRelMP();
            }
            return "NA";
        }
    }

}
