/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sfh.ship.report;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aarsathe
 */
public class ReportPageHeader implements PdfPCellEvent{

    private String shipTitle;
    private String sectionTitle;
    
    @Override
    public void cellLayout(PdfPCell ppc, Rectangle rctngl, PdfContentByte[] pcbs) {
        BaseFont bf=null;
        try {
            bf = BaseFont.createFont();
        } catch (DocumentException ex) {
            Logger.getLogger(ReportPageHeader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReportPageHeader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        PdfContentByte  cb = pcbs[PdfPTable.BACKGROUNDCANVAS];
        
        cb.rectangle(rctngl.getLeft(), rctngl.getBottom(), rctngl.getWidth(), rctngl.getHeight());
        cb.setCMYKColorFillF(0.0f,18.89f/255,37.78f/255,93f/255);
        cb.fill();
        
        cb.setColorFill(BaseColor.WHITE);
        cb.setFontAndSize(bf, 36);
        
        cb.beginText();
        cb.setTextMatrix( 5+rctngl.getLeft(),rctngl.getTop() - 35);
        cb.showText(sectionTitle);
        cb.endText();

        cb.setColorFill(BaseColor.WHITE);
        cb.setFontAndSize(bf, 20);
        cb.beginText();
        cb.setTextMatrix( 5+rctngl.getLeft(),rctngl.getTop() - 60);
        cb.showText(shipTitle);
        cb.endText();
    }

    /**
     * @param shipTitle the shipTitle to set
     */
    public void setShipTitle(String shipTitle) {
        this.shipTitle = shipTitle;
    }

    /**
     * @param sectionTitle the sectionTitle to set
     */
    public void setSectionTitle(String sectionTitle) {
        this.sectionTitle = sectionTitle;
    }
    
}
