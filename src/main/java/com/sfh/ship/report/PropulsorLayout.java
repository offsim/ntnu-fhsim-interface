/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sfh.ship.report;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.sfh.ship.SFHOpusPropulsorData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aarsathe
 */
public class PropulsorLayout implements PdfPCellEvent{ 

    /**
     * @return the lpp
     */
    public float getLpp() {
        return lpp;
    }

    /**
     * @param lpp the lpp to set
     */
    public void setLpp(float lpp) {
        this.lpp = lpp;
    }

    /**
     * @return the loa
     */
    public float getLoa() {
        return loa;
    }

    /**
     * @param loa the loa to set
     */
    public void setLoa(float loa) {
        this.loa = loa;
    }

    /**
     * @return the b
     */
    public float getB() {
        return b;
    }

    /**
     * @param b the b to set
     */
    public void setB(float b) {
        this.b = b;
    }

    /**
     * @return the t
     */
    public float getT() {
        return t;
    }

    /**
     * @param t the t to set
     */
    public void setT(float t) {
        this.t = t;
    }
    public class LayoutPosition{
        public float x,y,z;
        public String desc;
        SFHOpusPropulsorData.PropulsorType type;
    }
    
    private float left,bottom;
    private float width,height;

    private float vert_margin,hor_margin;
    private float ap_pos,fp_pos;
    private float l_pos_0,l_pos_1;
    
    private float side_height,top_width;

    private float lpp=67,loa=140,b=10,t=0;
    
    private ArrayList<LayoutPosition> propulsor = new ArrayList<LayoutPosition>();
    
    public void addPropulsor(String name, float x, float y, float z, SFHOpusPropulsorData.PropulsorType type){
        LayoutPosition n = new LayoutPosition();
        n.x = x; n.y = y; n.z = z;
        n.desc = name;
        n.type = type;
        propulsor.add(n);
    }
    
    @Override
    public void cellLayout(PdfPCell cell, Rectangle rect, PdfContentByte[] canvas) {
        PdfContentByte cb = canvas[PdfPTable.BACKGROUNDCANVAS];
        //cb.roundRectangle(rect.getLeft() + 1.5f, rect.getBottom() + 1.5f, rect.getWidth() - 3, rect.getHeight() - 3, 4);

        left = rect.getLeft();
        bottom = rect.getBottom();
        width = rect.getWidth();
        height = rect.getHeight();
             
        vert_margin = 0.1f*height;
        hor_margin = 0.2f*width;
        
        ap_pos = left + hor_margin + 0.025f*width;
        fp_pos = left + width - 0.025f*width - hor_margin;
        
        l_pos_0 = bottom + vert_margin+10;
        l_pos_1 = l_pos_0 + (height-2*vert_margin)/2;
        
        side_height = 0.5f*(height-2*vert_margin)/2;
        top_width = 0.6f*(height-2*vert_margin)/2;
        try {
            drawReferenceLines(cb);
        } catch (DocumentException | IOException ex) {
            Logger.getLogger(PropulsorLayout.class.getName()).log(Level.SEVERE, null, ex);
        }
        drawLateralShape(cb);
        drawTransverseShape(cb);
        try {
            drawPropulsors(cb);
        } catch (DocumentException | IOException ex) {
            Logger.getLogger(PropulsorLayout.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void drawPropulsors(PdfContentByte cb) throws DocumentException, IOException{
        BaseFont bf = BaseFont.createFont();
        int propulsorno = 1;
        for( LayoutPosition lay: propulsor){
            float x = ap_pos + lay.x/getLpp()*(fp_pos-ap_pos);
            float y_top = l_pos_0 +top_width/2 - top_width*lay.y/getB();
            float y_side = l_pos_1 - 15*lay.z/getT()/2;
            
            cb.setColorFill(BaseColor.LIGHT_GRAY);
            if( lay.type != SFHOpusPropulsorData.PropulsorType.Rudder ){
                if( lay.type == SFHOpusPropulsorData.PropulsorType.VSP )                
                    cb.setColorFill(BaseColor.ORANGE);
                else
                    cb.setColorFill(BaseColor.RED);
                cb.circle(x, y_top, 5);
                cb.fill();
                cb.circle(x, y_side, 5);
                cb.fill();
            }else{
                cb.setColorFill(BaseColor.LIGHT_GRAY);
                cb.rectangle(x-5.0f, y_top-1.25f, 10, 2.5f);
                cb.fill();
                cb.rectangle(x-5.0f, y_side-10.0f, 10, 20);
                cb.fill();
            }
            
            cb.setColorFill(BaseColor.BLACK);
            cb.setFontAndSize(bf, 10);
            cb.beginText();
            cb.setTextMatrix(x+2,y_top+2);
            cb.showText(Integer.toString(propulsorno) );
            cb.endText();
            cb.beginText();
            cb.setTextMatrix(x+2,y_side+2);
            cb.showText(Integer.toString(propulsorno) );
            cb.endText();

            propulsorno++;
        }
    }
    
    private void drawReferenceLines(PdfContentByte cb) throws DocumentException, IOException{
    
        BaseFont bf = BaseFont.createFont();

        // AP Line
        cb.moveTo( ap_pos, bottom+vert_margin );
        cb.lineTo( ap_pos, bottom+height-vert_margin);
        cb.stroke();

        cb.beginText();
        cb.setFontAndSize(bf, 12);
        cb.setTextMatrix(ap_pos,bottom+vert_margin/2);
        cb.showText("AP");
        cb.endText();
        
        // FP Line
        cb.moveTo( fp_pos, bottom+vert_margin );
        cb.lineTo( fp_pos, bottom+height-vert_margin);
        cb.stroke();

        cb.beginText();
        cb.setFontAndSize(bf, 12);
        cb.setTextMatrix(fp_pos,bottom+vert_margin/2);
        cb.showText("FP");
        cb.endText();
    
        // Base Line 0
       // cb.moveTo( left+hor_margin, l_pos_0-10 );
       // cb.lineTo( left+width-hor_margin,l_pos_0-10);
       // cb.stroke();

        // Base Line 1
        cb.moveTo( left+hor_margin, l_pos_1 );
        cb.lineTo( left+width-hor_margin,l_pos_1);
        cb.stroke();
        cb.beginText();
        cb.setFontAndSize(bf, 12);
        cb.setTextMatrix(left+hor_margin-12,l_pos_1-12);
        cb.showText("BL");
        cb.endText();

    
    }
    
    private void drawLateralShape(PdfContentByte cb){
    
        //Deck
        cb.moveTo( left+hor_margin, l_pos_1+side_height*0.75f );
        cb.lineTo( left+width*0.75f-hor_margin, l_pos_1+side_height*0.75f);
        cb.lineTo( left+width*0.77f-hor_margin, l_pos_1+side_height);
        cb.lineTo( left+width-hor_margin, l_pos_1+side_height);
        cb.stroke();

        // Bow
        cb.moveTo( left+width-hor_margin, l_pos_1+side_height);
        cb.lineTo( fp_pos, l_pos_1+15);
        cb.moveTo( fp_pos, l_pos_1+15);
        cb.curveTo(fp_pos+15, l_pos_1+15,fp_pos+15, l_pos_1,fp_pos, l_pos_1);
        cb.stroke();
        
        // Bottom
        cb.moveTo( fp_pos, l_pos_1);
        cb.lineTo( ap_pos+10, l_pos_1);
        cb.lineTo( ap_pos+10, l_pos_1+10);
        cb.lineTo( left+hor_margin, l_pos_1+15);
        cb.stroke();

        // Stern
        cb.moveTo( left+hor_margin, l_pos_1+15);
        cb.lineTo( left+hor_margin, l_pos_1+side_height*0.75f);
        cb.stroke();
        
        //waterline 
        cb.moveTo( left+hor_margin, l_pos_1 + 15);
        cb.lineTo( fp_pos, l_pos_1 + 15 );
        cb.stroke();
        
    }

    private void drawTransverseShape(PdfContentByte cb){

        //Port
        cb.moveTo( left+hor_margin, l_pos_0+top_width );
        cb.lineTo( fp_pos-35, l_pos_0+top_width);
        cb.stroke();

        // Stbd
        cb.moveTo( left+hor_margin, l_pos_0 );
        cb.lineTo( fp_pos-35, l_pos_0);
        cb.stroke();

        // Stern
        cb.moveTo( left+hor_margin, l_pos_0+top_width );
        cb.lineTo( left+hor_margin, l_pos_0);
        cb.stroke();
        
        //bow
        cb.moveTo( fp_pos-35, l_pos_0+top_width );
        cb.curveTo(fp_pos+2, l_pos_0+top_width, 1.05f*fp_pos, l_pos_0+top_width/2+10  , 1.05f*fp_pos, l_pos_0+top_width/2);
        cb.stroke();
        cb.moveTo( fp_pos-35, l_pos_0);
        cb.curveTo(fp_pos+2, l_pos_0, 1.05f*fp_pos, l_pos_0+top_width/2-10  , 1.05f*fp_pos, l_pos_0+top_width/2);
        cb.stroke();
    }

}
