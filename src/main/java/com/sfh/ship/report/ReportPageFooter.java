/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sfh.ship.report;

import com.sfh.osc.spec.FhsimModelVersionFinder;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author aarsathe
 */
public class ReportPageFooter implements PdfPCellEvent{

    private int page;
    private String date;
    private String version = "NoVersion";
    private String interfaceVersion = "NoVersion";
    
    public void setDate(String d){
        date = d;
    }
    
    public void setPage(int d){
        page = d;
    }

    public void setVersion(String v){
        version=v;
    }
    
    public void setInterfaceVersion(String v){
        interfaceVersion=v;
    }
    
    public static String getFhSimInterfaceVersion(String pomPath) {
        String version = null;
        Element el = null;
        try {
         File inputFile = new File(pomPath);
         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.parse(inputFile);
         doc.getDocumentElement().normalize();
         el = (Element) doc.getElementsByTagName("version").item(0);
      } catch (Exception e) {
         e.printStackTrace();
      }
    return el.getTextContent();
    }
    
    @Override
    public void cellLayout(PdfPCell ppc, Rectangle rctngl, PdfContentByte[] pcbs) {
        PdfContentByte  cb = pcbs[PdfPTable.BACKGROUNDCANVAS];
        cb.rectangle(rctngl.getLeft(), rctngl.getBottom(), rctngl.getWidth(), rctngl.getHeight());
        cb.setCMYKColorFillF(0.0f,18.89f/255,37.78f/255,93f/255);
        cb.fill();
        
        BaseFont bf=null;
        try {
            bf = BaseFont.createFont();
        } catch (DocumentException ex) {
            Logger.getLogger(ReportPageHeader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReportPageHeader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        cb.setColorFill(BaseColor.WHITE);
        cb.setFontAndSize(bf, 10);
        
        cb.beginText();
        cb.setTextMatrix( 5+rctngl.getLeft(),rctngl.getTop() - 15);
        cb.showText("Hull :" + FhsimModelVersionFinder.getVersion()+ " Interface :" + interfaceVersion + " Date: " + date);
        cb.endText();
        
        cb.beginText();
        cb.setTextMatrix( rctngl.getLeft() + rctngl.getWidth()-40-5,rctngl.getTop() - 15);
        cb.showText("Page " + Integer.toString(page));
        cb.endText();
    }
    
}
