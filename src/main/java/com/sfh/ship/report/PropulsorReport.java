/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sfh.ship.report;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sfh.ship.SFHActuatorPointData;
import com.sfh.ship.SFHOpusPropulsorData;
import com.sfh.ship.vesselExamples.BaseExample;
import com.sfh.ship.vesselExamples.BaseExample.PropulsorActuatorDataPair;

import com.sfh.ship.vesselExamples.PK410_NTNU_Gunnerus;
import com.sfh.ship.VeresFile;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 *
 * @author aarsathe
 */
public class PropulsorReport {
    public static void main(String args[]) throws FileNotFoundException, DocumentException{
      
        PK410_NTNU_Gunnerus data = new PK410_NTNU_Gunnerus();
        data.configureShip();
        
        createPropulsorReport(data, "Gunnerus.pdf");
     
    }    
    
    private static void createPropulsorReport(BaseExample data, String saveAs ) throws FileNotFoundException, DocumentException{

        Document document = new Document(PageSize.A4);
        
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(saveAs));
        document.open();
        appendToDocument(creatPageTable(createPopulsorInfo(data)), document);
        for( PropulsorActuatorDataPair p: data.getPropulsors() )
            appendToDocument(creatPageTable(createPropulsorDetail(p)), document);
        document.close();
    }
    
    private static PdfPCell createPropulsorDetail( PropulsorActuatorDataPair propulsorAssemply){
        PdfPTable info = new PdfPTable(2); 
    
        SFHOpusPropulsorData dataPropulsor = propulsorAssemply.propulsor;
        SFHActuatorPointData dataActuator = propulsorAssemply.actuator;
        
        PdfPCell titleCell = new PdfPCell( new Phrase("Name - "+ dataPropulsor.getName()) ); titleCell.setColspan(2);
        titleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);

        PdfPCell subtitleCell0 = new PdfPCell( new Phrase("Propeller data") ); subtitleCell0.setColspan(2);
        subtitleCell0.setBackgroundColor(BaseColor.LIGHT_GRAY);

        PdfPCell[][] values = null;
        PdfPCell[][] values2 = null;
        
        if(dataPropulsor.getType() != SFHOpusPropulsorData.PropulsorType.Rudder && dataPropulsor.getType() != SFHOpusPropulsorData.PropulsorType.VSP ){
            values = new PdfPCell[3][2];
            
            values[0][0] = new PdfPCell( new Phrase("Diameter [m]")); 
            values[0][1] = new PdfPCell( new Phrase( String.format("%s",dataPropulsor.getDiameter()))); 
            values[1][0] = new PdfPCell( new Phrase( "Ventilation Type [-]") ); 
            if(dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Open) 
               values[1][1] = new PdfPCell( new Phrase("Open")); 
            else if(dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Ducted) 
               values[1][1] = new PdfPCell( new Phrase("Ducted")); 
            else if(dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Tunnel) 
               values[1][1] = new PdfPCell( new Phrase("Tunnel")); 
            values[2][0] = new PdfPCell( new Phrase( "Parameter set [-]") ); 
            values[2][1] = new PdfPCell( new Phrase( dataPropulsor.getParameterString() ) ); 
            

            
        }else if(dataPropulsor.getType() == SFHOpusPropulsorData.PropulsorType.Rudder){
            values = new PdfPCell[3][2];
            
            values[0][0] = new PdfPCell( new Phrase("Height [m]")); 
            values[0][1] = new PdfPCell( new Phrase( String.format("%s",dataPropulsor.getHeight()))); 
            values[1][0] = new PdfPCell( new Phrase( "Chord [m]") ); 
            values[1][1] = new PdfPCell( new Phrase( String.format("%s",dataPropulsor.getChord()))); 
            values[2][0] = new PdfPCell( new Phrase( "Parameter set [-]") ); 
            values[2][1] = new PdfPCell( new Phrase( dataPropulsor.getParameterString() ) ); 
        }else{
            values = new PdfPCell[3][2];
            values[0][0] = new PdfPCell( new Phrase("Diameter [m]")); 
            values[0][1] = new PdfPCell( new Phrase( String.format("%s",dataPropulsor.getDiameter()))); 
            values[1][0] = new PdfPCell( new Phrase( "Height [m]") ); 
            values[1][1] = new PdfPCell( new Phrase( String.format("%s",dataPropulsor.getHeight())));         
            values[2][0] = new PdfPCell( new Phrase( "Parameter set [-]") ); 
            values[2][1] = new PdfPCell( new Phrase( "VSP") ); 
        } 

        PdfPCell subtitleCell1 = new PdfPCell( new Phrase("Actuator data") ); subtitleCell1.setColspan(2);
        subtitleCell1.setBackgroundColor(BaseColor.LIGHT_GRAY);
        values2= new PdfPCell[13][2];
        values2[0][0] = new PdfPCell( new Phrase("Min azimuth angle [deg]")); 
        values2[0][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getMinAngle()) )); 
        values2[1][0] = new PdfPCell( new Phrase("Max azimuth angle [deg]")); 
        values2[1][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getMaxAngle()) )); 
        values2[2][0] = new PdfPCell( new Phrase("Azimuth angle response time [second]")); 
        values2[2][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getAngularMotorTimeConstant()[2]) )); 
        values2[3][0] = new PdfPCell( new Phrase("Azimuth max change rate [deg/second]")); 
        values2[3][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getMaxChangeRateAzimuth()) )); 
        
        values2[4][0] = new PdfPCell( new Phrase("Min propulsor rotational speed [RPM]")); 
        values2[4][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getMinRPM()) )); 
        values2[5][0] = new PdfPCell( new Phrase("Max propulsor rotational speed [RPM]")); 
        values2[5][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getMaxRPM()) )); 
        values2[6][0] = new PdfPCell( new Phrase("Rotation speed to engine speed ratio (gear ratio) [-]")); 
        values2[6][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getGearRatio()) )); 
        values2[7][0] = new PdfPCell( new Phrase("Propulsor RPM response time [second]")); 
        values2[7][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getTimeConstantRPM()) )); 
        values2[8][0] = new PdfPCell( new Phrase("Propulsor RPM max change rate [RPM/second]")); 
        values2[8][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getMaxChangeRateRPM()) )); 

        values2[9][0] = new PdfPCell( new Phrase("Min pitch angle [deg]")); 
        values2[9][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getMinPitch()) )); 
        values2[10][0] = new PdfPCell( new Phrase("Max pitch angle [deg]")); 
        values2[10][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getMaxPitch()) )); 
        values2[11][0] = new PdfPCell( new Phrase("Pitch angle response time [seconds]")); 
        values2[11][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getTimeConstantPitch()) )); 
        values2[12][0] = new PdfPCell( new Phrase("Pitch angle max change rate [deg/second]")); 
        values2[12][1] = new PdfPCell( new Phrase(String.format("%s",dataActuator.getMaxChangeRatePitch()) )); 
        
        PdfPCell headerCell = new PdfPCell( new Phrase("Characteristic at bollard condition") ); headerCell.setColspan(2);
        
        PdfPCell ctCell = new PdfPCell();
        PdfPCell cqCell = new PdfPCell();
        
        
        ArrayList<Double> x = new ArrayList<Double>();
        ArrayList<Double> y0 = new ArrayList<Double>();
        ArrayList<Double> y1 = new ArrayList<Double>();

        if( dataPropulsor.getType() != SFHOpusPropulsorData.PropulsorType.Rudder){
            for( double a = dataActuator.getMinPitch(); a <= dataActuator.getMaxPitch(); a += 0.1){
                x.add(a);
                y0.add( dataPropulsor.getThrustForPitch(a)/1e3);
                y1.add( dataPropulsor.getTorqueForPitch(a)/1e3);
            }
        }else{
            for( double a = 0; a < dataActuator.getMaxAngle(); a += 0.1){
                x.add(a);
                y0.add( dataPropulsor.getLiftForAngle(a));
                y1.add( dataPropulsor.getDragForAngle(a));
            }
        }
        
        double [] _x =  new double[x.size()]; 
        double [] _y =  new double[y0.size()]; 
        double [] _yq =  new double[y1.size()]; 

        for( int i=0; i < x.size(); i++){
            _x[i] = x.get(i);
            _y[i] = y0.get(i);
            _yq[i] = y1.get(i);
        }
        
        PlotCellEvent ct;
        PlotCellEvent cq;        
        if( dataPropulsor.getType() != SFHOpusPropulsorData.PropulsorType.Rudder){
            ct = new PlotCellEvent(_x, _y,"Thrust","Angle","[kN]");
            cq = new PlotCellEvent(_x, _yq,"Torque","Angle","[kNm]");
        }else{
            ct = new PlotCellEvent(_x, _y,"Lift coefficient - Cl","Angle","[-]");
            cq = new PlotCellEvent(_x, _yq,"Drag coefficient - Cd","Angle","[-]");        
        }
        
        
        ctCell.setCellEvent(ct);
        cqCell.setCellEvent(cq);
        
        info.addCell(titleCell);
        info.addCell(subtitleCell0);
        for (PdfPCell[] value : values) {
            info.addCell(value[0]);
            info.addCell(value[1]);
        }
        info.addCell(subtitleCell1);
        for (PdfPCell[] value : values2) {
            info.addCell(value[0]);
            info.addCell(value[1]);
        }
        info.addCell(headerCell);
        info.addCell(ctCell);
        info.addCell(cqCell);
        
        return new PdfPCell(info);
    }
    
    private static PdfPCell createPopulsorInfo( BaseExample data){

        ArrayList<PropulsorActuatorDataPair> propulsors = data.getPropulsors();
        PdfPTable info = new PdfPTable(1); 
        
        PdfPCell layout = new PdfPCell(); // layout
        PdfPTable listTable = new PdfPTable(6); // layout

        PropulsorLayout n = new PropulsorLayout();
        
        VeresFile veres = new VeresFile(data.getHull().getModelData() + "/" + data.getHull().getModelFileStem());
        VeresFile.HydFile f = veres.hydFile;
     
        
        n.setLpp((float)f.Lpp);
        n.setB((float)f.Breadth);
        n.setT((float)f.Draught);
        
        for( PropulsorActuatorDataPair p: propulsors)
            n.addPropulsor( p.propulsor.getName(),(float)p.actuator.getLinearOffset()[0],(float)p.actuator.getLinearOffset()[1],(float)p.actuator.getLinearOffset()[2],p.propulsor.getType());

        layout.setCellEvent(n);
        
        
        layout.setFixedHeight(220.0f);
        
        createPropulsorListHeader(listTable);
        for( int i =0 ; i < propulsors.size() ; i++)
            createPropulsorListElement(listTable,propulsors.get(i),i);
        listTable.completeRow();
        info.addCell(layout);
        info.addCell(new PdfPCell(listTable));
        
        return new PdfPCell(info);
    }
    
    private static void createPropulsorListHeader(PdfPTable table){
        
        PdfPCell no = new PdfPCell( new Phrase("No"));
        PdfPCell na = new PdfPCell( new Phrase("Name") );
        PdfPCell ty = new PdfPCell( new Phrase("Type") );
        PdfPCell pa = new PdfPCell( new Phrase("Position AP/CL/BL") );
        PdfPCell t05 = new PdfPCell( new Phrase("T50") );
        PdfPCell t10 = new PdfPCell( new Phrase("T100") );
        
        no.setBackgroundColor(BaseColor.LIGHT_GRAY);
        na.setBackgroundColor(BaseColor.LIGHT_GRAY);
        ty.setBackgroundColor(BaseColor.LIGHT_GRAY);
        pa.setBackgroundColor(BaseColor.LIGHT_GRAY);
        t05.setBackgroundColor(BaseColor.LIGHT_GRAY);
        t10.setBackgroundColor(BaseColor.LIGHT_GRAY);

        table.addCell(no); 
        table.addCell(na); 
        table.addCell(ty); 
        table.addCell(pa);         
        table.addCell(t05); 
        table.addCell(t10); 
    }
    
    private static void createPropulsorListElement(PdfPTable table, PropulsorActuatorDataPair p, int i){
        PdfPCell no = new PdfPCell( new Phrase( Integer.toString(i) ) );
        PdfPCell na = new PdfPCell( new Phrase( p.propulsor.getName()) );

        String typeString = "";
        if( p.propulsor.getType() == SFHOpusPropulsorData.PropulsorType.Ducted )
            typeString = "Ducted propeller";
        else if( p.propulsor.getType() == SFHOpusPropulsorData.PropulsorType.Tunnel )
            typeString = "Tunnel propeller";
        else if( p.propulsor.getType() == SFHOpusPropulsorData.PropulsorType.Open )
            typeString = "Open propeller";
        else if( p.propulsor.getType() == SFHOpusPropulsorData.PropulsorType.Rudder )
            typeString = "Rudder";
        else if( p.propulsor.getType() == SFHOpusPropulsorData.PropulsorType.VSP )
            typeString = "Cycliodial propeller (Voith type)";
        String posString = p.actuator.getLinearOffset()[0]+"," + p.actuator.getLinearOffset()[1]+","+p.actuator.getLinearOffset()[2];
        
        PdfPCell ty = new PdfPCell( new Phrase(typeString) );
        PdfPCell pa = new PdfPCell( new Phrase(posString) );

        PdfPCell t05;
        PdfPCell t10;
        if( p.propulsor.getType() != SFHOpusPropulsorData.PropulsorType.Rudder ){ 
            t05 = new PdfPCell( new Phrase(""+Math.round(p.propulsor.getThrustForPitch( 0.5*p.actuator.getMaxPitch())/1e3)+" [kN]"  )  );
            t10 = new PdfPCell( new Phrase(""+Math.round(p.propulsor.getThrustForPitch( 1.0*p.actuator.getMaxPitch())/1e3)+" [kN]"  )  );
        }else{
            t05 = new PdfPCell( new Phrase("N/A"));
            t10 = new PdfPCell( new Phrase("N/A"));
        }
        
        table.addCell(no); 
        table.addCell(na); 
        table.addCell(ty); 
        table.addCell(pa);         
        table.addCell(t05); 
        table.addCell(t10); 
    }
    
    private static PdfPTable creatPageTable( PdfPCell content ){
        PdfPTable pageOne = new PdfPTable(1);
        pageOne.setWidthPercentage(100.0f);

        PdfPCell header = new PdfPCell();
        header.setFixedHeight(80);
        header.setCellEvent(new ReportPageHeader());

        PdfPCell footer = new PdfPCell();
        footer.setFixedHeight(40);
        footer.setCellEvent(new ReportPageFooter());
        
        content.setFixedHeight(640);
        
        pageOne.addCell( header );
        pageOne.addCell( content );
        pageOne.addCell( footer );
        return pageOne;
    }
    
    private static void appendToDocument(PdfPTable pageTable, Document document ) throws DocumentException{
        document.newPage();
        document.add(pageTable);
    }
}
