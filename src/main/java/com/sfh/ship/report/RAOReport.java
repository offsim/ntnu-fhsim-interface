package com.sfh.ship.report;

import com.sfh.ship.vesselExamples.BaseExample;
import com.sfh.ship.vesselExamples.UT776CD_Island_Crusader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Locale;

/**
 *
 * @author Tobias Torben
 */
public class RAOReport {

    private int nDir;
    private double[] dirs;
    private int nFreq;
    private double minT;
    private double maxT;
    private double[] freqs;
    private UT776CD_Island_Crusader vessel;
    private String fileName;
    private double amp;

    public RAOReport(double[] dirs, int nFreq, double amp, double minT, double maxT, UT776CD_Island_Crusader vessel, String fileName) {
        this.dirs = dirs;
        this.nDir = Array.getLength(dirs);
        this.nFreq = nFreq;
        this.minT = minT;
        this.maxT = maxT;
        this.amp = amp;

        double minFreq = 2 * Math.PI / maxT;
        double maxFreq = 2 * Math.PI / minT;
        double deltaFreq = (maxFreq - minFreq) / (nFreq - 1);
        this.freqs = new double[nFreq];
        for (int i = 0; i < nFreq; i++) {
            freqs[i] = minFreq + deltaFreq * i;
        }
        this.vessel = vessel;
        this.fileName = fileName;
    }

    public static void main(String[] args) {
        double[] dirs = {0,90,135};
        int nFreq = 21;
        double minT = 2;
        double maxT = 20;
        double amp = 0.01;
        String file = "../rao_testing/RAO.csv";
        UT776CD_Island_Crusader vessel = new UT776CD_Island_Crusader();
        vessel.configureShip();
        RAOReport report = new RAOReport(dirs, nFreq,amp, minT, maxT, vessel, file);
        report.generate();
    }

    public void generate() {
        double[] res, phase;
        double raoHeave, raoPitch, raoRoll, k;
        ArrayList<ArrayList<ArrayList<Double>>> peaks;
        
        try (PrintWriter out = new PrintWriter(new FileWriter(fileName, false))) {
            for (int i = 0; i < nDir; i++) {
                out.println(String.format(Locale.US,"\nHeading: %.1f\n", dirs[i]));
                out.println("Frequency;RAO Heave;RAO Roll;RAO Pitch");
                out.println("rad/s;[-];[-];[-]");
                for (int j = 0; j < nFreq; j++) {
                    peaks = vessel.runRAO(dirs[i]*Math.PI / 180, freqs[j], amp);
                    res = vessel.calculateRAO(peaks);
                    phase = vessel.calculatePhase(peaks, freqs[j]);
                    k = freqs[j]*freqs[j]/9.81;
                    raoHeave = res[2]/amp;
                    raoRoll = res[3] / (amp*180*k/Math.PI);
                    raoPitch = res[4] / (amp*180*k/Math.PI);
                    System.out.println(String.format(Locale.US,"Phase: %e;  %e;  %e;  %e;  %e;  %e\n",phase[0],phase[1],phase[2],phase[3],phase[4],phase[5]));
                    out.println(String.format(Locale.US, "%e;%e;%e;%e", freqs[j], raoHeave, raoRoll, raoPitch));
                }
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getnDir() {
        return nDir;
    }

    public void setnDir(int nDir) {
        this.nDir = nDir;
    }

    public double[] getDirs() {
        return dirs;
    }

    public void setDirs(double[] dirs) {
        this.dirs = dirs;
    }

    public int getnFreq() {
        return nFreq;
    }

    public void setnFreq(int nFreq) {
        this.nFreq = nFreq;
    }

    public double getMinT() {
        return minT;
    }

    public void setMinT(double minT) {
        this.minT = minT;
    }

    public double getMaxT() {
        return maxT;
    }

    public void setMaxT(double maxT) {
        this.maxT = maxT;
    }

    public double[] getFreqs() {
        return freqs;
    }

    public void setFreqs(double[] freqs) {
        this.freqs = freqs;
    }

    public BaseExample getVessel() {
        return vessel;
    }

    public void setVessel(UT776CD_Island_Crusader vessel) {
        this.vessel = vessel;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
