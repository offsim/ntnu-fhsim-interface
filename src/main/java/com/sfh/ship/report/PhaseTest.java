/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.ship.report;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sfh.ship.vesselExamples.PK410_NTNU_Gunnerus;
import com.sfh.ship.vesselExamples.BaseExample;
import com.sfh.ship.VeresFile;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author t_tor
 */
public class PhaseTest {

    private static int duration = 200;
    private static double dt = 1;
    private static double amplitude = 1;
    private static double period = 30;
    private static double dir = 0;
    private static double phaseLag = 0;
    private static double xPosVessel = 0;
    private static String filename = "phasePlot.pdf";
    private static String vesselName = "PK 410 NTNU Gunnerus R/V";

    public static void main(String args[]) throws FileNotFoundException, DocumentException {

        PK410_NTNU_Gunnerus data = new PK410_NTNU_Gunnerus();
        data.configureShip();
        VeresFile veres = new VeresFile(data.getHull().getModelData() + "/" + data.getHull().getModelFileStem());
        VeresFile.HydFile f = veres.hydFile;
     
        BaseExample.positionData posData;
        PdfPCell plot;
        PdfPTable page;
        double omega = 2 * Math.PI / period;
        ArrayList<double[]> surfaceElevation, surfaceSlope;
     
        Document document = new Document(PageSize.A4);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
        document.open();

        posData = data.runPhaseTrial(duration, dt, amplitude, omega, dir, phaseLag, f.Draught - f.VCG);
        surfaceElevation = getSurfaceElevationTimeSeries();
        surfaceSlope = getSurfaceSlopeTimeSeries();
        
        plot = createHeavePlot(posData, surfaceElevation.get(0), surfaceElevation.get(1), f.Draught - f.VCG);
        page = createPage(plot, data);
        document.newPage();
        document.add(page);
        
        plot = createPitchPlot(posData, surfaceSlope.get(0), surfaceSlope.get(1));
        page = createPage(plot, data);
        document.newPage();
        document.add(page);
       
        dir = Math.PI / 2;
        posData = data.runPhaseTrial(duration, dt, amplitude, omega, dir , phaseLag, f.Draught - f.VCG);
        
        plot = createRollPlot(posData, surfaceSlope.get(0), surfaceSlope.get(1));
        page = createPage(plot, data);
        document.newPage();
        document.add(page);
        
        document.close();
    }

    private static PdfPTable createPage(PdfPCell content, BaseExample data) {
        String dateString = (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).format(new Date());
        String version = data.getHull().getVersionString();
        PdfPTable page = new PdfPTable(1);
        page.setWidthPercentage(100.0f);

        PdfPCell header = new PdfPCell();
        header.setFixedHeight(70);

        ReportPageHeader evt = new ReportPageHeader();
        evt.setSectionTitle("FhSim Wave-Ship phase test");
        evt.setShipTitle(vesselName);
        header.setCellEvent(evt);

        PdfPCell footer = new PdfPCell();
        footer.setFixedHeight(25);
        ReportPageFooter evt2 = new ReportPageFooter();
        evt2.setDate(dateString);
        evt2.setPage(1);
        evt2.setVersion(version);
        footer.setCellEvent(evt2);

        content.setFixedHeight(640);

        page.addCell(header);
        page.addCell(content);
        page.addCell(footer);

        return page;
    }

    private static PdfPCell createHeavePlot(BaseExample.positionData posData, double[] timeVec, double[] surfaceElevation, double cog) {
        double[] heave = posData.d.toArrayY();
        for (int i = 0; i < posData.d.seriesLength(); i++) {
            heave[i] = heave[i] - cog;
        }
        
        PdfPTable info = new PdfPTable(1);
        PdfPCell c = new PdfPCell();
        MultiPlotCellEvent n = new MultiPlotCellEvent();
        n.addPlot("Heave [m]", posData.d.toArrayX(),heave);
        n.addPlot("Surface Elevation at midpoint [m]", timeVec, surfaceElevation, Color.BLUE);
        n.MakePlot("Following waves, surface elevation = -A*sin(wt-kx)", "Time [s]", "", true);
        c.setCellEvent(n);
        c.setFixedHeight(500.0f);
        info.addCell(c);

        return new PdfPCell(info);
    }

    private static PdfPCell createPitchPlot(BaseExample.positionData posData, double[] timeVec, double[] surfaceSlope) {
        double[] pitch = posData.alfa.toArrayY();
        
        PdfPTable info = new PdfPTable(1);
        PdfPCell c = new PdfPCell();
        MultiPlotCellEvent n = new MultiPlotCellEvent();
        n.addPlot("Pitch [rad]", posData.alfa.toArrayX(),pitch);
        n.addPlot("Surface Slope at midpoint [-]", timeVec, surfaceSlope, Color.BLUE);
        n.MakePlot("Following waves. Surface slope = k*A*cos(wt-kx)", "Time [s]", "", true);
        c.setCellEvent(n);
        c.setFixedHeight(500.0f);
        info.addCell(c);

        return new PdfPCell(info);
    }
    
    private static PdfPCell createRollPlot(BaseExample.positionData posData, double[] timeVec, double[] surfaceSlope) {
        double[] pitch = posData.theta.toArrayY();
        
        PdfPTable info = new PdfPTable(1);
        PdfPCell c = new PdfPCell();
        MultiPlotCellEvent n = new MultiPlotCellEvent();
        n.addPlot("Roll [rad]", posData.alfa.toArrayX(),pitch);
        n.addPlot("Surface Slope at midpoint [-]", timeVec, surfaceSlope, Color.BLUE);
        n.MakePlot("Beam waves at port side. Surface slope = k*A*cos(wt-kx)", "Time [s]", "", true);
        c.setCellEvent(n);
        c.setFixedHeight(500.0f);
        info.addCell(c);

        return new PdfPCell(info);
    }
    private static ArrayList<double[]> getSurfaceElevationTimeSeries() {
        int n = (int) (((double) duration) / dt);
        double omega = 2*Math.PI / period;
        double[] t_vec = new double[n + 1];
        double[] zeta = new double[n + 1];
        double t = 0;
        int i = 0;
        double k = omega * omega / 9.81;
        while (t <= duration) {
            t_vec[i] = t;
            zeta[i] = -amplitude * Math.sin(omega * t - k * xPosVessel + phaseLag);
            i++;
            t += dt;
        }

        ArrayList<double[]> ret = new ArrayList<double[]>();
        ret.add(t_vec);
        ret.add(zeta);
        return ret;
    }
    private static ArrayList<double[]> getSurfaceSlopeTimeSeries() {
        int n = (int) (((double) duration) / dt);
        double omega = 2*Math.PI / period;
        double[] t_vec = new double[n + 1];
        double[] zeta = new double[n + 1];
        double t = 0;
        int i = 0;
        double k = omega * omega / 9.81;
        while (t <= duration) {
            t_vec[i] = t;
            zeta[i] = k*amplitude * Math.cos(omega * t - k * xPosVessel + phaseLag);
            i++;
            t += dt;
        }

        ArrayList<double[]> ret = new ArrayList<double[]>();
        ret.add(t_vec);
        ret.add(zeta);
        return ret;
    }
}
