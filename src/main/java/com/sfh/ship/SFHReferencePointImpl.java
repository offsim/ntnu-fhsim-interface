/*
 * Copyright SINTEF Fisheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

import org.w3c.dom.Element;

/**
 * Implementation of the SFHReferencePointIntf
 * @see SFHReferencePointIntf
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public class SFHReferencePointImpl implements SFHReferencePointIntf{

    protected Reference relativeTo = Reference.AP;
    protected ForceSource forceFrom;
    protected String forceObject;
    protected String forcePort;
    protected String name;
    protected String ioName;
    protected String shipName;
    protected String shipPositionPort;
    protected boolean isForceCalculator = false;
    protected boolean isExternalForceInput = false;    
    protected boolean isLinearMotor = false;
    protected boolean isAngularMotor = false;
    protected double [] linearOffset = new double[3];
    protected double [] angularOffset = new double[3];
    private double [] permanentLinearOffset = new double[3];
    protected double [] permanentAngularOffset = new double[3];
    protected double [] linearMotorTimeConstant = new double[3];
    protected double [] angularMotorTimeConstant = new double[3];

    protected double [] linearOffsetToFhSim = new double[3];
    protected double [] angularOffsetToFhSim = new double[3];
    
    
    protected double []externalForce = new double[3];
    
    protected double [] forceNED = new double[3];
    protected double [] forceBody = new double[3];
    protected double [] torqueNED = new double[3];
    protected double [] torqueBody = new double[3];
    protected double [] positionNED = new double[3];
    protected double [] velocityNED = new double[3];
    protected double [] velocityBody = new double[3];
    protected double [] omegaNED = new double[3];
    protected double [] quaternionNED = new double[4];
    protected double [] axis1NED = new double[3];
    protected double [] axis2NED = new double[3];
    protected double [] axis3NED = new double[3];
    
    protected Element modelObject = null;
    protected Element ioObject = null;
    protected Element connectionObject = null;

    
    private boolean indexinItialized = false;
    private int indexIOObj;
    private int indexForceNED; 
    private int indexForceBody;
    private int indexTorqueNED;
    private int indexTorqueBody;
    private int indexPositionNED;
    private int indexVelocityNED;
    private int indexVelocityBody;
    private int indexAxis1NED;
    private int indexAxis2NED;
    private int indexAxis3NED;      
    
    
    protected SFHReferencePointImpl( SFHReferencePointData data){
        name = data.getName();
        ioName = data.getName()+"IO";
        shipName = data.getShipName();
        
        forceFrom = data.getForceFrom();
        forceObject = data.getForceObject();
        forcePort = data.getForcePort();        
        isForceCalculator = data.getIsForceCalculator();
/*
        if( isForceCalculator && forceFrom == ForceSource.NoForce){
            throw new RuntimeException("trying to instantiate " + name + " with forceCalculator property set with no force input");
        }
        
        if( forceFrom != ForceSource.NoForce && !isForceCalculator ){
            throw new RuntimeException("trying to instantiate " + name + " with force on point without forceCalculator property set");
        }
  */      
        relativeTo = data.getRelativeTo();
        isLinearMotor = data.getLinearMotor();
        isAngularMotor = data.getAngularMotor();
        
        permanentLinearOffset = data.getLinearOffset();
        permanentAngularOffset = data.getAngularOffset();
        
        for( int i=0; i < 3; i++)
            permanentAngularOffset[i] *= Math.PI/180.0;
        
        linearOffset[0] = 0; linearOffset[1] = 0; linearOffset[2] = 0;
        angularOffset[0] = 0; angularOffset[1] = 0; angularOffset[2] = 0;
        
        linearMotorTimeConstant = data.getLinearMotorTimeConstant();
        angularMotorTimeConstant = data.getAngularMotorTimeConstant();

        /*
        Local Connection Object
        */
        modelObject = FhDynamicModel.instance().createSimObjectElement(name, "Ship/LocalConnection", "SFHBaseLibrary");
        if( isForceCalculator ){
            double []linearForceOffset = data.getLinearForceOffset();

            modelObject.setAttribute("ForceCalculator", "1");
            modelObject.setAttribute("ForceAttackOffsetBody",linearForceOffset[0]+","+linearForceOffset[1]+","+linearForceOffset[2] );
        }
        if( !isLinearMotor ){
            modelObject.setAttribute("LinearOffset", permanentLinearOffset[0]+","+permanentLinearOffset[1]+","+permanentLinearOffset[2] );
        }else{
            modelObject.setAttribute("IsLinearMotor", "1");
            modelObject.setAttribute("LinearMotorConstants", linearMotorTimeConstant[0]/5+","+linearMotorTimeConstant[1]/5+","+linearMotorTimeConstant[2]/5);

            double [] rateLimit = data.getLinearMotorMaxChangeRate();
            if( rateLimit != null )
                modelObject.setAttribute("LinearMotorRateLimits", rateLimit[0]+","+rateLimit[1]+","+rateLimit[2]);
        }
        
        if( !isAngularMotor ){
            modelObject.setAttribute("AngularOffset", permanentAngularOffset[0]+","+permanentAngularOffset[1]+","+permanentAngularOffset[2] );
        }else{
            modelObject.setAttribute("IsAngularMotor", "1");
            modelObject.setAttribute("AngularMotorConstants", angularMotorTimeConstant[0]/5+","+angularMotorTimeConstant[1]/5+","+angularMotorTimeConstant[2]/5);
        }
        
        switch(relativeTo){
            case AP: shipPositionPort = "PositionAPNED"; break;
            case CF: shipPositionPort = "PositionZeroCrossNED"; break;
            case CG: shipPositionPort = "PositionNED"; break;
        }
        
        /*
        External Link for Local Connection
        */
        ioObject = FhDynamicModel.instance().createSimObjectElement(ioName, "ExternalLinkStandard", "FhSimBaseLibrary");
        String inputPorts ="PositionNED,VelocityNED,VelocityBody,OffsetBody,RotationBody,Axis1NED,Axis2NED,Axis3NED";
        ioObject.setAttribute("Size_PositionNED", "3");
        ioObject.setAttribute("Size_VelocityNED", "3");
        ioObject.setAttribute("Size_VelocityBody", "3");
        ioObject.setAttribute("Size_OffsetBody", "3");
        ioObject.setAttribute("Size_RotationBody", "3");
        ioObject.setAttribute("Size_Axis1NED", "3");
        ioObject.setAttribute("Size_Axis2NED", "3");
        ioObject.setAttribute("Size_Axis3NED", "3");
        if( isForceCalculator ){
            inputPorts += ","+"ForceNED,TorqueNED,ForceBody,TorqueBody";
            ioObject.setAttribute("Size_ForceNED", "3");
            ioObject.setAttribute("Size_TorqueNED", "3");
            ioObject.setAttribute("Size_ForceBody", "3");
            ioObject.setAttribute("Size_TorqueBody", "3");
        }

        String outputPorts = "";
        if( forceFrom == ForceSource.External ){
            outputPorts = "ForceNED";
            ioObject.setAttribute("Initial_ForceNED", "0,0,0");
        }
        
        if( isLinearMotor){
            if( forceFrom == ForceSource.External ) outputPorts += ",";
            outputPorts += "OffsetX,OffsetY,OffsetZ";
            ioObject.setAttribute("Initial_OffsetX", Double.toString(permanentLinearOffset[0]));
            ioObject.setAttribute("Initial_OffsetY", Double.toString(permanentLinearOffset[1]));
            ioObject.setAttribute("Initial_OffsetZ", Double.toString(permanentLinearOffset[2]));
        }
        if( isAngularMotor ){
            if( isLinearMotor || (forceFrom == ForceSource.External)) outputPorts += ",";
            outputPorts += "OffsetP,OffsetQ,OffsetR";
            ioObject.setAttribute("Initial_OffsetP", Double.toString(permanentAngularOffset[0]));
            ioObject.setAttribute("Initial_OffsetQ", Double.toString(permanentAngularOffset[1]));
            ioObject.setAttribute("Initial_OffsetR", Double.toString(permanentAngularOffset[2]));
        }
        
        
        ioObject.setAttribute("inputPortNames", inputPorts);
        ioObject.setAttribute("outputPortNames", outputPorts);
        
        connectionObject = FhDynamicModel.instance().createElement("Connection");

        connectionObject.setAttribute(name+".ParentPositionNED", shipName+"."+shipPositionPort);
        connectionObject.setAttribute(name+".ParentCGPositionNED", shipName+"."+"PositionNED");
        connectionObject.setAttribute(name+".ParentQuaternionNED", shipName+"."+"QuaternionNED");
        connectionObject.setAttribute(name+".ParentVelocityNED", shipName+"."+"VelocityNED");
        connectionObject.setAttribute(name+".ParentOmegaNED", shipName+"."+"OmegaNED");

        switch (forceFrom) {
            case NoForce:
                isExternalForceInput = false;
                if (isForceCalculator) {
                    connectionObject.setAttribute(name + ".ForceNED", "0,0,0");
                }
                break;
            case External:
                isExternalForceInput = true;
                connectionObject.setAttribute(name + ".ForceNED", ioName + "." + "ForceNED");
                break;
            case FhSimPort:
                isExternalForceInput = false;
                connectionObject.setAttribute(name + ".ForceNED", forceObject + "." + forcePort);
                break;
        }
        
        if( isLinearMotor ){
            connectionObject.setAttribute(name+".OffsetX", ioName+"."+"OffsetX");
            connectionObject.setAttribute(name+".OffsetY", ioName+"."+"OffsetY");
            connectionObject.setAttribute(name+".OffsetZ", ioName+"."+"OffsetZ");
        }
        if( isAngularMotor ){
            connectionObject.setAttribute(name+".OffsetP", ioName+"."+"OffsetP");
            connectionObject.setAttribute(name+".OffsetQ", ioName+"."+"OffsetQ");
            connectionObject.setAttribute(name+".OffsetR", ioName+"."+"OffsetR");
        }

        connectionObject.setAttribute(ioName+".PositionNED", name+"."+"PositionNED");
        connectionObject.setAttribute(ioName+".VelocityNED", name+"."+"VelocityNED");
        connectionObject.setAttribute(ioName+".VelocityBody", name+"."+"VelocityBody");

        connectionObject.setAttribute(ioName+".OffsetBody", name+"."+"OffsetBody");
        connectionObject.setAttribute(ioName+".RotationBody", name+"."+"RotationBody");
        connectionObject.setAttribute(ioName+".Axis1NED", name+"."+"Axis1NED");
        connectionObject.setAttribute(ioName+".Axis2NED", name+"."+"Axis2NED");
        connectionObject.setAttribute(ioName+".Axis3NED", name+"."+"Axis3NED");

        if( isForceCalculator ){
            connectionObject.setAttribute(ioName+".ForceNED", name+"."+"ForceNED");
            connectionObject.setAttribute(ioName+".TorqueNED", name+"."+"TorqueNED");
            connectionObject.setAttribute(ioName+".ForceBody", name+"."+"ForceBody");
            connectionObject.setAttribute(ioName+".TorqueBody", name+"."+"TorqueBody");
        }
    
        
        FhDynamicModel.instance().getObjectElement().appendChild(modelObject);
        FhDynamicModel.instance().getObjectElement().appendChild(ioObject);
        FhDynamicModel.instance().getInterconnectionElement().getParentNode().appendChild(connectionObject);
        FhDynamicModel.instance().registerObject(this,data);
    }

    
    @Override
    public double[] getLinearOffset() {
        return linearOffset.clone();
    }


    @Override
    public double[] getAngularOffset() {
        return angularOffset.clone();
    }


    @Override
    public double[] getForceNED() {
        return forceNED;
    }

    @Override
    public double[] getForceBody() {
        return forceBody;
    }

    @Override
    public double[] getTorqueNED() {
        return torqueNED; 
    }

    @Override
    public double[] getTorqueBody() {
        return torqueBody;
    }

    @Override
    public double[] getPositionNED() {
        return positionNED;
    }

    @Override
    public double[] getVelocityNED() {
        return velocityNED;
    }

    @Override
    public double[] getVelocityBody() {
        return velocityBody;
    }

    @Override
    public double[] getOmegaNED() {
        return omegaNED;
    }

    @Override
    public double[] getQuaternionNED() {
        return quaternionNED;
    }

    @Override
    public double[] getAxis1NED() {
        return axis1NED;
    }

    @Override
    public double[] getAxis2NED() {
        return axis2NED;
    }

    @Override
    public double[] getAxis3NED() {
        return axis3NED;
    }

    @Override
    public boolean getIsExternalForceInput() {
        return isExternalForceInput;
    }

    @Override
    public void setExternalForceInput(double[] force) {
        externalForce = force.clone();
    }

    @Override
    public void setLinearOffsetTarget(double[] offset) {
        setLinearOffsetTarget(offset[0],offset[1],offset[2]);
    }

    @Override
    public void setLinearOffsetTarget(double x, double y, double z) {
        
        if( !isLinearMotor )
            throw new RuntimeException(name +" is not a linear motor");
        
        linearOffset[0] = x;
        linearOffset[1] = y;
        linearOffset[2] = z;
    }

    @Override
    public void setAngularOffsetTarget(double[] offset) {
        setAngularOffsetTarget(offset[0],offset[1],offset[2]);
    }

    @Override
    public void setAngularOffsetTarget(double p, double q, double r) {
        if( !isAngularMotor )
            throw new RuntimeException(name +" is not an angular motor");
        angularOffset[0] = p*Math.PI/180.0;
        angularOffset[1] = q*Math.PI/180.0;
        angularOffset[2] = r*Math.PI/180.0;
    }
    
    
    @Override
    public void preTick() {
        
        if(  isExternalForceInput ){
            FhDynamicModel.instance().setFhSimPort(ioName, "ForceNED", externalForce);
        }
        double []toFhSim = new double[1];
        if( isLinearMotor ){
            toFhSim[0] = linearOffset[0]+getPermanentLinearOffset()[0];
            FhDynamicModel.instance().setFhSimPort(ioName, "OffsetX", toFhSim);
            toFhSim[0] = linearOffset[1]+getPermanentLinearOffset()[1];
            FhDynamicModel.instance().setFhSimPort(ioName, "OffsetY", toFhSim);
            toFhSim[0] = linearOffset[2]+getPermanentLinearOffset()[2];
            FhDynamicModel.instance().setFhSimPort(ioName, "OffsetZ", toFhSim);
        }

        if( isAngularMotor ){
            toFhSim[0] = angularOffset[0]+permanentAngularOffset[0];
            FhDynamicModel.instance().setFhSimPort(ioName, "OffsetP", toFhSim);
            toFhSim[0] = angularOffset[1]+permanentAngularOffset[1];
            FhDynamicModel.instance().setFhSimPort(ioName, "OffsetQ", toFhSim);
            toFhSim[0] = angularOffset[2]+permanentAngularOffset[2];
            FhDynamicModel.instance().setFhSimPort(ioName, "OffsetR", toFhSim);
        }
    }

    @Override
    public void tick(double Lookahead) {
        FhDynamicModel.instance().objectUpdated(this, Lookahead);
    }

    @Override
    public void postTick() {
        
        if(!indexinItialized ){
            indexinItialized = true;
            indexIOObj = FhDynamicModel.instance().getObjectIndex(ioName);
            if( isForceCalculator ){
                indexForceNED = FhDynamicModel.instance().getObjectPortIndex(ioName, "ForceNED"); 
                indexForceBody = FhDynamicModel.instance().getObjectPortIndex(ioName, "ForceBody");
                indexTorqueNED = FhDynamicModel.instance().getObjectPortIndex(ioName, "TorqueNED");
                indexTorqueBody = FhDynamicModel.instance().getObjectPortIndex(ioName, "TorqueBody");
            }
            indexPositionNED = FhDynamicModel.instance().getObjectPortIndex(ioName, "PositionNED");
            indexVelocityNED = FhDynamicModel.instance().getObjectPortIndex(ioName, "VelocityNED");
            indexVelocityBody = FhDynamicModel.instance().getObjectPortIndex(ioName, "VelocityBody");
            indexAxis1NED = FhDynamicModel.instance().getObjectPortIndex(ioName, "Axis1NED");
            indexAxis2NED = FhDynamicModel.instance().getObjectPortIndex(ioName, "Axis2NED");
            indexAxis3NED = FhDynamicModel.instance().getObjectPortIndex(ioName, "Axis3NED");     
        }
        
        if( isForceCalculator ){
            forceNED = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexForceNED, 3);
            forceBody = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexForceBody, 3);
            torqueNED = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexTorqueNED, 3);
            torqueBody = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexTorqueBody, 3);
        }
        
        positionNED = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexPositionNED, 3);
        velocityNED = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexVelocityNED, 3);
        velocityBody = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexVelocityBody, 3);
        //omegaNED = FhDynamicModel.instance().getFhSimPort(ioName, "OmegaNED", 3);
        //quaternionNED = FhDynamicModel.instance().getFhSimPort(ioName, "QuaternionNED", 3);
        axis1NED = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexAxis1NED, 3);
        axis2NED = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexAxis2NED, 3);
        axis3NED = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexAxis3NED, 3);       
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double[] getPermanentLinearOffset() {
        return permanentLinearOffset.clone();
    }
    
}
