/*
 * Copyright SINTEF Fisheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */

package com.sfh.ship;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Configuration of an actuator point
 * <p>
 * An actuator point is a reference point which also contain actuator functionality
 * such as servo motors for pitch angles, azimuth angle and propulsor RPM. Servo motors
 * for rotation of the other axes and movement along the linear axes are inherited from 
 * the Reference point. 
 * <p>
 * The actuator point is intended for use together with SFHOpusPropulsorData to make a 
 * propulsor (force calculator) and actuator (servo motor, ship placement) unit
 * 
 * @see SFHActuatorPointIntf
 * @see SFHReferencePointData
 * @see SFHReferencePointIntf
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
@XStreamAlias ("ActuatorPoint")
public class SFHActuatorPointData extends SFHReferencePointData{

    /**
     * dump object to XML
     * @param fname name of file to dump object to 
     * @param obj object data to dump
     */
    public static void dumpToXml(String fname, SFHActuatorPointData obj){
        XStream xstream = new XStream();
        xstream.processAnnotations(SFHActuatorPointData.class);
        PrintWriter out = null;
        try {
             out = new PrintWriter(fname);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SFHActuatorPointData.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        out.println(xstream.toXML(obj));
        out.close();
    }
    
    /**
     * Load object form XML
     * @param fname xml file with data
     * @return a new object with data from XML file
     */
    public static SFHActuatorPointData loadFromXml(String fname){
        XStream xstream = new XStream();
        xstream.processAnnotations(SFHActuatorPointData.class);
        SFHActuatorPointData out = (SFHActuatorPointData) xstream.fromXML(new File(fname));
        out.relativeTo = SFHReferencePointIntf.Reference.AP;
        out.initialFailureMode = new double[10];
        return out;
    }    

    /**
     * Load an object form XML and give it a name
     * @param objectName new name of object
     * @param fname file name of XML data
     * @return a new object with data from XML file and a new name
     */
    public static SFHActuatorPointData loadFromXml(String objectName, String fname){
        SFHActuatorPointData out = loadFromXml(fname);
        out.setName(objectName);
        return out;
    }        


    public static SFHActuatorPointData findByNameInObjectList(String Name, ArrayList<Object> list){
        for( Object o: list){
            if( o instanceof SFHActuatorPointData)
                if( ((SFHActuatorPointData)o).getName().equals(Name)){
                    return (SFHActuatorPointData)o;
                }            
        }
        return null;
    }
         
    
    /**
    * Torque limitation on the motor related to the RPM of the propulsor
    */
    private double torqueLimit = 100e12;
    
    /**
     * Maximum angle for azimuth in degrees
     */
    private double maxAngle = 0;

    /**
     * Minimum angle for azimuth in degrees
     */
    private double minAngle = -0;

    /**
     * Maximum propulsor RPM in RPM (duh!)
     */
    private double maxRPM = 0;

    /**
     * Minimum propulsor RPM in RPM (duh!)
     */
    private double minRPM = -0;

    /**
     * Maximum propulsor blade angle in degrees
     */
    private double maxPitch = 0;

    /**
     * Minimum propulsor blade angle in degrees
     */
    private double minPitch = -0;

    /**
     * Gear ratio between propeller RPM and motor RPM. 
     */
    private double gearRatio = 2;

    /**
     * 1. order time constant for propeller blade angle motor
     */
    private double timeConstantPitch = 15;

    /**
     * 1. order time constant for propeller RPM 
     */
    private double timeConstantRPM = 5;

    /**
     * Max change rate of the propeller blade angle in degrees/s
     */
    private double maxChangeRatePitch = 5;
    
    
    /**
     * Max change rate of the propeller RPM angle in RPM/s
     */
    private double maxChangeRateRPM = 50;

    /**
     * Max change rate of the azimuth angle in degrees/s
     */
    private double maxChangeRateAzimuth = 2.5;

    
    /**
     * Malfunction rotation rate when failure mode for azimuth motor malfunction is triggered. degrees/s
     */
    private double angularMotorMalfunctionRate = 2;

    /**
     * True if propulsor is a rudder
     */
    @XStreamOmitField
    private boolean rudder = false;

    /**
     * True if propulsor is an azimuthng thruster 
     */
    @XStreamOmitField
    private boolean azimuth = false;
    @XStreamOmitField
    private boolean controllablePitch = false;
    @XStreamOmitField
    private boolean controllableRPM = false;
    
    /**
     * True if connected to a VSP 
     */
    @XStreamOmitField
    private boolean VSPconnection=false;
    
    /**
     * Initial pitch angle for propeller blades in degrees
     */
    @XStreamOmitField
    private double initialPitch=0;
    
    /**
     * Initial RPM for propeller
     */
    @XStreamOmitField
    private double initialRPM=0;

    /**
     * Initial actuator failure mode 
     */
    @XStreamOmitField
    private double [] initialFailureMode = new double[10];
    
    /**
     * Constructor
     * Allocates some arrays
     */
    public SFHActuatorPointData(){
        initialFailureMode = new double[10];
        for( int i=0; i < 10; i++) initialFailureMode[i]=0;
    }
    
    /**
     * Create an actuator point without properties
     * @param myName 
     */
    public SFHActuatorPointData(String myName) {
        super(myName);
        
        initialPitch = 0;
        initialRPM = 0;
        for( int i=0; i< 10; i++)
            initialFailureMode[i] = 0;
    }

    /**
     * Create an actuator point at an offset from a reference on a hull
     * @param myName Actuator point name (must be unique)
     * @param x Offset in x (longitudinal) direction in meters
     * @param y Offset in y (transverse) direction in meters
     * @param z Offset in z (downward) direction in meters
     * @param ref The point on the hull which the offset is specified relative to
     */
    public SFHActuatorPointData( String myName, double x, double y, double z,SFHReferencePointIntf.Reference ref){
        super(myName,x,y,z,ref);
        initialPitch = 0;
        initialRPM = 0;
        for( int i=0; i< 10; i++)
            initialFailureMode[i] = 0;
    }
    
    /**
     * Create an actuator point at an offset on the hull relative to AP 
     * @param myName Actuator point name (must be unique)
     * @param x Offset in x (longitudinal) direction in meters
     * @param y Offset in y (transverse) direction in meters
     * @param z Offset in z (downward) direction in meters
     */
    public SFHActuatorPointData( String myName, double x, double y, double z){
        super(myName,x,y,z);
        initialPitch = 0;
        initialRPM = 0;
        for( int i=0; i< 10; i++)
            initialFailureMode[i] = 0;
    }    

    /**
     * Return the initial value of the pitch angle in degrees
     * @return the initial value of the pitch angle in degrees
     */
    double GetInitialPitch() {
        return initialPitch;
    }

    /**
     * Return the initial value of the propulsor RPM 
     * @return the initial value of the propulsor RPM 
     */
    double GetInitialRPM() {
        return initialRPM;
    }

    /**
     * The initial failure mode of the actuator
     * @return [1x10] vector of failure modes which are currently active
     */
    double[] GetInitialFailureMode() {
        return initialFailureMode;
    }
    /**
     * @return the maxAngle
     */
    public double getMaxAngle() {
        return maxAngle;
    }

    /**
     * @param maxAngle the maxAngle to set
     */
    public void setMaxAngle(double maxAngle) {
        this.maxAngle = maxAngle;
    }

    /**
     * @return the minAngle
     */
    public double getMinAngle() {
        return minAngle;
    }

    /**
     * @param minAngle the minAngle to set
     */
    public void setMinAngle(double minAngle) {
        this.minAngle = minAngle;
    }

    /**
     * @return the maxRPM
     */
    public double getMaxRPM() {
        return maxRPM;
    }

    /**
     * @param maxRPM the maxRPM to set
     */
    public void setMaxRPM(double maxRPM) {
        this.maxRPM = maxRPM;
    }

    /**
     * @return the minRPM
     */
    public double getMinRPM() {
        return minRPM;
    }

    /**
     * @param minRPM the minRPM to set
     */
    public void setMinRPM(double minRPM) {
        this.minRPM = minRPM;
    }

    /**
     * @return the maxPitch
     */
    public double getMaxPitch() {
        return maxPitch;
    }

    /**
     * @param maxPitch the maxPitch to set
     */
    public void setMaxPitch(double maxPitch) {
        this.maxPitch = maxPitch;
    }

    /**
     * @return the minPitch
     */
    public double getMinPitch() {
        return minPitch;
    }

    /**
     * @param minPitch the minPitch to set
     */
    public void setMinPitch(double minPitch) {
        this.minPitch = minPitch;
    }

    /**
     * @return the gearRatio
     */
    public double getGearRatio() {
        return gearRatio;
    }

    /**
     * @param gearRatio the gearRatio to set
     */
    public void setGearRatio(double gearRatio) {
        this.gearRatio = gearRatio;
    }

    /**
     * @return the angularMotorMalfunctionRate
     */
    public double getAngularMotorMalfunctionRate() {
        return angularMotorMalfunctionRate;
    }

    /**
     * @param angularMotorMalfunctionRate the angularMotorMalfunctionRate to set
     */
    public void setAngularMotorMalfunctionRate(double angularMotorMalfunctionRate) {
        this.angularMotorMalfunctionRate = angularMotorMalfunctionRate;
    }

    /**
     * @return the timeConstantPitch
     */
    public double getTimeConstantPitch() {
        return timeConstantPitch;
    }

    /**
     * @param timeConstantPitch the timeConstantPitch to set
     */
    public void setTimeConstantPitch(double timeConstantPitch) {
        this.timeConstantPitch = timeConstantPitch;
    }

    /**
     * @return the timeConstantRPM
     */
    public double getTimeConstantRPM() {
        return timeConstantRPM;
    }

    /**
     * @param timeConstantRPM the timeConstantRPM to set
     */
    public void setTimeConstantRPM(double timeConstantRPM) {
        this.timeConstantRPM = timeConstantRPM;
    }

    /**
     * @return the rudder
     */
    public boolean isRudder() {
        return rudder;
    }

    /**
     * @param rudder the rudder to set
     */
    public void setRudder(boolean rudder) {
        this.rudder = rudder;
    }

    /**
     * @return the azimuth
     */
    public boolean isAzimuth() {
        return azimuth;
    }

    /**
     * @param azimuth the azimuth to set
     */
    public void setAzimuth(boolean azimuth) {
        this.azimuth = azimuth;
    }

    /**
     * @return the controllablePitch
     */
    public boolean isControllablePitch() {
        return controllablePitch;
    }

    /**
     * @param controllablePitch the controllablePitch to set
     */
    public void setControllablePitch(boolean controllablePitch) {
        this.controllablePitch = controllablePitch;
    }

    /**
     * @return the controllableRPM
     */
    public boolean isControllableRPM() {
        return controllableRPM;
    }

    /**
     * @param controllableRPM the controllableRPM to set
     */
    public void setControllableRPM(boolean controllableRPM) {
        this.controllableRPM = controllableRPM;
    }

    /**
     * Retrieve the maximum rate of change for the pitch dynamics of the actuator. In degrees pr second
     * @return the maximum rate of change for the pitch actuator
     */
    public double getMaxChangeRatePitch() {
        return maxChangeRatePitch;
    }

    /**
     * Set the maximum rate of change for the pitch actuator in degrees per second
     * @param maxChangeRatePitch the maximum rate of change for the pitch actuator
     */
    public void setMaxChangeRatePitch(double maxChangeRatePitch) {
        this.maxChangeRatePitch = maxChangeRatePitch;
    }

    /**
     * Retrieve the maximum rate of change for the motor in the actuator
     * @return the max rate of change in RPM per second
     */
    public double getMaxChangeRateRPM() {
        return maxChangeRateRPM;
    }

    /**
     * Set the maximum rate of change of the motor in the actuator
     * @param maxChangeRateRPM the maximum rate of change for the motor 
     */
    public void setMaxChangeRateRPM(double maxChangeRateRPM) {
        this.maxChangeRateRPM = maxChangeRateRPM;
    }

    /**
     * Retrieve the maximum rate of change for the azimuth motor in the actuator. In degrees per second
     * @return the maximum rate of change for the azimuth motor
     */
    public double getMaxChangeRateAzimuth() {
        return maxChangeRateAzimuth;
    }

    /**
     * Set the maximum rate of change for the azimuth motor in the actuator. In degrees per second
     * @param maxChangeRateAzimuth the update rate of change of the azimuth motor
     */
    public void setMaxChangeRateAzimuth(double maxChangeRateAzimuth) {
        this.maxChangeRateAzimuth = maxChangeRateAzimuth;
    }

    /**
     * Test if the actuator point is a connection to a VSP propulsor
     * @return true if connected to a VSP propulsor
     */
    public boolean isVSPconnection() {
        return VSPconnection;
    }

    /**
     * Toggle connection to a VSP propulsor. Implies angular motor
     * @param VSPconnection true if connection to VSP propulsor is required
     */
    public void setVSPconnection(boolean VSPconnection) {
        this.VSPconnection = VSPconnection;
        
        if( VSPconnection ){
            isAngularMotor = true;
        }
    }

    /**
     * Creates a new instance of a {@code SFHActuatorPointIntf} object
     * configured by this data object.
     *
     * @return a new instance of a {@code SFHActuatorPointIntf} object
     * configured by this data object.
     */
    public SFHActuatorPointIntf create() {
        return new SFHActuatorPointImpl(this);
    }

    /**
     * Get the current torque limit on the propulsor in Nm
     * @return the torque limit of the propulsor (on the propulsor shaft) in Nm
     */
    public double getTorqueLimit() {
        return torqueLimit;
    }

    /**
     * Set the torque limit on the propulsor
     * @param torqueLimit the torque limit of the propulsor (on the propulsor shaft) in Nm
     */
    public void setTorqueLimit(double torqueLimit) {
        this.torqueLimit = torqueLimit;
    }

}
