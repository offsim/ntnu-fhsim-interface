/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.ship;

import java.util.Locale;
import org.w3c.dom.Element;

/**
 *
 * @author Tobias Torben
 */
public class SFHSpringImpl implements SFHSpringIntf {
    
    private double stiffness;
    private double relaxedLength;
    private String springName;
    private String connectionNameA;
    private String connectionNameB;
    private double[] fixedConnectionA;
    private double[] fixedConnectionB;

    
    protected SFHSpringImpl(SFHSpringData data) {
        stiffness = data.getStiffness();
        relaxedLength = data.getRelaxedLength();
        springName = data.getName(); 
        
        Element springElement = FhDynamicModel.instance().createLibElement();
        Element connection = FhDynamicModel.instance().createElement("Connection");
        
        springElement.setAttribute("LibName", "FhSimBaseLibrary");
        springElement.setAttribute("SimObject","Cable/LinearSpring");
        springElement.setAttribute("Name", springName);
        springElement.setAttribute("Stiffness", Double.toString(stiffness));
        springElement.setAttribute("RelaxedLength", Double.toString(relaxedLength));
        
        if (data.getConnectionNameA() != null) {
            connectionNameA = data.getConnectionNameA();
            connection.setAttribute(springName+".PosA", connectionNameA+".PositionNED");
        }
        
        else {
            fixedConnectionA = data.getFixedConnectionA();
            connection.setAttribute(springName+".PosA",String.format(Locale.US,"%f,%f,%f", fixedConnectionA[0], fixedConnectionA[1], fixedConnectionA[2]));
        }
        
        if (data.getConnectionNameB() != null) {
            connectionNameB = data.getConnectionNameB();
            connection.setAttribute(springName+".PosB", connectionNameB+".PositionNED");
        }
        
        else {
            fixedConnectionB = data.getFixedConnectionB();
            connection.setAttribute(springName+".PosB",String.format(Locale.US,"%f,%f,%f", fixedConnectionB[0], fixedConnectionB[1], fixedConnectionB[2]));
        }
        
        
        FhDynamicModel.instance().getInterconnectionElement().getParentNode().appendChild(connection);        
        FhDynamicModel.instance().getObjectElement().appendChild(springElement);
        FhDynamicModel.instance().registerObject(this, data);
        
    }
    
    @Override
    public void preTick() {
    }

    @Override
    public void tick(double Lookahead) {
        FhDynamicModel.instance().objectUpdated(this, Lookahead);
    }

    @Override
    public void postTick() {
    }
    
    @Override
    public double getStiffness() {
        return stiffness;
    }

    @Override
    public void setStiffness(double stiffness) {
        this.stiffness = stiffness;
    }

    @Override
    public double getRelaxedLength() {
        return relaxedLength;
    }

    @Override

    public void setRelaxedLength(double relaxedLength) {
        this.relaxedLength = relaxedLength;
    }

    @Override

    public String getSpringName() {
        return springName;
    }

    @Override
    public void setSpringName(String springName) {
        this.springName = springName;
    }

    @Override
    public String getConnectionNameA() {
        return connectionNameA;
    }

    @Override
    public void setConnectionNameA(String connectionNameA) {
        this.connectionNameA = connectionNameA;
    }

    @Override

    public String getConnectionNameB() {
        return connectionNameB;
    }

    @Override
    public void setConnectionNameB(String connectionNameB) {
        this.connectionNameB = connectionNameB;
    }

    @Override
    public double[] getFixedConnectionA() {
        return fixedConnectionA;
    }

    @Override
    public void setFixedConnectionA(double[] fixedConnectionA) {
        this.fixedConnectionA = fixedConnectionA;
    }

    @Override
    public double[] getFixedConnectionB() {
        return fixedConnectionB;
    }

    @Override
    public void setFixedConnectionB(double[] fixedConnectionB) {
        this.fixedConnectionB = fixedConnectionB;
    }
    
}
