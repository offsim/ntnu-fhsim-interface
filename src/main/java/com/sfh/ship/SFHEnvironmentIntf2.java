/*
 * Copyright SINTEF FIsheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

/**
 * Interface to environment model which conforms to the requirements of the SimBa
 * seismic simulator project. Contains interface to the interpolated current and 
 * density models. 
 * 
 * This environment model controls a two layer current and density model with two 
 * interpolation points. The current and densities will vary as a linear interpolation
 * between the points. The current and densities will have the value of the closest point 
 * when positions on the "outside" of the region between the two points. 
 * 
 * The two points are defined in global North-east-down coordinates
 * 
 * @author karl.gunnar.aarsather@sintef.no
 */
public interface SFHEnvironmentIntf2 extends SFHEnvironmentIntf{

    /**
     * Point selection enumeration
     */
    enum POINT{
        A,
        B;
    };
    
    /**
     * Set the position of the interpolation points A and B in the north-east plane (surface)
     * @param thepoint select point A of B
     * @param n global north position 
     * @param e global east position
     */
    public void setInterpolationPointPosition(POINT thepoint, double n, double e);
    
    /**
     * Update the shallow (top) and deep (bottom) current vectors of point A or B
     * @param thepoint select point A of B
     * @param shallowCurrent [1x3] double array with the current in the shallow (top) layer in NED coordinates
     * @param deepCurrent  [1x3] double array with the current in the deep (bottom) layer in NED coordinates
     */
    public void setInterpolationPointCurrent(POINT thepoint, double [] shallowCurrent, double [] deepCurrent );
    public double [] getInterpolationPointShallowCurrent(POINT thepoint );
    public double [] getInterpolationPointDeepCurrent(POINT thepoint );

    /**
     * Update the depth of the shallow (top) layer and the transition rate to the bottom layer
     * @param thepoint select point A of B
     * @param shallowDepth Depth of shallow layer in meters
     * @param depthTransition Rate of change from shallow (top) layer to deep (bottom layer)
     */
    public void setInterpolationPointDepthTransition(POINT thepoint, double shallowDepth, double depthTransition);
    
    /**
     * Update the densities in the shallow (top) and deep (bottom) layers
     * @param thepoint select point A of B
     * @param rhoShallow density of top layer in kg/m**3 (1025 default)
     * @param rhoDeep density of bottom layer in kg/m**3 (1025 default)
     */
    public void setInterpolationPointDensity(POINT thepoint, double rhoShallow, double rhoDeep);
    public double [] getInterpolationPointDensity(POINT thepoint);

}
