/*
 * Copyright SINTEF FIsheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

/**
 * Interface which defines the required methods to partake in a FhDynamicModel simulation
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public interface ActorIntf {

    /**
     * Method invoked prior to stepping the FhDynamicModel
     */
    void preTick();
    
    /**
     * Method invoked in order to inform the FhDynamicModel that this object
     * is ready to advance the simulation time
     * @param Lookahead Simulation step time in seconds 
     */
    void tick(double Lookahead);
    
    /**
     * Method invoked after the FhDynamicModel has advanced the simulation time
     */
    void postTick();
    
}
