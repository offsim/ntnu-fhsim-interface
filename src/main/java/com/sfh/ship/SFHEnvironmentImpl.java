/*
 * Copyright SINTEF FIsheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

import com.osc.wave.IWaveModel;
import java.util.Arrays;
import java.util.Comparator;
import org.w3c.dom.Element;

/**
 * Interface implementation for the FhSim environment object
 * @see SFHEnvironmentIntf
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public class SFHEnvironmentImpl implements SFHEnvironmentIntf{

    protected double TimeForField1;
    protected double TransitionTime;

    protected final double[] windNED = new double [3];
    protected final double[] currentNED;
    
    protected final double [] Omega1;
    protected final double [] Amplitude1;
    protected final double [] WaveNumber1;
    protected final double [] Phase1;
    protected final double [] Psi1;
    
    
    protected final double [] Omega0;
    protected final double [] Amplitude0;
    protected final double [] WaveNumber0;
    protected final double [] Phase0;
    protected final double [] Psi0;
    
    protected final double [] Omega;
    protected final double [] Amplitude;
    protected final double [] WaveNumber;
    protected final double [] Phase;
    protected final double [] Psi;
    
    
    protected double WindDirection = 0;
    protected double WindSpeed = 0;
    protected double CurrentDirection = 0;
    protected double CurrentSpeed = 0;
    
    protected final String Name;
    protected final String NameIO;
    
    /**
     * Constructor 
     * @see SFHEnvironmentData
     * @param data environment definition data 
     */
    protected SFHEnvironmentImpl( SFHEnvironmentData data ) {
        this.currentNED = new double [3];
        Name = data.getName();
        NameIO = Name+"IO";

        TimeForField1 = 1;
        TransitionTime = 0.1;
        
        int numberOfWaves = FhDynamicModel.instance().getNumberOfWavesInFhSim();
        
        if(numberOfWaves<=0)
            throw new RuntimeException("Number of waves in fhsim not set in FhDynamicModel!");
        
        /*
        Environment elements
        */
        Element envElement = FhDynamicModel.instance().createElement("Lib");
	envElement.setAttribute("LibName",	"SFHBaseLibrary");
	envElement.setAttribute("SimObject",	"DeepSeaGravityWaves");
	envElement.setAttribute("Name",		data.getName());
	envElement.setAttribute("OscMode", "1");
	envElement.setAttribute("Spectrum", "Spectrum"); // requested without default value, used to select material for visualization
	envElement.setAttribute("WaveTheory", "OSC");
	envElement.setAttribute("NumberOfWavesInField", Integer.toString(numberOfWaves) );
	
	envElement.setAttribute("BottomRoughness", ""+data.getBottomRoughneess());
	envElement.setAttribute("BottomStructureScale", ""+data.getBottomtStructureScale());

       
        Omega1 = new double[numberOfWaves];
        Amplitude1 = new double[numberOfWaves];
        WaveNumber1 = new double[numberOfWaves];
        Phase1 = new double[numberOfWaves];
        Psi1 = new double[numberOfWaves];
        
        Omega = new double[numberOfWaves];
        Amplitude = new double[numberOfWaves];
        WaveNumber = new double[numberOfWaves];
        Phase = new double[numberOfWaves];
        Psi = new double[numberOfWaves];

        Omega0 = new double[numberOfWaves];
        Amplitude0 = new double[numberOfWaves];
        WaveNumber0 = new double[numberOfWaves];
        Phase0 = new double[numberOfWaves];
        Psi0 = new double[numberOfWaves];
        
        if( data.getInitialFrequency()== null ){
            for( int i=0; i < numberOfWaves; i++ ){
                Omega0[i] = 1;
                Amplitude0[i] = 0;
                WaveNumber0[i]= 0;
                Phase0[i]= 0;
                Psi0[i]= 0;
            }
        }else{
            assert(data.getInitialFrequency().length >= numberOfWaves );
            for( int i=0; i < numberOfWaves; i++ ){
                Omega0[i] = data.getInitialFrequency()[i];
                Amplitude0[i] = data.getInitialAmplitude()[i];
                WaveNumber0[i] = data.getInitialNumber()[i];
                Phase0[i] = data.getInitialPhase()[i];
                Psi0[i] = data.getInitialDirection()[i];
            }
        }
            
        
        for( int i=0; i < numberOfWaves; i++ ){
            Omega1[i] = Omega0[i];
            Amplitude1[i] = Amplitude0[i];
            WaveNumber1[i]= WaveNumber0[i];
            Phase1[i]= Phase0[i];
            Psi1[i]= Psi0[i];
        
            Omega[i] = Omega0[i] ;
            Amplitude[i] = Amplitude0[i];
            WaveNumber[i]= WaveNumber0[i];
            Phase[i]= Phase0[i];
            Psi[i]= Psi0[i];
        }
        
        envElement.setAttribute("SeaDepth", Double.toString(data.getSeaDepth()) );
        FhDynamicModel.instance().getObjectElement().appendChild(envElement);        
	
        /*
        IO element
        */
        Element envCom = FhDynamicModel.instance().createSimObjectElement(NameIO,"ExternalLinkStandard","FhSimBaseLibrary");
        envCom.setAttribute("inputPortNames", "");
        envCom.setAttribute("outputPortNames", "WindNED,CurrentNED,WaveAmplitude,WaveFrequency,WaveNumber,WaveDirection,WavePhase");
        envCom.setAttribute("Initial_WindNED", "0,0,0");
        envCom.setAttribute("Initial_CurrentNED", "0,0,0");
        envCom.setAttribute("Initial_WaveAmplitude", GenerateArrayString(Amplitude0));
        envCom.setAttribute("Initial_WaveFrequency", GenerateArrayString(Omega0));
        envCom.setAttribute("Initial_WaveNumber", GenerateArrayString(WaveNumber0));
        envCom.setAttribute("Initial_WaveDirection", GenerateArrayString(Psi0));
        envCom.setAttribute("Initial_WavePhase", GenerateArrayString(Phase0));
        FhDynamicModel.instance().getObjectElement().appendChild(envCom);        
        
        
        /*
        interconnections - hook up wave properties to external io 
        */
        Element connection = FhDynamicModel.instance().createElement("Connection");
        connection.setAttribute(Name+".WaveAmplitude", NameIO+".WaveAmplitude");
        connection.setAttribute(Name+".WaveFrequency", NameIO+".WaveFrequency");
        connection.setAttribute(Name+".WaveNumber", NameIO+".WaveNumber");
        connection.setAttribute(Name+".WaveDirection", NameIO+".WaveDirection");
        connection.setAttribute(Name+".WavePhase", NameIO+".WavePhase");
        connection.setAttribute(Name+".CurrentVelocityNED", NameIO+".CurrentNED");
        
        FhDynamicModel.instance().getInterconnectionElement().getParentNode().appendChild(connection);
        FhDynamicModel.instance().registerObject(this,data);                
    }
   
    @Override
    public void tick(double Lookahead) {
	FhDynamicModel.instance().objectUpdated(this, Lookahead);
    }

    @Override
    public void preTick() {
        double Time = FhDynamicModel.instance().getCurrentTime();
        
        windNED[0] = WindSpeed * Math.cos(WindDirection);
        windNED[1] = WindSpeed * Math.sin(WindDirection);
        windNED[2] = 0;

        currentNED[0] = CurrentSpeed * Math.cos(CurrentDirection);
        currentNED[1] = CurrentSpeed * Math.sin(CurrentDirection);
        currentNED[2] = 0;
        
        double fieldProgress = (TimeForField1-Time)/TransitionTime;
        if( fieldProgress > 1 ) fieldProgress = 1;
        if( fieldProgress < 0 ) fieldProgress = 0; 
        double field1Mix = 1 - fieldProgress;
        double field0Mix = fieldProgress;
        
        for( int i=0; i < Omega0.length; i++ ){
            Amplitude[i] = Amplitude0[i]*field0Mix+field1Mix*Amplitude1[i];
            Omega[i] = Omega0[i]*field0Mix+field1Mix*Omega1[i];
            WaveNumber[i] = WaveNumber0[i]*field0Mix+field1Mix*WaveNumber1[i];
            Psi[i] = Psi0[i]*field0Mix+field1Mix*Psi1[i];
            Phase[i] = Phase0[i]*field0Mix+field1Mix*Phase1[i];
        }
        
        FhDynamicModel.instance().setFhSimPort(NameIO, "WindNED", windNED);
        FhDynamicModel.instance().setFhSimPort(NameIO, "CurrentNED", currentNED);
        
        FhDynamicModel.instance().setFhSimPort(NameIO,"WaveAmplitude",Amplitude);
        FhDynamicModel.instance().setFhSimPort(NameIO,"WaveFrequency",Omega);
        FhDynamicModel.instance().setFhSimPort(NameIO,"WaveNumber",WaveNumber);
        FhDynamicModel.instance().setFhSimPort(NameIO,"WaveDirection",Psi);
        FhDynamicModel.instance().setFhSimPort(NameIO,"WavePhase",Phase);
    }

    public void postTick() {
    }

    @Override
    public void waveModelUpdated(IWaveModel model, double transitionTimeInMilliseconds) {
        final double[] omega = model.getOmega();
        final double[] phase = model.getPhase();
        final double[] zeta = model.getZeta_a();
        final double[] psi = model.getPsi();
        final double[] k = model.getWavenum();

        Integer[] idx = new Integer[zeta.length];
        for( int i = 0 ; i < idx.length; i++ ) idx[i] = i;              
        Arrays.sort(idx, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {                        
                return Double.compare(zeta[i2], zeta[i1]);
            }                   
        });        

        for( int i=0; i < Omega0.length; i++ ){
            Amplitude0[i] = Amplitude1[i];
            Omega0[i]=Omega1[i];
            WaveNumber0[i]=WaveNumber1[i];
            Psi0[i]=Psi1[i];
            Phase0[i]=Phase1[i];
        }

        for( int i=0; i < Omega1.length; i++ ){
            Amplitude1[i] = zeta[ idx[i] ];
            Omega1[i]=omega[ idx[i] ];
            WaveNumber1[i]=k[ idx[i] ];
            Psi1[i]=psi[ idx[i] ];
            //flip sign and add 180 deg to the random phase to conform to
            //-A*sin(wt-kx+phase) used in Fathom,vs +A*sin(wt-kx-phase) used in FhSim
            Phase1[i]= -(phase[ idx[i] ]+Math.PI);
        }
        setFhSimWaveTransitionTime(transitionTimeInMilliseconds/1e3);
    }

    private void setFhSimWaveTransitionTime(double d) {
        TimeForField1 = FhDynamicModel.instance().getCurrentTime()+d;
        TransitionTime=d;
    }

    @Override
    public void SetWindDirection(double direction) {
        WindDirection = direction;
    }

    @Override
    public void SetWindSpeed(double speed) {
        WindSpeed = speed;
    }

    @Override
    public void SetCurrentDirection(double direction) {
        CurrentDirection = direction;
    }

    @Override
    public void SetCurrentSpeed(double speed) {
        CurrentSpeed = speed;
    }
    
    protected String GenerateArrayString(double[] val) {
        String retval="";
        for(int i=0; i < val.length-1; i++){
            retval += Double.toString(val[i])+",";
        }
        retval += Double.toString(val[val.length-1]);
        return retval;
    }
}
