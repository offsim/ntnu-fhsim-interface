/*
 * Copyright (C) 2004, 2013, Offshore Simulator Centre AS (OSC AS).
 * All rights reserved.
 OSC AS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.sfh.ship;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author pierre
 */
public class VeresFile {

    public static ArrayList<String> getMfgsInDirectory(String directory) throws IOException {
        ArrayList<String> list = new ArrayList<>();

        try (Stream<Path> walk = Files.walk(Paths.get(directory))) {

            List<String> result = walk.map(x -> x.toString())
                    .filter(f -> f.endsWith(".mgf")).collect(Collectors.toList());

            result.forEach(System.out::println);

            for (String string : result) {
                list.add(string);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;

    }

    public static void main(String[] args) throws IOException {

        //   getMfg("C:\\Users\\pierre\\Documents\\fhsim_models_repo\\src\\main\\resources\\models\\fhsim_models");
        //  VeresFile veres = new VeresFile("C:\\Users\\pierre\\Documents\\fhsim_models\\AHTS_UT797CX_ILO_IslandVictory\\dynmodel\\fhsim\\waterlines\\loadMotion04\\input");
        VeresFile veres = new VeresFile("C:\\Users\\pierre\\Documents\\fhsim_models\\RV_PK410_2_NTNU_Gunnerus\\dynmodel\\fhsim\\waterlines\\loadmaxdraft\\input");

        //    VeresFile veres = new VeresFile("C:\\Users\\pierre\\Documents\\fhsim_models_repo\\src\\main\\resources\\models\\fhsim_models\\HLIV_CJOB720_GEO_ORION\\dynmodel\\fhsim\\waterlines\\LC01B\\Original\\input");
        //VeresFile veres = new VeresFile("C:\\Users\\pierre\\Documents\\fhsim_models\\OSC_VARD306_SUB_NormandOceanic\\dynmodel\\fhsim\\waterlines\\DWL\\input");
        VeresFile.HydFile f = veres.hydFile;
        //TO DO: FIND THE NAME IN THE VERES FILES (POSSIBLE TO READ IT IN THE HYD)
        System.out.println(f.Name);
        System.out.println(String.format("Description : %s", f.WaterlineDescription));
        System.out.println(String.format("stern to APP for %f", veres.getSAPPO()));
        System.out.println(String.format("loa : %f", veres.getLOA()));
        System.out.println(String.format("Lpp : %f", f.Lpp));
        System.out.println(String.format("minimum z : %f", veres.getMgfZMin()));
        System.out.println(String.format("maximum z  : %f", veres.getMgfZMax()));
        System.out.println(String.format("r44 : (r44/breadth)  : %f (%f)", f.r44, f.r44/f.Breadth *100 ));

    }

    public HydFile hydFile;

    /**
     * Class representing a single ship section
     */
    public final class Section {

        public double x;    ///< Location of section relative to aft perpendicular
        public double[] y; ///< Point offsets (from centerline)
        public double[] z; ///< Point offsets (from baseline)
    }
    public ArrayList<Section> hull = new ArrayList<>();

    public VeresFile(String root) {

        try {
            hydFile = getHydData(new FileInputStream(new File(root + ".hyd")));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VeresFile.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            getMgfData(new FileInputStream(new File(root + ".mgf")));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VeresFile.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * REpresentation of the contents of a VERES hyd file (hydrostatic data)
     */
    public class HydFile {

        /**
         * Length between perpendiculars
         */
        public double Lpp;
        /**
         * Vessel breadth
         */
        public double Breadth;
        /**
         * Draft at midship
         */
        public double Draught;
        /**
         * Sinkage
         */
        public double Sinkage;
        /**
         * Trim angle (positive aft)
         */
        public double Trim;
        /**
         * Displacement in KG
         */
        public double Displacement;
        /**
         * Distance from keel to buoancy center
         */
        public double KB;
        /**
         * Distance from keel to center of gravity
         */
        public double VCG;
        /**
         * Distance from aft perpendicular to buoancy center
         */
        public double LCB;
        /**
         * Distance from aft perpendicular to center of gravity
         */
        public double LCG;
        /**
         * Block coefficient
         */
        public double Cb;
        /**
         * Waterline coefficient
         */
        public double Cw;
        /**
         * Prismatic coefficient
         */
        public double Cp;
        /**
         * Center section coefficient
         */
        public double Cm;
        /**
         * Metacentric height in longship direction
         */
        public double GMl;
        /**
         * Metacentric height in transverse direction
         */
        public double GMt;
        /**
         * Roll radius of gyration
         */
        public double r44;
        /**
         * Pitch radius of gyration
         */
        public double r55;
        /**
         * Yaw radius of gyration
         */
        public double r66;
        /**
         * Roll-yaw coupled motion gyration radius
         */
        public double r46;

        /**
         * Name of the ship in the .hyd file
         */
        public String Name;

        /**
         * Description of the waterline in the .hyd file
         */
        public String WaterlineDescription;
    }

    /**
     * Read and parse a hyd file from an stream
     *
     * @param hydFile input stream which contains the hyd file
     * @return new HydFile instance which contains the data
     * @throws FileNotFoundException
     */
    public HydFile getHydData(InputStream hydFile) throws FileNotFoundException {

        Scanner s = new Scanner(hydFile);

        HydFile retval = new HydFile();

        int lineNo = 1;
        while (s.hasNextLine()) {
            String nextLine = s.nextLine();
            String[] tokens = parseSearchText(nextLine);

            switch (lineNo) {
                case 5:
                    retval.Name = nextLine.replace("Ship name:", "");
                    break;
                case 6:
                    retval.WaterlineDescription = nextLine.replace("Loading condition description:", "");
                    break;
                case 12:
                    retval.Lpp = Double.parseDouble(tokens[tokens.length - 1]);
                    break;
                case 13:
                    retval.Breadth = Double.parseDouble(tokens[tokens.length - 1]);
                    break;
                case 14:
                    retval.Draught = Double.parseDouble(tokens[tokens.length - 1]);
                    break;
                case 15:
                    retval.Sinkage = Double.parseDouble(tokens[tokens.length - 1]);
                    break;
                case 16:
                    retval.Trim = Double.parseDouble(tokens[tokens.length - 1]);
                    break;
                case 23:
                    retval.Displacement = 1000 * Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 24:
                    retval.KB = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 25:
                    retval.VCG = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 26:
                    retval.LCB = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 27:
                    retval.LCG = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 28:
                    retval.Cb = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 29:
                    retval.Cw = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 30:
                    retval.Cp = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 31:
                    retval.Cm = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 32:
                    retval.GMl = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 33:
                    retval.GMt = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 34:
                    retval.r44 = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 35:
                    retval.r55 = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 36:
                    retval.r66 = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
                case 37:
                    retval.r46 = Double.parseDouble(tokens[tokens.length - 1].replace("*", ""));
                    break;
            }
            lineNo++;
        }
        return retval;
    }

    /**
     * Parse an string as an Hyd file
     *
     * @param contents contents of hyd file as a string
     * @return new HydFile instance with data
     * @throws FileNotFoundException when file was not found
     */
    public HydFile getHydData(String contents) throws FileNotFoundException {
        return getHydData(new ByteArrayInputStream(contents.getBytes()));
    }

    /**
     * Read the contents of an Mgf file and return a MgfFile instance with the
     * data
     *
     * @param contents file path
     * @return MgfFile instance with station offset data
     * @throws FileNotFoundException when no file is found
     */
    public MgfFile getMgfData(String contents) throws FileNotFoundException {
        return getMgfData(new ByteArrayInputStream(contents.getBytes()));
    }

    /**
     * Read the contents of an Mgf file and return a MgfFile instance with the
     * data
     *
     * @return mgfFile instance with station offset data
     * @return MgfFile instance with station offset data
     */
    public MgfFile getMgfData(InputStream mgfFile) {
        Scanner s = new Scanner(mgfFile);
        MgfFile retval = new MgfFile();

        int lineNo = 1;
        while (s.hasNextLine()) {
            if (lineNo <= 5) {
                String[] tokens = parseSearchText(s.nextLine());
                lineNo++;
            } else {
                Section ss = new Section();

                String[] tokens2 = parseSearchText(s.nextLine());
                lineNo++;
                tokens2 = parseSearchText(s.nextLine());
                lineNo++;
                ss.x = Double.parseDouble(tokens2[0]);

                tokens2 = parseSearchText(s.nextLine());
                lineNo++;
                int numPoints = Integer.parseInt(tokens2[0]);
                ss.y = new double[numPoints];
                ss.z = new double[numPoints];

                for (int i = 0; i < numPoints; i++) {
                    tokens2 = parseSearchText(s.nextLine());
                    lineNo++;
                    ss.y[i] = Double.parseDouble(tokens2[0]);
                    ss.z[i] = Double.parseDouble(tokens2[1]);

                    if (ss.x > 0) {
                        ss.y[i] *= -1;
                    }
                }
                hull.add(ss);
            }
        }
        return retval;
    }

    /**
     * Read a file and place the contents in a string
     *
     * @param filePath file Name
     * @return string with file contents
     */
    public static String readFileAsString(String filePath) {
        byte[] buffer = new byte[(int) new File(filePath).length()];
        BufferedInputStream f = null;
        try {
            f = new BufferedInputStream(new FileInputStream(filePath));
            f.read(buffer);
        } catch (IOException ignored) {
        } finally {
            if (f != null) {
                try {
                    f.close();
                } catch (IOException ignored) {
                }
            }
        }
        return new String(buffer);
    }

    /**
     * Class representing a VERES Mgf (geometry) file
     */
    public class MgfFile {

    }

    /**
     *
     * * @return x coordinate of the center of gravity relative to OSC model
     * origo
     */
    public double getCGXrelOSCModelOrigo() {
        double sappo = getSAPPO();
        double loa = getLOA();
        return (sappo + hydFile.LCG) - loa / 2;
    }

    /**
     *
     * * @return coordinate offset of APP relative to OSC model origo
     */
    public double getAPPrelOSCModelOrigo() {
        double sappo = getSAPPO();
        double loa = getLOA();
        return sappo - (loa / 2);
    }

    /**
     *
     * * @return coordinate offset of FPP relative to OSC model origo
     */
    public double getFPPrelOSCModelOrigo() {

        double sappo = getSAPPO();
        double loa = getLOA();
        return sappo - (loa / 2) + hydFile.Lpp;
    }

    /**
     *
     * * @return x coordinate of the center of gravity relative to OSC model
     * origo
     */
    public double getCGXrelMP() {
        return hydFile.LCG - getLOA() / 2;
    }

    /**
     * @return lowest z coordinate in the mgf file
     */
    public double getMgfZMin() {

        double min = Double.POSITIVE_INFINITY;

        if (hull != null) {

            for (int section = 0; section < hull.size(); section++) {
                for (int i = 0; i < hull.get(section).z.length; i++) {

                    if (min > hull.get(section).z[i]) {
                        min = hull.get(section).z[i];

                    }
                }

            }
        }
        return min;
    }

    /**
     * @return largest z coordinate in the mgf file
     */
    public double getMgfZMax() {

        double max = Double.NEGATIVE_INFINITY;

        if (hull != null) {

            for (int section = 0; section < hull.size(); section++) {

                for (int i = 0; i < hull.get(section).z.length; i++) {

                    if (max < hull.get(section).z[i]) {
                        max = hull.get(section).z[i];

                    }
                }
            }
        }

        return max;
    }

    /**
     *
     * @return Length overall in x axis in m
     */
    public double getLOA() {

        double max = Double.NEGATIVE_INFINITY;

        if (hull != null) {

            for (int section = 0; section < hull.size(); section++) {

                if (max < hull.get(section).x) {
                    max = hull.get(section).x;

                }
            }
        }

        double min = Double.POSITIVE_INFINITY;

        if (hull != null) {

            for (int section = 0; section < hull.size(); section++) {

                if (min > hull.get(section).x) {
                    min = hull.get(section).x;

                }
            }
        }

        return max - min;

    }

    /**
     *
     * Stern to Aft perpendicular offset
     *
     * @return offset between Stern and Aft Perpendicular in x axis in m
     */
    public double getSAPPO() {
        /*
                From Veres Manual:
                The vessel descripton is given in a local coordinate system to preserve compatbility with previous versions of
                the program. The user is provided with a certain degree of freedom in choosing the vertical position of this
                local coordinate system. The origin of the local coordinate system is located at Lpp/2. The z-axis is pointing
                upwards, and the x-axis is pointing towards the stern, and is parallel to the baseline. The vertical position of
                the origin may be taken arbitrarily, and its posi􀆟on relative to the base line may be specified manually in the
                user interface. To enhance flexibility, the user may also specify sinkage and trim relative to the waterline given
                by the vessel draught. A positive trim angle implies that the draught is increased at the stern and reduced at
                the bow. Further, the sinkage and trim are specified rela􀆟ve to the local coordinate system, at Lpp/2.
         */

        double max = Double.NEGATIVE_INFINITY;
        double sappo = 0;

        if (hull != null) {

            for (int section = 0; section < hull.size(); section++) {

                if (max < hull.get(section).x) {
                    max = hull.get(section).x;

                }
            }

            sappo = Math.abs(max) - hydFile.Lpp / 2;
        }

        return sappo;

    }

    public static double[][] getInertiaMatrix(InputStream file) {
        double[][] ret = new double[6][];
        for (int i = 0; i < 6; i++) {
            ret[i] = new double[6];
        }
        Scanner s = new Scanner(file);

        for (int i = 0; i < 11; i++) {
            s.nextLine();
        }

        for (int i = 0; i < 6; i++) {
            String[] tokens = parseSearchText(s.nextLine());
            ret[i][0] = Double.parseDouble(tokens[0]);
            ret[i][1] = Double.parseDouble(tokens[1]);
            ret[i][2] = Double.parseDouble(tokens[2]);
            ret[i][3] = Double.parseDouble(tokens[3]);
            ret[i][4] = Double.parseDouble(tokens[4]);
            ret[i][5] = Double.parseDouble(tokens[5]);
        }
        return ret;
    }

    /**
     * Split a line into tokens
     *
     * @param line line of file
     * @return list of tokens on line
     */
    protected static String[] parseSearchText(String line) {
        ArrayList<String> result = new ArrayList<>();

        boolean returnTokens = true;
        String currentDelims = " \t\r\n\"";
        StringTokenizer parser = new StringTokenizer(line, currentDelims, returnTokens);

        String token = null;
        while (parser.hasMoreTokens()) {
            token = parser.nextToken(currentDelims).trim();
            if (!token.isEmpty()) {
                result.add(token);
            }
        }
        return result.toArray(new String[result.size()]);
    }

}
