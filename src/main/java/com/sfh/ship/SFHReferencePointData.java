/*
 * Copyright SINTEF Fisheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * Class to configure a reference point attached to the hull. 
 * <p>
 * The reference point defines it's own local coordinate system which inherits its position and
 * orientation from the attached hulls position, rotation and offset. 
 * <p>
 * The reference point will track the vessel with a given body fixed offset, both 
 * in linear and rotation. The linear and angular offsets may be specified independently. 
 * Reference points may be actuated in linear or angular direction. The offsets will then 
 * be driven by 1. order dynamics whose response is determined by time constants. Each axis 
 * in linear and angular degrees of freedom have their own time constants. The time constant determines
 * the time taken for the actuator to reach 63% of the reference value. The reference is achieved
 * in 5*time-constant seconds.
 * <p>
 * The reference point coordinate system will be parallel to the vessel's body fixed coordinate system
 * when no angular offsets are specified. The reference point coordinate system is rotated sequentially 
 * around the x,y and z axes.
 * 
 * @see SFHReferencePointIntf
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public class SFHReferencePointData {
    /**Reference point is relative to this point on the hull*/
    protected SFHReferencePointIntf.Reference relativeTo = SFHReferencePointIntf.Reference.AP;
    
    /**Reference point receives force from this source*/
    @XStreamOmitField
    protected SFHReferencePointIntf.ForceSource forceFrom = SFHReferencePointIntf.ForceSource.NoForce;

    /**Name of the object which provides force on the point, empty of no force*/
    @XStreamOmitField
    protected String forceObject;

    /**Name of the output force which provides force input no the reference point*/
    @XStreamOmitField
    protected String forcePort;

    /**Name of the reference point, must be unique*/
    @XStreamOmitField
    protected String name;

    /**Name of the ship this reference point is connected to, must be set*/
    @XStreamOmitField
    protected String shipName;

    /**Flag which determines if the reference point receives force and calculate for/torque pairs for the vessel*/
    @XStreamOmitField
    protected boolean isForceCalculator = false;

    /**Flag to determine if the reference point can be actuated along its linear axes*/
    protected boolean isLinearMotor=false;
    /**Flag to determine if the reference point can be actuated in in rotation about the axes*/
    protected boolean isAngularMotor=false;

    /**The linear offset of the reference point. If the point can be actuated, this is the initial value*/
    protected double [] linearOffset = {0,0,0};
    
    /**The angular offset of the reference point. If the point can be actuated, this is the initial value*/
    protected double [] angularOffset = {0,0,0};
    
    /**Time constant for the 1. order dynamic which represents the linear motor. time constants for [x,y,z] axis*/
    protected double [] linearMotorTimeConstant = {10,10,10};

    /**Time constant for the 1. order dynamic which represents the angular motor. time constants for [rot_x,rot_y,rot_z] axis*/
    protected double [] angularMotorTimeConstant = {10,10,10};

    /**Linear offset for the force attack point. Used to emulate the difference between propulsor position and hull fastening point*/
    protected double [] linearForceOffset = {0,0,0};
        
    private double [] linearMotorMaxChangeRate = {0,0,0};
    
    /**
     * Constructor
     * initializes some arrays
     */
    public SFHReferencePointData() {
        angularMotorTimeConstant = new double[3];
        angularMotorTimeConstant[0] = 0;
        angularMotorTimeConstant[1] = 0;
        angularMotorTimeConstant[2] = 0;

        linearMotorTimeConstant = new double[3];
        linearMotorTimeConstant[0] = 0;
        linearMotorTimeConstant[1] = 0;
        linearMotorTimeConstant[2] = 0;

        linearMotorMaxChangeRate = new double[3];
        linearMotorMaxChangeRate[0] = 0;
        linearMotorMaxChangeRate[1] = 0;
        linearMotorMaxChangeRate[2] = 0;
        
        linearOffset = new double[3];
        linearOffset[0] = 0;
        linearOffset[1] = 0;
        linearOffset[2] = 0;
        
        linearForceOffset = new double[3];
        linearForceOffset[0]=0;
        linearForceOffset[1]=0;
        linearForceOffset[2]=0;
        
        angularOffset = new double[3];
        angularOffset[0] = 0;
        angularOffset[1] = 0;
        angularOffset[2] = 0;
        
        relativeTo = SFHReferencePointIntf.Reference.AP;
    }
    
    
    /**
     * Construct a new reference point without an offset 
     * @param myName the name of the reference point, must be unique
     */
    public SFHReferencePointData( String myName ){
        name = myName;
        relativeTo = SFHReferencePointIntf.Reference.CF;
    }
    
    /**
     * Construct a new reference point at a given linear offset relative to AP
     * @param myName the name of the reference point, must be unique
     * @param x the offset in x direction
     * @param y the offset in y direction
     * @param z the offset in z direction
     */
    public SFHReferencePointData( String myName, double x, double y, double z){
        name = myName;
        linearOffset[0] = x;linearOffset[1] = y;linearOffset[2] = z;
        relativeTo = SFHReferencePointIntf.Reference.AP;
    }
    
    /**
     * Construct a new reference point at a given linear offset, relative to a specific reference on the hull
     * @param myName the name of the reference point, must be unique
     * @param x the offset in x direction
     * @param y the offset in y direction
     * @param z the offset in z direction
     * @param ref the point on the hull which the offset is relative to
     */
    public SFHReferencePointData( String myName, double x, double y, double z,SFHReferencePointIntf.Reference ref){
        name = myName;
        linearOffset[0] = x;linearOffset[1] = y;linearOffset[2] = z;
        relativeTo = ref;
    }
    
    /**
     * Return the point on the hull which the point is relative to 
     * @return the reference point 
     */
    public SFHReferencePointIntf.Reference getRelativeTo() {
        return relativeTo;
    }

    /**
     * Update the reference point
     * @param relativeTo the updated reference point type
     */
    public void setRelativeTo(SFHReferencePointIntf.Reference relativeTo) {
        this.relativeTo = relativeTo;
    }

    /**
     * Return the type of force input on the reference point
     * @return the force input type
     */
    public SFHReferencePointIntf.ForceSource getForceFrom() {
        return forceFrom;
    }

    /**
     * Update the source type for the input force. Setting a force source other than
     * NoForce causes the reference point to become  "ForceCalculator"
     * @param forceFrom the updated force input type
     */
    public void setForceFrom(SFHReferencePointIntf.ForceSource forceFrom) {
        this.forceFrom = forceFrom;
        if( forceFrom != SFHReferencePointIntf.ForceSource.NoForce )
            setIsForceCalculator(true, forceFrom);
        else
            setIsForceCalculator(false, forceFrom);
    }

    /**
     * The name of the object which provides input force
     * @return the name of the force source object
     */
    public String getForceObject() {
        return forceObject;
    }

    /**
     * Update the source of the input force
     * @param forceObject the name if the new force object
     */
    public void setForceObject(String forceObject) {
        this.forceObject = forceObject;
    }

    /**
     * Return the name of the output port which provides input force
     * @return the name of the force source port
     */
    public String getForcePort() {
        return forcePort;
    }

    /**
     * Update the name of the output port which provides input port
     * @param forcePort the new force output port
     */
    public void setForcePort(String forcePort) {
        this.forcePort = forcePort;
    }    
    
    /**
     * Return the name of the reference point. Must be unique
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Update the name of the reference point. Must be unique
     * @param name the updated name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Return the name of the ship which the reference point is connected to
     * @return the name of the ship object
     */
    public String getShipName() {
        return shipName;
    }

    /**
     * Update the name of the ship which the reference point is connected to
     * @param shipName the name of the ship object
     */
    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    /**
     * Test if this reference point translates an input force to a force/torque pair
     * @return true if force calculation is enabled
     */
    public boolean getIsForceCalculator() {
        return isForceCalculator;
    }

    /**
     * Update the force calculator property with a value and new force source
     * @param isForceCalculator the updated force calculator flag
     * @param src the updated force source
     */
    public void setIsForceCalculator(boolean isForceCalculator, SFHReferencePointIntf.ForceSource src) {
        if( !isForceCalculator && src != SFHReferencePointIntf.ForceSource.NoForce)
            throw new RuntimeException("Configuration exception");
        if( isForceCalculator && src == SFHReferencePointIntf.ForceSource.NoForce)
            throw new RuntimeException("Configuration exception");
        
        this.isForceCalculator = isForceCalculator;
        this.forceFrom = src;
    }

    /**
     * Test if the reference point is a linear motor 
     * @return true if linear axes can be actuated during runtime
     */
    public boolean getLinearMotor() {
        return isLinearMotor;
    }

    /**
     * Update the reference point linear motor state
     * @param isLinearMotor true if linear aces can be actuated during runtime
     */
    public void setIsLinearMotor(boolean isLinearMotor) {
        this.isLinearMotor = isLinearMotor;
    }

    /**
     * Test of the reference point is actuated in rotation during runtime
     * @return true of the axes can be rotated
     */
    public boolean getAngularMotor() {
        return isAngularMotor;
    }

    /**
     * Update the angular motor flag
     * @param isAngularMotor the updated angular motor flag, true of enabled
     */
    public void setIsAngularMotor(boolean isAngularMotor) {
        this.isAngularMotor = isAngularMotor;
    }

    /**
     * Get the linear offset, fixed if no linear motor or used as initial value 
     * @return [1x3] vector of the linear offset
     */
    public double[] getLinearOffset() {
        return linearOffset.clone();
    }

    /**
     * Update the linear offset, fixed if no linear motor or used as initial value 
     * @param linearOffset the updated linear offset as a [1x3] vector in meters
     */
    public void setLinearOffset(double[] linearOffset) {
        this.linearOffset[0] = linearOffset[0];
        this.linearOffset[1] = linearOffset[1];
        this.linearOffset[2] = linearOffset[2];
    }
    
    /**
     * Update the linear offset, either fixed if no linear motor or initial value 
     * @param x new offset in x direction in meter
     * @param y new offset in y direction in meter
     * @param z new offset in z direction in meter
     */
    public void setLinearOffset(double x, double y, double z) {
        this.linearOffset[0] = x;
        this.linearOffset[1] = y;
        this.linearOffset[2] = z;
    }

    /**
     * Get the angular offset, fixed if no angular motor or used as initial value 
     * @return [1x3] vector of the angular offsets in degrees
     */
    public double[] getAngularOffset() {
        return angularOffset.clone();
    }

    /**
     * Update the angular offset, fixed if no angular motor or used as initial value 
     * @param angularOffset [1x3] vector of the angular offsets in degrees
     */
    public void setAngularOffset(double[] angularOffset) {
        this.angularOffset[0] = angularOffset[0];
        this.angularOffset[1] = angularOffset[1];
        this.angularOffset[2] = angularOffset[2];
    }
    
    /**
     * Update the angular offset, fixed if no angular motor or used as initial value 
     * @param x The angular rotation about the x-axis i degrees
     * @param y The angular rotation about the y-axis i degrees
     * @param z The angular rotation about the z-axis i degrees
     */
    public void setAngularOffset(double x, double y, double z) {
        this.angularOffset[0] = x;
        this.angularOffset[1] = y;
        this.angularOffset[2] = z;
    }

    
    /**
     * Return the time constants for the linear motor actuators. Each time constant controls 
     * a 1. order dynamic and the time constant gives the time taken for the dynamic to reach
     * 63% of its reference value. 99% of the reference value is reached within 5*time constant
     * @return [1x3] vector of linear motor time constants in x-y-z direction 
     */
    public double[] getLinearMotorTimeConstant() {
        return linearMotorTimeConstant.clone();
    }

    /**
     * Return the time constants for the linear motor actuators. Each time constant controls 
     * a 1. order dynamic and the time constant gives the time taken for the dynamic to reach
     * 63% of its reference value. 99% of the reference value is reached within 5*time constant
     * @param linearMotorTimeConstant [1x3] vector of linear motor time constants in x-y-z direction 
     */
    public void setLinearMotorTimeConstant(double[] linearMotorTimeConstant) {
        this.linearMotorTimeConstant = linearMotorTimeConstant;
    }

    /**
     * Return the time constants for the angular motor actuators. Each time constant controls 
     * a 1. order dynamic and the time constant gives the time taken for the dynamic to reach
     * 63% of its reference value. 99% of the reference value is reached within 5*time constant
     * @return [1x3] vector of angular motor time constants in x-y-z rotation
     */
    public double[] getAngularMotorTimeConstant() {
        return angularMotorTimeConstant;
    }

    /**
     * Return the time constants for the angular motor actuators. Each time constant controls 
     * a 1. order dynamic and the time constant gives the time taken for the dynamic to reach
     * 63% of its reference value. 99% of the reference value is reached within 5*time constant
     * @param angularMotorTimeConstant [1x3] vector of angular motor time constants in x-y-z rotation
     */
    public void setAngularMotorTimeConstant(double[] angularMotorTimeConstant) {
        this.angularMotorTimeConstant = angularMotorTimeConstant;
    }
    
    /**
     * Returns a new {@code SFHReferencePointIntf} configured by this data
     * object.
     *
     * @return a new {@code SFHReferencePointIntf} configured by this data
     * object.
     */
    public SFHReferencePointIntf create() {
        return new SFHReferencePointImpl(this);
    }
    
    /**
     * @return the linearForceOffset
     */
    public double[] getLinearForceOffset() {
        double []retval = new double [3];
        
        retval[0] = linearForceOffset[0];
        retval[1] = linearForceOffset[1];
        retval[2] = linearForceOffset[2];
        
        return retval;
    }

    /**
     * @param x
     * @param y
     * @param z
     */
    public void setLinearForceOffset(double x, double y, double z) {
        linearForceOffset[0] = x;
        linearForceOffset[1] = y;
        linearForceOffset[2] = z;
    }    

    /**
     * Return the max change rate for the linear motors in m/s
     * @return double [3] of the linear motor max change rate along the local axes
     */
    public double[] getLinearMotorMaxChangeRate() {
        return linearMotorMaxChangeRate;
    }

    /**
     * Set the max change rate for the linear motors in m/s
     * @param linearMotorMaxChangeRate double [3] of the linear motor max change rate along the local axes
     */
    public void setLinearMotorMaxChangeRate(double[] linearMotorMaxChangeRate) {
        this.linearMotorMaxChangeRate = linearMotorMaxChangeRate;
    }

    /**
     * Set the max change rate for the linear motors in m/s
     * @param rate_x linear motor max change rate in m/s along the local x-axis
     * @param rate_y linear motor max change rate in m/s along the local y-axis
     * @param rate_z linear motor max change rate in m/s along the local z-axis
     */
    public void setLinearMotorMaxChangeRate(double rate_x, double rate_y, double rate_z) {
        this.linearMotorMaxChangeRate[0] = rate_x;
        this.linearMotorMaxChangeRate[1] = rate_y;
        this.linearMotorMaxChangeRate[2] = rate_z;
    }

    /**
     * Set a uniform max change rate for the linear motors in m/s
     * @param rate the linear motor max change rate in m/s for all axes
     */
    public void setLinearMotorMaxChangeRate(double rate) {
        this.linearMotorMaxChangeRate[0] = rate;
        this.linearMotorMaxChangeRate[1] = rate;
        this.linearMotorMaxChangeRate[2] = rate;
    }
}
