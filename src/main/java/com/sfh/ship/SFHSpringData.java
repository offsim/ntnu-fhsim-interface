/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.ship;

/**
 *
 * @author Tobias Torben
 */
public class SFHSpringData  {
    private double stiffness;
    private double relaxedLength;
    private String name;
    private SFHReferencePointData refpointA;
    private SFHReferencePointData refpointB;
    private String connectionNameA;
    private String connectionNameB;
    private double[] fixedConnectionA;
    private double[] fixedConnectionB;
    public static enum End {A, B};
    
    
    public SFHSpringData(double stiffness, double relaxedLength, String name) {
        this.stiffness = stiffness;
        this.relaxedLength = relaxedLength;
        this.name = name;    
    }
    
    public void ConnectToReference(SFHReferencePointData data, End endToConnect){
        
        if (endToConnect == End.A) {
            refpointA = data;
            connectionNameA = data.getName();
            data.setIsForceCalculator(true, SFHReferencePointIntf.ForceSource.FhSimPort);
            data.setForceObject(name);
            data.setForcePort("ForceA");
        }
        
        else if (endToConnect == End.B) {
            refpointB = data;
            connectionNameB = data.getName();
            data.setIsForceCalculator(true, SFHReferencePointIntf.ForceSource.FhSimPort);
            data.setForceObject(name);
            data.setForcePort("ForceB");
        }
        
        else {
            System.out.println("Invalid END value\n");
            System.exit(1);
        }        
    }
    
    public void ConnectToFixedPoint(double[] point, End endToConnect) {
        if (endToConnect == End.A) {
            fixedConnectionA = point;
        }
        
        else if (endToConnect == End.B) {
            fixedConnectionB = point;
        }
        
        else {
            System.out.println("Invalid END value\n");
            System.exit(1);
        }        
    }
    
    public SFHSpringImpl create() {
        return new SFHSpringImpl(this);
    }
    
    public double getStiffness() {
        return stiffness;
    }

    public void setStiffness(double stiffness) {
        this.stiffness = stiffness;
    }

    public double getRelaxedLength() {
        return relaxedLength;
    }

    public void setRelaxedLength(double relaxedLength) {
        this.relaxedLength = relaxedLength;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
     public SFHReferencePointData getRefpointA() {
        return refpointA;
    }

    public void setRefpointA(SFHReferencePointData refpointA) {
        this.refpointA = refpointA;
    }

    public SFHReferencePointData getRefpointB() {
        return refpointB;
    }

    public void setRefpointB(SFHReferencePointData refpointB) {
        this.refpointB = refpointB;
    }

    public String getConnectionNameA() {
        return connectionNameA;
    }

    public void setConnectionNameA(String connectionNameA) {
        this.connectionNameA = connectionNameA;
    }

    public String getConnectionNameB() {
        return connectionNameB;
    }

    public void setConnectionNameB(String connectionNameB) {
        this.connectionNameB = connectionNameB;
    }

    public double[] getFixedConnectionA() {
        return fixedConnectionA;
    }

    public void setFixedConnectionA(double[] fixedConnectionA) {
        this.fixedConnectionA = fixedConnectionA;
    }

    public double[] getFixedConnectionB() {
        return fixedConnectionB;
    }

    public void setFixedConnectionB(double[] fixedConnectionB) {
        this.fixedConnectionB = fixedConnectionB;
    }

}
