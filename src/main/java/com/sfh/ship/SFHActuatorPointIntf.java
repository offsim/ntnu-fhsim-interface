/*
 * Copyright SINTEF FIsheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

/**
 * Interface to an actuator point (both a reference point and an actuator with
 * serve motors and RPM)
 *
 * <p>The ActuatorPoint is a specialization of the reference point which contains
 * first order dynamics for pitch servo motor, and RPM motor (with gear ratio) and
 * a interface to the Z-axis angular motor (azimuth actuator) for the underlying
 * reference point. 
 * 
 * <p>The actuator point also contain error mechanisms to manipulate the behavior of 
 * the RPM/pitch/azimuth motor functionality and the feedback values produced. @sa FailureModes
 * 
 * @see SFHReferencePointIntf
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public interface SFHActuatorPointIntf extends SFHReferencePointIntf {

    /**
     * The types of failure modes supported by the actutaor point
     */
    enum FailureModes {
        /** Engine fails, RPM falls*/
        failureModeRPMError, 
        /** Engine locks. RPM is uncontrollable*/
        failureModeRPMLock, 
        /** Pitch engine locks, Pitch is uncontrollable*/
        failureModePitchLock, 
        /** Pitch engine malfunctions. Pitch falls*/
        failureModePitchMalfunction,
        /** Azimuth engine locks. Azimuth angle uncontrollable*/
        failureModeAzimuthLock, 
        /** Azimuth engine locks at constant rate. Azimuth angle uncontrollable*/
        failureModeAzimuthMalfunction, 
        /** Feedback signal from pitch do not change*/
        failureModeFeedbackPitchFreeze, 
        /** Feedback signal from RPM do not change*/
        failureModeFeedbackRPMFreeze, 
        /** Feedback signal of azimuth angle do not change*/
        failureModeFeedbackAzimuthFreeze,
        /** Feedback of produced thrust do not change*/
        failureModeFeedbackThrustFreeze, 
        /** Number of failure modes*/
        failureModes
    };

    /**
     * Set the maximum angle of the propulsor in degrees, rudder angle for
     * rudders and azimuth angle for azimuth thrusters
     * @return Max propulsor angle in degrees
     */
    public double MaxAngle();

    /**
     * Set the minimum angle of the propulsor in degrees, rudder angle for
     * rudders and azimuth angle for azimuth thrusters
     * @return Min propulsor angle in degrees
     */
    public double MinAngle();

    /**
     * Get the current max RPM of the propulsor. Refers the the rotation speed
     * of the propeller
     * @return the current maximum RPM of the propulsor
     */
    public double MaxRPM();

    /**
     * Get the current min RPM of the propulsor. Refers the the rotation speed
     * of the propeller
     * @return the current minimum RPM of the propulsor
     */
    public double MinRPM();

    /**
     * Get the current maximum pitch angle of the pitch servo. Angle in degrees
     * @return the current max pitch angle 
     */
    public double MaxPitch();

    /**
     * Get the current minimum pitch angle of the pitch servo. Angle in degrees
     * @return the current min pitch angle 
     */
    public double MinPitch();

    /**
     * Update maximum azimuth angle of the azimuth motor. Angle in degrees
     * @param val The updated maximum angle in degrees
     */
    public void MaxAngle(double val);

    /**
     * Update minimum azimuth angle of the azimuth motor. Angle in degrees
     * @param val The updated maximum angle in degrees
     */
    public void MinAngle(double val);

    /**
     * Update the propulsor maximum RPM
     * @param val new maximum RPM value 
     */
    public void MaxRPM(double val);

    /**
     * Update the propulsor minimum RPM
     * @param val new minimum RPM value 
     */
    public void MinRPM(double val);

    /**
     * Update the servo motor maximum pitch angle in degrees
     * @param val the updated maximum pitch angle in degrees
     */
    public void MaxPitch(double val);

    /**
     * Update the servo motor minimum pitch angle in degrees
     * @param val the updated minimum pitch angle in degrees
     */
    public void MinPitch(double val);


    /**
     * Return the current commanded pitch angle in degrees
     * @return the current commanded pitch angle in degrees
     */
    public double getComandedPitch();

    /**
     * Set a new commanded pitch angle in degrees
     * @param comandedPitch new commanded pitch in degrees
     */
    public void setComandedPitch(double comandedPitch);


    /**
     * Return the current commanded propulsor RPM 
     * @return The current commanded propulsor RPM
     */
    public double getCommandedRPM();

    /**
     * Set a new commanded propulsor RPM 
     * @param commandedRPM the new commanded propuslor RPM 
     */
    public void setCommandedRPM(double commandedRPM);

    /**
     * Get the currently commanded azimuth angle in degrees
     * @return the currently commanded azimuth angle in degrees
     */
    public double getCommandeAngle();

    /**
     * Updated the commanded azimuth angles in degrees
     * @param commandeAngle The new commanded azimuth angle in degrees
     */
    public void setCommandeAngle(double commandeAngle);

    /**
     * Return the commanded pitch angle normalized with the pitch angle limits.
     * @return normalized commanded pitch angle in the region [-1,1]
     */
    public double getComandedPitchNormalized();

    /**
     * Update the commanded pitch angle with a normalized value
     * @param comandedPitch new commanded pitch angle as a fraction of min/max angle in the region [-1,1]
     */
    public void setComandedPitchNormalized(double comandedPitch);

    /**
     * Return the commanded RPM normalized with the RPM limits 
     * @return normalized commanded RPM value in the region [-1,1]
     */
    public double getCommandedRPMNormalized();

    /**
     * Update the commanded RPM with a normalized value 
     * @param commandedRPM new commanded RPM as a fraction of max/min RPM in the region [-1,1]
     */
    public void setCommandedRPMNormalized(double commandedRPM);

    /**
     * Return the commanded azimuth angle normalized with the azimuth angle limits 
     * @return normalized commanded azimuth angle in the region [-1,1]
     */
    public double getCommandeAngleNormalized();

    /**
     * Update the commanded azimuth angle with a normalized value
     * @param commandeAngle new commanded azimuth angle as a fraction of min/max angle in the region [-1,1]
     */
    public void setCommandeAngleNormalized(double commandeAngle);


    /**
     * Return actual RPM of the propulsor
     * @return The actual propulsor RPM
     */
    public double ActualRPM();

    /**
     * Return actual RPM of the motor
     * @return The actual motor RPM
     */
    public double ActualMotorRPM();

    /**
     * Get the actual pitch angle in degrees
     * @return Actual pitch angle in degrees
     */
    public double ActualPitch();

    /**
     * Get the actual azimuth angle in degrees
     * @return the actual azimuth angle in degrees
     */
    public double ActualAngle();

    /**
     * Get the feedback value of the RPM (includes errors)
     * @return Feedback RPM value
     */
    public double RPMFeedback();

    /**
     * Get the feedback value of the pitch angle in degrees (includes errors)
     * @return Feedback pitch angle in degrees
     */
    public double PitchFeedback();

    /**
     * Get the feedback value of the azimuth angle in degrees (includes errors)
     * @return Feedback azimuth angle value in degrees
     */
    public double AngleFeedback();

    /**
     * Get the feedback value of the produced thrust in N
     * @return Feedback thrust in N
     */
    public double ThrustFeedback();


    /**
     * Get engine enabled flag
     * @return true if RPM engine in actuator point is enabled
     */
    public boolean getEngineEnabled();

    /**
     * Update engine enabled flag
     * @param val new value of engine enabled
     */
    public void setEngineEnabled(boolean val);

    /**
     * Set the power available to the RPM motor. If power is insufficient the
     * RPM value will decrease until the power is sufficient to drive the engine
     * @param watts the available power in watts
     */
    public void setAvailablePower(double watts);

    /**
     * Is the actuator point a rudder connection
     * @return true if connected to a rudder
     */
    public boolean IsRudder();

    /**
     * Is the actuator point an azimuth connection
     * @return true on azimuth connection
     */
    public boolean IsAzimuth();

    /**
     * Test for RPM error: RPM decreases towards zero
     * @return true on RPM error
     */
    public boolean FailureModeRPMError();

    /**
     * Test for RPM lock error: RPM cannot be changed
     * @return true on RPM lock error
     */
    public boolean FailureModeRPMLock();

    /**
     * Test for failure mode pitch lock: Pitch angle cannot be changed
     * @return true on pitch lock error
     */
    public boolean FailureModePitchLock();

    /**
     * Test for pitch malfunction error: pitch angle decreases towards zero
     * @return true on pitch malfunction
     */
    public boolean FailureModePitchMalfunction();

    /**
     * Test for azimuth lock failure mode: azimuth angle cannot be changed
     * @return true on azimuth lock error
     */
    public boolean FailureModeAzimuthLock();

    /**
     * Test for failure mode pitch feedback freeze: readout of pitch angle is stuck
     * @return true on pitch feedback freeze
     */
    public boolean FailureModeFeedbackPitchFreeze();

    /**
     * Test for failure mode RPM feedback freeze: readout of RPM value is stuck
     * @return true on RPM feedback error
     */
    public boolean FailureModeFeedbackRPMFreeze();

    /**
     * Test for failure mode azimuth feedback freeze: readout of the azimuth angle is stuck
     * @return true on azimuth feedback error 
     */
    public boolean FailureModeFeedbackAzimuthFreeze();

    /**
     * Test for failure mode feedback trust freeze: thrust readout is stuck
     * @return true on thrust feedback error 
     */
    public boolean FailureModeFeedbackThrustFreeze();

    /**
     * Test for failure mode azimuth malfunction: azimuth angle changes continously
     * @return true on azimuth malfunction 
     */
    public boolean FailureModeAzimuthMalfunction();
    
    /**
     * Update the RPM error flag: when enabled RPM will decrease towards zero
     * @param val new value of RPM error flag
     */
    public void FailureModeRPMError(boolean val);

    /**
     * Update the RPM lock flag: when enabled the actual will RPM not respond to changes
     * @param val new value of RPM lock flag
     */
    public void FailureModeRPMLock(boolean val);

    /**
     * Update the pitch lock flag: when enabled the actual pitch not respond to changes
     * @param val new value of pitcM lock flag
     */
    public void FailureModePitchLock(boolean val);

    /**
     * Update the pitch malfunction flag: when enabled the pitch will decrease towards zero
     * @param val new value of pitch malfunction flag
     */
    public void FailureModePitchMalfunction(boolean val);

    /**
     * Update the azimuth malfunction flag: when enabled the azimuth engine will be stock on a static rate of change
     * @param val new value of azimuth malfunction flag
     */
    public void FailureModeAzimuthMalfunction(boolean val);

    /**
     * Update the azimuth lock failure flag: azimuth angle does not respond to changes
     * @param val the new value of the azimuth lock flag
     */
    public void FailureModeAzimuthLock(boolean val);

    /**
     * Update the pitch feedback freeze flag: feedback value of pitch angle is stuck
     * @param val the new value of the pitch feedback freeze flag
     */
    public void FailureModeFeedbackPitchFreeze(boolean val);

    /**
     * Update the RPM feedback freeze flag: feedback value of RPM is stuck
     * @param val the new value of the RPM feedback freeze flag
     */
    public void FailureModeFeedbackRPMFreeze(boolean val);

    /**
     * Update the azimuth angle feedback freeze flag: feedback value of azimuth angle is stuck
     * @param val the new value of the azimuth angle feedback freeze flag
     */
    public void FailureModeFeedbackAzimuthFreeze(boolean val);

    /**
     * Update the thrust force feedback freeze flag: feedback value of thrust force is stuck
     * @param val the new value of the thrust force feedback freeze flag
     */
    public void FailureModeFeedbackThrustFreeze(boolean val);
}
