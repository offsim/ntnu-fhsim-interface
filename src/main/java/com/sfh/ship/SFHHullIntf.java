/*
 * Copyright SINTEF FIsheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

/**
 * Interface specification to the ship model during simulation
 * @see SFHHullData
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public interface SFHHullIntf extends ActorIntf {
   
    /**
     * Return the name of the vessel
     * @return The name of the ship object as set in the data class and in FhSim
     */
    String GetName();
    
    /**
     * Return the vertical center of buoyancy for the vessel (VERES calculated)
     * @return The vertical center of buoyancy as measured positive form the vessel baseline
     */
    double GetVerticalCentreOfBuoyancy();
    
    /**
     * Return the longitudinal center of buoyancy (VERES calculated)
     * @return The longitudinal position of buoyancy in meters from AP
     */
    public double GetLongitudinalCentreOfBuoyancy();
    
    
    /**
     * Get the length between perpendiculars (rudder stem and bow piercing position)
     * @return 
     */
    double GetLengthBetweenPerpendiculars();
    
    /**
     * Return the design draught of the vessel (VERES input)
     * @return The design draught measured positive from the vessel baseline
     */
    double GetDesignDraught();
    
    /**
     * Return the current draught of the vessel
     * @return The current submergence of the vessel midship as measured from the vessel baseline
     */
    double GetDraught();
    
    /**
     * Retrieve the position of the vessel center of gravity relative to the AP/CL/BL 
     * @return 3x1 vector of CG offset
     */
    double [] GetCGPosition();
    
    /**
     * @return The ship position in North-East-Down in meters 
     */
    double [] GetGlobalPosition();
    
    /**
     * @return The ship orientation in roll-pitch-yaw in radians 
     */
    double [] GetGlobalOrientation();

    /**
     * @return Linear ship velocity in North-East-Down in m/s
     */
    double [] GetGlobalVelocity();
    
    /**
     * Get the rotation velocity in the global frame
     * @return 1x3 vector of rotational rates about the global coordinate system axes
     */
    double [] GetGlobalRotationVelocity();
    
    /**
     * Get the linear ship velocities in the body fixed frame
     * @return Linear ship velocity in local coordinates in m/s
     */
    double [] GetLocalVelocity();

    /**
     * Get the linear ship velocities in the body fixed frame relative to water velocity
     * @return Linear ship velocity relative to water in local coordinates in m/s
     */
    public double[] GetLocalWaterVelocity() ;  
    
    /**
     * Get the rotational ship velocities in the body fixed frame
     * @return Rotational ship velocity in local coordinates in rad/s
     */
    double [] GetLocalRotationVelocity();

    /**
     * Get the current force acting on the vessel in 6DOF
     * @return 1x6 vector of current forces in N and Nm
     */
    double [] GetCurrentForce();
    
    /**
     * Get the first order wave force acting on the vessel in 6DOF
     * @return 1x6 vector of first order wave forces in N and Nm
     */
    double [] GetFirstOrderForce();
    
    /**
     * Get the second order wave force acting on the vessel in 6DOF
     * @return 1x6 vector of second order wave forces in N and Nm
     */
    double [] GetSecondOrderForce();
    
    /**
     * Get the wind force acting on the vessel in 3DOF
     * @return 1x3 vector of wind forces in N
     */
    double [] GetWindForce();

    /**
     * Get the wind torque acting on the vessel in 3DOF
     * @return 1x3 vector of wind forces in Nm
     */
    double [] GetWindTorque();
    
    
    /**
     * Instantaneously set the position and heading of the vessel relative to the midpoint on the baseline
     * 
     * This method will trigger a state change in FhSim without dynamic, and 
     * will therefore result in an infinite first order derivative. 
     * Note: Use with caution
     * @param north new north coordinate in meter
     * @param east new east coordinate in meter
     * @param down new down coordinate in meter
     * @param heading new heading relative to north
     */
    void setPositionAndHeadingMidpointNED(double north, double east, double down, double heading);
    
    /**
     * Instantaneously set the position and heading of the vessel relative to the midpoint
     * 
     * This method will trigger a state change in FhSim without dynamic, and 
     * will therefore result in an infinite first order derivative. 
     * Note: Use with caution
     * @param north new north coordinate in meter
     * @param east new east coordinate in meter
     * @param heading new heading relative to north
     */
    void setPositionAndHeadingMidpointNE(double north, double east, double heading);

    /**
     * Instantaneously set the position of the vessel center of gravity.
     * 
     * This method will trigger a state change in FhSim without dynamic, and 
     * will therefore result in an infinite first order derivative. 
     * Note: Use with caution
     * @param north new north coordinate in meter
     * @param east new east coordinate in meter
     * @param down new down coordinate in meter
     */
    void SetPositionNED(double north, double east, double down);

    /**
     * Instantaneously set the position of the vessel center of gravity.
     * 
     * This method will trigger a state change in FhSim without dynamic, and 
     * will therefore result in an infinite first order derivative. 
     * Note: Use with caution
     * @param north new north coordinate in meter
     * @param east new east coordinate in meter
     */
    void SetPositionNE(double north, double east);

    /**
     * Instantaneously set the position of the vessel midpoint on the baseline.
     * 
     * This method will trigger a state change in FhSim without dynamic, and 
     * will therefore result in an infinite first order derivative. 
     * Note: Use with caution
     * @param north new north coordinate in meter
     * @param east new east coordinate in meter
     * @param down new down coordinate in meter
     */
    void SetPositionMidpointNED(double north, double east, double down);

    /**
     * Instantaneously set the position of the vessel midpoint on the baseline.
     * 
     * This method will trigger a state change in FhSim without dynamic, and 
     * will therefore result in an infinite first order derivative. 
     * Note: Use with caution
     * @param north new north coordinate in meter
     * @param east new east coordinate in meter
     */
    void SetPositionMidpointNE(double north, double east);
    
    
    /**
     * Instantaneously set the heading of the vessel.
     * This method will trigger a state change in FhSim without dynamic, and 
     * will therefore result in an infinite first order derivative. 
     * Note: Use with caution
     * @param heading_north the new heading in degrees from north axis
     */
    void SetHeadingNED(double heading_north);
    
    /**
     * Set an external force on the hull, in body fixed coordinates
     * @param forceBodyInput [1x3] vector of forces in N
     */
    public void setForceBodyInput(double[] forceBodyInput);

    /**
     * Set an external torque on the hull, in body fixed ccordinates
     * @param torqueBodyInput [1x3] vector of torqes in Nm
     */
    public void setTorqueBodyInput(double[] torqueBodyInput);
    
    public double [] rotateToHullFrame(double [] v);
    
}
