/*
 * Copyright SINTEF FIsheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

/**
 * A ship fixed reference point which can be attached to the ship. The reference
 * point retains its local position relative to the reference. The position is 
 * determined by the local offset which can be manipulated at runtime. The 
 * reference point provides a local axis system with default orientation parallel
 * to the vessel body fixed frame (x-forward, z starboard, z down)
 * 
 * The reference point provides two additional functions
 * 1) Linear and angular motor articulation of the point
 * 2) Translation of force to force/torque pair and application of the pair to the vessel
 * 
 * @see SFHReferencePointData
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public interface SFHReferencePointIntf extends ActorIntf {
    
    /**
     * Enumeration which determines which point on the vessel
     * the reference point is relative to
     */
     public enum Reference{
        /** relative to center of gravity*/
         CG, 
        /** relative to flotation point in waterline*/
        CF, 
        /** relative to aft perpendicular in baseline*/
        AP  
    }
    
     /**
      * The source of an force input
      */
    public enum ForceSource{
        /** The force is set from outside of FhSim (ie. from this API)*/
        External,   
        /** The force is set from another port in FhSim*/
        FhSimPort,  
        /** No force is feed through the reference point*/
        NoForce     
    }

    /**
     * Get the name of the reference point
     * @return the name of the reference point
     */
    public String getName();
    
    /**
     * Test if the linear and angular motors of this reference point are controlled
     * by a OpusPropulsor. Used to determine if the reference point should connect
     * motor control ports to its own external IO block (control by this API) or 
     * defer it to the setup of the SFHOpusPropulsor
     * @see SFHOpusPropulsorData
     * @see SFHOpusPropulsorIntf
     * @return true if OpusPropulsor controls the motor
     */
    //public boolean getPropulsorControlMotor();
    
    /**
     * Toggle external control of the linear and angular motors of this reference
     * point. The reference point will defer connection of its motor control ports.
     * The motor control ports must be connected by another object. They cannot be left
     * unconnected as they are input ports.
     * @see SFHOpusPropulsorData
     * @see SFHOpusPropulsorIntf
     * @param val true if the reference point should defer motor connections
     */
    //public void setPropulsorControlMotor(boolean val);
    
    /**
     * Test if this reference point receives external force (from outside FhSim)
     * @see ForceSource
     * @return true if this reference point receives external force
     */
    public boolean getIsExternalForceInput();
    
    /**
     * Set the external force input on this reference point in the global NED 
     * reference frame
     * @param force 1x3 force in N in the NED frame
     */
    public void setExternalForceInput(double [] force);
    
    /**
     * Set the target for the linear motor offset. The target is specified in
     * parent body (ship fixed) frame and does not change with angular rotation.
     * 
     * The linear offset only has effect if the reference point is created with 
     * a data class designated as a linear motor. 
     * 
     * @see SFHReferencePointData
     * 
     * @param offset 1x3 vector of offsets in the parent body frame, in meters
     */
    public void setLinearOffsetTarget(double[] offset);

    /**
     * Set the target for the linear motor offset. The target is specified in
     * parent body (ship fixed) frame and does not change with angular rotation.
     * 
     * The linear offset only has effect if the reference point is created with 
     * a data class designated as a linear motor. 
     * 
     * @see SFHReferencePointData
     * @param x x-axis offset in body frame
     * @param y y-axis offset in body frame
     * @param z z-axis offset in body frame 
     */
    public void setLinearOffsetTarget(double x, double y, double z);

    /**
     * Get the currently attained linear offset. Might differ from the offset
     * target due to dynamics of the linear motor. 
     * 
     * @see setLinearOffsetTarget
     * 
     * @return 1x3 vector of offset values in parent body fixed frame in meter
     */
    public double [] getLinearOffset();

    /**
     * Set the target of angular motor offset. The offset is specified as a sequence 
     * of rotations about the reference point local axes in the x,y and z order. 
     * 
     * Angular rotations does not affect the linear offset
     * 
     * @param offset 1x3 vector of rotations about the reference point x,y and z axes
     */
    public void setAngularOffsetTarget(double[] offset);
    
    /**
     * Set the target of angular motor offset. The offset is specified as a sequence 
     * of rotations about the reference point local axes in the x,y and z order. 
     * 
     * Angular rotations does not affect the linear offset
     * 
     * @param p the rotation about the reference point x axis
     * @param q the rotation about the reference point y axis
     * @param r the rotation about the reference point z axis
     */
    public void setAngularOffsetTarget(double p, double q, double r);

    /**
     * Get the currently attained angular offset. Might differ from the offset
     * target due to dynamics of the angular motor. 
     * @see setAngularOffsetTarget
     * @return 1x3 vector of offset values in parent body fixed frame in radians
     */
    public double [] getAngularOffset();

    /**
     * Get the force acting on the reference point in the global north-east-down
     * frame. 
     * @return 1x3 vector of force in the global NED frame in N 
     */
    public double [] getForceNED();

    /**
     * Get the force acting on the reference point in the global local reference
     * point frame. The frame is affected by the angular rotations
     * @return 1x3 vector of force in the reference point local frame in N 
     */
    public double [] getForceBody();
    
    /**
     * Get the torque acting on the reference point in the global north-east-down
     * frame. 
     * @return 1x3 vector of force in the global NED frame in N 
     */
    public double [] getTorqueNED();

    /**
     * Get the torque acting on the reference point in the global local reference
     * point frame. The frame is affected by the angular rotations
     * @return 1x3 vector of force in the reference point local frame in N 
     */
    public double [] getTorqueBody();

    /**
     * Return the global frame north-east-down position of the reference point
     * This includes the effect of movement of the parent body and the linear motor/offset
     * @return 1x3 vector of NED position in meters
     */
    public double [] getPositionNED();
    
    /**
     * 
     * @return 1x3 vector of NED velocity in m/s
     */
    public double [] getVelocityNED();    

    /**
     * Return the velocity of the reference point decomposed in the body fixed frame.
     * The body fixed frame is the result of both the rotation of the parent object, 
     * and the rotation of the local axes in a x-y-z order. 
     * 
     * If no rotation is specified for the reference point this will correspond
     * to the linear velocity of the parent body.
     * 
     * @return 1x3 vector of body velocity in m/s
     */
    public double [] getVelocityBody();    

    /**
     * Return the angular velocity of the reference point in the north-east-down 
     * reference frame. 
     * @return 1x3 of rotations around the NED axes in rad
     */
    public double [] getOmegaNED();
    
    /**
     * Return the orientation of the reference point including the results of the
     * local axes rotations in a x-y-z axis order
     * @return 1x4 orientation vector as a quaternion in NED
     */
    public double [] getQuaternionNED();    

    /**
     * Get the unit vector of the reference points X axis (axis nr. 1) in the 
     * global NED frame. The vector contains the rotation of the parent object
     * as well as the result of the local rotation in a x-y-z axis order
     * @return 1x3 normal vector
     */
    public double [] getAxis1NED();

    /**
     * Get the unit vector of the reference points Y axis (axis nr. 2) in the 
     * global NED frame. The vector contains the rotation of the parent object
     * as well as the result of the local rotation in a x-y-z axis order
     * @return 1x3 normal vector
     */
    public double [] getAxis2NED();
    
    /**
     * Get the unit vector of the reference points Z axis (axis nr. 3) in the 
     * global NED frame. The vector contains the rotation of the parent object
     * as well as the result of the local rotation in a x-y-z axis order
     * @return 1x3 normal vector
     */
    public double [] getAxis3NED();

    /**
     * Get the permanent linear offset of the reference point in body fixed
     * coordinates. This is the part of the linear offset which is invariant
     * during simulation
     * @return [1x3] vector with the offset in [m] in body frame
     */
    public double[] getPermanentLinearOffset();
}
