/**
This package contain a library for working with ship models based on SINTEF Fisheries and Aquaculture's 
* FhSim simulation program. 
* 
* <p> FhSim is a general system simulation tool which couples models into 
* a "system of system" simulation model which is executed. Data is routed between models with input 
* and output ports. The participating models, fixed parameters for each model and input/output coupling 
* is specified in an input file. The input file can be cumbersome to construct so this package contains 
* an API to configure and connect underlying models relevant to constructing simulations of vessels. 
* 
* <p>
* The API depends on the general java interface for FhSim (implemented as a java native interface). 
* 
* <p>
* The construction of the FhSim input file, invocation of FhSim and stepping of the simulation model
* is abstracted into the class FhDynamicModel. FhDynamicModel is not meant to be used directly by users 
* of this API, but its presence and existence explains how FhSim is hidden from view.
* 
* <p> Each class which participates in a simulation with FhDynamicModel must conform th the ActorIntf 
* interface specification. This interface specifies the methods required to participate in a simulation. 
* The simulation loop is made from repeated calls to "preTick" "Tick" and "postTick". 
* 
* <p>
* The library contains a set of similarly named classes and are separated into the following three
* conceptual classes
* <dl>
    <dt>Data</dt>
    <dd>Static configuration and initial values drink</dd>
    <dt>Actor Interface</dt>
    <dd>Definition of the interface to model during simulation</dd>
    <dt>Actor Implementation</dt>
    <dd>Implementation of the interface to FhSim, handles object creation and data flow of input and output signals</dd>
  </dl>
  * 
  * <p>
  * The intended use of the classes are to configure an object (such as an hull, propeller or actuator) through
  * a data (<Name>Data) class, instantiating an actor (an object conforming to <Name>Intf) from the implementation's (<Name>Impl)
  * static CreateNew method.
<br/>
 <img src="doc-files/axes.jpg"/>
 <br/> */
package com.sfh.ship;

