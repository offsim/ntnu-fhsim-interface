/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.ship;

/**
*This is an interface to the LinearSpring SimObject in FhSimBaseLibrary. It
*has two in-ports; PosA and PosB, for the position of its ends, and two out-ports
*for the force from the spring at the ends. 
*
* The spring is defined from a relaxed length [m], and a stiffness [N/m]. It can
* be connectec to a fixed point, or to a reference point. See SFHSpringData
*/

/**
 *
 * @author Tobias Torben
 */
public interface SFHSpringIntf extends ActorIntf {
    
    public double getStiffness();

    public void setStiffness(double stiffness);

    public double getRelaxedLength();

    public void setRelaxedLength(double relaxedLength);

    public String getSpringName();

    public void setSpringName(String name);
    
    public String getConnectionNameA();
    
    public void setConnectionNameA(String name);
    
    public String getConnectionNameB();
    
    public void setConnectionNameB(String name);
    
    public double[] getFixedConnectionA();
    
    public void setFixedConnectionA(double[] point);
    
    public double[] getFixedConnectionB();
    
    public void setFixedConnectionB(double[] point);
    
}
