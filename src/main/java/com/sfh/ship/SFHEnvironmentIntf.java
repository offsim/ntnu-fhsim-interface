/*
 * Copyright SINTEF FIsheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;
import com.osc.wave.IWaveModelListener;

/**
 * Dummy class to preserve symmetry vs. other actor models
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public interface SFHEnvironmentIntf extends ActorIntf,IWaveModelListener{

    /**
     * Update the direction of the wind
     * @param direction the direction of the wind in radians from north
     */
    public void SetWindDirection(double direction);
    
    /**
     * Update the wind speed in meters per seconds
     * @param speed the new wind speed in m/s
     */
    public void SetWindSpeed(double speed);
    
    /**
     * Update the current direction
     * @param direction the direction of the current in radians from north
     */
    public void SetCurrentDirection(double direction);
    
    /**
     * Update the current speed in meters per seconds
     * @param speed the new current speed in m/s
     */
    public void SetCurrentSpeed(double speed);    
}
