/*
 * Copyright SINTEF Fisheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Interface to the configuration of a dynamic ship model based on VERES
 * @see SFHHullIntf
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
@XStreamAlias("Hull")
public class SFHHullData {

    public static void dumpToXml(String fname, SFHHullData obj){
        XStream xstream = new XStream();
        xstream.processAnnotations(SFHHullData.class);
        PrintWriter out = null;
        try {
             out = new PrintWriter(fname);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SFHHullData.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        out.println(xstream.toXML(obj));
        out.close();
    }
    
    public static SFHHullData loadFromXml(String fname){
        XStream xstream = new XStream();
        xstream.processAnnotations(SFHHullData.class);
        SFHHullData out = (SFHHullData) xstream.fromXML(new File(fname));
        out.ReferencePointForce = new ArrayList<>();
        out.flipPhase = 0;
        return out;
    }

    public static SFHHullData loadFromXml(String objectName, String fname){
        SFHHullData out = loadFromXml(fname);
        out.setName(objectName);
        return out;
    }
    
    
    public static SFHHullData findByNameInObjectList(String Name, ArrayList<Object> list){
        for( Object o: list){
            if( o instanceof SFHHullData)
                if( ((SFHHullData)o).getName().equals(Name)){
                    return (SFHHullData)o;
                }            
        }
        return null;
    }
   


    /**
     * Get the lateral area of the ship hull above the water plane
     * @return the area in m**2
     */
    public double getWindLateralArea() {
        return windLateralArea;
    }

    /**
     * Set the lateral area of the ship hull above the water plane
     * @param windLateralArea new area in m**2
     */
    public void setWindLateralArea(double windLateralArea) {
        this.windLateralArea = windLateralArea;
    }

    /**
     * Get the lateral area of the ship hull above the water plane
     * @return the area in m**2
     */
    public double getWindFrontalArea() {
        return windFrontalArea;
    }

    /**
     * Set the lateral area of the ship hull above the water plane
     * @param windFrontalArea the new area in m**2
     */
    public void setWindFrontalArea(double windFrontalArea) {
        this.windFrontalArea = windFrontalArea;
    }

    /**
     * Get the characteristic length of the ship wrt. the wind
     * This is usually the length overall 
     * @return the characteristic length in m
     */
    public double getWindCharacteristicLength() {
        return windCharacteristicLength;
    }

    /**
     * Set the characteristic length of the ship wrt. the wind
     * This is usually the length overall 
     * @param windCharacteristicLength the new length in m
     */
    public void setWindCharacteristicLength(double windCharacteristicLength) {
        this.windCharacteristicLength = windCharacteristicLength;
    }

    /**
     * @return the manYv
     */
    public double getManYv() {
        return manYv;
    }

    /**
     * @param manYv the manYv to set
     */
    public void setManYv(double manYv) {
        this.manYv = manYv;
    }

    /**
     * @return the manYr
     */
    public double getYr_man() {
        return manYr;
    }

    /**
     * @param Yr_man the mainYr to set
     */
    public void setYr_man(double Yr_man) {
        this.manYr = Yr_man;
    }

    /**
     * @return the manNv
     */
    public double getManNv() {
        return manNv;
    }

    /**
     * @param manNv the manNv to set
     */
    public void setManNv(double manNv) {
        this.manNv = manNv;
    }

    /**
     * @return the manNr
     */
    public double getManNr() {
        return manNr;
    }

    /**
     * @param manNr the manNr to set
     */
    public void setManNr(double manNr) {
        this.manNr = manNr;
    }

    /**
     * @return the manYrd
     */
    public double getManYrd() {
        return manYrd;
    }

    /**
     * @param manYrd the manYrd to set
     */
    public void setManYrd(double manYrd) {
        this.manYrd = manYrd;
    }

    /**
     * @return the manYvd
     */
    public double getManYvd() {
        return manYvd;
    }

    /**
     * @param manYvd the manYvd to set
     */
    public void setManYvd(double manYvd) {
        this.manYvd = manYvd;
    }

    /**
     * @return the manNrd
     */
    public double getManNrd() {
        return manNrd;
    }

    /**
     * @param manNrd the manNrd to set
     */
    public void setManNrd(double manNrd) {
        this.manNrd = manNrd;
    }

    /**
     * @return the manNvd
     */
    public double getManNvd() {
        return manNvd;
    }

    /**
     * @param manNvd the manNvd to set
     */
    public void setManNvd(double manNvd) {
        this.manNvd = manNvd;
    }

    /**
     * @return the manXrr
     */
    public double getManXrr() {
        return manXrr;
    }

    /**
     * @param manXrr the manXrr to set
     */
    public void setManXrr(double manXrr) {
        this.manXrr = manXrr;
    }

    /**
     * @return the manXud
     */
    public double getXud_man() {
        return manXud;
    }

    /**
     * @param Xud_man the manXud to set
     */
    public void setXud_man(double Xud_man) {
        this.manXud = Xud_man;
    }

    /**
     * @return the manXvr
     */
    public double getXvr_man() {
        return manXvr;
    }

    /**
     * @param Xvr_man the manXvr to set
     */
    public void setXvr_man(double Xvr_man) {
        this.manXvr = Xvr_man;
    }

    /**
     * @return the manXvv
     */
    public double getXvv_man() {
        return manXvv;
    }

    /**
     * @param Xvv_man the manXvv to set
     */
    public void setXvv_man(double Xvv_man) {
        this.manXvv = Xvv_man;
    }

    /**
     * @return the manXvvvv
     */
    public double getXvvvv_man() {
        return manXvvvv;
    }

    /**
     * @param Xvvvv_man the manXvvvv to set
     */
    public void setXvvvv_man(double Xvvvv_man) {
        this.manXvvvv = Xvvvv_man;
    }

    /**
     * @return the veresWaveDrift
     */
    public boolean isVeresWaveDrift() {
        return veresWaveDrift;
    }

    /**
     * @param veresWaveDrift the veresWaveDrift to set
     */
    public void setVeresWaveDrift(boolean veresWaveDrift) {
        this.veresWaveDrift = veresWaveDrift;
    }

    /**
     * @return the hullId
     */
    public String getHullId() {
        return hullId;
    }

    /**
     * Set at hull ID (deprecated)
     * @param hullId the hullId to set
     */
    public void setHullId(String hullId) {
        this.hullId = hullId;
    }

    /**
     * Get the height from the center of gravity to the pressure center of the wind force
     * @return the height form COG to the pressure center in m
     */
    public double getWindPressureCenterHeight() {
        return windPressureCenterHeight;
    }

    /**
     * @param windPressureCenterHeight the windPressureCenterHeight to set
     */
    public void setWindPressureCenterHeight(double windPressureCenterHeight) {
        this.windPressureCenterHeight = windPressureCenterHeight;
    }

    /**
     * @return the residualResistanceScale
     */
    public double getResidualResistanceScale() {
        return residualResistanceScale;
    }

    /**
     * @param residualResistanceScale the residualResistanceScale to set
     */
    public void setResidualResistanceScale(double residualResistanceScale) {
        this.residualResistanceScale = residualResistanceScale;
    }

    /**
     * @return the windA0x
     */
    public double getWindA0x() {
        return windA0x;
    }

    /**
     * @return the windA0y
     */
    public double getWindA0y() {
        return windA0y;
    }

    /**
     * @return the windA0n
     */
    public double getWindA0n() {
        return windA0n;
    }

    /**
     * @return the windAx
     */
    public double [] getWindAx() {
        return windAx;
    }

    /**
     * @return the windAy
     */
    public double [] getWindAy() {
        return windAy;
    }

    /**
     * @return the windAn
     */
    public double [] getWindAn() {
        return windAn;
    }

    /**
     * @return the windBx
     */
    public double [] getWindBx() {
        return windBx;
    }

    /**
     * @return the windBy
     */
    public double [] getWindBy() {
        return windBy;
    }

    /**
     * @return the windBn
     */
    public double [] getWindBn() {
        return windBn;
    }

    // *****************************
    // Motion damping parameters
    // *****************************
    
    /**
     * Artificial damping in linear dofs
     */
    private double artificualXu=0,artificualYv=0,artificualZw=0;
    private double artificualXuu=0,artificualYvv=0,artificualZww=0;
    
    /**
     * Artificial damping in rotational dofs
     */
    private double artificualPp=0,artificualQq=0,artificualNr=0;
    private double artificualPpp=0,artificualQqq=0,artificualNrr=0;
    
    /**
     * Linear DP damping in surge direction
     */
    private double dpXu=0, dpXv=0, dpXr=0;
    
    /**
     * Linear DP damping in sway direction
     */
    private double dpYu=0, dpYv=0, dpYr=0;
    
    /**
     * Linear DP damping in yaw direction
     */
    private double dpNu=0, dpNv=0, dpNr=0;

    /**
     * Bilge keel model
     */
    private double bilgeKeelStart;
    private double bilgeKeelEnd;
    private double bilgeKeelHeight;
    
    private boolean legacyManeuverModel=true;
    private double manYv    ;
    private double manYr    ;
    private double manNv    ;
    private double manNr    ;
    private double manYrd   ;
    private double manYvd   ;
    private double manNrd   ;
    private double manNvd   ;
    private double manXrr   ;
    private double manXud   ;
    private double manXvr   ;
    private double manXvv   ;
    private double manXvvvv ;
     // *****************************
     // Residual reistance parameters
     // *****************************
    
    /**
     * Number of thrusters used in Cr regression
     */
    private int NumberOfThrusters;
    
    /**
     * Number of brackets used in Cr regression
     */
    private int NumberOfBrackets;

    /**
     * Number of propeller bosses used in Cr regression
     */
    private int NumberOfBosses; /// not SuperMario Bosses
    
    /**
     * Number of rudders used in Cr regression
     */
    private int NumberOfRudders;
    
    /**
     * Main propeller diameter used in Cr regression
     */
    private int MainPropellerDiameter;

    /**
     * Number of thrusters used in Cr regression
     * @return the NumberOfThrusters
     */
    public int getNumberOfThrusters() {
        return NumberOfThrusters;
    }

    /**
     * Number of thrusters used in Cr regression
     * @param NumberOfThrusters the NumberOfThrusters to set
     */
    public void setNumberOfThrusters(int NumberOfThrusters) {
        this.NumberOfThrusters = NumberOfThrusters;
    }

    /**
     * Number of brackets used in Cr regression
     * @return the NumberOfBrackets
     */
    public int getNumberOfBrackets() {
        return NumberOfBrackets;
    }

    /**
     * Number of brackets used in Cr regression
     * @param NumberOfBrackets the NumberOfBrackets to set
     */
    public void setNumberOfBrackets(int NumberOfBrackets) {
        this.NumberOfBrackets = NumberOfBrackets;
    }

    /**
     * Number of propeller bosses used in Cr regression
     * @return the NumberOfBosses
     */
    public int getNumberOfBosses() {
        return NumberOfBosses;
    }

    /**
     * Number of propeller bosses used in Cr regression
     * @param NumberOfBosses the NumberOfBosses to set
     */
    public void setNumberOfBosses(int NumberOfBosses) {
        this.NumberOfBosses = NumberOfBosses;
    }

    /**
     * Number of rudders used in Cr regression
     * @return the NumberOfRudders
     */
    public int getNumberOfRudders() {
        return NumberOfRudders;
    }

    /**
     * Number of rudders used in Cr regression
     * @param NumberOfRudders the NumberOfRudders to set
     */
    public void setNumberOfRudders(int NumberOfRudders) {
        this.NumberOfRudders = NumberOfRudders;
    }

    /**
     * Main propeller diameter used in Cr regression
     * @return the MainPropellerDiameter
     */
    public int getMainPropellerDiameter() {
        return MainPropellerDiameter;
    }

    /**
     * Main propeller diameter used in Cr regression
     * @param MainPropellerDiameter the MainPropellerDiameter to set
     */
    public void setMainPropellerDiameter(int MainPropellerDiameter) {
        this.MainPropellerDiameter = MainPropellerDiameter;
    }
    
    
    /**
     * Constructor: Sets sane default values
     */
    public SFHHullData()
    {
	this.Name = "DefaultShip";
	//this.HydFileString = "";
	//this.RE2FileString = "";
	//this.RE7FileString = "";
	//this.RE8FileString = "";
	//this.MGFFileString = "";

        this.legacyManeuverModel = true;
	this.InitialSurgeVelocity = 0;
	this.InitialRollPitchYaw = new double[3];
	this.InitialNorthEastDown = new double[3];
	for( int i=0; i < 3; i++) this.InitialRollPitchYaw[i] = 0;
	for( int i=0; i < 3; i++) this.InitialNorthEastDown[i] = 0;

        ReferencePointForce = new ArrayList<>();
        
        flipPhase = 0;
    }


    /**
     * Add a "reference point" to the ship model. 
     * A reference point is a point on the vessel with a local coordinate system which can be used to read the instantaneous position 
     * and velocity in global coordinates (North-east-down). The reference point coordinate system is initially parallel to the vessel 
     * coordinate system, but can be rotated about its local axes. Rotations are preformed by subsequently rotating the reference points along its x,y,z axes. 
     *
     * @param reference
     * @param assumeForceOnPoint
     */
    public void AddReferencePoint(SFHReferencePointData reference, boolean assumeForceOnPoint ){
        reference.setShipName(Name);
        if( reference.getIsForceCalculator() || assumeForceOnPoint) 
            ReferencePointForce.add(reference.getName());    
    }
    
    /**
     * Add a "reference point" to the ship model. 
     * A reference point is a point on the vessel with a local coordinate system which can be used to read the instantaneous position 
     * and velocity in global coordinates (North-east-down). The reference point coordinate system is initially parallel to the vessel 
     * coordinate system, but can be rotated about its local axes. Rotations are preformed by subsequently rotating the reference points along its x,y,z axes. 
     *
     * @param reference
     */
    public void AddReferencePoint(SFHReferencePointData reference ){
        AddReferencePoint(reference,false);
    }
    
      private String Name = "DefaulShip";	    ///< Ship Name in the simulation 
   // @XStreamOmitField
   // private String HydFileString;   ///< String containing the VERES ".hyd" file with hydrostatic data
   // @XStreamOmitField
   // private String RE2FileString;   ///< String with the contents of the VERES ".re2" file with wavedrift data
   // @XStreamOmitField
   // private String RE7FileString;   ///< String with the contents of the VERES ".re7" file with hydrodynamic coefficients
   // @XStreamOmitField
   // private String RE8FileString;   ///< String with the contents of the VERES ".re8" file with 1. order wave loads
   // @XStreamOmitField
   // private String MGFFileString;   ///< String with the contents of the VERES ".mgf" file with hull geometry
    
    /**The path to the VERES data*/
    private String modelData = "";
    /**The Name stem of the VERES result files ex "input" from input.re2/input.re7/input.re8*/
    private String modelFileStem = "input";
    
    /**Side area used for wind calculation [m**2]*/
    private double windLateralArea; 
    /**Frontal area used in wind calculation [m**2]*/
    private double windFrontalArea;          
    /**Characteristic length of vessel in wind calculations [m]*/
    private double windCharacteristicLength; 
    /**Pressure center height over baseline, used to generate heeling moment [m]*/
    private double windPressureCenterHeight; 
    
    /**DC-gain component of wind curve for lateral direction*/
    private double windA0x;
    /**Even components of wind curve for lateral direction*/
    private double [] windAx;
    /**Odd components of wind curve for lateral direction*/
    private double [] windBx;
    
    /**DC-gain component of wind curve for transverse direction*/
    private double windA0y;
    /**Even components of wind curve for transverse direction*/
    private double [] windAy;
    /**Odd components of wind curve for transverse direction*/
    private double [] windBy;
    
    /**DC-gain component of wind curve for yaw direction*/
    private double windA0n;
    /**Even components of wind curve for yaw direction*/
    private double [] windAn;
    /**Odd components of wind curve for yaw direction*/
    private double [] windBn;
    
    private String version="";
    /**Only used for the report*/
    private double dwl;
    
    private VeresFile veres;
    
    @XStreamOmitField
    private int flipPhase = 0;
    
    private double	InitialSurgeVelocity;	///< Surge velocity at the start of the simulation
    private double[]	InitialRollPitchYaw;	///< Roll,pitch and yaw at the start of the simulation [1x3] vector
    private double[]	InitialNorthEastDown;	///< Initial posiytion (Nort-east-down) at the start of the simulation [1x3] vector
    private boolean[]	FreezeDOF;		///< Frozen and free degrees of freedom during the simulation [1x6] vector

    @XStreamOmitField
    ArrayList<String> ReferencePointForce;	    ///< List of reference point names
    
    @XStreamOmitField
    private ArrayList<String> ConnectedGearForce;	    ///< List of reference point names
    @XStreamOmitField
    private ArrayList<String> ConnectedGearTorque;	    ///< List of reference point names

    //@XStreamOmitField
    //ArrayList<String> ReferencePointObserve = new ArrayList<String>();	    ///< List of reference point names
    
    @XStreamAsAttribute
    private String hullId ="/";
    
    @XStreamAsAttribute
    private boolean veresWaveDrift=true;

    private double residualResistanceScale=1.0;
    
    /**
     * Get the Name/identifier of the vessel in the simulation
     * @return the Name of the vessel model in the simulation
     */
    public String getName() {
	return Name;
    }

    /**
     * Set the Name of the ship object used during simulation
     * @param Name the of the ship object in the simulation
     */
    public void setName(String Name) {
	this.Name = Name;
    }

    public void addGearForce(String gearName, String forcePort){
        if( getConnectedGearForce() == null )
            ConnectedGearForce = new ArrayList<String>();
        getConnectedGearForce().add(gearName + "." + forcePort);
    }

    public void addGearTorque(String gearName, String torquePort){
        if( getConnectedGearTorque() == null )
            ConnectedGearTorque = new ArrayList<String>();
        getConnectedGearTorque().add(gearName + "." + torquePort);
    }
    
    
    /**
     * Get the initial forward speed (surge) of the vessel
     * @return the initial surge velocity of the ship
     */
    public double getInitialSurgeVelocity() {
	return InitialSurgeVelocity;
    }

    /**
     * Set the initial forward speed (surge) of the vessel
     * @param InitialSurgeVelocity the initial surge velocity in the simulation
     */
    public void setInitialSurgeVelocity(double InitialSurgeVelocity) {
	this.InitialSurgeVelocity = InitialSurgeVelocity;
    }

    /**
     * Get the initial rotation angles of the vessel (roll, pitch yaw)
     * @return the initial roll, pitch and yaw angles
     */
    public double[] getInitialRollPitchYaw() {
	return InitialRollPitchYaw;
    }

    /**
     * Set the initial rotation angles of the vessel (roll, pitch yaw)
     * @param InitialRollPitchYaw the initial roll, pitch and yaw angles
     */
    public void setInitialRollPitchYaw(double[] InitialRollPitchYaw) {
	this.InitialRollPitchYaw = InitialRollPitchYaw;
    }

    /**
     * Return the initial position of the vessel 
     * @return the initial position of the vessels center of gravity in North-East-Down coordinates [m]
     */
    public double[] getInitialNorthEastDown() {
	return InitialNorthEastDown;
    }

    /**
     * Set the initial position of the vessel 
     * @param InitialNorthEastDown the initial position of the vessels center of gravity in North-East-Down coordinates [m]
     */
    public void setInitialNorthEastDown(double[] InitialNorthEastDown) {
	this.InitialNorthEastDown = InitialNorthEastDown;
    }

    /**
     * Return the 'freeze' state of the 6DOF body motions. Frozen DOF's are clamped during the simulation
     * @return the vector of 6 freeze states
     */
    public boolean[] getFreezeDOF() {
	return FreezeDOF;
    }

    /**
     * Fix or free the body frame degrees of freedom
     * 
     * @param FreezeDOF	An array of booleans of size [1x6] which indicates whether the body fixed degrees of freedom should be fixed(true) or free(false)
     */
    public void setFreezeDOF(boolean[] FreezeDOF) {
	this.FreezeDOF = FreezeDOF;
    }
    
    /**
     * Set the linear DP damping coefficients for surge force
     * @param Xu Surge force from surge velocity N/(m/s)
     * @param Xv Surge force from sway velocity N/(m/s)
     * @param Xr Surge force from yaw velocity Nm/(1/s)
     */
    public void setDPDampingSurge( double Xu, double Xv, double Xr)
    {
	this.dpXu = Xu;
	this.dpXv = Xv;
	this.dpXr = Xr;
    }

    /**
     * Set the linear DP damping coefficients for sway force
     * @param Yu Sway force from surge velocity N/(m/s)
     * @param Yv Sway force from sway velocity N/(m/s)
     * @param Yr Sway force from yaw velocity Nm/(1/s)
     */
    public void setDPDampingSway( double Yu, double Yv, double Yr)
    {
	this.dpYu = Yu;
	this.dpYv = Yv;
	this.dpYr = Yr;
    }
    
    /**
     * Set the linear DP damping coefficients for yaw moment
     * @param Nu Yaw moment from surge velocity Nm/(m/s)
     * @param Nv Yaw moment from sway velocity Nm/(m/s)
     * @param Nr Yaw moment from yaw velocity Nm/(1/s)
     */
    public void setDPDampingYaw( double Nu, double Nv, double Nr)
    {
	this.dpNu = Nu;
	this.dpNv = Nv;
	this.dpNr = Nr;
    }

    /**
     * Set tunable "artificial" damping in surge. Scales with ship mass
     * @param x linear contribution
     * @param xx quadratic contribution
     */
    public void SetTunableDampingSurge( double x, double xx ){
        artificualXu = x;
        artificualXuu = xx;
    }
    
    /**
     * Set tunable "artificial" damping in sway. Scales with ship mass
     * @param v linear contribution
     * @param vv quadratic contribution
     */
    public void SetTunableDampingSway( double v, double vv ){
        artificualYv = v;
        artificualYvv = vv;
    }

    /**
     * Set tunable "artificial" damping in heave. Scales with ship mass
     * @param w linear contribution
     * @param ww quadratic contribution
     */
    public void SetTunableDampingHeave( double w, double ww ){
        artificualZw = w;
        artificualZww = ww;
    }

    /**
     * Set tunable "artificial" damping in roll. Scales with ship mass*Lpp
     * @param p linear contribution
     * @param pp quadratic contribution
     */
    public void SetTunableDampingRoll( double p, double pp ){
        artificualQq = p;
        artificualQqq = pp;
    }
    
    /**
     * Set tunable "artificial" damping in pitch. Scales with ship mass*Lpp
     * @param q linear contribution
     * @param qq quadratic contribution
     */
    public void SetTunableDampingPitch( double q, double qq ){
        artificualPp = q;
        artificualPpp = qq;
    }

    /**
     * Set tunable "artificial" damping in yaw. Scales with ship mass*Lpp
     * @param r linear contribution
     * @param rr quadratic contribution
     */
    public void SetTunableDampingYaw( double r, double rr ){
        artificualNr = r;
        artificualNrr = rr;
    }

    /**
     * Return the tunable damping in surge
     * @return [1x2] vector of linear and quadratic damping parameters in surge
     */
    public double[] GetTunableDampingSurge( ){
        double [] retval = new double[2];
        retval[0] = artificualXu;
        retval[1] = artificualXuu;
        return retval;
    }
    
    /**
     * Return the tunable damping in sway
     * @return [1x2] vector of linear and quadratic damping parameters in sway
     */
    public double[] GetTunableDampingSway(  ){
        double [] retval = new double[2];
        retval[0] = artificualYv;
        retval[1] = artificualYvv;
        return retval;
    }

    /**
     * Return the tunable damping in heave
     * @return [1x2] vector of linear and quadratic damping parameters in heave
     */
    public double[] GetTunableDampingHeave(  ){
        double [] retval = new double[2];
        retval[0] = artificualZw;
        retval[1] = artificualZww;
        return retval;
    }

    /**
     * Return the tunable damping in roll
     * @return [1x2] vector of linear and quadratic damping parameters in roll
     */
    public double[] GetTunableDampingRoll(  ){
        double [] retval = new double[2];
        retval[0] = artificualQq;
        retval[1] = artificualQqq;
        return retval;
    }
    
    /**
     * Return the tunable damping in pitch
     * @return [1x2] vector of linear and quadratic damping parameters in pitch
     */
    public double[] GetTunableDampingPitch( ){
        double [] retval = new double[2];
        retval[0] = artificualPp;
        retval[1] = artificualPpp;
        return retval;
    }

    /**
     * Return the tunable damping in yaw
     * @return [1x2] vector of linear and quadratic damping parameters in yaw
     */
    public double[] GetTunableDampingYaw(  ){
        double [] retval = new double[2];
        retval[0] = artificualNr;
        retval[1] = artificualNrr;
        return retval;
    }

    
    /**
     * Get the surge DP damping coefficients as a array
     * @return 1x3 array with [Xu,Xv,Xr]
     */
    public double [] getDpDampingSurge()
    {
	double [] tmp = new double [3];
	tmp[0] = dpXu; tmp[1] = dpXv; tmp[2] = dpXr;
	return tmp;
    }
    
    /**
     * Get the sway DP damping coefficients as a array
     * @return 1x3 array with [Yu,Yv,Yr]
     */
    public double [] getDpDampingSway()
    {
	double [] tmp = new double [3];
	tmp[0] = dpYu; tmp[1] = dpYv; tmp[2] = dpYr;
	return tmp;
    }

    /**
     * Get the yaw DP damping coefficients as a array
     * @return 1x3 array with [Nu,Nv,Nr]
     */
    public double [] getDpDampingYaw()
    {
	double [] tmp = new double [3];
	tmp[0] = dpNu; tmp[1] = dpNv; tmp[2] = dpNr;
	return tmp;
    }

    /**
     * The path to the VERES data
     * @return the VERES file location
     */
    public String getModelData() {
        return modelData;
    }

    /**
     * The path to the VERES data
     * @param modelData the location of the VERES results
     */
    public void setModelData(String modelData) {
        this.modelData = modelData;
        updateVeresFile();
    }

    /**
     * DC-gain component of wind curve for lateral direction
     * @param windA0x the windA0x to set
     */
    public void setWindA0x(double windA0x) {
        this.windA0x = windA0x;
    }

    /**
     * Even components of wind curve for lateral direction
     * @param windAx the windAx to set
     */
    public void setWindAx(double[] windAx) {
        this.windAx = windAx;
    }

    /**
     * Odd components of wind curve for lateral direction
     * @param windBx the windBx to set
     */
    public void setWindBx(double[] windBx) {
        this.windBx = windBx;
    }

    /**
     * DC-gain component of wind curve for transverse direction
     * @param windA0y the windA0y to set
     */
    public void setWindA0y(double windA0y) {
        this.windA0y = windA0y;
    }

    /**
     * Even components of wind curve for transverse direction
     * @param windAy the windAy to set
     */
    public void setWindAy(double[] windAy) {
        this.windAy = windAy;
    }

    /**
     * Odd components of wind curve for transverse direction
     * @param windBy the windBy to set
     */
    public void setWindBy(double[] windBy) {
        this.windBy = windBy;
    }

    /**
     * DC-gain component of wind curve for yaw direction
     * @param windA0n the windA0n to set
     */
    public void setWindA0n(double windA0n) {
        this.windA0n = windA0n;
    }

    /**
     * Even components of wind curve for yaw direction
     * @param windAn the windAn to set
     */
    public void setWindAn(double[] windAn) {
        this.windAn = windAn;
    }

    /**
     * Odd components of wind curve for yaw direction
     * @param windBn the windBn to set
     */
    public void setWindBn(double[] windBn) {
        this.windBn = windBn;
    }

    /**
     * The Name stem of the VERES result files ex "input" from input.re2/input.re7/input.re8
     * @return the modelFileStem
     */
    public String getModelFileStem() {
        return modelFileStem;
    }

    /**
     * The Name stem of the VERES result files ex "input" from input.re2/input.re7/input.re8
     * @param modelFileStem the modelFileStem to set
     */
    public void setModelFileStem(String modelFileStem) {
        this.modelFileStem = modelFileStem;
        updateVeresFile();
    }
    
    
    private void updateVeresFile()
    {
        if (this.modelFileStem != null && this.modelData != null)
              veres = new VeresFile(this.modelData + "/" + this.modelFileStem);
    }
    
    /**
     * 
     * @return x coordinate of the COG relative to OSC model Origo 
     * returns Double.NEGATIVE_INFINITY; if the model is not ready
     * 
     */
    public double getCGXrelOSCOrigo()
    {
        if(veres!=null)
            return veres.getCGXrelOSCModelOrigo();
        return Double.NEGATIVE_INFINITY;
    }
    
    
    /**
     * 
     * @return x coordinate of the COG relative to AP in m (NED)
     * returns Double.NEGATIVE_INFINITY; if the model is not ready
     * 
     */
    public double getCGXrelAP()
    {
        if(veres!=null)
            return veres.hydFile.LCG;
        return Double.NEGATIVE_INFINITY;
    }
    
    /**
     * 
     * @return z coordinate of the COG relative to BL in m (NED)
     * returns Double.NEGATIVE_INFINITY; if the model is not ready
     * 
     */
    public double getCGZrelBL()
    {
        if(veres!=null)
            return - Math.abs(veres.hydFile.VCG);
        return Double.NEGATIVE_INFINITY;
    }
    
    /**
     * 
     * @return y coordinate of the COG relative to CL in m (NED)
     * 
     */
    public double getCGYrelCL()
    {
        // I do not find distance from CL...
        return 0;
    }
    
    /*****
     * 
     * Methods used for tuning and report generation to lock the ship in various
     * degrees of freedom
     * 
     *****/
    
    /**
     * set test mode for hull, enables locking of DOFs
     */
    private boolean _sfh_test=false;
    
    /**
     * Free sway degree of freedom during test
     */
    private boolean _only_sway = false;
    
    /**
     * Free yaw degree of freedom during test
     */
    private boolean _only_yaw = false;
    
    /**
     * Free roll degree of freedom during test
     */
    private boolean _only_roll = false;
    
    /**
     * Free pitch degree of freedom during test
     */
    private boolean _only_pitch = false;

    /**
     * Check for hull test mode
     * @return true if hull is locked for tuning/test purposes
     */
    public boolean isSfh_test() {
        return _sfh_test;
    }
    
    /**
     * Enable/disable hull test mode
     * @param _sfh_test true if ship is to be locked, false if free (normal)
     */
    public void setSfh_test(boolean _sfh_test) {
        this._sfh_test = _sfh_test;
    }

    
    /**
     * Returns a new {@code SFHHullIntf} object configured by this data object.
     *
     * @return a new {@code SFHHullIntf} object configured by this data object.
     */
    public SFHHullIntf create() {
        return new SFHHullImpl(this);
    }

    /**
     * Check if sway degree of freedom is fixed/free
     * @see setSfh_test
     * @return true if sway is free during test
     */
    public boolean isOnly_sway() {
        return _only_sway;
    }

    /**
     * Set if sway degree of freedom is fixed/free
     * @see setSfh_test
     * @param _only_sway true if sway is free during test
     */
    public void setOnly_sway(boolean _only_sway) {
        this._only_sway = _only_sway;
    }

    /**
     * Check if yaw degree of freedom is fixed/free
     * @see setSfh_test
     * @return true if yaw is free during test
     */
    public boolean isOnly_yaw() {
        return _only_yaw;
    }

    /**
     * Set if yaw degree of freedom is fixed/free
     * @see setSfh_test
     * @param _only_yaw true if yaw is free during test
     */
    public void setOnly_yaw(boolean _only_yaw) {
        this._only_yaw = _only_yaw;
    }

    /**
     * Check if roll degree of freedom is fixed/free
     * @see setSfh_test
     * @return true if roll is free during test
     */
    public boolean isOnly_roll() {
        return _only_roll;
    }

    /**
     * Set if roll degree of freedom is fixed/free
     * @see setSfh_test
     * @param _only_roll true if roll is free during test
     */
    public void setOnly_roll(boolean _only_roll) {
        this._only_roll = _only_roll;
    }

    /**
     * Check if pitch degree of freedom is fixed/free
     * @see setSfh_test
     * @return true if pitch is free during test
     */
    public boolean isOnly_pitch() {
        return _only_pitch;
    }

    /**
     * Set if pitch degree of freedom is fixed/free
     * @see setSfh_test
     * @param _only_pitch true if pitch is free during test
     */
    public void setOnly_pitch(boolean _only_pitch) {
        this._only_pitch = _only_pitch;
    }

    /**
     * @return the version
     */
    public String getVersionString() {
        return version;
    }
    
     /**
     * @return the Design Water Line, to calculate the coordinates according to midpoint
     * ONly used for the report
     */
    public double getDWL() {
        return dwl;
    }

    /**
     * Set legacy maneouvre model mode (compability setting)
     * @return the legacyManeuverModel
     */
    public boolean isLegacyManeuverModel() {
        return legacyManeuverModel;
    }

    /**
     * Toggle/disable legacy manuvre model
     * @param legacyManeuverModel enable/disable legacy maneuvre model
     */
    public void setLegacyManeuverModel(boolean legacyManeuverModel) {
        this.legacyManeuverModel = legacyManeuverModel;
    }

    /**
     * Get the list of additional force input from attached gears (such as seismic)
     * @return list of \<object\>\.<port\> formatted strings to be connected to the hull
     */
    public ArrayList<String> getConnectedGearForce() {
        return ConnectedGearForce;
    }

    /**
     * Get the list of additional torque input from attached gears (such as seismic)
     * @return list of \<object\>.\<port\> formatted strings to be connected to the hull
     */
    public ArrayList<String> getConnectedGearTorque() {
        return ConnectedGearTorque;
    }

    public int flipPhase() {
        return flipPhase;
    }
    /**
     * Emergency workaround method for flipping the direction of the excitation moments
     * 0: no change, 1: flip pitch, 2: flip pitch and roll, 3: flip pitch, roll and yaw
     * @param flip Flip the phase of the wave force in pitch 
     */
    public void flipPhase(int flip) {
        flipPhase = flip;
    }

    /**
     * @return the bilge keel start position from zero section (rel mgf file)
     */
    public double getBilgeKeelStart() {
        return bilgeKeelStart;
    }

    /**
     * @param bilgeKeelStart bilge keel start position from zero section (rel mgf file)
     */
    public void setBilgeKeelStart(double bilgeKeelStart) {
        this.bilgeKeelStart = bilgeKeelStart;
    }

    /**
     * @return the bilge keel end position from zero section (rel mgf file)
     */
    public double getBilgeKeelEnd() {
        return bilgeKeelEnd;
    }

    /**
     * @param bilgeKeelEnd bilge keel end position from zero section (rel mgf file)
     */
    public void setBilgeKeelEnd(double bilgeKeelEnd) {
        this.bilgeKeelEnd = bilgeKeelEnd;
    }

    /**
     * @return the bilge keel height in meters
     */
    public double getBilgeKeelHeight() {
        return bilgeKeelHeight;
    }

    /**
     * @param bilgeKeelHeight bilge keel height in meters
     */
    public void setBilgeKeelHeight(double bilgeKeelHeight) {
        this.bilgeKeelHeight = bilgeKeelHeight;
    }
}
