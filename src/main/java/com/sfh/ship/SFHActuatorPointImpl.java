/*
 * Copyright SINTEF Fisheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;
import com.sfh.jni.FhSim;
import org.w3c.dom.Element;

/**
 * Implementation of an SFHActuatorPointIntf
 * @see SFHActuatorPointIntf
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public class SFHActuatorPointImpl extends SFHReferencePointImpl implements SFHActuatorPointIntf{
    final private double DEG2RAD = Math.PI/180;
    final private double RAD2DEG = 180/Math.PI;
    
    private boolean engineEnabled = true;
    
    private double[] failureModes = new double [10];
    final private double[] rpmLimits = new double[2];
    final private double[] pitchLimits = new double[2];
    final private double[] angleLimits = new double[2];
    final private double[] availablePower = {1e12};
    
    private boolean isControllablePitch=false, isRudder=false,isAzimuth=false, isControllableRPM=false;
    
    final private double[] commandedPitch = new double[1];
    final private double[] commandedRPM = new double[1];
    final private double[] commandedAngle = new double[1];
    private double[] RPM = new double[1];
    private double[] RPMMotor = new double[1];
    private double[] Pitch = new double[1];
    private double[] Angle = new double[1];
    private double[] RPM_FB = new double[1];
    private double[] Thrust_FB = new double[1];
    private double[] Pitch_FB = new double[1];
    private double[] Angle_FB = new double[1];
    private double[] ForceBody2 = new double[3];
    private double[] TorqueBody2 = new double[3];
    
    private boolean VSPMode;

    private boolean indexInitialized = false;
    private int ioObjectIndex;
    
    private int RPMIndex;
    private int RPMMotorIndex;
    private int PitchIndex;
    private int AngleIndex;
    private int RPM_FBIndex;
    private int Thrust_FBIndex;
    private int Pitch_FBIndex;
    private int Angle_FBIndex;
    private int ForceBody2Index;
    private int TorqueBody2Index;

    
    
    
    
    /**
     * Constructor
     * @see SFHActuatorPointData
     * @param data Actuator point configuration 
     */
    protected SFHActuatorPointImpl(SFHActuatorPointData data){
        super( data );
        Element obj = FhDynamicModel.instance().getNamedObjectElement(name);

        if( data.isVSPconnection() ){
            obj.setAttribute("SimObject", "Ship/CycloidalActuator");                        
            obj.setAttribute("AngularMotorConstants", angularMotorTimeConstant[0]/5+","+angularMotorTimeConstant[1]/5+","+angularMotorTimeConstant[2]/5);
            obj.setAttribute("IsAngularMotor", "1");
            setVSPMode(true);
            
            if( VSPMode && !isAngularMotor )
                throw new RuntimeException("Error: VSP connection: " + getName() +" is configured without angular rotation: possible reason setIsAngularMotor(false) called after conneciton associated with VSP propulsor");
            
        }else{
            setVSPMode(false);
            obj.setAttribute("SimObject", "Ship/PropulsorActuator");            
        }


        /*
            Torque limit and set high value if missing (torque limit of 0 implies missing field form XStream)
        */
        double tl = data.getTorqueLimit();
        if (tl < 1) tl = 100e12;
        
        obj.setAttribute("TorqueLimit", Double.toString(tl));
        obj.setAttribute("GearRatio", Double.toString(data.getGearRatio()));
        obj.setAttribute("OscMode", "1");
        obj.setAttribute("AngularMotorMalfunctionRate", Double.toString(DEG2RAD*data.getAngularMotorMalfunctionRate()));
        obj.setAttribute("TimeConstantPitch", Double.toString(data.getTimeConstantPitch()/5));
        obj.setAttribute("TimeConstantRPM", Double.toString(data.getTimeConstantRPM()/5));
        
        obj.setAttribute("MaxChangeRateRPM", Double.toString(data.getMaxChangeRateRPM()));
        
        if( isVSPMode() )
            obj.setAttribute("MaxChangeRatePitch", Double.toString(data.getMaxChangeRatePitch()));
        else
            obj.setAttribute("MaxChangeRatePitch", Double.toString(DEG2RAD*data.getMaxChangeRatePitch()));

        obj.setAttribute("MaxChangeRateAzimuth", Double.toString(DEG2RAD*data.getMaxChangeRateAzimuth()));
        
        Element io = FhDynamicModel.instance().getNamedObjectElement(ioName);
        String inputs = io.getAttribute("inputPortNames");
        String outputs = io.getAttribute("outputPortNames");
        if( inputs.length() != 0) inputs += ",";
        if( outputs.length() != 0) outputs += ",";
        
        inputs += "RPM,RPMMotor,Pitch,Angle,RPM_FB,Thrust_FB,Pitch_FB,Angle_FB,ForceBody2,TorqueBody2";
        outputs += "CommandedPitch,CommandedRPM,CommandedAngle,FailureMode,AngleLimits,RPMLimits,PitchLimits,AvailablePower";
        
        io.setAttribute("inputPortNames", inputs);
        io.setAttribute("outputPortNames", outputs);
        io.setAttribute("Size_RPM", "1");
        io.setAttribute("Size_RPMMotor", "1");
        io.setAttribute("Size_Pitch", "1");
        io.setAttribute("Size_Angle", "1");
        io.setAttribute("Size_RPM_FB", "1");
        io.setAttribute("Size_Thrust_FB", "1");
        io.setAttribute("Size_Pitch_FB", "1");
        io.setAttribute("Size_Angle_FB", "1");
        io.setAttribute("Size_ForceBody2", "3");
        io.setAttribute("Size_TorqueBody2", "3");

        io.setAttribute("Initial_AngleLimits", Double.toString(DEG2RAD*data.getMinAngle())+","+Double.toString(DEG2RAD*data.getMaxAngle()));
        io.setAttribute("Initial_RPMLimits", Double.toString(data.getMinRPM())+","+Double.toString(data.getMaxRPM()));
        if( isVSPMode())
            io.setAttribute("Initial_PitchLimits", Double.toString(data.getMinPitch())+","+Double.toString(data.getMaxPitch()));
        else
            io.setAttribute("Initial_PitchLimits", Double.toString(DEG2RAD*data.getMinPitch())+","+Double.toString(DEG2RAD*data.getMaxPitch()));
        io.setAttribute("Initial_CommandedPitch", Double.toString(DEG2RAD*data.GetInitialPitch()));
        io.setAttribute("Initial_CommandedRPM", Double.toString(data.GetInitialRPM()));
        io.setAttribute("Initial_CommandedAngle", Double.toString(permanentAngularOffset[2]) );
        io.setAttribute("Initial_AvailablePower", Double.toString( availablePower[0]));
        io.setAttribute("Initial_FailureMode", GenerateArrayString(data.GetInitialFailureMode()));
        
        isControllablePitch=data.isControllablePitch();
        isRudder=data.isRudder();
        isAzimuth=data.isRudder();
        isControllableRPM=data.isControllableRPM();

        failureModes = data.GetInitialFailureMode();
        rpmLimits[0] = data.getMinRPM(); rpmLimits[1] = data.getMaxRPM();
        angleLimits[0] = DEG2RAD*data.getMinAngle(); angleLimits[1] = DEG2RAD*data.getMaxAngle();
        
        if( isVSPMode() ){
            pitchLimits[0] = data.getMinPitch(); pitchLimits[1] = data.getMaxPitch();
        }else{
            pitchLimits[0] = DEG2RAD*data.getMinPitch(); pitchLimits[1] = DEG2RAD*data.getMaxPitch();
        }
        
        ForceBody2[0] = ForceBody2[1]= ForceBody2[2] = 0;
        TorqueBody2[0] = TorqueBody2[1]= TorqueBody2[2] = 0;
        
        RPM[0] =0;
        Pitch[0] =0;
        Angle[0] =0;
        RPM_FB[0] =0;
        Angle_FB[0] =0;        
        Pitch_FB[0] =0;
        RPMMotor[0] =0;
        Thrust_FB[0] =0;
        
        if( isVSPMode() )
            commandedPitch[0] =data.GetInitialPitch();
        else
            commandedPitch[0] =data.GetInitialPitch()*DEG2RAD;
        commandedRPM[0] = data.GetInitialRPM();
        commandedAngle[0] = 0;
        
        connectionObject.setAttribute(ioName + ".RPM", name + ".RPM");
        connectionObject.setAttribute(ioName + ".RPMMotor", name + ".RPMMotor");
        connectionObject.setAttribute(ioName + ".Pitch", name + ".Pitch");
        connectionObject.setAttribute(ioName + ".Angle", name + ".Angle");
        connectionObject.setAttribute(ioName + ".RPM_FB", name + ".RPM_FB");
        connectionObject.setAttribute(ioName + ".Thrust_FB", name + ".Thrust_FB");
        connectionObject.setAttribute(ioName + ".Pitch_FB", name + ".Pitch_FB");
        connectionObject.setAttribute(ioName + ".Angle_FB", name + ".Angle_FB");
        connectionObject.setAttribute(ioName + ".ForceBody2", name + ".ForceBody2");
        connectionObject.setAttribute(ioName + ".TorqueBody2", name + ".TorqueBody2");

        connectionObject.setAttribute(name + ".CommandedPitch", ioName + ".CommandedPitch");
        connectionObject.setAttribute(name + ".CommandedRPM", ioName + ".CommandedRPM");
        if( isAngularMotor && !isVSPMode() )
            connectionObject.setAttribute(name + ".OffsetR", ioName + ".CommandedAngle");
        else if( isAngularMotor && isVSPMode() )
            connectionObject.setAttribute(name + ".ThrustAngle", ioName + ".CommandedAngle");
            
        connectionObject.setAttribute(name + ".FailureMode", ioName + ".FailureMode");
        connectionObject.setAttribute(name + ".AngleLimits", ioName + ".AngleLimits");
        connectionObject.setAttribute(name + ".PitchLimits", ioName + ".PitchLimits");
        connectionObject.setAttribute(name + ".RPMLimits", ioName + ".RPMLimits");

        if( !data.isForceCalculator || forceFrom == ForceSource.External){
            connectionObject.setAttribute(name+".ShaftTorque", "0");        
        }
        connectionObject.setAttribute(name + ".AvailablePower", ioName + ".AvailablePower");
        FhDynamicModel.instance().getInterconnectionElement().getParentNode().appendChild(connectionObject);
        //FhDynamicModel.instance().registerObject(this);
    }
    
    @Override
    public double MaxAngle() {
        return angleLimits[1]*RAD2DEG;
    }

    @Override
    public double MinAngle() {
        return angleLimits[0]*RAD2DEG;
    }

    @Override
    public double MaxRPM() {
        return rpmLimits[1];
    }

    @Override
    public double MinRPM() {
        return rpmLimits[0];
    }

    @Override
    public double MaxPitch() {
        if( isVSPMode() )
            return pitchLimits[1];
        else
            return pitchLimits[1]*RAD2DEG;
    }

    @Override
    public double MinPitch() {
        if( isVSPMode() )
            return pitchLimits[0];
        else
            return pitchLimits[0]*RAD2DEG;
    }

    @Override
    public void MaxAngle(double val) {
        angleLimits[1] = val*DEG2RAD;
    }

    @Override
    public void MinAngle(double val) {
        angleLimits[0] = val*DEG2RAD;
    }

    @Override
    public void MaxRPM(double val) {
        rpmLimits[1] = val;
    }

    @Override
    public void MinRPM(double val) {
        rpmLimits[0] = val;
    }

    @Override
    public void MaxPitch(double val) {
        if( isVSPMode() )
            pitchLimits[1] = val;
        else
            pitchLimits[1] = val*DEG2RAD;
    }

    @Override
    public void MinPitch(double val) {
        if( isVSPMode())
            pitchLimits[0] = val;
        else
            pitchLimits[0] = val*DEG2RAD;
    }


    @Override
    public boolean IsRudder() {
        return isRudder;
    }

    @Override
    public boolean IsAzimuth() {
        return isAzimuth;
    }


    @Override
    public boolean FailureModeRPMError() {
        return failureModes[ SFHActuatorPointIntf.FailureModes.failureModeRPMError.ordinal()]>0;
    }

    @Override
    public boolean FailureModeRPMLock() {
        return failureModes[ SFHActuatorPointIntf.FailureModes.failureModeRPMLock.ordinal()]>0;
    }

    @Override
    public boolean FailureModePitchLock() {
        return failureModes[ SFHActuatorPointIntf.FailureModes.failureModePitchLock.ordinal()]>0;
    }

    @Override
    public boolean FailureModePitchMalfunction() {
        return failureModes[ SFHActuatorPointIntf.FailureModes.failureModePitchMalfunction.ordinal()]>0;
    }

    @Override
    public boolean FailureModeAzimuthLock() {
        return failureModes[ SFHActuatorPointIntf.FailureModes.failureModeAzimuthLock.ordinal()]>0;
    }

    @Override
    public boolean FailureModeFeedbackPitchFreeze() {
        return failureModes[ SFHActuatorPointIntf.FailureModes.failureModeFeedbackPitchFreeze.ordinal()]>0;
    }

    @Override
    public boolean FailureModeFeedbackRPMFreeze() {
        return failureModes[ SFHActuatorPointIntf.FailureModes.failureModeFeedbackRPMFreeze.ordinal()]>0;
    }

    @Override
    public boolean FailureModeFeedbackAzimuthFreeze() {
        return failureModes[ SFHActuatorPointIntf.FailureModes.failureModeFeedbackAzimuthFreeze.ordinal()]>0;
    }

    @Override
    public boolean FailureModeFeedbackThrustFreeze() {
        return failureModes[ SFHActuatorPointIntf.FailureModes.failureModeFeedbackThrustFreeze.ordinal()]>0;
    }

    @Override
    public void FailureModeRPMError(boolean val) {
        failureModes[ SFHActuatorPointIntf.FailureModes.failureModeRPMError.ordinal()] = val?1.0:0.0;
    }

    @Override
    public void FailureModeRPMLock(boolean val) {
        failureModes[ SFHActuatorPointIntf.FailureModes.failureModeRPMLock.ordinal()] = val?1.0:0.0;
    }

    @Override
    public void FailureModePitchLock(boolean val) {
        failureModes[ SFHActuatorPointIntf.FailureModes.failureModePitchLock.ordinal()] = val?1.0:0.0;
    }

    @Override
    public void FailureModePitchMalfunction(boolean val) {
        failureModes[ SFHActuatorPointIntf.FailureModes.failureModePitchMalfunction.ordinal()] = val?1.0:0.0;
    }

    @Override
    public void FailureModeAzimuthLock(boolean val) {
        failureModes[ SFHActuatorPointIntf.FailureModes.failureModeAzimuthLock.ordinal()] = val?1.0:0.0;
    }

    @Override
    public void FailureModeFeedbackPitchFreeze(boolean val) {
        failureModes[ SFHActuatorPointIntf.FailureModes.failureModeFeedbackPitchFreeze.ordinal()] = val?1.0:0.0;
    }

    @Override
    public void FailureModeFeedbackRPMFreeze(boolean val) {
        failureModes[ SFHActuatorPointIntf.FailureModes.failureModeFeedbackRPMFreeze.ordinal()] = val?1.0:0.0;
    }

    @Override
    public void FailureModeFeedbackAzimuthFreeze(boolean val) {
        failureModes[ SFHActuatorPointIntf.FailureModes.failureModeFeedbackAzimuthFreeze.ordinal()] = val?1.0:0.0;
    }

    @Override
    public void FailureModeFeedbackThrustFreeze(boolean val) {
        failureModes[ SFHActuatorPointIntf.FailureModes.failureModeFeedbackThrustFreeze.ordinal()] = val?1.0:0.0;
    }

    @Override
    public boolean FailureModeAzimuthMalfunction() {
        return failureModes[ SFHActuatorPointIntf.FailureModes.failureModeAzimuthMalfunction.ordinal()]>0;
    }

    @Override
    public void FailureModeAzimuthMalfunction(boolean val) {
        failureModes[ SFHActuatorPointIntf.FailureModes.failureModeAzimuthMalfunction.ordinal()] = val?1.0:0.0;
    }
    
    
    @Override
    public double ActualRPM() {
        return RPM[0];
    }

    @Override
    public double ActualMotorRPM() {
        return RPMMotor[0];
    }

    @Override
    public double ActualPitch() {
        if(isVSPMode())
            return Pitch[0];
        else
            return Pitch[0]*RAD2DEG;
    }

    @Override
    public double ActualAngle() {
        return Angle[0]*RAD2DEG;
    }

    @Override
    public void preTick() {
        super.preTick();
        
        double []rpmCommand = {0};
        double []angleCommand = {0};
        double []_angleLimits = {0,0};
        if(engineEnabled)
            rpmCommand[0]=commandedRPM[0];
        angleCommand[0] = permanentAngularOffset[2]+commandedAngle[0];
        _angleLimits[0] = permanentAngularOffset[2] + angleLimits[0];
        _angleLimits[1] = permanentAngularOffset[2] + angleLimits[1];
        //pitchCommand[0] = 0.7*Math.PI*Math.tan(commandedPitch[0]);
        FhDynamicModel.instance().setFhSimPort(ioName, "FailureMode", failureModes);
        FhDynamicModel.instance().setFhSimPort(ioName, "CommandedPitch", commandedPitch);
        FhDynamicModel.instance().setFhSimPort(ioName, "CommandedRPM", rpmCommand);
        FhDynamicModel.instance().setFhSimPort(ioName, "CommandedAngle", angleCommand);
        FhDynamicModel.instance().setFhSimPort(ioName, "AngleLimits", _angleLimits);
        FhDynamicModel.instance().setFhSimPort(ioName, "RPMLimits", rpmLimits);
        FhDynamicModel.instance().setFhSimPort(ioName, "PitchLimits", pitchLimits);
        FhDynamicModel.instance().setFhSimPort(ioName, "AvailablePower", availablePower);
    }

    @Override
    public void tick(double Lookahead) {
        super.tick(Lookahead);
    }

    @Override
    public void postTick() {
        super.postTick();
        
        if( !indexInitialized ){
            ioObjectIndex = FhDynamicModel.instance().getObjectIndex(ioName);
            RPMIndex = FhDynamicModel.instance().getObjectPortIndex(ioName, "RPM");
            RPMMotorIndex = FhDynamicModel.instance().getObjectPortIndex(ioName, "RPMMotor");
            PitchIndex = FhDynamicModel.instance().getObjectPortIndex(ioName, "Pitch");
            AngleIndex = FhDynamicModel.instance().getObjectPortIndex(ioName, "Angle");
            RPM_FBIndex = FhDynamicModel.instance().getObjectPortIndex(ioName, "RPM_FB");
            Thrust_FBIndex = FhDynamicModel.instance().getObjectPortIndex(ioName, "Thrust_FB");
            Pitch_FBIndex = FhDynamicModel.instance().getObjectPortIndex(ioName, "Pitch_FB");
            Angle_FBIndex = FhDynamicModel.instance().getObjectPortIndex(ioName, "Angle_FB");
            ForceBody2Index = FhDynamicModel.instance().getObjectPortIndex(ioName, "ForceBody2");
            TorqueBody2Index = FhDynamicModel.instance().getObjectPortIndex(ioName, "TorqueBody2");
            indexInitialized=true;
        }
        
        RPM = FhDynamicModel.instance().getFhSimPortFromIndex(ioObjectIndex, RPMIndex, 1);
        RPMMotor = FhDynamicModel.instance().getFhSimPortFromIndex(ioObjectIndex, RPMMotorIndex, 1);
        Pitch = FhDynamicModel.instance().getFhSimPortFromIndex(ioObjectIndex, PitchIndex, 1);
        Angle = FhDynamicModel.instance().getFhSimPortFromIndex(ioObjectIndex, AngleIndex, 1);
        Thrust_FB = FhDynamicModel.instance().getFhSimPortFromIndex(ioObjectIndex, Thrust_FBIndex, 1);
        Pitch_FB = FhDynamicModel.instance().getFhSimPortFromIndex(ioObjectIndex, Pitch_FBIndex, 1);
        Angle_FB = FhDynamicModel.instance().getFhSimPortFromIndex(ioObjectIndex, Angle_FBIndex, 1);
        ForceBody2 = FhDynamicModel.instance().getFhSimPortFromIndex(ioObjectIndex, ForceBody2Index, 3);
        TorqueBody2 = FhDynamicModel.instance().getFhSimPortFromIndex(ioObjectIndex, TorqueBody2Index, 3);
        
      /*
        !!         String based approac abandoned !!
        RPM         = FhDynamicModel.instance().getFhSimPort(ioName, "RPM", 1);
        RPMMotor    = FhDynamicModel.instance().getFhSimPort(ioName, "RPMMotor", 1);
        Pitch       = FhDynamicModel.instance().getFhSimPort(ioName, "Pitch", 1);
        Angle       = FhDynamicModel.instance().getFhSimPort(ioName, "Angle", 1);
        RPM_FB      = FhDynamicModel.instance().getFhSimPort(ioName, "RPM_FB", 1);
        Thrust_FB   = FhDynamicModel.instance().getFhSimPort(ioName, "Thrust_FB", 1);
        Pitch_FB    = FhDynamicModel.instance().getFhSimPort(ioName, "Pitch_FB", 1);
        Angle_FB    = FhDynamicModel.instance().getFhSimPort(ioName, "Angle_FB", 1);
        ForceBody2  = FhDynamicModel.instance().getFhSimPort(ioName, "ForceBody2", 3);
        TorqueBody2 = FhDynamicModel.instance().getFhSimPort(ioName, "TorqueBody2", 3);
        */
        Angle[0] -= permanentAngularOffset[2];
        Angle_FB[0] -= permanentAngularOffset[2];
    }
    private String GenerateArrayString(double[] val) {
        String retval="";
        for(int i=0; i < val.length-1; i++){
            retval += Double.toString(val[i])+",";
        }
        retval += Double.toString(val[val.length-1]);
        return retval;
    }    

    @Override
    public double RPMFeedback() {
        return RPM_FB[0];
    }

    @Override
    public double PitchFeedback() {
        if( isVSPMode() )
            return Pitch_FB[0];
        else
            return Pitch_FB[0]*RAD2DEG;
    }

    @Override
    public double AngleFeedback() {
        return Angle_FB[0]*RAD2DEG;
    }

    @Override
    public double ThrustFeedback() {
        return Thrust_FB[0];
    }

    
    /**
     * @return the comandedPitch
     */
    @Override
    public double getComandedPitch() {
        if( isVSPMode())
          return commandedPitch[0];
        else
          return commandedPitch[0]*RAD2DEG;
    }

    /**
     * @param comandedPitch the comandedPitch to set
     */
    @Override
    public void setComandedPitch(double comandedPitch) {
        if( isVSPMode() )
            this.commandedPitch[0] = comandedPitch;
        else
            this.commandedPitch[0] = comandedPitch*DEG2RAD;
    }

    /**
     * @return the commandedRPM
     */
    @Override
    public double getCommandedRPM() {
        return commandedRPM[0];
    }

    /**
     * @param commandedRPM the commandedRPM to set
     */
    @Override
    public void setCommandedRPM(double commandedRPM) {
        this.commandedRPM[0] = commandedRPM;
    }

    /**
     * @return the commandeAngle
     */
    @Override
    public double getCommandeAngle() {
        return commandedAngle[0]*RAD2DEG;
    }

    /**
     * @param commandeAngle the commandeAngle to set
     */
    @Override
    public void setCommandeAngle(double commandeAngle) {
        this.commandedAngle[0] = commandeAngle*DEG2RAD;
    }

    @Override
    public boolean getEngineEnabled() {
        return engineEnabled;
    }

    @Override
    public void setEngineEnabled(boolean val) {
        engineEnabled = val;
    }

    @Override
    public double getComandedPitchNormalized() {
        if( getComandedPitch() < 0)
            return -getComandedPitch()/MinPitch();
        else
            return getComandedPitch()/MaxPitch();
    }

    @Override
    public void setComandedPitchNormalized(double command) {
        if( command < 0)
            setComandedPitch(MinPitch()*Math.abs(command));
        else
            setComandedPitch(MaxPitch()*command);
    }

    @Override
    public double getCommandedRPMNormalized() {
        if( getCommandedRPM() < 0)
            return -getCommandedRPM()/MinRPM();
        else
            return getCommandedRPM()/MaxRPM();
    }

    @Override
    public void setCommandedRPMNormalized(double command) {
        if( command < 0)
            setCommandedRPM(MinRPM()*Math.abs(command));
        else
            setCommandedRPM(MaxRPM()*command);
    }

    @Override
    public double getCommandeAngleNormalized() {
        if( getCommandeAngle() < 0)
            return -getCommandeAngle()/MinAngle();
        else
            return getCommandeAngle()/MaxAngle();
    }

    @Override
    public void setCommandeAngleNormalized(double command) {
        
        if( command < 0)
            setCommandeAngle(MinAngle()*Math.abs(command));
        else
            setCommandeAngle(MaxAngle()*command);
    }

    @Override
    public void setAvailablePower(double watts) {
        availablePower[0] = watts;
    }

    /**
     * @return the VSPMode
     */
    public boolean isVSPMode() {
        return VSPMode;
    }

    /**
     * @param VSPMode the VSPMode to set
     */
    public final void setVSPMode(boolean VSPMode) {
        this.VSPMode = VSPMode;
    }
}
