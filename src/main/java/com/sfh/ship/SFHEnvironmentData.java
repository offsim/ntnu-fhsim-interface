/*
 * Copyright SINTEF Fisheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

import java.util.ArrayList;

/**
 * Interface to the configuration of the environment in FhSim
 * 
 * The environment consists of three components
 * 1) The wave field
 * 2) The current
 * 3) The wind
 * 
 * The wave field is configured by an initial wave field and later 
 * updated by an IWaveModel. 
 * the current and wind is controlled by the actor interface SFHEnvironmentIntf
 * 
 * @see com.osc.wave.IWaveModel
 * @see com.sfh.ship.SFHEnvironmentIntf
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public class SFHEnvironmentData {

        /**
     * Find an instance if SFHEnvironmentData in the Object list "list" which matches
     * the name "Name", cast the object to SFHEnvironmentData  and return. 
     * 
     * Used to recreate a model for a collection of Object references really pointing to
     * configuration data classes
     * @param Name The name of the object
     * @param list list of Object references to configuration data classes
     * @return an instance of SFHEnvironmentData with name=Name in the list, null otherwise
     */
    public static SFHEnvironmentData findByNameInObjectList(String Name, ArrayList<Object> list){
        for( Object o: list){
            if( o instanceof SFHEnvironmentData)
                if( ((SFHEnvironmentData)o).getName().equals(Name)){
                    return (SFHEnvironmentData)o;
                }            
        }
        return null;
    }
    
    /**
     * Roughness (height of peaks) on the sea bottom
     */
    private double bottomRoughneess = 10;
    
    /**
     * Scale for the bottom, the "wavelength" of the sea bottom height variation
     */
    private double bottomtStructureScale = 1e3;
    
    /**
     * Initial current in m/s in north-east-down system
     */
    private double [] initialCurrent = new double[3];

    /**
     * Initial wind in m/s in north-east-down system
     */
    private double [] initialWind = new double[3];

    
    /**
     * Initial wave field amplitude vector
     */
    private double [] initialAmplitude = null;
    
    /**
     * Initial wave field frequency vector
     */
    private double [] initialFrequency = null;
    /**
     * Initial wave field random phase vector
     */
    private double [] initialPhase = null;

    /**
     * Initial wave field wave number (k) vector
     */
    private double [] initialNumber = null;

    /**
     * Initial wave field direction of propagation relative to north vector
     */
    private double [] initialDirection = null;
    
    /**
     * Constructor which names the environment "DefaultEnvironment"
     */
    public SFHEnvironmentData()
    {
	this.Name = "DefaultEnvironment";
    }
    
    /**
     * Name of the object used as an identifier in FhSim for both object and IO block
     */
    private String Name;
    
  
    /**
     * The sea depth in the environment
     */
    private double m_seaDepth;
    

    /**
     * The name of the environment object
     * @return the name of the object (used as identifier in FhSim)
     */
    public String getName() {
        return Name;
    }

    /**
     * Set the object name
     * @param Name the name of the object (used as identifier in FhSim)
     */
    public void setName(String Name) {
        this.Name = Name;
    }


    /**
     * Get the sea depth in meters 
     * @return the sea depth in meters
     */
    public double getSeaDepth() {
        return m_seaDepth;
    }

    /**
     * Set the sea depth in meters
     * @param depth the new sea depth in meters
     */
    public void setSeaDepth(double depth) {
        m_seaDepth = depth;
    }

    /**
     * Get the initial amplitude vector (used during initialization)
     * Amplitudes are "half waveheight" in meters (positive values)
     * @return the initial amplitude vector 
     */
    public double[] getInitialAmplitude() {
        return initialAmplitude;
    }

    /**
     * Set the initial amplitude vector (used during initialization)
     * Amplitudes are "half waveheight" in meters (positive values)
     * @param initialAmplitude the new initial amplitude vector
     */
    public void setInitialAmplitude(double[] initialAmplitude) {
        this.initialAmplitude = initialAmplitude;
    }

    /**
     * Get the initial frequency vector of the wave field. (used during initialization)
     * The wave frequency is given in rad/s
     * @return the initial frequency vector
     */
    public double[] getInitialFrequency() {
        return initialFrequency;
    }

    /**
     * Set the initial frequency vector of the wave field. (used during initialization)
     * The wave frequency is given in rad/s
     * @param initialFrequency the initialFrequency to set
     */
    public void setInitialFrequency(double[] initialFrequency) {
        this.initialFrequency = initialFrequency;
    }

    /**
     * Get the initial wave number vector of the wave field(used during initialization).
     * @return the vector of initial wave numbers
     */
    public double[] getInitialNumber() {
        return initialNumber;
    }

    /**
     * Get the initial wave number vector of the wave field (used during initialization).
     * @param initialNumber the vector of initial wave numbers
     */
    public void setInitialNumber(double[] initialNumber) {
        this.initialNumber = initialNumber;
    }

    /**
     * Get the vector of initial wave directions in the wave field (used during initialization).
     * The direction is given as the angle between the global north axis and the direction of wave propagation
     * @return the initial direction vector in rad
     */
    public double[] getInitialDirection() {
        return initialDirection;
    }

    /**
     * Set the vector of initial wave directions in the wave field (used during initialization).
     * The direction is given as the angle between the global north axis and the direction of wave propagation
     * @param initialDirection the initial direction vector in rad
     */
    public void setInitialDirection(double[] initialDirection) {
        this.initialDirection = initialDirection;
    }

    /**
     * Get the vector of random phase angles for the wave field (used during initialization).
     * @return the vector of initial phase angles 
     */
    public double[] getInitialPhase() {
        return initialPhase;
    }

    /**
     * 
     * @param initialPhase the initialPhase to set
     */
    public void setInitialPhase(double[] initialPhase) {
        this.initialPhase = initialPhase;
    }

    /**
     * @return the initialCurrent
     */
    public double[] getInitialCurrent() {
        return initialCurrent;
    }

    /**
     * @param initialCurrent the initialCurrent to set
     */
    public void setInitialCurrent(double[] initialCurrent) {
        this.initialCurrent = initialCurrent;
    }

    /**
     * @return the initialWind
     */
    public double[] getInitialWind() {
        return initialWind;
    }

    /**
     * @param initialWind the initialWind to set
     */
    public void setInitialWind(double[] initialWind) {
        this.initialWind = initialWind;
    }

    /**
     * Creates a new instance of a {@code SFHEnvironmentIntf} configured from
     * this data object.
     *
     * @return a new instance of a {@code SFHEnvironmentIntf} configured from
     * this data object.
     */
    public SFHEnvironmentIntf create() {
        return new SFHEnvironmentImpl(this);
    }

    /**
     * Roughness (height of peaks) on the sea bottom
     * @return the bottom roughness
     */
    public double getBottomRoughneess() {
        return bottomRoughneess;
    }

    /**
     * Roughness (height of peaks) on the sea bottom
     * Good default value is in the order of 10
     * @param bottomRoughneess new bottom roughness
     */
    public void setBottomRoughneess(double bottomRoughneess) {
        this.bottomRoughneess = bottomRoughneess;
    }

    /**
     * Scale for the bottom, the "wavelength" of the sea bottom height variation
     * @return the bottom structure scale value
     */
    public double getBottomtStructureScale() {
        return bottomtStructureScale;
    }

    /**
     * Scale for the bottom, the "wavelength" of the sea bottom height variation
     * Good default value is in the order of 100->1000
     * @param bottomtStructureScale new bottom structure scale
     */
    public void setBottomtStructureScale(double bottomtStructureScale) {
        this.bottomtStructureScale = bottomtStructureScale;
    }

}
    