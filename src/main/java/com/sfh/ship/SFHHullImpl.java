/*
 * Copyright SINTEF FIsheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

import java.util.ArrayList;
import org.w3c.dom.Element;

/**
 * Interface implementation for the FhSim ship model
 * @see SFHHullIntf
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public class SFHHullImpl implements SFHHullIntf {

    private String Name;
    private String ShipIO;

    private final String ExternalForceIO;

    private boolean OscPositionMove = false;
    private boolean OscRotationMove = false;
    private double[] ShipPositionUpdate = new double[5];
    private double[] globalOrientation;
    
    private double Lpp = 0;
    private double vcb = 0;
    private double lcb = 0;
    private double draught = 0;
    private double designDraught;
    private double[] cgPos = {0,0,0};
    private double[] windForce = {0,0,0};
    private double[] windTorque = {0,0,0};
    private double[] positionNED;
    private double[] currentForce = {0,0,0};
    private double[] secondOrderForce = {0,0,0,0,0,0};
    private double[] firstOrderForce = {0,0,0,0,0,0};
    private double[] velocityBody = {0,0,0,0,0,0};
    private double[] velocityRelBody = {0,0,0,0,0,0};
    private double[] velocityNED = {0,0,0};
    private double[] omegaBody = {0,0,0};
    private double[] omegaNED = {0,0,0};
    private double[] quaternionNED = {1,0,0,0};
            
    private double[] forceBodyInput = {0,0,0};
    private double[] torqueBodyInput = {0,0,0};
    
    private double[] oscInfo = {0,0,0,0,0,0,0,0};    
    
    
    private boolean indexInitialized = false;
    private int indexShipIO;
    private int indexPosition;
    private int indexVelocity;
    private int indexVelocityRel;
    private int indexOmega;
    private int indexQuaternion;
    private int indexWindForce;
    private int indexWindTorque;
    private int indexCurrentForce;
    private int indexSecondOrderForce;
    private int indexFirstOrderForce;
    private int indexVelocityBody;
    private int indexOSCInfo;
    private int indexOrientation;
    
    /**
     * Constructor 
     * @see SFHActuatorPointData
     * @param data configuration data
     */
    protected SFHHullImpl(SFHHullData data) {

        FhDynamicModel theModel = FhDynamicModel.instance();

        Element shipElement = theModel.createElement("Lib");
        shipElement.setAttribute("LibName", "SFHBaseLibrary");
        shipElement.setAttribute("SimObject", "Ship/Ship");
        shipElement.setAttribute("Name", data.getName());
        shipElement.setAttribute("PotentialFileName", data.getModelFileStem());
        shipElement.setAttribute("PotentialFileLocation", data.getModelData());        
        
        shipElement.setAttribute("OscMode", "1");
        int numberOfWaves = FhDynamicModel.instance().getNumberOfWavesInFhSim();
        if(numberOfWaves<=0)
            throw new RuntimeException("Number of waves in fhsim not set in FhDynamicModel!");
        shipElement.setAttribute("NumberOfWavesInField", Integer.toString(numberOfWaves ));
        
        
        String forceScale = "1,1,1,1,1,1,1,0";

        if( !data.isVeresWaveDrift() )
            forceScale = "1,1,0,1,1,1,1,0";
        shipElement.setAttribute("ForceScale", forceScale);
        if( data.isSfh_test() ){
            shipElement.setAttribute("DOF0Free", "0");
            shipElement.setAttribute("DOF3Free", "0");
            shipElement.setAttribute("DOF4Free", "0");

            if( data.isOnly_sway())
                shipElement.setAttribute("DOF1Free", "1");
            else
                shipElement.setAttribute("DOF1Free", "0");
        
            if( data.isOnly_yaw())
                shipElement.setAttribute("DOF5Free", "1");
            else
                shipElement.setAttribute("DOF5Free", "0");
            
            if( data.isOnly_roll())
                shipElement.setAttribute("DOF3Free", "1");
            else
                shipElement.setAttribute("DOF3Free", "0");
            
            if( data.isOnly_pitch())
                shipElement.setAttribute("DOF4Free", "1");
            else
                shipElement.setAttribute("DOF4Free", "0");
        }
            
        
        if( data.flipPhase() > 0 && data.flipPhase() < 6){
            shipElement.setAttribute("FlipPhaseForOSC", data.flipPhase()+"");
        } 
        
        double[] surge_dp = data.getDpDampingSurge();
        double[] sway_dp = data.getDpDampingSway();
        double[] yaw_dp = data.getDpDampingYaw();

        double[] surge_a = data.GetTunableDampingSurge();
        double[] sway_a = data.GetTunableDampingSway();
        double[] heave_a = data.GetTunableDampingHeave();

        double[] roll_a = data.GetTunableDampingRoll();
        double[] pitch_a = data.GetTunableDampingPitch();
        double[] yaw_a = data.GetTunableDampingYaw();
        

        shipElement.setAttribute("OSCLegacyManeuvreModel", Boolean.toString(data.isLegacyManeuverModel()));

        shipElement.setAttribute("StabilizeDOF5","1");
        shipElement.setAttribute("StabilizeDOF1","1");

        shipElement.setAttribute("ScaleResidualResistance",Double.toString(data.getResidualResistanceScale()));
        
        shipElement.setAttribute("ForceInput", "NED,BODY");
        shipElement.setAttribute("SurgeVelocity", Double.toString(data.getInitialSurgeVelocity()));

        shipElement.setAttribute("alfaN", Double.toString(data.getInitialRollPitchYaw()[0]*Math.PI/180.0));
        shipElement.setAttribute("alfaE", Double.toString(data.getInitialRollPitchYaw()[1]*Math.PI/180.0));
        shipElement.setAttribute("alfaD", Double.toString(data.getInitialRollPitchYaw()[2]*Math.PI/180.0));

        shipElement.setAttribute("Xu_dp", Double.toString(surge_dp[0]));
        shipElement.setAttribute("Xv_dp", Double.toString(surge_dp[1]));
        shipElement.setAttribute("Xr_dp", Double.toString(surge_dp[2]));
        shipElement.setAttribute("Yu_dp", Double.toString(sway_dp[0]));
        shipElement.setAttribute("Yv_dp", Double.toString(sway_dp[1]));
        shipElement.setAttribute("Yr_dp", Double.toString(sway_dp[2]));
        shipElement.setAttribute("Nu_dp", Double.toString(yaw_dp[0]));
        shipElement.setAttribute("Nv_dp", Double.toString(yaw_dp[1]));
        shipElement.setAttribute("Nr_dp", Double.toString(yaw_dp[2]));
        
        shipElement.setAttribute("DampingBlendSpeed", "2.5");
        shipElement.setAttribute("Yv_man",   Double.toString(data.getManYv()) );   
        shipElement.setAttribute("Yr_man",   Double.toString(data.getYr_man()) );   
        shipElement.setAttribute("Nv_man",   Double.toString(data.getManNv()) );   
        shipElement.setAttribute("Nr_man",   Double.toString(data.getManNr()) );   
        shipElement.setAttribute("Yrd_man",  Double.toString(data.getManYrd()) );  
        shipElement.setAttribute("Yvd_man",  Double.toString(data.getManYvd()) );  
        shipElement.setAttribute("Nrd_man",  Double.toString(data.getManNrd()) );  
        shipElement.setAttribute("Nvd_man",  Double.toString(data.getManNvd()) );  
        shipElement.setAttribute("Xrr_man",  Double.toString(data.getManXrr()) );  
        shipElement.setAttribute("Xud_man",  Double.toString(data.getXud_man()) );  
        shipElement.setAttribute("Xvr_man",  Double.toString(data.getXvr_man()) );  
        shipElement.setAttribute("Xvv_man",  Double.toString(data.getXvv_man()) );  
        shipElement.setAttribute("Xvvvv_man",Double.toString(data.getXvvvv_man()) );
        
        shipElement.setAttribute("ResistanceFriction", "ITTC");
        shipElement.setAttribute("ResistanceResidual", "Hollenbach");
        shipElement.setAttribute("ResistanceResidualHollenbachNumRudders", Integer.toString(data.getNumberOfRudders()) );
        shipElement.setAttribute("ResistanceResidualHollenbachNumBrac", Integer.toString(data.getNumberOfBrackets()));
        shipElement.setAttribute("ResistanceResidualHollenbachNumBoss", Integer.toString(data.getNumberOfBosses()));
        shipElement.setAttribute("ResistanceResidualHollenbachNumThruster", Integer.toString(data.getNumberOfThrusters()));
        shipElement.setAttribute("ResistanceResidualHollenbachDiamPropeller", Integer.toString(data.getMainPropellerDiameter()));

        shipElement.setAttribute("DamperLinear", surge_a[0]+","+sway_a[0]+","+heave_a[0]);
        shipElement.setAttribute("DamperRotLinear", roll_a[0]+","+pitch_a[0]+","+yaw_a[0]);

        shipElement.setAttribute("DamperQuadratic", surge_a[1]+","+sway_a[1]+","+heave_a[1]);
        shipElement.setAttribute("DamperRotQuadratic", roll_a[1]+","+pitch_a[1]+","+yaw_a[1]);

        double bkStart = data.getBilgeKeelStart();
        double bkEnd = data.getBilgeKeelEnd();
        double bkHeight = data.getBilgeKeelHeight();

        if( (bkStart + bkEnd + bkHeight) != 0){
            shipElement.setAttribute("BilgeKeelStartPosition",""+bkStart);
            shipElement.setAttribute("BilgeKeelEndPosition",""+bkEnd);
            shipElement.setAttribute("BilgeKeelHeight",""+bkHeight);
        }
        
        Element windElement = theModel.createElement("Lib");
        windElement.setAttribute("LibName", "SFHBaseLibrary");
        windElement.setAttribute("SimObject", "Ship/WindForce");
        windElement.setAttribute("Name", data.getName()+"Wind");
        
        windElement.setAttribute("ProjectedLateralArea", Double.toString(data.getWindLateralArea()));
        windElement.setAttribute("ProjectedFrontArea", Double.toString(data.getWindFrontalArea()) );
        windElement.setAttribute("CharacteristicLength", Double.toString(data.getWindCharacteristicLength()) );
        windElement.setAttribute("VerticalPressureCenter", Double.toString(data.getWindPressureCenterHeight()) );        

        windElement.setAttribute("FourierA0x", Double.toString(data.getWindA0x()) );
        windElement.setAttribute("FourierA0y", Double.toString(data.getWindA0y()));
        windElement.setAttribute("FourierA0n", Double.toString(data.getWindA0n()));
        windElement.setAttribute("FourierAx", arrayToFhSimString(data.getWindAx()) );
        windElement.setAttribute("FourierAy", arrayToFhSimString(data.getWindAy()));
        windElement.setAttribute("FourierAn", arrayToFhSimString(data.getWindAn()));
        windElement.setAttribute("FourierBx", arrayToFhSimString(data.getWindBx()));
        windElement.setAttribute("FourierBy", arrayToFhSimString(data.getWindBy()));
        windElement.setAttribute("FourierBn", arrayToFhSimString(data.getWindBn()));

        int forceFromNewTypeRefPoint = data.ReferencePointForce.size();
        
        Element forceSum = theModel.createElement("Lib");
        forceSum.setAttribute("LibName", "FhSimBaseLibrary");
        forceSum.setAttribute("SimObject", "Math/Sum");
        forceSum.setAttribute("PortWidth", "3");
        forceSum.setAttribute("NumInput", Integer.toString( forceFromNewTypeRefPoint +1));
        forceSum.setAttribute("Name", data.getName() + "ForceSum");

        Element torqueSum = theModel.createElement("Lib");
        torqueSum.setAttribute("LibName", "FhSimBaseLibrary");
        torqueSum.setAttribute("PortWidth", "3");
        torqueSum.setAttribute("SimObject", "Math/Sum");
        torqueSum.setAttribute("NumInput", Integer.toString( forceFromNewTypeRefPoint +1));
        torqueSum.setAttribute("Name", data.getName() + "TorqueSum");

        Element forceExtSum = theModel.createElement("Lib");
        forceExtSum.setAttribute("LibName", "FhSimBaseLibrary");
        forceExtSum.setAttribute("SimObject", "Math/Sum");
        forceExtSum.setAttribute("PortWidth", "3");
        forceExtSum.setAttribute("NumInput", "2");
        forceExtSum.setAttribute("Name", data.getName() + "forceExtSum");

        Element torqueExtSum = theModel.createElement("Lib");
        torqueExtSum.setAttribute("LibName", "FhSimBaseLibrary");
        torqueExtSum.setAttribute("PortWidth", "3");
        torqueExtSum.setAttribute("SimObject", "Math/Sum");
        torqueExtSum.setAttribute("NumInput", "2");
        torqueExtSum.setAttribute("Name", data.getName() + "torqueExtSum");

        
        ShipIO = data.getName() + "IO";
        Element shipIO = theModel.createElement("Lib");
        shipIO.setAttribute("LibName", "FhSimBaseLibrary");
        shipIO.setAttribute("SimObject", "ExternalLinkStandard");
        shipIO.setAttribute("Name", ShipIO);
        shipIO.setAttribute("outputPortNames", "PositionUpdate,ForceBody,TorqueBody");
        shipIO.setAttribute("inputPortNames", "OscInfo,QuaternionNED,PositionNED,OrientationNED,OmegaNED,VelocityNED,VelocityRelBody,VelocityBody,FirstOrderForceBody,SecondOrderForceBody,CurrentForceBody,WindForceBody,WindTorqueBody");
        shipIO.setAttribute("Size_WindForceBody", "3");
        shipIO.setAttribute("Size_WindTorqueBody", "3");
        shipIO.setAttribute("Size_PositionNED", "3");
        shipIO.setAttribute("Size_OrientationNED", "3");
        shipIO.setAttribute("Size_VelocityNED", "3");
        shipIO.setAttribute("Size_OmegaNED", "3");
        shipIO.setAttribute("Size_VelocityBody", "6");
        shipIO.setAttribute("Size_VelocityRelBody", "6");
        shipIO.setAttribute("Size_FirstOrderForceBody","6");
        shipIO.setAttribute("Size_SecondOrderForceBody","6");
        shipIO.setAttribute("Size_OscInfo","8");
        shipIO.setAttribute("Size_QuaternionNED","4");
        shipIO.setAttribute("Size_CurrentForceBody","6");
        shipIO.setAttribute("Initial_PositionUpdate","0,0,0,0,0");
        shipIO.setAttribute("Initial_ForceBody","0,0,0");
        shipIO.setAttribute("Initial_TorqueBody","0,0,0");
        
        ExternalForceIO = data.getName() + "ExternalIO";
        Element externalIO = theModel.createElement("Lib");
        externalIO.setAttribute("LibName", "FhSimBaseLibrary");
        externalIO.setAttribute("SimObject", "ExternalLinkStandard");
        externalIO.setAttribute("Name", ExternalForceIO);
        externalIO.setAttribute("outputPortNames", "Force,Torque");
        externalIO.setAttribute("inputPortNames", "");
        externalIO.setAttribute("Initial_Force", "0,0,0");
        externalIO.setAttribute("Initial_Torque", "0,0,0");


        Element objectsElement = theModel.getObjectElement();
        objectsElement.appendChild(shipElement);
        objectsElement.appendChild(shipIO);
        objectsElement.appendChild(externalIO);
        objectsElement.appendChild(forceSum);
        objectsElement.appendChild(torqueSum);
        objectsElement.appendChild(forceExtSum);
        objectsElement.appendChild(torqueExtSum);
        objectsElement.appendChild(windElement);

        Element connections = theModel.getInterconnectionElement();

        connections.setAttribute(shipElement.getAttribute("Name") + ".OscPositionControl", shipIO.getAttribute("Name") + ".PositionUpdate");
        
        connections.setAttribute(shipElement.getAttribute("Name") + ".WaveAmplitude", "DefaultEnvironmentIO" + ".WaveAmplitude");
        connections.setAttribute(shipElement.getAttribute("Name") + ".WaveFrequency", "DefaultEnvironmentIO" + ".WaveFrequency");
        connections.setAttribute(shipElement.getAttribute("Name") + ".WaveDirection", "DefaultEnvironmentIO" + ".WaveDirection");
        connections.setAttribute(shipElement.getAttribute("Name") + ".WaveNumber", "DefaultEnvironmentIO" + ".WaveNumber");
        connections.setAttribute(shipElement.getAttribute("Name") + ".WavePhase", "DefaultEnvironmentIO" + ".WavePhase");

        connections.setAttribute(shipElement.getAttribute("Name") + ".TorqueNED", torqueSum.getAttribute("Name") + ".Out");

        
        connections.setAttribute(shipElement.getAttribute("Name") + ".ForceNED", forceSum.getAttribute("Name") + ".Out");
        connections.setAttribute(shipElement.getAttribute("Name") + ".TorqueNED", torqueSum.getAttribute("Name") + ".Out");

        connections.setAttribute(shipElement.getAttribute("Name") + ".ForceBODY", shipIO.getAttribute("Name") + ".ForceBody");
        connections.setAttribute(shipElement.getAttribute("Name") + ".TorqueBODY", shipIO.getAttribute("Name") + ".TorqueBody");

        connections.setAttribute(shipIO.getAttribute("Name") + ".PositionNED", shipElement.getAttribute("Name") + ".PositionNED");
        connections.setAttribute(shipIO.getAttribute("Name") + ".VelocityNED", shipElement.getAttribute("Name") + ".VelocityNED");
        connections.setAttribute(shipIO.getAttribute("Name") + ".OmegaNED", shipElement.getAttribute("Name") + ".OmegaNED");
        connections.setAttribute(shipIO.getAttribute("Name") + ".VelocityBody", shipElement.getAttribute("Name") + ".VelocityBody");
        connections.setAttribute(shipIO.getAttribute("Name") + ".VelocityRelBody", shipElement.getAttribute("Name") + ".RelativeVelocityBODY");
        connections.setAttribute(shipIO.getAttribute("Name") + ".OrientationNED", shipElement.getAttribute("Name") + ".RotationBody");
        connections.setAttribute(shipIO.getAttribute("Name") + ".FirstOrderForceBody",shipElement.getAttribute("Name") + ".FirstOrderWaveBody");
        connections.setAttribute(shipIO.getAttribute("Name") + ".SecondOrderForceBody",shipElement.getAttribute("Name") + ".WaveDriftBody");
        connections.setAttribute(shipIO.getAttribute("Name") + ".CurrentForceBody",shipElement.getAttribute("Name") + ".CrossFlowBody");
        connections.setAttribute(shipIO.getAttribute("Name") + ".OscInfo",shipElement.getAttribute("Name") + ".OscInfoPort");

        connections.setAttribute(windElement.getAttribute("Name")+".WindNED", "DefaultEnvironmentIO.WindNED");
        connections.setAttribute(windElement.getAttribute("Name")+".VelocityNED", shipElement.getAttribute("Name") +".VelocityNED");
        connections.setAttribute(windElement.getAttribute("Name")+".QuaternionNED", shipElement.getAttribute("Name") +".QuaternionNED");
        
        connections.setAttribute(shipIO.getAttribute("Name")+".QuaternionNED", shipElement.getAttribute("Name") +".QuaternionNED");
        
        
        for( int i = 0; i < forceFromNewTypeRefPoint; i++){
            connections.setAttribute(forceSum.getAttribute("Name") + ".In" + (i + 1), data.ReferencePointForce.get(i) + ".ForceNED");
            connections.setAttribute(torqueSum.getAttribute("Name") + ".In" + (i + 1), data.ReferencePointForce.get(i) + ".TorqueNED");
        }
        
        connections.setAttribute(forceSum.getAttribute("Name") + ".In" + (forceFromNewTypeRefPoint+1), forceExtSum.getAttribute("Name") + ".Out");
        connections.setAttribute(torqueSum.getAttribute("Name") + ".In" + (forceFromNewTypeRefPoint+1), torqueExtSum.getAttribute("Name") + ".Out");

        connections.setAttribute(forceExtSum.getAttribute("Name") + ".In1", externalIO.getAttribute("Name") + ".Force");
        connections.setAttribute(torqueExtSum.getAttribute("Name") + ".In1", externalIO.getAttribute("Name") + ".Torque");
        
        connections.setAttribute(forceExtSum.getAttribute("Name") + ".In2", windElement.getAttribute("Name")+".ForceNED");
        connections.setAttribute(torqueExtSum.getAttribute("Name") + ".In2", windElement.getAttribute("Name")+".TorqueNED");
        connections.setAttribute(shipIO.getAttribute("Name") + ".WindForceBody",windElement.getAttribute("Name")+".ForceNED");
        connections.setAttribute(shipIO.getAttribute("Name") + ".WindTorqueBody",windElement.getAttribute("Name")+".TorqueNED");

        ArrayList<String> connectedGearForce = data.getConnectedGearForce();
        ArrayList<String> connectedGeartorque = data.getConnectedGearTorque();
        
        if( connectedGearForce != null && connectedGearForce.size() > 0 ){
            forceExtSum.setAttribute("NumInput", 2+connectedGearForce.size()+"");
            for( int i=3; i < 3+connectedGearForce.size(); i++)
                connections.setAttribute(forceExtSum.getAttribute("Name") + ".In"+i, connectedGearForce.get(i-3));
        }

        if( connectedGeartorque != null && connectedGeartorque.size() > 0 ){
            torqueExtSum.setAttribute("NumInput", 2+connectedGeartorque.size()+"");
            for( int i=3; i < 3+connectedGeartorque.size(); i++)
                connections.setAttribute(torqueExtSum.getAttribute("Name") + ".In"+i, connectedGeartorque.get(i-3));
        }
        
        
        Element initialCondition = theModel.getInitializationElement();
        initialCondition.setAttribute(shipElement.getAttribute("Name") + ".StatePosition", data.getInitialNorthEastDown()[0] + "," + data.getInitialNorthEastDown()[1] + "," + data.getInitialNorthEastDown()[2]);

        
        
        
        ShipPositionUpdate[0] = 0;
        ShipPositionUpdate[1] = data.getInitialNorthEastDown()[0];
        ShipPositionUpdate[2] = data.getInitialNorthEastDown()[1];
        ShipPositionUpdate[3] = data.getInitialNorthEastDown()[2];

        
        ShipPositionUpdate[4] = data.getInitialRollPitchYaw()[2];
        
        FhDynamicModel.instance().registerObject(this,data);
    }

    /**
     * Return the position of the COG in NED coordinates in m
     * @return [1x3] with NED coordinates
     */
    public double[] GetGlobalShipPosition() {
        return positionNED;
    }

    /**
     * Return the global orientation of the ship in NED coordinates in rad
     * @return [1x3] vector of rotations about the NED axes
     */
    public double[] GetGlobalShipOrientation() {
        return globalOrientation;
    }

    /**
     * Return the linear velocities of the hull in NED in m/s
     * @return [1x3] vector of the linear ship velocities in NED
     */
    public double[] GetGlobalShipVelocity() {
        return velocityNED;
    }

    /**
     * Return the linear velocity of the ship in the body fixed frame in m/s
     * @return [1x3] vector of linear velocities in the body frame
     */
    public double[] GetLocalShipVelocity() {
        return velocityBody;
    }


    @Override
    public void tick(double Lookahead) {
        FhDynamicModel.instance().objectUpdated(this, Lookahead);
    }

    @Override
    public void preTick() {
      
       ShipPositionUpdate[0] = (OscRotationMove || OscPositionMove)?1.0:0.0;
       if( OscPositionMove ){
           OscPositionMove = false;
       }
       if(OscRotationMove ){
           OscRotationMove = false;
       }
       
       FhDynamicModel.instance().setFhSimPort(ShipIO, "PositionUpdate", ShipPositionUpdate);
       FhDynamicModel.instance().setFhSimPort(ShipIO, "ForceBody", forceBodyInput);
       FhDynamicModel.instance().setFhSimPort(ShipIO, "TorqueBody", torqueBodyInput);
       
    }

    @Override
    public void postTick() {

        if(!indexInitialized){
            indexInitialized = true;
            indexShipIO = FhDynamicModel.instance().getObjectIndex(ShipIO);
            indexPosition = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "PositionNED");
            indexVelocity = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "VelocityNED");
            indexOmega = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "OmegaNED");
            indexQuaternion = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "QuaternionNED");
            indexWindForce = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "WindForceBody");
            indexWindTorque = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "WindTorqueBody");
            indexCurrentForce = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "CurrentForceBody");
            indexSecondOrderForce = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "SecondOrderForceBody");
            indexFirstOrderForce = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "FirstOrderForceBody");
            indexVelocityBody = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "VelocityBody");
            indexOSCInfo = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "OscInfo");
            indexOrientation = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "OrientationNED");
            
            indexVelocityRel = FhDynamicModel.instance().getObjectPortIndex(ShipIO, "VelocityRelBody");
        }
        
    
        positionNED = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexPosition, 3);
        windForce = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexWindForce, 3);
        windTorque = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexWindTorque, 3);
        currentForce = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexCurrentForce, 3);
        secondOrderForce = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexSecondOrderForce, 6);
        firstOrderForce = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexFirstOrderForce, 6);
        velocityBody = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexVelocityBody, 6);
        velocityRelBody = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexVelocityRel, 6);
        velocityNED = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexVelocity, 3);
        omegaNED = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexOmega, 3);

        quaternionNED = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexQuaternion, 4);
        globalOrientation = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexOrientation, 3);

        oscInfo = FhDynamicModel.instance().getFhSimPortFromIndex(indexShipIO, indexOSCInfo, 8);
    
        /*
        positionNED = FhDynamicModel.instance().getFhSimPort(ShipIO, "PositionNED", 3);
        windForce = FhDynamicModel.instance().getFhSimPort(ShipIO, "WindForceBody", 3);        
        windTorque = FhDynamicModel.instance().getFhSimPort(ShipIO, "WindTorqueBody", 3);        
        currentForce = FhDynamicModel.instance().getFhSimPort(ShipIO, "CurrentForceBody", 3);
        secondOrderForce = FhDynamicModel.instance().getFhSimPort(ShipIO, "SecondOrderForceBody", 6);
        firstOrderForce = FhDynamicModel.instance().getFhSimPort(ShipIO, "FirstOrderForceBody", 6);
        velocityBody = FhDynamicModel.instance().getFhSimPort(ShipIO, "VelocityBody", 6);
        velocityNED = FhDynamicModel.instance().getFhSimPort(ShipIO, "VelocityNED", 3);
        omegaNED = FhDynamicModel.instance().getFhSimPort(ShipIO, "OmegaNED", 3);

        quaternionNED = FhDynamicModel.instance().getFhSimPort(ShipIO, "QuaternionNED", 4);
        globalOrientation = FhDynamicModel.instance().getFhSimPort(ShipIO, "OrientationNED", 3);

        oscInfo = FhDynamicModel.instance().getFhSimPort(ShipIO, "OscInfo", 8);
        */
        omegaBody[0] = velocityBody[3];
        omegaBody[1] = velocityBody[4];
        omegaBody[2] = velocityBody[5];


        
        ShipPositionUpdate[1] = GetGlobalShipPosition()[0];
        ShipPositionUpdate[2] = GetGlobalShipPosition()[1];
        ShipPositionUpdate[3] = GetGlobalShipPosition()[2];
        ShipPositionUpdate[4] = GetGlobalOrientation()[2];
        
        
        vcb = -oscInfo[7];// VERES coordinates on output port
        lcb = oscInfo[6];
        Lpp = oscInfo[5];
        designDraught = oscInfo[1];
        draught = oscInfo[0];
        cgPos[0] = oscInfo[2];
        cgPos[1] = oscInfo[3];
        cgPos[2] = -oscInfo[4]; // VERES coordinates on output port
    }

    @Override
    public String GetName() {
        return Name;
    }

    @Override
    public double GetVerticalCentreOfBuoyancy() {
        return vcb;
    }

    @Override
    public double GetLongitudinalCentreOfBuoyancy() {
        return lcb;
    }

    @Override
    public double GetLengthBetweenPerpendiculars() {
        return Lpp;
    }

    @Override
    public double GetDesignDraught() {
        return designDraught;
    }

    @Override
    public double GetDraught() {
        return draught;
    }

    @Override
    public double[] GetGlobalPosition() {
        return GetGlobalShipPosition();
    }

    @Override
    public double[] GetGlobalOrientation() {
        return globalOrientation;
    }

    @Override
    public double[] GetGlobalVelocity() {
        return velocityNED;
    }

    @Override
    public double[] GetGlobalRotationVelocity() {
        return omegaNED;
    }

    @Override
    public double[] GetLocalVelocity() {
        return velocityBody;
    }

    @Override
    public double[] GetLocalWaterVelocity() {
        return velocityRelBody;
    }

    @Override
    public double[] GetLocalRotationVelocity() {
        return omegaBody;
    }

    @Override
    public double[] GetCurrentForce() {
        return currentForce;
    }

    @Override
    public double[] GetFirstOrderForce() {
        return firstOrderForce;
    }

    @Override
    public double[] GetSecondOrderForce() {
        return secondOrderForce;
    }

    @Override
    public double[] GetWindForce() {
        return windForce;
    }

    @Override
    public double[] GetWindTorque() {
        return windTorque;
    }

    @Override
    public double[] GetCGPosition() {
        return cgPos;
    }

    @Override
    public void SetPositionNED(double north, double east, double down) {
        OscPositionMove = true;
        ShipPositionUpdate[1] = north;
        ShipPositionUpdate[2] = east;
        ShipPositionUpdate[3] = down;
    }

    @Override
    public void SetPositionNE(double north, double east) {
        SetPositionNED(north, east, 0);
    }

    @Override
    public void SetPositionMidpointNED(double north, double east, double down) {
        OscPositionMove = true;
        
        double lcg_north;
        double lcg_east;
        if(OscRotationMove){ // if a new course is already set, use this to calculate midpoint offset
            lcg_north = (this.GetCGPosition()[0]-this.GetLengthBetweenPerpendiculars()/2.0) * Math.cos(ShipPositionUpdate[4] );
            lcg_east  = (this.GetCGPosition()[0]-this.GetLengthBetweenPerpendiculars()/2.0) * Math.sin(ShipPositionUpdate[4] );
        }else{
            lcg_north = (this.GetCGPosition()[0]-this.GetLengthBetweenPerpendiculars()/2.0) * Math.cos( this.GetGlobalOrientation()[2]);
            lcg_east  = (this.GetCGPosition()[0]-this.GetLengthBetweenPerpendiculars()/2.0) * Math.sin( this.GetGlobalOrientation()[2]);
        }
        
        
        double vcg_down = this.GetDesignDraught()+this.GetCGPosition()[2];//bugfix for veres coordinate fix on cg posotion
        
        ShipPositionUpdate[1] = north + lcg_north;
        ShipPositionUpdate[2] = east + lcg_east;
        ShipPositionUpdate[3] = down + vcg_down;
    }

    @Override
    public void SetPositionMidpointNE(double north, double east) {
        SetPositionMidpointNED(north, east, 0);
    }

    @Override
    public void SetHeadingNED(double heading_north) {
        OscRotationMove = true;
        ShipPositionUpdate[4] = heading_north;        
    }

    @Override
    public void setPositionAndHeadingMidpointNE(double north, double east, double heading){
        setPositionAndHeadingMidpointNED(north,east,0,heading);
    }

    @Override
    public void setPositionAndHeadingMidpointNED(double north, double east, double down, double heading){
        OscPositionMove = true;
        OscRotationMove = true;
        double lcg_north;
        double lcg_east;
        lcg_north = (this.GetCGPosition()[0]-this.GetLengthBetweenPerpendiculars()/2.0) * Math.cos(heading);
        lcg_east  = (this.GetCGPosition()[0]-this.GetLengthBetweenPerpendiculars()/2.0) * Math.sin(heading);
           
        double vcg_down = this.GetDesignDraught()+this.GetCGPosition()[2]; //bugfix for veres coordinate fix on cg posotion
        
        ShipPositionUpdate[1] = north + lcg_north;
        ShipPositionUpdate[2] = east + lcg_east;
        ShipPositionUpdate[3] = down + vcg_down;
        ShipPositionUpdate[4] = heading;
        
    }
    
    
    /**
     * @param forceBodyInput the forceBodyInput to set
     */
    @Override
    public void setForceBodyInput(double[] forceBodyInput) {
        this.forceBodyInput = forceBodyInput;
    }


    /**
     * @param torqueBodyInput the torqueBodyInput to set
     */
    @Override
    public void setTorqueBodyInput(double[] torqueBodyInput) {
        this.torqueBodyInput = torqueBodyInput;
    }

    
    private static String arrayToFhSimString( double [] array ){
        String str = "";
        for( int i=0; i < array.length; i++){
            if( i == (array.length-1) ){
                str += Double.toString(array[i]);
            }else{
                str += Double.toString(array[i])+",";
            }
        }
        return str;
    }

    @Override
    public double[] rotateToHullFrame(double[] v) {     
        double[]out = new double[]{0,0,0};
        double[]q = quaternionNED.clone();
        q[1] *= -1;
        q[2] *= -1;
        q[3] *= -1;
        
        double n = Math.sqrt( q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
        
        q[0] /= n;
        q[1] /= n;
        q[2] /= n;
        q[3] /= n;
        
        out[0] += (Math.pow(q[0], 2.0)+Math.pow(q[1],2.0) - Math.pow(q[2], 2.0)-Math.pow(q[3],2.0) )*v[0];
        out[0] += 2*(q[1]*q[2]-q[0]*q[3])*v[1];
        out[0] += 2*(q[1]*q[3] + q[0]*q[2])*v[2];

        out[1] += (2*q[1]*q[2] + 2*q[0]*q[3])*v[0];
        out[1] += (Math.pow(q[0], 2.0)-Math.pow(q[1], 2.0) + Math.pow(q[2], 2.0)-Math.pow(q[3], 2.0))*v[1];	
        out[1] += (2*q[2]*q[3] - 2*q[0]*q[1])*v[2];
        
        out[2] += (2*q[1]*q[3] - 2*q[0]*q[2])*v[0];	
        out[2] += (2*q[2]*q[3] + 2*q[0]*q[1])*v[1];
        out[2] += ( Math.pow(q[0], 2.0)-Math.pow(q[1], 2.0)-Math.pow(q[2], 2.0)+Math.pow(q[3], 2.0) )*v[2];
        return out;
    }
}
