/*
 * Copyright SINTEF FIsheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

import org.w3c.dom.Element;

/**
 * Implementation of the OPUS propulsor model
 * @see SFHOpusPropulsorIntf
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public class SFHOpusPropulsorImpl implements SFHOpusPropulsorIntf{

    /**Nominal thrust return value*/
    private double nominalThrust;
    /**Corrected thrust return value*/
    private double correctedThrust;
    
    /**Force in body frame return value*/
    private double[] forceBody;
    /**Torque in body frame return value*/
    private double[] torqueBody;
    /**Power consumed by the propulsor*/
    private double[] consumedPower = {0};
    /**The type of propulsor*/
    private SFHOpusPropulsorData.PropulsorType type;
    
    /**The name of the IO block in FhSim*/
    private String PropulsorIOName;
    /**The name of the propulsor in FhSim*/
    private String PropulsorName;
    /**The name of the actuator point which this propulsor is connected to*/
    private String ShipConnectionName;
    
    private double thrustDeduction=1;
    private double wakeFraction=1;
    
    private boolean indexInitialized = false;
    private int indexIOObj;
    private int indexTmpT;
    private int indexTmpQ;
    private int indexTmpTc;
    private int indexTmpQc;
    private int indexForceBody;
    private int indexTorqueBody;
    private int indexConsumedPower;    
    
    
    
    /**
     * Create a new Opus propulsor simulation object
     * @param data static configuration data and initial values
     */
    
    protected SFHOpusPropulsorImpl(SFHOpusPropulsorData data){
        
        PropulsorName = data.getName();
        PropulsorIOName = data.getName()+"IO";
        ShipConnectionName = data.getConnectionName();
        
        thrustDeduction = data.getThrustDeduction();
        wakeFraction = data.getWakeFraction();
        
        Element propulsorElement = FhDynamicModel.instance().createLibElement();
        propulsorElement.setAttribute("LibName", "SFHBaseLibrary");
        propulsorElement.setAttribute("Name", data.getName());

        Element propulsorElementIO = FhDynamicModel.instance().createLibElement();
        Element connection = FhDynamicModel.instance().createElement("Connection");

        connection.setAttribute(PropulsorName+".PositionNED",ShipConnectionName+".PositionNED");
        connection.setAttribute(PropulsorName+".VelocityNED",ShipConnectionName+".VelocityNED");
        //connection.setAttribute(ShipConnectionName+".ForceNED",PropulsorName+".ForceNED");
                
        propulsorElementIO.setAttribute("LibName", "FhSimBaseLibrary");
        propulsorElementIO.setAttribute("SimObject", "ExternalLinkStandard");
        propulsorElementIO.setAttribute("Name", PropulsorIOName);
        type = data.getType();
        
        switch( type ){
            case Open:
            case Ducted:
            case Tunnel:
                if( data.isPropulsorStrut() ){
                    propulsorElement.setAttribute("SimObject", "Ship/Propeller4QWithStrut");
                }else{
                    propulsorElement.setAttribute("SimObject", "Ship/Propeller4Q");
                }
                switch(type){
                    case Open:
                        propulsorElement.setAttribute("PropellerType", "Open");
                        break;
                    case Ducted:
                        propulsorElement.setAttribute("PropellerType", "Ducted");
                        break;
                    case Tunnel:
                        propulsorElement.setAttribute("PropellerType", "Tunnel");
                        break;
                }
                propulsorElement.setAttribute("D", Double.toString(data.getDiameter()*data.getPropellerDiameterScaling() ));
                
                if( data.isPropulsorStrut() ){
                    propulsorElement.setAttribute("StrutChord", Double.toString(data.getChord()) );                
                    propulsorElement.setAttribute("StrutHeight", Double.toString(data.getHeight()) );                
                }
                
                propulsorElement.setAttribute("ShaftPowerScaling", Double.toString(data.getShaftPowerScaling()) );
                propulsorElement.setAttribute("PropellerThrustScaling", Double.toString(data.getPropellerThrustScaling()) );
                propulsorElement.setAttribute("PD", GenerateArrayString(data.GetPropellerPD()));
                double [][] Ct = data.GetPropellerCT();
                double [][] Cq = data.GetPropellerCQ();
                
                for(int i=0; i < Ct.length; i++){
                    String ct_str = "Ct"+Integer.toString(i);
                    String cq_str = "Cq"+Integer.toString(i);

                    propulsorElement.setAttribute(ct_str, GenerateArrayString(Ct[i] ));
                    propulsorElement.setAttribute(cq_str, GenerateArrayString(Cq[i] ));
                }
                
                propulsorElementIO.setAttribute("inputPortNames", "T,Tnom,Q,Qnom,Fbody,Qbody,Rot,Pos,ConsumedPower");
                propulsorElementIO.setAttribute("Size_ConsumedPower","1");
                propulsorElementIO.setAttribute("Initial_PD", "0");
                propulsorElementIO.setAttribute("Initial_RPM", "0.0");
                propulsorElementIO.setAttribute("Size_T","3");
                propulsorElementIO.setAttribute("Size_Tnom","3");
                propulsorElementIO.setAttribute("Size_Q","3");
            	propulsorElementIO.setAttribute("Size_Qnom","3");        
                propulsorElementIO.setAttribute("Size_Fbody","3");
                propulsorElementIO.setAttribute("Size_Qbody","3");
                propulsorElementIO.setAttribute("Size_Rot","3");
                propulsorElementIO.setAttribute("Size_Pos","3");
                
                if( data.connectionIsActuatorPoint() ){
                    propulsorElementIO.setAttribute("outputPortNames", "");
                    connection.setAttribute(PropulsorName+".RPM", ShipConnectionName+".RPM");
                    connection.setAttribute(PropulsorName+".PD", ShipConnectionName+".PD");
                    connection.setAttribute(ShipConnectionName+".ShaftTorque", PropulsorName + ".ShaftTorqueCorrectedBody");
                }else{
                    propulsorElementIO.setAttribute("outputPortNames", "PD,RPM");
                    propulsorElementIO.setAttribute("Initial_PD", "0");
                    propulsorElementIO.setAttribute("Initial_RPM", "0.0");
                    connection.setAttribute(PropulsorName+".RPM", PropulsorIOName+".RPM");
                    connection.setAttribute(PropulsorName+".PD", PropulsorIOName+".PD");
                }
                

                if( data.isPropulsorStrut() ){
                    connection.setAttribute(PropulsorIOName+".T", PropulsorName+".ForceCorrected2NED");
                    connection.setAttribute(PropulsorIOName+".Tnom", PropulsorName+".Force2NED");
                }else{
                    connection.setAttribute(PropulsorIOName+".T", PropulsorName+".ForceCorrectedNED");
                    connection.setAttribute(PropulsorIOName+".Tnom", PropulsorName+".ForceNED");
                }
                connection.setAttribute(PropulsorIOName+".Fbody",ShipConnectionName+".ForceBody");

                connection.setAttribute(PropulsorIOName+".Q", PropulsorName+".TorqueCorrectedNED");
                connection.setAttribute(PropulsorIOName+".Qnom", PropulsorName+".TorqueNED");
                connection.setAttribute(PropulsorIOName+".Qbody",ShipConnectionName+".TorqueBody");
                
                connection.setAttribute(PropulsorIOName+".Pos",ShipConnectionName+".OffsetBody");
                connection.setAttribute(PropulsorIOName+".Rot",ShipConnectionName+".RotationBody");
        
                connection.setAttribute(PropulsorName+".QuaternionNED",ShipConnectionName+".QuaternionNED");

                connection.setAttribute(PropulsorIOName+".ConsumedPower",PropulsorName+".ShaftPowerCorrected");
                
                // enable azimuth loss model
                if( data.connectionIsAzimuthMotor() && !data.connectionIsOffCenter()){
                    FhDynamicModel.instance().getNamedObjectElement(ShipConnectionName).setAttribute("AzimuthLoss", "1");
                }else if( data.connectionIsAzimuthMotor() && data.connectionIsOffCenter() ){
        //            FhDynamicModel.instance().getNamedObjectElement(ShipConnectionName).setAttribute("AzimuthLoss", "1");
                    propulsorElement.setAttribute("AzimuthMainThrustLoss", "1");
                }
                propulsorElement.setAttribute("ThrustDeduction", Double.toString(thrustDeduction));
                propulsorElement.setAttribute("WakeFraction", Double.toString(wakeFraction));
                break;
            case Rudder:
                propulsorElement.setAttribute("SimObject", "Ship/Rudder");
                propulsorElement.setAttribute("RudderType", "Curve");
                propulsorElement.setAttribute("Height", Double.toString(data.getHeight()));
                propulsorElement.setAttribute("Chord", Double.toString(data.getChord()));
                propulsorElement.setAttribute("Alfa",GenerateArrayString( data.GetRudderAngle()) );
                propulsorElement.setAttribute("Cl",GenerateArrayString( data.GetRudderCL()) );
                propulsorElement.setAttribute("Cd",GenerateArrayString( data.GetRudderCD()) );
                propulsorElement.setAttribute("ForceCorrection",""+data.getPropellerThrustScaling() );


                propulsorElementIO.setAttribute("outputPortNames", "Angle");
                propulsorElementIO.setAttribute("inputPortNames", "F,Fbody,Qbody,Rot,Pos,ConsumedPower");
                propulsorElementIO.setAttribute("Initial_Angle", "0");
                propulsorElementIO.setAttribute("Size_F","3");
                propulsorElementIO.setAttribute("Size_ConsumedPower","1");
                propulsorElementIO.setAttribute("Size_Fbody","3");
                propulsorElementIO.setAttribute("Size_Qbody","3");
                propulsorElementIO.setAttribute("Size_Rot","3");
                propulsorElementIO.setAttribute("Size_Pos","3");
                
                connection.setAttribute(PropulsorName+".RudderAngleBODY", ShipConnectionName+".Angle");
                connection.setAttribute(PropulsorIOName+".F", PropulsorName+".ForceNED");

                connection.setAttribute(PropulsorIOName+".Fbody",ShipConnectionName+".ForceBody");
                connection.setAttribute(PropulsorIOName+".Qbody",ShipConnectionName+".TorqueBody");
                
                connection.setAttribute(PropulsorIOName+".Pos",ShipConnectionName+".OffsetBody");
                connection.setAttribute(PropulsorIOName+".Rot",ShipConnectionName+".RotationBody");

                connection.setAttribute(PropulsorName+".QuaternionNED",ShipConnectionName+".ParentQuaternionNED");

                connection.setAttribute(ShipConnectionName+".ShaftTorque",  "0");
                // half hack, assume name on ship connection IO
                connection.setAttribute(PropulsorIOName+".ConsumedPower","0");
                break;
            case VSP:
                propulsorElement.setAttribute("SimObject", "Ship/CycloidalPropeller");
                propulsorElement.setAttribute("FoilLength", Double.toString(data.getHeight()));
                propulsorElement.setAttribute("Diameter", Double.toString(data.getDiameter()*data.getPropellerDiameterScaling() ));
                
                //propulsorElement.setAttribute("Ct0", "0.74");
                //propulsorElement.setAttribute("Cq0", "0.57");
                propulsorElement.setAttribute("ShaftPowerScaling", Double.toString(data.getShaftPowerScaling()) );
                propulsorElement.setAttribute("PropellerThrustScaling", Double.toString(data.getPropellerThrustScaling()) );
                
                connection.setAttribute(PropulsorName+".QuaternionNED", ShipConnectionName+".QuaternionNED");
                connection.setAttribute(PropulsorName+".RPM", ShipConnectionName+".RPM");
                connection.setAttribute(PropulsorName+".ThrustVectorBody", ShipConnectionName+".ThrustVectorBody");
                connection.setAttribute(PropulsorName+".ThrustIntensityBody", ShipConnectionName+".Pitch");

                connection.setAttribute(ShipConnectionName+".ShaftTorque", PropulsorName + ".ShaftTorqueCorrectedBody");
                
                propulsorElementIO.setAttribute("inputPortNames", "");
                propulsorElementIO.setAttribute("outputPortNames", "");

                propulsorElementIO.setAttribute("inputPortNames", "T,Tnom,Q,Qnom,Fbody,Qbody,Rot,Pos,ConsumedPower");
                propulsorElementIO.setAttribute("Size_ConsumedPower","1");
                propulsorElementIO.setAttribute("Size_T","3");
                propulsorElementIO.setAttribute("Size_Tnom","3");
                propulsorElementIO.setAttribute("Size_Q","3");
            	propulsorElementIO.setAttribute("Size_Qnom","3");        
                propulsorElementIO.setAttribute("Size_Fbody","3");
                propulsorElementIO.setAttribute("Size_Qbody","3");
                propulsorElementIO.setAttribute("Size_Rot","3");
                propulsorElementIO.setAttribute("Size_Pos","3");                
 
                connection.setAttribute(PropulsorIOName+".T", PropulsorName+".ForceNED");
                connection.setAttribute(PropulsorIOName+".Tnom", PropulsorName+".ForceNED");
                connection.setAttribute(PropulsorIOName+".Q", PropulsorName+".TorqueNED");
                connection.setAttribute(PropulsorIOName+".Qnom", PropulsorName+".TorqueNED");

                connection.setAttribute(PropulsorIOName+".Fbody",ShipConnectionName+".ForceBody");
                connection.setAttribute(PropulsorIOName+".Qbody",ShipConnectionName+".TorqueBody");
                
                connection.setAttribute(PropulsorIOName+".Pos",ShipConnectionName+".OffsetBody");
                connection.setAttribute(PropulsorIOName+".Rot",ShipConnectionName+".RotationBody");
                 
                // half hack, assume name on ship connection IO
                connection.setAttribute(PropulsorIOName+".ConsumedPower",PropulsorName+".ShaftPowerCorrected");
                break;
        }

        FhDynamicModel.instance().getInterconnectionElement().getParentNode().appendChild(connection);
        
        FhDynamicModel.instance().getObjectElement().appendChild(propulsorElement);
        FhDynamicModel.instance().getObjectElement().appendChild(propulsorElementIO);

        FhDynamicModel.instance().registerObject(this,data);
    }



    @Override
    public void SetInlineThrusterLoss(boolean inline_loss) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void SetTransvareThrusterLoss(boolean transverse_loss) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void SetCoandaTrhusterLoss(boolean conada_loss) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void SetVentilationThrusterLoss(boolean ventilation_loss) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void SetThrusterThrusterInteractionLoss(boolean thr_thr_loss) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void SetRudderInflowLoss(boolean inflow_loss) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double GetForceHeave() {
        return forceBody[2];
    }

    @Override
    public double GetForceSurge() {
        return forceBody[0];
    }

    @Override
    public double GetForceSway() {
        return forceBody[1];
    }

    @Override
    public double GetMomentRoll() {
        return torqueBody[0];
    }

    @Override
    public double GetMomentPitch() {
        return torqueBody[1];
    }

    @Override
    public double GetMomentYaw() {
        return torqueBody[2];
    }

    @Override
    public double GetNominalThrust() {
        return nominalThrust;
    }

    @Override
    public double GetActualTrhust() {
        return correctedThrust;
    }



    @Override
    public void preTick() {
    }

    @Override
    public void tick(double Lookahead) {
        FhDynamicModel.instance().objectUpdated(this, Lookahead);
    }

    @Override
    public void postTick() {
        
            if(!indexInitialized){
                indexInitialized = true;
                indexIOObj = FhDynamicModel.instance().getObjectIndex(PropulsorIOName);
                if( type != SFHOpusPropulsorData.PropulsorType.Rudder ){
                    indexTmpT = FhDynamicModel.instance().getObjectPortIndex(PropulsorIOName, "Tnom");
                    indexTmpQ = FhDynamicModel.instance().getObjectPortIndex(PropulsorIOName, "Qnom");
                    indexTmpTc = FhDynamicModel.instance().getObjectPortIndex(PropulsorIOName, "T");
                    indexTmpQc = FhDynamicModel.instance().getObjectPortIndex(PropulsorIOName, "Q");
                }
                indexForceBody = FhDynamicModel.instance().getObjectPortIndex(PropulsorIOName, "Fbody");
                indexTorqueBody = FhDynamicModel.instance().getObjectPortIndex(PropulsorIOName, "Qbody");
                indexConsumedPower = FhDynamicModel.instance().getObjectPortIndex(PropulsorIOName, "ConsumedPower");    
            }

        
        if( type != SFHOpusPropulsorData.PropulsorType.Rudder ){
             // retreive output 
            double[] tmpT = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexTmpT, 3);
            double[] tmpQ = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexTmpQ, 3);
            nominalThrust = Math.sqrt(Math.pow(tmpT[0],2)+Math.pow(tmpT[1],2)+Math.pow(tmpT[2],2))/thrustDeduction;
            //nominalTorque = Math.sqrt(Math.pow(tmpQ[0],2)+Math.pow(tmpQ[1],2)+Math.pow(tmpQ[2],2));

            double[] tmpTc = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexTmpTc, 3);
            double[] tmpQc = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexTmpQc, 3);
            correctedThrust = Math.sqrt(Math.pow(tmpTc[0],2)+Math.pow(tmpTc[1],2)+Math.pow(tmpTc[2],2));
            //correctedTorque = Math.sqrt(Math.pow(tmpQc[0],2)+Math.pow(tmpQc[1],2)+Math.pow(tmpQc[2],2));
        }
        forceBody = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexForceBody, 3);
        torqueBody = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexTorqueBody, 3);
        
//        feedbackPositionBody = FhDynamicModel.instance().getFhSimPort(PropulsorIOName, "Pos", 3);
//        feedbackRotationBody = FhDynamicModel.instance().getFhSimPort(PropulsorIOName, "Rot", 3);        

        consumedPower = FhDynamicModel.instance().getFhSimPortFromIndex(indexIOObj, indexConsumedPower, 1);
       
     }

    private String GenerateArrayString(double[] val) {
        String retval="";
        for(int i=0; i < val.length-1; i++){
            retval += Double.toString(val[i])+",";
        }
        retval += Double.toString(val[val.length-1]);
        return retval;
    }
    @Override
    public double getConsumedPower() {
        return consumedPower[0];
    }
}
