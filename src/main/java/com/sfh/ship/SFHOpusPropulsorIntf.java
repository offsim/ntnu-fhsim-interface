/*
 * Copyright SINTEF FIsheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship;

/**
 * Interface to an propulsor (force calculator) during simulation
 * @see SFHOpusPropulsorData
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
public interface SFHOpusPropulsorIntf extends ActorIntf{
    

    /**
     * Enable/disable in-line thruster loss
     * @param inline_loss Toggle in-line thruster loss
     */
    void SetInlineThrusterLoss( boolean inline_loss );
    
    /**
     * Enable/disable transverse thruster loss
     * @param transverse_loss Toggle transverse thruster loss
     */
    void SetTransvareThrusterLoss( boolean transverse_loss );
    
    /**
     * Enable/disable Coanda effect losses
     * @param conada_loss Toggle loss from Coanda effect
     */
    void SetCoandaTrhusterLoss( boolean conada_loss );
    
    /**
     * Enable/disable ventilation loss
     * @param ventilation_loss Toggle ventilation loss
     */
    void SetVentilationThrusterLoss( boolean ventilation_loss );
    
    /**
     * Enable/disable thruster vs thruster interaction loss
     * @param thr_thr_loss Toggle thruster interaction loss
     */
    void SetThrusterThrusterInteractionLoss( boolean thr_thr_loss );
    
    /**
     * Enable/disable rudder inflow loss
     * @param inflow_loss Toggle rudder inflow loss
     */
    void SetRudderInflowLoss(boolean inflow_loss );
    
    /**
     * The heave (vertical) force in body fixed force frame (positive downwards)
     * @return The heave force in N
     */
    double GetForceHeave();
    
    /**
     * The surge (longitudinal) force in the body fixed frame (positive forwards)
     * @return The surge force in N
     */
    double GetForceSurge();

    /**
     * The sway (transverse) force in the body fixed frame (positive right)
     * @return The sway force in N
     */
    double GetForceSway();

    /**
     * The roll moment in the body fixed frame along the longitudinal axis
     * @return The roll moment in Nm
     */
    double GetMomentRoll();
    /**   
     * The pitch moment in the body fixed frame along the transverse axis
     * @return The pitch moment in Nm
     */
    double GetMomentPitch();

    /**
     * The yaw moment in the body fixed frame along the vertical axis
     * @return The yaw moment in Nm
     */
    double GetMomentYaw();
    
    /**
     * Return the norm of the propulsion force without loss corrections 
     * @return The norm of the nominal thrust force in N
     */
    double GetNominalThrust();
    
    /**
     * Returns the norm of the propulsion force with thrust loss correction
     * @return The norm of the corrected thrust force in N
     */
    double GetActualTrhust();
    
    /**
     * Consumed power in Watt
     * @return the power consumed to drive the propulsor unit
     */
    double getConsumedPower();

}
