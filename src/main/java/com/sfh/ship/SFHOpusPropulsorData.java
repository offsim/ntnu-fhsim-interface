/*
 * Copyright SINTEF Fisheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.ship; 

import static com.sfh.ship.SFHOpusPropulsorData.PropulsorType.Rudder;
import static com.sfh.ship.SFHOpusPropulsorData.PropulsorType.VSP;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * This class contains the configuration of a propulsor which is a simple 
 * functional relationship between RPM, pitch angle and azimuth angle to 
 * thrust force and absorbed torque.<br>
 * Source of static data for a propulsor. This class contains the configuration 
 * which sets the propusor type to either of
 * <dl>
 * <dt>Open</dt><dd>An open propeller</dd>
 * <dt>Ducted</dt><dd>A propeller inside a duct</dd>
 * <dt>Tunnel</dt><dd>A Propeller inside a tunnel</dd>
 * <dt>Rudder</dt><dd>A rudder</dd>
 * <dt>VSP</dt><dd>A Voith-Schnieider type cycloidal propeller</dd>
 * </dl>
 * <p>
 * The SFHOpusPropulsorData must be connected to a SFHReferencePointData or 
 * SFHActuatorPointData class to be included in a simulation. 
 * <p>
 * The class also lets you scale the torque and force coefficients of the 
 * underlying parameter sets. 
 * @see SFHOpusPropulsorIntf
 * @author Karl Gunnar Aarsæther, karl.gunnar.aarsather@sintef.no
 */
@XStreamAlias("OpusPropulsor")
public class SFHOpusPropulsorData {

    private String filename;
    
    private void setFilename(String fname)
    {
        filename = fname;
    }    
    
    public String getFileName()
    {
        return filename;
    }   

    public static void dumpToXml(String fname, SFHOpusPropulsorData obj){
        XStream xstream = new XStream();
        xstream.processAnnotations(SFHOpusPropulsorData.class);
        PrintWriter out = null;
        try {
             out = new PrintWriter(fname);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SFHHullData.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        out.println(xstream.toXML(obj));
        out.close();
    }
    
    public static SFHOpusPropulsorData loadFromXml(String fname){
        XStream xstream = new XStream();
        xstream.processAnnotations(SFHOpusPropulsorData.class);
        SFHOpusPropulsorData out = (SFHOpusPropulsorData) xstream.fromXML(new File(fname));
        out.setFilename(fname);
        return out;
    }    

    public static SFHOpusPropulsorData loadFromXml(String objectName, String fname){
        SFHOpusPropulsorData out = loadFromXml(fname);
        out.setName(objectName);
        out.setFilename(fname);
        return out;
    }    

    public static SFHOpusPropulsorData findByNameInObjectList(String Name, ArrayList<Object> list){
        for( Object o: list){
            if( o instanceof SFHOpusPropulsorData)
                if( ((SFHOpusPropulsorData)o).getName().equals(Name)){
                    return (SFHOpusPropulsorData)o;
                }            
        }
        return null;
    }
       
    
    
    /**
     * Scale of the diameter of the propulsor
     * @return the diameter scale
     */
    public double getPropellerDiameterScaling() {
        return propellerDiameterScaling;
    }

    /**
     * Scale of the diameter of the propulsor
     * @param propellerDiameterScaling the updated propeller diameter scale
     */
    public void setPropellerDiameterScaling(double propellerDiameterScaling) {
        this.propellerDiameterScaling = propellerDiameterScaling;
    }

    /**
     * Thrust deduction factor (1-t)
     * @return the thrustDeduction
     */
    public double getThrustDeduction() {
        return thrustDeduction;
    }

    /**
     * Thrust deduction factor (1-t)
     * @param thrustDeduction the thrustDeduction to set
     */
    public void setThrustDeduction(double thrustDeduction) {
        this.thrustDeduction = thrustDeduction;
    }

    /**
     * Wake fraction factor (1-w)
     * @return the wakeFraction
     */
    public double getWakeFraction() {
        return wakeFraction;
    }

    /**
     * Wake fraction factor (1-w)
     * @param wakeFraction the wakeFraction to set
     */
    public void setWakeFraction(double wakeFraction) {
        this.wakeFraction = wakeFraction;
    }

    /**
     * @return the propulsorStrut
     */
    public boolean isPropulsorStrut() {
        return propulsorStrut;
    }

    /**
     * @param propulsorStrut the propulsorStrut to set
     */
    public void setPropulsorStrut(boolean propulsorStrut) {
        this.propulsorStrut = propulsorStrut;
    }
    
    
    /**
     * Available propulsor types 
     */
    public enum PropulsorType {
        /**Open propeller*/Open,   
        /** Ducted propeller*/Ducted, 
        /** Tunnel propeller*/Tunnel, 
        /** Rudder */Rudder, 
        /** Simplified Cycloidal propeller*/VSP     
    };

    /**
     * Return the scaling factor applied to the shaft power of the propuslor 
     * @return scale factor (dimensionless)
     */
    public double getShaftPowerScaling() {
        return shaftPowerScaling;
    }

    /**
     * Update the scaling factor applied to the shaft power of the propuslor 
     * @param shaftPowerScaling scale factor (dimensionless)
     */
    public void setShaftPowerScaling(double shaftPowerScaling) {
        this.shaftPowerScaling = shaftPowerScaling;
    }

    /**
     * Return the scaling factor applied to the thrust of the propuslor 
     * @return scale factor (dimensionless)
     */
    public double getPropellerThrustScaling() {
        return propellerThrustScaling;
    }

    /**
     * Update the scaling factor applied to the thrust of the propuslor 
     * @param propellerThrustScaling scale factor (dimensionless)
     */
    public void setPropellerThrustScaling(double propellerThrustScaling) {
        this.propellerThrustScaling = propellerThrustScaling;
    }

    /**
     * Return a short description of the parameter set in use. Used for report generation
     * @return short description string 
     */
    public String getParameterString() {
        return parameterString;
    }


    /**
     * Test if propulsor is connected to an actuator point or a simple reference point
     * @return true of propulsor connected to an actuator point
     */
    public boolean connectionIsActuatorPoint(){
        if( refpoint == null ) return false;
        return refpoint instanceof SFHActuatorPointData;
    }

    /**
     * Test if propulsor is connected to an actuator point used as an azimuth motor
     * @return true of propulsor connected to azimuth motor
     */
    public boolean connectionIsAzimuthMotor(){
        if( refpoint == null ) return false;
        return refpoint.getAngularMotor();
    }
    
    public boolean connectionIsOffCenter(){
        if( refpoint == null ) return false;
        return Math.abs(refpoint.getLinearOffset()[1])>0;
    }
    
    /**
     * Connect this propulsor to a SFHReferencePoint. Deprecated
     * @param data data class for the reference point 
     */
    public void ConnectToReference(SFHReferencePointData data){
        refpoint = data;
        connectionName = data.getName();
        
        data.setIsForceCalculator(true, SFHReferencePointIntf.ForceSource.FhSimPort);
        data.setForceObject(name);
        if( this.getType() == Rudder)
            data.setForcePort("ForceNED");
        else if(this.getType() == VSP)
            throw new RuntimeException("Cannot connect a VSP to a SFHReferencePoint*" );
        else
            data.setForcePort("ForceCorrectedNED");

        
    }
            
    /**
     * Connect this propulsor to an ActuatorPoint
     * Links the propulsor with an actuator point. The actuator point will
     * provide position, orientation, velocity, azimuth angle, pitch angle and RPM input, while
     * the propulsor provides a force which is applied to the actuator point
     * @param data the data class for the actuator point
     */
    public void ConnectToReference(SFHActuatorPointData data){
        refpoint = data;
        connectionName = data.getName();
                
        data.setIsForceCalculator(true, SFHReferencePointIntf.ForceSource.FhSimPort);
        data.setForceObject(name);
        if( this.getType() == Rudder){
            data.setForcePort("ForceNED");
            data.setVSPconnection(false);
        }else if( this.getType() == VSP){
            data.setForcePort("ForceNED");
            data.setVSPconnection(true);
        }else{
            if( isPropulsorStrut() ){
                data.setForcePort("ForceCorrected2NED");   
            }else{
                data.setForcePort("ForceCorrectedNED");   
            }
            data.setVSPconnection(false);
        }
    }

    /**
     * The reference point the propulsor is attached to
    */
    @XStreamOmitField
    private SFHReferencePointData refpoint=null;
    
    /**
     * Propulsor type
     */
    private PropulsorType type;
    
    /**
     * Name of the propulsor
     */
    @XStreamOmitField
    private String name;
    
    /**
     *  Marketing/Technical name of the propulsor
     */
    private String technicalName;
    
    /**
     * Name of the reference point which the propulsor is connected to
     */
    @XStreamOmitField
    private String connectionName;
    
    /**
     * Diameter of the propulsor, applicable only to propellers
     */
    private double diameter;
    
    /**
     * Chord length (breadth) of the rudder. Only applicable to rudders and strut propulsors
     */
    private double chord;
    
    /**
     * Chord (span) of the rudder. Only applicable to rudders and strut propulsors
     */
    private double height;

    private boolean propulsorStrut=false;
    
    /**
     * angles for rudder data (x-axis of data)
     */
    private double [] rudderAngle = null;
    
    /**
     * Lift coefficients for rudder (y-axis)
     */
    private double [] rudderCL = null;

    /**
     * Drag coefficients for rudder (y-axis)
     */
    private double [] rudderCD = null;
    
    /**
     * Pitch diameter ratio for the propeller data (x-axis)
     */
    private double [] propellerPD = null;
    
    /**
     * Thrust coefficient as a function of PD value (x-axis) and hydrodynamic 
     * angle (beta). Data is a set of fourier coefficients, each PD row is of length
     * 2N+1 with th DC component followd by N cosine terms and N sine terms
     */
    private double [][] propellerCT = null;

    /**
     * Torque coefficient as a function of PD value (x-axis) and hydrodynamic 
     * angle (beta). Data is a set of fourier coefficients, each PD row is of length
     * 2N+1 with th DC component followd by N cosine terms and N sine terms
     */
    private double [][] propellerCQ = null;

    /**
     * Scale of the torque of the propulsor
     */
    private double shaftPowerScaling=1.0;
    
    /**
     * Scale of the thrust of the propulsor
     */
    private double propellerThrustScaling=1.0;
    
    /**
     * Scale of the diameter of the propulsor 
     */
    private double propellerDiameterScaling=1.0;

    /**
     * Thrust deduction factor (1-t)
     */
    private double thrustDeduction=0.8;

    /**
     * Wake fraction factor (1-w)
     */
    private double wakeFraction =0.9;
    
    
    
    /**
     * Short identifier of parameter set
    */
    @XStreamAlias("description")
    @XStreamAsAttribute
    private String parameterString = "";
    
    /**
     * Return the propulsor name
     * @return The propulsor name
     */
    public String getName(){
        return name;
    }
    
    /**
     * Set the propulsor name
     * @param newName The new propulsor name
     */
    public void setName(String newName){
        name = newName;
    }

    /**
     * Return the diameter of the propulsor
     * @return The diameter in meters
     */
    public double getDiameter() {
        return diameter;
    }

    /**
     * Set the propulsor diameter
     * @param diameter The diameter in meters
     */
    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    /**
     * Return the the rudder chord length in meter
     * @return The chord length in meters
     */
    public double getChord() {
        return chord;
    }

    /**
     * Set the rudder chord length in meter
     * @param chord The chord length in meters
     */
    public void setChord(double chord) {
        this.chord = chord;
    }

    /**
     * Get the rudder height in meters
     * @return The rudder height in meters
     */
    public double getHeight() {
        return height;
    }
    
    /**
     * Set the rudder height in meters
     * @param height The rudder height in meters 
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * Get the name of the reference point which the propulsor is connected to
     * @return The name/identifier
     */
    public String getConnectionName() {
        return connectionName;
    }

    /**
     * Set the name of the reference point which the propulsor is connected to
     * @param connectionName The name/identifier 
     */
    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }
    
    public String getTechnicalName(){
        return technicalName;
    }
            
            
    
    /**
     * Construct a new named propulsor of a specified type
     * @param _name The name of the propulsor
     * @param _type The type of propulsor
     */
    public SFHOpusPropulsorData( String _name, PropulsorType _type ){
        this.refpoint = null;
        name = _name;
        type = _type;
        propulsorStrut=false;
    }

    /**
     * Estimate propeller dataset thrust for a given pitch angle, rotational speed and forward speed
     * @param alfa the pitch angle in degrees
     * @param rpm the rotational velocity in RPM
     * @param u the forward speed in m/s
     * @return estimate of the produced thrust at that condition
     */
    public double getThrustForPitch(double alfa, double rpm, double u){
//probelm when the thrust is null
        double n = rpm / 60;

        if( type == Rudder){
            throw new RuntimeException("I am a rudder exception!? - getThrustForPitch called on a rudder");
        }
        
        double this_diameter = diameter * propellerDiameterScaling;
        
        if( type == VSP ){
            double _ct0 = 1.2*Math.pow(Math.sin( alfa*Math.PI/2.0 ),2.0);
            return 1025*Math.pow(n,2.0)*Math.pow(this_diameter,2.0)*this_diameter*height*_ct0;
        }
        alfa = alfa*Math.PI/180.0;        
            
        // find pd value corresponding to pitch angle
        double _pd = 0.7*Math.PI*Math.tan(alfa);
        
        // beta angle 
        double _b = Math.atan2(u*wakeFraction, 0.7*Math.PI*n*this_diameter);
        int i;
        for( i=0; i < propellerPD.length-1; i++)
            if( propellerPD[i] > _pd)
                break;
        double _ct;
        
        if( i == 0 ){
            _ct = eval(propellerCT[0],_b);
        }else if( i == (propellerPD.length-1)){
            _ct = eval(propellerCT[propellerPD.length-1],_b);
        }else{
            double _ct0 = eval(propellerCT[i-1],_b);
            double _ct1 = eval(propellerCT[i],_b);
            _ct = _ct0+(_ct1-_ct0)/(propellerPD[i]-propellerPD[i-1])*(_pd-propellerPD[i-1]);
        }
        
        return thrustDeduction*propellerThrustScaling*0.5*1025*( Math.pow(u*wakeFraction,2.0) + Math.pow(0.7*Math.PI*n*this_diameter,2.0))*Math.PI/4*this_diameter*this_diameter*_ct;
    }
    
    /**
     * Estimate thrust at 1 RPS and zero forward speed
     * @param alfa the pitch angle in degrees
     * @return estimate of the thrust
     */
    public double getThrustForPitch(double alfa){
        return getThrustForPitch(alfa, 60,0);
    }

    /**
     * Estimate the thrust at zero forward speed
     * @param alfa the pitch angle in degrees
     * @param rpm the rotational speed in RPM 
     * @return  estimate of the thrust
     */
    public double getThrustForPitch(double alfa, double rpm){
        return getThrustForPitch(alfa, rpm,0);
    }

    /**
     * Get propulsor shaft torque for a given pitch angle, 60 RPM and zero speed
     * @param alfa pitch angle in degrees
     * @return Shaft torque in NM
     */
    public double getTorqueForPitch(double alfa){
        return getTorqueForPitch(alfa, 60,0);
    }

    /**
     * Get propulsor shaft torque for a given pitch angle, shaft RPM and zero speed
     * @param alfa pitch angle in degrees
     * @param rpm shaft RPM 
     * @return Shaft torque in NM
     */
    public double getTorqueForPitch(double alfa, double rpm){
        return getTorqueForPitch(alfa, rpm,0);
    }
    
    
    /**
     * Get propulsor shaft torque for a given pitch angle, shaft RPM and advance speed
     * @param alfa pitch angle in degrees
     * @param rpm shaft RPM 
     * @param u forward speed in m/s
     * @return Shaft torque in NM
     */
    public double getTorqueForPitch(double alfa, double rpm, double u){

        double n = rpm / 60;

        if( type == Rudder){
            throw new RuntimeException("I am a rudder exception!? - getThrustForPitch called on a rudder");
        }
        double this_diameter = diameter * propellerDiameterScaling;
        
        if( type == VSP ){
            double _cq0 = 0.5*(0.2+Math.pow(Math.sin( alfa*Math.PI/2.0 ),2.0)*0.8);
            return 1025*Math.pow(n,2.0)*Math.pow(this_diameter,2.0)*this_diameter*height*_cq0;
        }
        
        alfa = alfa*Math.PI/180.0;
            
        // find pd value corresponding to pitch angle
        double _pd = 0.7*Math.PI*Math.tan(alfa);
        
        // beta angle 
        double _b = Math.atan2(u*wakeFraction, 0.7*Math.PI*n*this_diameter);
        int i;
        for( i=0; i < propellerPD.length-1; i++)
            if( propellerPD[i] > _pd)
                break;
        double _cq;
        
        if( i == 0 ){
            _cq = eval(propellerCQ[0],_b);
        }else if( i == (propellerPD.length-1)){
            _cq = eval(propellerCQ[propellerPD.length-1],_b);
        }else{
            double _cq0 = eval(propellerCQ[i-1],_b);
            double _cq1 = eval(propellerCQ[i],_b);
            _cq = _cq0+(_cq1-_cq0)/(propellerPD[i]-propellerPD[i-1])*(_pd-propellerPD[i-1]);
        }
        return shaftPowerScaling*Math.abs(0.5*1025*( Math.pow(u*wakeFraction,2.0) + Math.pow(0.7*Math.PI*n*this_diameter,2.0))*Math.PI/4*this_diameter*this_diameter*this_diameter*_cq);
    }
    
    /**
     * Get rudder lift for a specific angle
     * @param alfa the rudder angle in degrees
     * @return the lift coefficient or the rudder
     */
    public double getLiftForAngle(double alfa){
        if( type != Rudder){
            throw new RuntimeException("I am not a rudder exception!? - getLiftForAngle not called on a rudder");
        }
        alfa = alfa*Math.PI/180.0;
        int i;
        for( i=0; i < rudderAngle.length-1; i++)
            if( rudderAngle[i] > alfa)
                break;
        double _cl;
        
        if( i == 0 ){
            _cl = rudderCL[0];
        }else if( i == (rudderAngle.length-1)){
            _cl = rudderCL[rudderAngle.length-1];
        }else{
            double _cl0 = rudderCL[i-1];
            double _cl1 = rudderCL[i];
            _cl = _cl0+(_cl1-_cl0)/(rudderAngle[i]-rudderAngle[i-1])*(alfa-rudderAngle[i-1]);
        }
        return _cl;
    }

    /**
     * Get rudder drag for a specific angle
     * @param alfa the rudder angle in degrees
     * @return the drag coefficient or the rudder
     */
    public double getDragForAngle(double alfa){
        if( type != Rudder){
            throw new RuntimeException("I am not a rudder exception!? - getLiftForAngle not called on a rudder");
        }
        alfa = alfa*Math.PI/180.0;
        int i;
        for( i=0; i < rudderAngle.length-1; i++)
            if( rudderAngle[i] > alfa)
                break;
        double _cd;
        
        if( i == 0 ){
            _cd = rudderCD[0];
        }else if( i == (rudderAngle.length-1)){
            _cd = rudderCD[rudderAngle.length-1];
        }else{
            double _cd0 = rudderCD[i-1];
            double _cd1 = rudderCD[i];
            _cd = _cd0+(_cd1-_cd0)/(rudderAngle[i]-rudderAngle[i-1])*(alfa-rudderAngle[i-1]);
        }
        return _cd;
    }
   
    /**
     * Return the PD vector of the data
     * @return vector of PD values
     */
    public double [] GetPropellerPD(){
        return propellerPD;
    }
    
    /**
     * Get the propeller Ct data
     * @return vector of Ct values 
     */
    public double [][] GetPropellerCT(){
        return propellerCT;
    }
 
    /**
     * Get the propeller Cq data
     * @return vector of Cq values
     */
    public double [][] GetPropellerCQ(){
        return propellerCQ;
    }

    /**
     * Get the rudder angle for the data
     * @return vector of rudder angle sin radians
     */
    public double [] GetRudderAngle(){
        return rudderAngle;
    }

    /**
     * Get the rudder lift coefficients
     * @return the Cl coefficient 
     */
    public double [] GetRudderCL(){
        return rudderCL;
    }
    
    /**
     * Get the rudder drag coefficient 
     * @return The Cd coefficient
     */
    public double [] GetRudderCD(){
        return rudderCD;
    }

    /**
     * Return the type of propusor either of Rudder/Open propeller/Ducted propeller/Tunnel propeller/VSP
     * @return The propulsor type
     */
    public PropulsorType getType(){
        return type;
    }
    
    /**
     * Update propulsor type
     * @param newType  The new propulsor type
     */
    public void setType(PropulsorType newType){
        type = newType;
    }

    /**
     * Returns a new {@code SFHOpusPropulsorIntf} object configured by this data
     * object.
     *
     * @return a new {@code SFHOpusPropulsorIntf} object configured by this data
     * object.
     */
    public SFHOpusPropulsorIntf create() {
        return new SFHOpusPropulsorImpl(this);
    }

    private double eval(double[] d, double _b) {
        double retval = d[0];
        for( int j=1; j < ((d.length - 1) / 2 + 1); j++)
                retval += d[j]*Math.cos( j * _b);
        for( int j=((d.length - 1) / 2 + 1); j < d.length; j++)
                retval +=  d[j]*Math.sin( (j-(d.length - 1) / 2) * _b);
        return retval;        
    }

    
    /**
     * Make an effort to find the RPM and pitch combination which gives a specific thrust
     * The RPM is increased from minimum RPM and the propulsor is checked if it can deliver 
     * a thrust matching the required thrust for any pitch angles. The power required 
     * to deliver the thrust is also checked
     * @param thrust target thrust in Newtons
     * @param w maximum power in Watts
     * @return array of [RPM command, pitch command], null if no solution was found
     */
    public double [] rpmPitchForThrust(double thrust, double w){
        double [] retval;
        
        SFHActuatorPointData act = (SFHActuatorPointData)refpoint;
        double rpm=act.getMinRPM();
        double pitch=0;
        while( (rpm <= act.getMaxRPM())  && (w>2*Math.PI*rpm/60.0*getTorqueForPitch(pitch, rpm)) ){
            pitch = bisectPitch(rpm, thrust);
            double ratio = getThrustForPitch(pitch, rpm)/thrust;
            if( ratio > 0.99 ){
                retval = new double[2];
                retval[0] = Math.round(rpm*100.0)/100.0;
                retval[1] = Math.round(pitch*100.0)/100.0;
                return retval;                    
            }
            rpm += 1;        
        }
        return null;
    } 

    public double [] rpmPitchForTorque(double torque, double w){
        double [] retval;
        
        SFHActuatorPointData act = (SFHActuatorPointData)refpoint;
        double rpm=act.getMinRPM();
        double pitch=0;
        while( (rpm <= act.getMaxRPM())  && (w>2*Math.PI*rpm/60.0*getTorqueForPitch(pitch, rpm)) ){
            pitch = bisectPitch(rpm, torque,true);
            double ratio = getTorqueForPitch(pitch, rpm)/torque;
            if( ratio > 0.99 ){
                retval = new double[2];
                retval[0] = Math.round(rpm*100.0)/100.0;
                retval[1] = Math.round(pitch*100.0)/100.0;
                return retval;                    
            }
            rpm += 1;        
        }
        return null;
    } 

    
    private double bisectPitch(double rpm, double target) {
        return bisectPitch( rpm,  target, false);
    }
    private double bisectPitch(double rpm, double target, boolean torque) 
    {
   	double epsilon = 0.00001;
   	double a, b, m, y_m, y_a;
    
        SFHActuatorPointData act = (SFHActuatorPointData)refpoint;
        
   	a = act.getMinPitch();  b = act.getMaxPitch();
    
        int iterations = 0;
        
        if( a==b){
            iterations = 100;
            epsilon = -1;
        }
        
   	while ( (b-a) > epsilon )
   	{
   	    m = (a+b)/2;           // Mid point
    
            if( torque ){
                y_m = getTorqueForPitch(m, rpm, 0)-target;
                y_a = getTorqueForPitch(a, rpm, 0)-target;
            }else{
                y_m = getThrustForPitch(m, rpm, 0)-target;
                y_a = getThrustForPitch(a, rpm, 0)-target;
            }
    
   	    if ( (y_m > 0 && y_a < 0) || (y_m < 0 && y_a > 0) )
   	    {  // f(a) and f(m) have different signs: move b
   	       b = m;
   	    }
   	    else
   	    {  // f(a) and f(m) have same signs: move a
   	       a = m;
   	    }
            iterations++;
            if(iterations>100)
                break;
   	}
   	return  (a+b)/2;
      }
}
