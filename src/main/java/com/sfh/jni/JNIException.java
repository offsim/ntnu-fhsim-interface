package com.sfh.jni;
public class JNIException extends Exception {
    
    public JNIException (String message) {
        super (message);
    }
    
    /**
     * Called by native code during construction to set the location. 
     */
    public void __jni_setLocation (String functionName, String file, int line) {
        JNIExceptions.addStackTraceElement (this, functionName, file, line);
    }
}
