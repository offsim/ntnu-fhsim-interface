package com.sfh.jni;
import java.util.Map;
import java.util.HashMap;

public class FhSimException extends JNIException {
    
    private static final Map<Integer,String> errorCodes = new HashMap<> ();
    private static void addErrorCode (int code, String def, String msg) {
        errorCodes.put (code, def + ": " + msg);
    }
    static {
        addErrorCode (0, "OK", "Everything is OK.");
        addErrorCode (1, "ERROR", "SFH INTERNAL ERROR");
        addErrorCode (2, "ERROR", "RUNTIME ERROR");
        addErrorCode (3, "ERROR", "GENERIC ERROR");
        addErrorCode (4, "ERROR", "INPUT ERROR");
        addErrorCode (-1, "ERROR", "FhSim Load error.");
        addErrorCode (-2, "ERROR", "FhSim UnLoad error.");
        /* ... */
    }
    
    public FhSimException (int code, String message) {
        super (message + " (" + code + ": " + errorCodes.get (code) + ")");
    }
}