package com.sfh.jni;

import java.lang.reflect.Field;

/**
 * This is a static class which contains the Java interface to SINTEF Fisheries and Aquacultures "FhSim" time-domain simulation tool
 *
 * The Java interface to FhSim gives access to both the base simulation tool, FhSim, and the extended Simulation and visualization tool, FhVis.
 * 
 * @author Karl Gunnar Aarsæther, SINTEF Fisheries and Aquaculture
 * @version     %I%, %G%
 * @since       1.0
 */
public class FhSim {

        final static Object   Lock;
	static long	WindowHandle;
	static String	ResourcePath;
	static String	JNIpath; 
	static boolean	Visualization; 
	static boolean	JNIInitialized;
	static boolean	SimDllLoaded;
	static boolean	SimInitialized;
	static boolean	SimRunning;
	static double	TimeoutMultiplier;
	static int 	LogLevelFile;
	static int 	LogLevelDisplay;
	static String [] StateNames;
	static String [] ObjectNames;
	
	/**
	*	Returns the path to a runtime location of native libraries in addition to the system path
	*	@return the path to the JNI and FhSim binary directory
	*	@see #setJNIPath(String)
	*	@see #initJNI()
	*/
	public static String getJNIPath() {
		return JNIpath;
	}

	/**
	* Sets path to a runtime location of native libraries in addition to the system path
	*
	* The path is used prior to loading of the native JNI library to update the JVM library 
	* library path. This allows the application to dynamically set the location of the FhSim
	* JNI without adding it to the system path. 
	* @param Path The path to where the JNI and FhSim binaries are located
	* @see #initJNI()
	*/
	public static void setJNIPath(String Path) {
		JNIpath = Path;
	}
	
	
	/**
	* Returns the path to the Ogre3D resources directory used for visualization
	* @return the path to the configuration files and the resource files for Ogre3D
	* @see #setResourcePath(String)
	*/
	public static String getResourcePath() {
		return ResourcePath;
	}

	/**
	* Sets the path to the Ogre3D configuration and resource files
	* 
	* This path should point to the directory containing "ogre.cfg", "plugins.cfg" and "resources.cfg" configuration files for Ogre3D
	* The path must be set prior to initialization of the model as it is used during initialization of the model and visualization engine.
	* This parameter does not apply to simulations without visualization. 
	* @param resourcePath the path to the configuration files and the resource files for Ogre3D
	* @see #getResourcePath()
	*/
	public static void setResourcePath(String resourcePath) {
		ResourcePath = resourcePath;
	}

	/**
	* Returns the status of the visualization flag 
	* @return true if visualization (FhVis) is enabled
	* @see #setVisualization(boolean)
	*/
	public static boolean isVisualization() {
		return Visualization;
	}

	/**
	* Sets the visualization flag which toggles the use of visualization during simulation
	* 
	* The visualization is powered by Ogre3D and depends on Ogre3Ds presence in the library search path.
	* The Ogre3D resources are loaded from the directory pointed to by the resource path variable. 
	* @param visualization enables (true) or disables (false) visualization in FhSim
	* @see #isVisualization()
	* @see #setResourcePath(String)
	* @see #getResourcePath()
	*/
	public static void setVisualization(boolean visualization) {
		Visualization = visualization;
	}

	/**
	* Return the window handle, or id, where Ogre3D draws the visualization.
	* 
	* This parameter does not apply to simulation without visualization. 
	* @return the handle to the window for the visualization, 0 if the visualization creates its own window 
	*/
	public static long getWindowHandle() {
		return WindowHandle;
	}
	
	/**
	* Sets the window handle, or id, where Ogre3D draws the visualization. 
	* <p>
	* FhVis will create its own window if the windows handle is set to zero (default).
	* This parameter does not apply to simulation without visualization.
	* @param windowHandle the handle to the window where FhVis will draw its visualization
	*/
	public static void setWindowHandle(long windowHandle) {
		WindowHandle = windowHandle;
	}
	
	/**
	* Returns the log level used when writing to the log file. 
	*<p> 
	* The log levels in FhSim are inclusive and are defined as
	* <ul>
	* <li> "FATAL ERROR!" - log level 0
	* <li> "ERROR"        - log level 1
	* <li> "Warning"      - log level 2
	* <li> "Info"         - log level 3
	* <li> "Debug"        - log level 4
	*
	* @return the log verbosity [0-4] used when writing to the log file
	* @see #setLogLevelFile(int)
	* @see #setLogLevelConsole(int)
	* @see #getLogLevelConsole()
	*/
	public static int getLogLevelFile()
	{
		return LogLevelFile;
	}
	
	/**
	* Sets the log level used when writing to the log file
	*<p>
	* The log levels in FhSim are inclusive and are defined as
	* <ul>
	* <li> "FATAL ERROR!" - log level 0
	* <li> "ERROR"        - log level 1
	* <li> "Warning"      - log level 2
	* <li> "Info"         - log level 3
	* <li> "Debug"        - log level 4
	* @param LogLevel the log verbosity [0-4] when writing to the log file
	* @see #getLogLevelFile()
	* @see #setLogLevelConsole(int)
	* @see #getLogLevelConsole()
	*/
	public static void setLogLevelFile(int LogLevel)
	{
		LogLevelFile = LogLevel;
	}

	/**
	* Sets the log level used to print to the console
	*<p>
	* The log levels in FhSim are inclusive and are defined as
	* <ul>
	* <li> "FATAL ERROR!" - log level 0
	* <li> "ERROR"        - log level 1
	* <li> "Warning"      - log level 2
	* <li> "Info"         - log level 3
	* <li> "Debug"        - log level 4
	* @param LogLevel the log verbosity [0-4] when writing to the console
	* @see #setLogLevelFile(int)
	* @see #getLogLevelFile()
	* @see #getLogLevelConsole()
	*/
	public static void setLogLevelConsole(int LogLevel)
	{
		LogLevelDisplay = LogLevel;
	}

	/**
	* Return the log level used to print to the console
	*<p>
	* The log levels in FhSim are inclusive and are defined as
	* <ul>
	* <li> "FATAL ERROR!" - log level 0
	* <li> "ERROR"        - log level 1
	* <li> "Warning"      - log level 2
	* <li> "Info"         - log level 3
	* <li> "Debug"        - log level 4
	* @return the log verbosity [0-4] used when writing to the console
	* @see #setLogLevelFile(int)
	* @see #getLogLevelFile()
	* @see #setLogLevelConsole(int)
	*/
	public static int getLogLevelConsole()
	{
		return LogLevelDisplay;
	}
	

	/**
	* Return whether the FhSim binary is loaded 
	* @return true if the FhSim/FhVis native library is loaded
	*/
	public static boolean isFhSimDllLoaded()
	{
		return SimDllLoaded;
	}

	/**
	* Return if a simulation is currently running in the FhSim binary
	* @return true if a simulation model is in the running state in the FhSim/FhVis native library 
	*/
	public static boolean isRunning()
	{
		return SimRunning;
	}

	/**
	* Returns whether a model is initialized and ready for execution 
	* @return true if a simulation model is initialized in the FhSim/FhVis native library 
	*/
	public static boolean isInitialized()
	{
		return SimInitialized;
	}

	/**
	* Returns whether the java path has been updated with the binaries for FhSim 
	* @return true if the FhSim JNI is initialized
	*/
	public static boolean isJNIInitialized()
	{
		return JNIInitialized;
	}
	
	/**
	* Prepares a simulation model from an input file on disk
	* <p>
	* Returns false if simulation already running. 
	* <ul>
	* <li> Sets up JNI if not initialized.
	* <li> Loads FhSim/FhVis dll if not already loaded
	* <li> initializes the simulation from the given model file
	* @param ModelFile path to FhSim/FhVis model file
	* @param ResultsFile Path to file where stimulation results are stored as a CSV file 
	* @param LogFile Path to file which holds the log messages
	* @return true if setup was successful
	*/
	public static boolean setupFromFile(String ModelFile, String ResultsFile, String LogFile)throws UnsatisfiedLinkError
	{
		if( SimRunning ) return false;
		if( !JNIInitialized && !(JNIInitialized=initJNI() ) ) return false; 
		
		if( !SimDllLoaded && !(SimDllLoaded=LoadFhSimNative(Visualization,JNIpath) ))  return false;
		
                synchronized(Lock){
                    if( !SimInitialized && !(SimInitialized = InitFromFileNative(ModelFile,ResultsFile,LogFile,LogLevelFile,LogLevelDisplay,ResourcePath,WindowHandle)) ) return false;
                }
		return JNIInitialized && SimInitialized && SimDllLoaded;
	}
	
	/**
	* Prepares a simulation models from a input file passed as an argument in a string
	* Returns false if simulation already running. 
	* <ul>
	* <li> Sets up JNI if not initialized.
	* <li> Loads FhSim/FhVis dll if not already loaded
	* <li> initializes the simulation from the given model string
	* @param ModelString A FhSim/FhVis model file contained in a string
	* @param ResultsFile Path to file where simulation results are stored as a CSV file 
	* @param LogFile Path to file which holds the log messages
	* @return true if setup was successful
	*/
	public static boolean setupFromString(String ModelString, String ResultsFile, String LogFile) throws UnsatisfiedLinkError
	{
		if( SimRunning ) stop();
		if( !JNIInitialized && !(JNIInitialized=initJNI() ) ) return false; 
		
		if( !SimDllLoaded && !(SimDllLoaded=LoadFhSimNative(Visualization, JNIpath) ))  return false;
		
                synchronized(Lock){
                    if( !SimInitialized && !(SimInitialized = InitFromStringNative(ModelString,ResultsFile,LogFile,LogLevelFile,LogLevelDisplay,ResourcePath,WindowHandle)) ) return false;
                }    
		return JNIInitialized && SimInitialized && SimDllLoaded;
	}

	/**
	* Simulates an already initialized model 
	* @param DeltaSimTime The time step which the simulation is to advance
	* @param TimeoutMultiplier The "wall-clock" scale of the time step, 1 second time step with "Timeoutmultiplier=2" is competed within 1*2 seconds
	* @return true on successfully stepping on simulation time, false if simulation time exceeded or uninitialized simulation
	*/
	public static boolean simulate(double DeltaSimTime, double TimeoutMultiplier)
	{
		if( !(JNIInitialized && SimInitialized && SimDllLoaded) ) return false;
		
                synchronized(Lock){
                    SimRunning = SimulateNative(DeltaSimTime,TimeoutMultiplier);
                }
		
                return SimRunning;
	}
	
	/**
	* Simulates an already initialized model 
	* @param DeltaSimTime The time step which the simulation is to advance
	* @return true on successfully stepping on simulation time, false if simulation time exceeded or uninitialized simulation
	*/
	public static boolean simulate(double DeltaSimTime)
	{
		if( !(JNIInitialized && SimInitialized && SimDllLoaded) ) return false;
                synchronized(Lock){
        		SimRunning = SimulateNative(DeltaSimTime,TimeoutMultiplier);
                }
		return SimRunning; 
	}
	

	/**
	* Returns the names of all the states in all the objects in the simulation model
	* The length of the returned array of strings is the number of states in the simulation.
	* The return array can be used together with the return value of #getObjectNames() to
	* determine which object contains which state. 
	* @return A string array of the FhSim/FhVis state identifiers 
	* @see #getObjectNames()
	* @see #getStateValues(double[])
	*/
	public static String [] getStateNames()
	{
		if( StateNames.length == 0 ){
                    synchronized(Lock){
			StateNames = GetStateTagsNative(); 
                    }
                }
		return StateNames;
	}

	/**
	* Return the names of all the objects in the simulation model
	* The length of the returned array corresponds to the number of states in the simulation.
	* The return array can be used together with the return value of #getStateNames() to
	* determine which object contains which state. 
	* @return A string array of the FhSim/FhVis object identifiers 
	* @see #getStateNames()
	* @see #getStateValues(double[])
	*/
	public static String [] getObjectNames()
	{
		if( ObjectNames.length == 0){
                    synchronized(Lock){
			ObjectNames = GetStateObjectsNative();
                    }
                }
		return ObjectNames;
	}

	
	/**
	* Returns the floating point value of the states in the simulation model
	* The stateVector parameter must be pre-allocated to be of the same size as 
	* the number of objects and/or number of states as returned by this interface.
	* The return array and time can be used together with the return value of #getStateNames() 
	* and #getObjectNames to determine the value of each state in each object at a specific time. 
	* @param	stateVector a double vector of state values which will be updated
	* @return the current simulation time
	* @see #getObjectNames()
	* @see #getStateNames()
	*/
	public static double getStateValues( double [] stateVector)
	{
            synchronized(Lock){
		return GetStatesNative(stateVector);
            }
	}

	/**
	* Return the external output of the simulation as routed through an "External" object
	* @param FhSimExternalBlock The name of the "External" block in the FhSim/FhVis model
	* @param FhSimOutputPort The output port name of the "External" block
	* @param PortWidth The number of elements in the output signal
	* @return a double vector of values
	*/
	public static double [] getOutput(String FhSimExternalBlock, String FhSimOutputPort, int PortWidth)
	{
            synchronized(Lock){
		return GetOutputNative(FhSimExternalBlock,FhSimOutputPort,PortWidth);
            }
	}
	
	/**
	* Sets the external input to the simulation as routed through an "External" object
	* @param FhSimExternalBlock The name of the "External" block in the FhSim/FhVis model
	* @param FhSimInputPort The input port name of the "External" block
	* @param FhSimInput The values to copy to FhSim/FhVis
	* @return true on success
	*/
	public static boolean setInput(String FhSimExternalBlock, String FhSimInputPort, double [] FhSimInput)
	{
            synchronized(Lock){
		return SetInputNative(FhSimExternalBlock,FhSimInputPort,FhSimInput);
            }
	}
	
	/**
	* Returns the error status of FhSim
	* @return Error message from FhSim/FhVis
	*/
	public static String getErrorString()
	{
            synchronized(Lock){
		return GetErrorStringNative();
            }
	}
	
	/**
	* Stops the currently running simulation
	* @return true if simulation successfully stopped
	*/
	public static boolean stop()
	{
		SimInitialized = false;
		SimRunning = false;
		StateNames = new String[0];
		ObjectNames = new String[0];
		
		if( !SimDllLoaded ){
                    return true;
                }
                else{
                    synchronized(Lock){
                        return StopSimNative();
                    }
                }
	}
	
	/**
	* Unloads the FhSim binary (not JNI) from memory
	*/
	public static void unload()
	{
		if( SimRunning) SimRunning = !stop();

		if( SimDllLoaded ){
                    synchronized(Lock){
                        SimDllLoaded = !UnloadFhSimNative();
                    }
                }
		
	}
	
	/**
	* Updates the java library path and load the FhSim native library
	* @return true on successful initialization/load of native code
	*/
	private static boolean initJNI() throws UnsatisfiedLinkError
	{
		System.setProperty( "java.library.path", JNIpath +"/" );

		Field fieldSysPath;
		try {
			fieldSysPath = ClassLoader.class.getDeclaredField( "sys_paths" );
			fieldSysPath.setAccessible( true );
			try {
				fieldSysPath.set( null, null );
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			if (Visualization) { System.loadLibrary("FhSimJNIVis"); }
			else {System.loadLibrary("FhSimJNI");}
			JNIInitialized = true;
			return JNIInitialized;
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return JNIInitialized;
	}


	public static int getSimObjectIndex(String name){
            synchronized(Lock){
		return GetSimObjectIndexNative(name);
            }
	}

	public static int getSimObjectPortIndex(String name, String port){
            synchronized(Lock){
		return GetSimObjectPortIndexNative(name,port);
            }
	}

	public static int getOutputWidthWithIndex(int objNo, int portNo){
            synchronized(Lock){
		return GetOutputWidthWithIndexNative( objNo, portNo);
            }
	}

	public static double [] getOutputWithIndex(int objNo, int portNo,int signalWidth){
            synchronized(Lock){
		return GetOutputWithIndexNative(objNo, portNo,signalWidth);
            }
	}

	private static synchronized native int GetSimObjectIndexNative(String name);
	private static synchronized native int GetSimObjectPortIndexNative(String name, String port);
	private static synchronized  native int GetOutputWidthWithIndexNative(int objNo, int portNo);
	private static synchronized native double [] GetOutputWithIndexNative(int objNo, int portNo, int signalWidth);
	
	/**
	* @return true on success
	*/
	private static native boolean LoadFhSimNative( boolean Visualization, String FhSimPath);

	/**
	*	@return true on success
	*/
	private static native boolean UnloadFhSimNative();

	/**
	*	@return true on success
	*/
	private static native boolean InitFromFileNative(String ModelFile, String ResultsFile, String LogFile, int iFileLogLevel, int iDisplayLogLevel, String ResourcePath,  long WindowHandle );

	/**
	*	@return true on success
	*/
	private static native boolean InitFromStringNative(String ModelString, String ResultsFile,String LogFile, int iFileLogLevel, int iDisplayLogLevel, String ResourcePath,  long WindowHandle);

	/**
	*	@return true on success
	*/
	private static native boolean SimulateNative( double DeltaSimTime, double TimeoutMultiplier );

	
	/**
	*	@return the output value as a java double array
	*/
	private static native double [] GetOutputNative(String Block, String Tag, int elements);

	/**
	*	@return true on success
	*/
	private static native boolean SetInputNative(String Block, String Tag, double[] In);

	/**
	*	@return the error string
	*/
	private static native String GetErrorStringNative();

	/**
	*	@return true on success
	*/
	private static native boolean StopSimNative();


	/**
	* Returns the number of states in the simulation
	* @return the number of states in the simulation
	*/
	private static native int GetNumStatesNative();	

	/**
	* Returns the names of all the states in the simulation
	* @return the names of all the states in the simulation
	*/
	private static native String[] GetStateTagsNative();

	/**
	* Returns the names of the objects in the simulation
	* @return names of all the objects in the simulation
	*/
	private static native String[] GetStateObjectsNative();

	/**
	* Retrieves the current time and the floating point value of the model states
	* @return the current simulation time
	*/
	private static native double GetStatesNative(double [] states);		

	/**
	* Retrieves the address of the simulation manager
	* @return the address of the simulation manager
	*/
	public static native long GetSimMgrNative();

        /**
	* Retrieves the number of days left of the license
	* @return the number of days left
	*/
	public static native int GetLicenseDuration();

        
        /**
         * Write current FhSim state to a binary file. Can be used to resume simulations
         * at the current time in another FhSim instance with identical input file
         * @param filestring the full path to the file where the states should be written. 
         * @return true on success, false on failure 
         */
        public static native boolean DumpStateVectorNative(String filestring);
        
	static
	{
                Lock = new Object();
		StateNames		= new String[0];
		ObjectNames		= new String[0];
		SimDllLoaded	= false;
		JNIInitialized	= false;
		SimInitialized 	= false;	
		SimRunning		= false;
		Visualization	= true; 
		WindowHandle	= 0;
		JNIpath			= "";
		ResourcePath	= ".";
		TimeoutMultiplier =1.0;
		LogLevelFile	= 4;
		LogLevelDisplay = 2;
	}
}
