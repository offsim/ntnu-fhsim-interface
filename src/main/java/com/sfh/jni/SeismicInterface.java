package com.sfh.jni;

/**
 * This is a static class which contains the Java interface to SINTEF Fisheries and Aquaculture's FhSim simulation program 
 * and SINTEF Fisheries and Aquaculture's CoRiBo Dynamics library.
 * 
 * @author Therese Frostad, SINTEF Fisheries and Aquaculture
 */

public class SeismicInterface {


	// LIST OF METHODS IMPLEMENTED FROM THE C++ SEISMIC RIG / CORIBO DYNAMICS LIBRARY. 
	// USED BY THE SEISMIC CLASSES, AND BASED ON PASSING OF C++ADDRESSES/POINTERS. 
	
	
	
	

	// SEISMIC RIG

	public static native long		getSeismicRig(long jSimmgr, String jRigname);

	public static native boolean		xmlInfo(long jSeismRig, String jFilename);

	public static native int		getNumStreamers(long jSeismRig);

	public static native long[]		getAllStreamers(long jSeismRig);

	public static native int		getNumSpreadropes(long jSeismRig);

	public static native long[]		getAllSpreadropes(long jSeismRig);

	public static native int		getNumTowropes(long jSeismRig);

	public static native long[]		getAllTowropes(long jSeismRig);

	public static native int		getNumDeflector(long jSeismRig);
	
	public static native long[]		getAllDeflectors(long jSeismRig);

	public static native boolean	chiralityIsStarboard(long jSeismRig);

	public static native boolean	chiralityIsPort(long jSeismRig);

	// STREAMER

	public static native int		getNumNautilusBirdsFromStreamer(long jStreamercable);
	
	public static native long[]		getAllNautilusBirdsFromStreamer(long jStreamercable, int size);

	public static native int		getNumSrdDigiCoilsFromStreamer(long jStreamercable);
	
	public static native long[]		getAllSrdDigiCoilsFromStreamer(long jStreamercable, int size);

	public static native long		getTailBuoyFromStreamer(long jStreamercable);

	public static native long		getConnectionSegmentFromStreamer(long jStreamercable);

	public static native long		getHeadBuoyConnectionSegmentFromStreamer(long jStreamercable);

	public static native long		getHeadBuoyCableFromStreamer(long jStreamercable);

	public static native long		getWinch(long jStreamercable);

	public static native long		getTowingBlock(long jStreamercable); 

	public static native long		getSecondaryTowingBlock(long jStreamercable); 

	public static native int[]		getFirstFreeDiscreteElementIndexSuperSubFromStreamer(long jStreamercable);

	public static native double		getPaidOutLengthFromStreamer(long jStreamercable); // [m]

	
	// WINCH

	public static native double[]		getReactionForcesFromWinch(long jWinch); // [N,N,N,Nm,Nm,Nm]

	public static native boolean		setWinchRotation(long jWinch, double theta, double omega); // theta[rad] omega[rad/s]

	public static native double			getWinchTheta(long jWinch); // [rad]

	public static native double			getWinchOmega(long jWinch); // [rad/s]

	public static native double		getRetractedLengthFromWinch(long jWinch); // [m]

	public static native double[]		getPosFromWinch(long jWinch); // [m,m,m]

	public static native double[]		getVelFromWinch(long jWinch); // [m/s,m/s,m/s]

	public static native double[]		getOriFromWinch(long jWinch); // quaternion[w, x, y, z]

	public static native double[]		getAngVelFromWinch(long jWinch); // [rad/s,rad/s,rad/s]

	public static native double			getDrumVirtualRadius(long jWinch); // [m]
	
	public static native double[]		getStandardizedOrientationNEDFromWinch(long jWinch); // quaternion[w, x, y, z]

	// TOWING BLOCK

	public static native double[]		getReactionForcesFromTowingBlock(long jTowingBlock); // [N,N,N,Nm,Nm,Nm]

	public static native double[]		getPosFromTowingBlock(long jTowingBlock); // [m,m,m]

	public static native double[]		getVelFromTowingBlock(long jTowingBlock); // [m/s,m/s,m/s]

	public static native double[]		getOriFromTowingBlock(long jTowingBlock); // quaternion[w, x, y, z]

	public static native double[]		getAngVelFromTowingBlock(long jTowingBlock); // [rad/s,rad/s,rad/s]

	public static native double[]		blockCenterPosition(long jTowingBlock); // [m,m,m]
	
	public static native double[]		blockCenterOrientation(long jTowingBlock); // quaternion[w, x, y, z]
	
	public static native double			blockPivotAngle(long jTowingBlock); // [rad]

	public static native double[]		getStandardizedOrientationNEDFromTowingBlock(long jTowingBlock); // quaternion[w, x, y, z]


	// SPREADROPE
	
	public static native boolean		connectUpperEndOfSpreadropeToStreamer(long jSpreadrope);

	public static native boolean		connectUpperEndOfSpreadropeToWinch(long jSpreadrope);

	public static native boolean		isUpperEndOfSpreadropeConnectedToStreamer(long jSpreadrope);

	public static native boolean		isUpperEndOfSpreadropeConnectedToWinch(long jSpreadrope);

	public static native boolean		connectLowerEndOfSpreadropeToStreamer(long jSpreadrope);

	public static native boolean		connectLowerEndOfSpreadropeToWinch(long jSpreadrope);

	public static native boolean		isLowerEndOfSpreadropeConnectedToStreamer(long jSpreadrope);

	public static native boolean		isLowerEndOfSpreadropeConnectedToWinch(long jSpreadrope);

	public static native boolean		disconnectUpperEndOfSpreadrope(long jSpreadrope);

	public static native boolean		disconnectLowerEndOfSpreadrope(long jSpreadrope);


	// HEAD BUOY CABLE 

	public static native boolean		connectUpperEndOfHeadBuoyCableToStreamer(long jHeadBuoyCable);

	public static native boolean		connectUpperEndOfHeadBuoyCableToWinch(long jHeadBuoyCable);

	public static native boolean		isUpperEndOfHeadBuoyCableConnectedToStreamer(long jHeadBuoyCable);

	public static native boolean		isUpperEndOfHeadBuoyCableConnectedToWinch(long jHeadBuoyCable);


	// SUPERGRID LINE STRUCTURE (parent of STREAMER, and POINT RETRACTABLE CABLE such as SPREADROPE and TOWROPE)


	public static native int		getNumLineSegmentsFromSupergridLineStructure(long jSupergridLineStructure);
	
	public static native long[]		getAllLineSegmentsFromSupergridLineStructure(long jSupergridLineStructure, int size);

	public static native int		getNumDiscreteElementsFromSupergridLineStructure(long jSupergridLineStructure);

	public static native long[]		getAllDiscreteElementsFromSupergridLineStructure(long jSupergridLineStructure, int size);

	public static native int		getNumRigidElementsFromSupergridLineStructure(long jSupergridLineStructure);

	public static native long[]		getAllRigidElementsFromSupergridLineStructure(long jSupergridLineStructure, int size);

	public static native double		getLengthFromSupergridLineStructure(long jSupergridLineStructure);

	public static native double[]		getRigidElementTensionsFromSupergridLineStructure(long jSupergridLineStructure, int size);

	// POINT RETRACTABLE CABLE (parent of SPREADROPE, TOWROPE and HEADBUOYCABLE)


	public static native boolean		setRetractedLength(long jPointRetractableCable, double retractedLength, double retractionSpeed); // retractedLength[m], retractionSpeed [m/s]

	public static native double			getRetractedLength(long jPointRetractableCable); // [m]

	public static native double			getRetractionSpeed(long jPointRetractableCable); // [m/s]

	public static native double[]		getEndpointReactionForce(long jPointRetractableCable); // [N, N, N]

	public static native double[]		getCableUpperEndPosition(long jPointRetractableCable); // [m, m, m]

	public static native double[]		getCableLowerEndPosition(long jPointRetractableCable); // [m, m, m]


	// LINE SEGMENT (parent of CABLE SEGMENT and UNIT SEGMENT)

	public static native int		getNumDiscreteElementsFromLineSegment(long jLineSegment);

	public static native long[]		getAllDiscreteElementsFromLineSegment(long jLineSegment, int size);

	public static native double		getSegmentLength(long jLineSegment);

	public static native String		getSegmentTypeName(long jLineSegment);

	public static native double		getCableStartPosition(long jLineSegment);

	public static native int		getCableIndex(long jLineSegment);

	// UNIT SEGMENT

	public static native double[]		getPosFromUnitSegment(long jUnitSegment); // [m,m,m]

	public static native double[]		getVelFromUnitSegment(long jUnitSegment); // [m/s,m/s,m/s]

	public static native double[]		getAngVelFromUnitSegment(long jUnitSegment); // [rad/s,rad/s,rad/s]

	public static native double[]		getOriFromUnitSegment(long jUnitSegment); // quaternion[w, x, y, z]

	public static native double[]		getStandardizedOrientationNEDFromUnitSegment(long jUnitSegment); // quaternion[w, x, y, z]


	// DISCRETE ELEMENT

	public static native int		getNumSubDiscreteElementsFromDiscreteElement(long jDiscreteElement);

	public static native long[]		getAllSubDiscreteElementsFromDiscreteElement(long jDiscreteElement, int size);

	public static native boolean		isDiscreteElementActivated(long jDiscreteElement);

	public static native long		getSuperDiscreteElementFromDiscreteElement(long jDiscreteElement);

	public static native long		getRigidElementFromDiscreteElement(long jDiscreteElement);

	public static native double[]		getPosFromDiscreteElement(long jDiscreteElement); // [m,m,m]

	public static native double[]		getVelFromDiscreteElement(long jDiscreteElement); // [m/s,m/s,m/s]

	public static native double[]		getOriFromDiscreteElement(long jDiscreteElement); // quaternion[w, x, y, z]

	public static native double[]		getAngVelFromDiscreteElement(long jDiscreteElement); // [rad/s,rad/s,rad/s]

	public static native long		getParentLineSegmentFromDiscreteElement(long jDiscreteElement);

	public static native double		getLengthFromDiscreteElement(long jDiscreteElement);

	public static native double		getRadiusFromDiscreteElement(long jDiscreteElement);

	public static native double		getMassFromDiscreteElement(long jDiscreteElement);

	

	// RIGID ELEMENT
	
	public static native long		getDiscreteElementFromModifiableRigidElement(long jRigidElement);

	public static native double[]		getPosFromRigidElement(long jRigidElement); // [m,m,m]

	public static native double[]		getVelFromRigidElement(long jRigidElement); // [m/s,m/s,m/s]

	public static native double[]		getOriFromRigidElement(long jRigidElement); // quaternion[w, x, y, z]

	public static native double[]		getAngVelFromRigidElement(long jRigidElement); // [rad/s,rad/s,rad/s]

	public static native long		getParentLineSegmentFromRigidElement(long jRigidElement);

	

	// NAUTILUS BIRD

	public static native boolean		detachNautilusBird(long jNautilusBirdSegment);

	public static native boolean		attachNautilusBird(long jNautilusBirdSegment);

	public static native boolean		isNautilusBirdAttached(long jNautilusBirdSegment);

	public static native boolean		freezeNautilusBird(long jNautilusBirdSegment); // freeze foil angles in current position. to resume active control, call SetTargetDepth or SetLateralDistanceOffset

	public static native boolean		setTargetDepthOnNautilusBird(long jNautilusBirdSegment, double targetDepth); // [m]
	
	public static native boolean		setLateralDistanceOffsetOnNautilusBird(long jNautilusBirdSegment, double offset); // lateral control. set current distance offset. positive value means starboard control

	public static native boolean		setLateralSteeringOnNautilusBird(long jNautilusBirdSegment, double factor); // direct lateral control [-1.0, +1.0]. positive value means starboard steering.

	public static native boolean		setVerticalSteeringOnNautilusBird(long jNautilusBirdSegment, double factor); // direct vertical control [-1.0, +1.0]. positive value means downward steering.

	public static native double		getTargetDepthFromNautilusBird(long jNautilusBirdSegment); // [m]

	public static native double		getLateralDistanceOffsetFromNautilusBird(long jNautilusBirdSegment); // lateral control. set current distance offset. positive value means starboard control

	public static native double		getLateralSteeringFromNautilusBird(long jNautilusBirdSegment);  // direct lateral control [-1.0, +1.0]. positive value means starboard steering.

	public static native double		getVerticalSteeringFromNautilusBird(long jNautilusBirdSegment); // direct vertical control [-1.0, +1.0]. positive value means downward steering.

	public static native boolean	setDepthSensorBiasOnNautilusBird(long jNautilusBirdSegment, double depthBias);


	// SRD DIGI COIL

	public static native boolean		attachSRD(long jSrdDigiCoil);

	public static native boolean		detachSRD(long jSrdDigiCoil);

	public static native boolean		attachCMX(long jSrdDigiCoil);

	public static native boolean		detachCMX(long jSrdDigiCoil);

	public static native boolean		attachDigiFin(long jSrdDigiCoil);

	public static native boolean		detachDigiFin(long jSrdDigiCoil);

	public static native boolean		attachDigiBird(long jSrdDigiCoil);

	public static native boolean		detachDigiBird(long jSrdDigiCoil);

	public static native boolean		isSrdAttached(long jSrdDigiCoil);

	public static native boolean		isCMXAttached(long jSrdDigiCoil);

	public static native boolean		isDigiFinAttached(long jSrdDigiCoil);

	public static native boolean		isDigiBirdAttached(long jSrdDigiCoil);

	public static native boolean		setTargetDepthOnSrdDigiCoil(long jSrdDigiCoil, double targetDepth); // vertical control. set desired control depth in meters.

	public static native boolean		setLateralDistanceOffsetOnSrdDigiCoil(long jSrdDigiCoil, double offset); // lateral control. set current distance offset. positive value means starboard control

	public static native boolean		setLateralSteeringOnSrdDigiCoil(long jSrdDigiCoil, double factor); // direct lateral control [-1.0, +1.0]. positive value means starboard steering.

	public static native boolean		setVerticalSteeringOnSrdDigiCoil(long jSrdDigiCoil, double factor); // direct vertical control [-1.0, +1.0]. positive value means downward steering.

	public static native double		getTargetDepthFromSrdDigiCoil(long jSrdDigiCoil); // [m]

	public static native double		getLateralDistanceOffsetFromSrdDigiCoil(long jSrdDigiCoil); // lateral control. set current distance offset. positive value means starboard control

	public static native double		getLateralSteeringFromSrdDigiCoil(long jSrdDigiCoil);  // direct lateral control [-1.0, +1.0]. positive value means starboard steering.

	public static native double		getVerticalSteeringFromSrdDigiCoil(long jSrdDigiCoil); // direct vertical control [-1.0, +1.0]. positive value means downward steering.

	// HEAD BUOY CABLE

	public static native long		getHeadBuoyFromHeadBuoyCable(long jHeadbuoycable);

	// DEFLECTOR FOIL 

	public static native double[]		getStandardizedOrientationNEDFromDeflectorFoil(long jDeflectorFoil); // quaternion[w, x, y, z]

}