/*
 * Copyright (C) 2004, 2019, Offshore Simulator Centre AS (OSC AS).
 * All rights reserved.
 OSC AS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.sfh.osc.spec;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import java.io.File;

/**
 *
 * @author pierre
 */
public class OSCModel {

    private String path;
    private Model model;

    public OSCModel() {

    }

    public OSCModel(String path) {

        model = (Model) getXStream().fromXML(new File(path));
    }

    XStream getXStream() {
        XStream xs = new XStream(new PureJavaReflectionProvider());
        xs.ignoreUnknownElements();
        xs.alias("model", Model.class);
        return xs;
    }

    /**
     *
     * @return x position of the osc model relative to AP
     */
    public double getXPos() {
        if (model == null) {
            return Double.NEGATIVE_INFINITY;
        }
        return model.specifications.mp.xPos.value;

    }

    /**
     *
     * @return y position of the osc model relative to CL NED
     */
    public double getYPos() {
        if (model == null) {
            return Double.NEGATIVE_INFINITY;
        }
        return model.specifications.mp.yPos.value;

    }

    /**
     *
     * @return z position of the osc model relative to BL NED
     */
    public double getZPos() {
        if (model == null) {
            return Double.NEGATIVE_INFINITY;
        }
        return model.specifications.mp.zPos.value;

    }

    private static class Model {

        Specifications specifications;

        private static class Specifications {

            MP mp;

            private static class MP {

                Pos xPos, yPos, zPos;

                private static class Pos {

                    double value;
                    String unit;
                }
            }
        }
    }

    public static void main(String[] args) {
        OSCModel mod = new OSCModel("C:\\Users\\pierre\\Documents\\fhsim_models\\OSC_VARD306_NOI_NormandVision\\OSC_VARD306_NOI_NormandVision.xml");

        System.out.println(mod.getXPos());
        System.out.println(mod.getYPos());
        System.out.println(mod.getZPos());
        
        OSCModel mod0 = new OSCModel();
        System.out.println(mod0.getXPos());
        System.out.println(mod0.getYPos());
        System.out.println(mod0.getZPos());
        
    }

}
