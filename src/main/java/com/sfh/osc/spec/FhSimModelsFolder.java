package com.sfh.osc.spec;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pierre
 */
public class FhSimModelsFolder {

    private static String folder;

    public static String getFolder() {
        if (folder == null) {
            folder = System.getenv("OPUS_MODELS_PATH");
            if (folder == null) {
                Logger.getLogger(FhsimModelVersionFinder.class.getName()).log(Level.CONFIG, null, "OPUS_MODELS_PATH environment variable is not set to fhsim models folder");
            } else {
                Logger.getLogger(FhsimModelVersionFinder.class.getName()).log(Level.INFO, "FhSim Models Directory " + folder);
            }

        }
        return folder;
    }
}
