package com.sfh.osc.spec;

import com.sfh.osc.spec.FhSimModelsFolder;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.NoWorkTreeException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

/**
 *
 * @author pierre
 */
public class FhsimModelVersionFinder {

    static String version;

    public static String getVersion() {

        if (version == null) {
            String directory = FhSimModelsFolder.getFolder();
            if (directory != null) {
                try {
                    //resolves symbolic link
                    directory = Paths.get(directory).toRealPath().toFile().getAbsolutePath();
                    System.out.println(" directory : " + directory);

                    directory += "\\..\\..\\..\\..\\..\\";

                    FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
                    Repository repository;

                    repository = repositoryBuilder.setGitDir(new File(directory + ".git"))
                            .readEnvironment() // scan environment GIT_* variables
                            .findGitDir() // scan up the file system tree
                            .setMustExist(true)
                            .build();
                    Ref head = repository.findRef("HEAD");

                    version = head.getObjectId().abbreviate(6).name();

                    Git git = new Git(repository);
                    Status status;
                    try {
                        status = git.status().call();
                        version = status.isClean() ? version : (version + "/tainted");
                    } catch (GitAPIException ex) {
                        Logger.getLogger(FhsimModelVersionFinder.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NoWorkTreeException ex) {
                        Logger.getLogger(FhsimModelVersionFinder.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    Logger.getLogger(FhsimModelVersionFinder.class.getName()).log(Level.INFO, "FhSim Models version " + version + " on branch " + repository.getBranch());

                } catch (IOException ex) {
                    Logger.getLogger(FhsimModelVersionFinder.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return version;
    }
}
