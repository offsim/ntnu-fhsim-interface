/*
 * Copyright SINTEF Fisheries and Aquaqulture
 * Use of this software and source code is
 * prohibited without explicit authorization
 */
package com.sfh.osc.spec;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Hull characteritics specification
 *
 * @see SFHHullIntf
 * @author pierre@osc.no
 */
@XStreamAlias("HullSpec")
public class OSCHullDataSpec {

    private String name;
    @XStreamOmitField
    private String FileNameRoot;

    private String LPP_m;
    private String Loa_m;

    private String bredth_m;
    private String draught_m;
    private String blockCoefficient;
    private String displacement_t;
    private String GRelAP_m;
    private String GRelBL_m;
    private String GRelCL_m;
    private String BRelBL_m;
    private String metacentricHeight_m; //GM

    private String fullSpeedForward_kt;
    private String fullSpeedForwardRange; //+/-%
    private String fullSpeedBackward_kt;
    private String fullSpeedBackwardRange;
    private String DPCrabSpeed_kt;
    private String DPCrabSpeedRange; //+/-%
    private String surgeDecay_s;
    private String surgeDecayRange; //+/-%
    private String swayDecay_s;
    private String swayDecayRange; //+/-%
    private String YawDecay_s;
    private String YawDecayRange; //+/-%
    private String pirouetteManeuver_RPM;
    private String pirouetteManeuverRange; //+/-%

    private String heelPeriod_s;
    private String heelPeriodRange; //+/-%   
    private String heelDecay; //number of oscillation before 5% under initial heel value;
    private String heelDecayRange; //+/-%

    private String StabilityBookletReference;
    private List<OSCThrusterSpec> thrusters;
    private String stabitlityBookletCase;
    private String heel_deg;
    @XStreamOmitField
    private boolean is_transit_load_condition;

    public String getStabilityBookletReference() {
        return StabilityBookletReference;
    }

    public void setStabilityBookletReference(String StabilityBookletReference) {
        this.StabilityBookletReference = StabilityBookletReference;
    }

    public List<OSCThrusterSpec> getThrusters() {
        return thrusters;
    }

    public void setThrusters(List<OSCThrusterSpec> thrusters) {
        this.thrusters = thrusters;
    }

    public String getFullSpeedForwardRange() {
        return fullSpeedForwardRange;
    }

    public void setFullSpeedForwardRange(String fullSpeedForwardRange) {
        this.fullSpeedForwardRange = fullSpeedForwardRange;
    }

    public String getFullSpeedBackwardRange() {
        return fullSpeedBackwardRange;
    }

    public void setFullSpeedBackwardRange(String fullSpeedBackwardRange) {
        this.fullSpeedBackwardRange = fullSpeedBackwardRange;
    }

    public String getDPCrabSpeedRange() {
        return DPCrabSpeedRange;
    }

    public void setDPCrabSpeedRange(String DPCrabSpeedRange) {
        this.DPCrabSpeedRange = DPCrabSpeedRange;
    }

    public String getSurgeDecayRange() {
        return surgeDecayRange;
    }

    public void setSurgeDecayRange(String surgeDecayRange) {
        this.surgeDecayRange = surgeDecayRange;
    }

    public String getSwayDecayRange() {
        return swayDecayRange;
    }

    public void setSwayDecayRange(String swayDecayRange) {
        this.swayDecayRange = swayDecayRange;
    }

    public String getYawDecayRange() {
        return YawDecayRange;
    }

    public void setYawDecayRange(String YawDecayRange) {
        this.YawDecayRange = YawDecayRange;
    }

    public String getPirouetteManeuverRange() {
        return pirouetteManeuverRange;
    }

    public void setPirouetteManeuverRange(String pirouetteManeuverRange) {
        this.pirouetteManeuverRange = pirouetteManeuverRange;
    }

    public String getHeelPeriodRange() {
        return heelPeriodRange;
    }

    public void setHeelPeriodRange(String heelPeriodRange) {
        this.heelPeriodRange = heelPeriodRange;
    }

    public String getHeelDecayRange() {
        return heelDecayRange;
    }

    public void setHeelDecayRange(String heelDecayRange) {
        this.heelDecayRange = heelDecayRange;
    }

    String rollInertia_kg_m2;

    public String getLPP_m() {
        return LPP_m;
    }

    public void setLPP_m(String LPP_m) {
        this.LPP_m = LPP_m;
    }

    public String getLoa_m() {
        return Loa_m;
    }

    public void setLoa_m(String Loa_m) {
        this.Loa_m = Loa_m;
    }

    public String getBredth_m() {
        return bredth_m;
    }

    public void setBredth_m(String bredth_m) {
        this.bredth_m = bredth_m;
    }

    public String getDraught_m() {
        return draught_m;
    }

    public void setDraught_m(String draught_m) {
        this.draught_m = draught_m;
    }

    public String getBlockCoefficient() {
        return blockCoefficient;
    }

    public void setBlockCoefficient(String blockCoefficient) {
        this.blockCoefficient = blockCoefficient;
    }

    public String getDisplacement_t() {
        return displacement_t;
    }

    public void setDisplacement_t(String displacement_t) {
        this.displacement_t = displacement_t;
    }

    public String getGRelAP_m() {
        return GRelAP_m;
    }

    public void setGRelAP_m(String GRelAP_m) {
        this.GRelAP_m = GRelAP_m;
    }

    public String getGRelBL_m() {
        return GRelBL_m;
    }

    public void setGRelBL_m(String GRelBL_m) {
        this.GRelBL_m = GRelBL_m;
    }

    public String getGRelCL_m() {
        return GRelCL_m;
    }

    public void setGRelCL_m(String GRelCL_m) {
        this.GRelCL_m = GRelCL_m;
    }

    public String getBRelBL_m() {
        return BRelBL_m;
    }

    public void setBRelBL_m(String BRelBL_m) {
        this.BRelBL_m = BRelBL_m;
    }

    public String getMetacentricHeight_m() {
        return metacentricHeight_m;
    }

    public void setMetacentricHeight_m(String metacentricHeight_m) {
        this.metacentricHeight_m = metacentricHeight_m;
    }

    public String getFullSpeedForward_kt() {
        return fullSpeedForward_kt;
    }

    public void setFullSpeedForward_kt(String fullSpeedForward_kt) {
        this.fullSpeedForward_kt = fullSpeedForward_kt;
    }

    public String getFullSpeedBackward_kt() {
        return fullSpeedBackward_kt;
    }

    public void setFullSpeedBackward_kt(String fullSpeedBackward_kt) {
        this.fullSpeedBackward_kt = fullSpeedBackward_kt;
    }

    public String getDPCrabSpeed_kt() {
        return DPCrabSpeed_kt;
    }

    public void setDPCrabSpeed_kt(String DPCrabSpeed_kt) {
        this.DPCrabSpeed_kt = DPCrabSpeed_kt;
    }

    public String getSurgeDecay_s() {
        return surgeDecay_s;
    }

    public void setSurgeDecay_s(String surgeDecay_s) {
        this.surgeDecay_s = surgeDecay_s;
    }

    public String getSwayDecay_s() {
        return swayDecay_s;
    }

    public void setSwayDecay_s(String swayDecay_s) {
        this.swayDecay_s = swayDecay_s;
    }

    public String getYawDecay_s() {
        return YawDecay_s;
    }

    public void setYawDecay_s(String YawDecay_s) {
        this.YawDecay_s = YawDecay_s;
    }

    public String getPirouetteManeuver_RPM() {
        return pirouetteManeuver_RPM;
    }

    public void setPirouetteManeuver_RPM(String pirouetteManeuver_RPM) {
        this.pirouetteManeuver_RPM = pirouetteManeuver_RPM;
    }

    /**
     *
     * @return roll period from the heel decay
     */
    public String getRollPeriod_s() {
        return heelPeriod_s;
    }

    /**
     *
     * @param rollPeriod_s roll period from the heel decay
     */
    public void setRollPeriod_s(String rollPeriod_s) {
        this.heelPeriod_s = rollPeriod_s;
    }

    public String getHeelDecay() {
        return heelDecay;
    }

    public void setHeelDecay(String heelDecay) {
        this.heelDecay = heelDecay;
    }

    public String getRollInertia_kg_m2() {
        return rollInertia_kg_m2;
    }

    public void setRollInertia_kg_m2(String rollInertia_kg_m2) {
        this.rollInertia_kg_m2 = rollInertia_kg_m2;
    }

    public String getCurrentSpeedConvergenceTime_s() {
        return currentSpeedConvergenceTime_s;
    }

    public void setCurrentSpeedConvergenceTime_s(String currentSpeedConvergenceTime_s) {
        this.currentSpeedConvergenceTime_s = currentSpeedConvergenceTime_s;
    }

    public String getWindLateralArea_m2() {
        return windLateralArea_m2;
    }

    public void setWindLateralArea_m2(String windLateralArea_m2) {
        this.windLateralArea_m2 = windLateralArea_m2;
    }

    public String getWindFrontalArea_m2() {
        return windFrontalArea_m2;
    }

    public void setWindFrontalArea_m2(String windFrontalArea_m2) {
        this.windFrontalArea_m2 = windFrontalArea_m2;
    }

    public String getWindCharacteristicLength_m() {
        return windCharacteristicLength_m;
    }

    public void setWindCharacteristicLength_m(String windCharacteristicLength_m) {
        this.windCharacteristicLength_m = windCharacteristicLength_m;
    }

    String currentSpeedConvergenceTime_s;

    private String windLateralArea_m2;
    private String windFrontalArea_m2;
    private String windCharacteristicLength_m;
    private int numberOfThrusters;

    public static void dumpToXml(String fname, OSCHullDataSpec obj) {
        XStream xstream = new XStream();
        xstream.processAnnotations(OSCHullDataSpec.class);
        PrintWriter out
                = null;
        try {
            out = new PrintWriter(fname);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OSCHullDataSpec.class.getName()).log(Level.SEVERE,
                    null, ex);
            return;
        }
        out.println(xstream.toXML(obj));
        out.close();
    }

    public static OSCHullDataSpec loadFromXml(String fname) {
        XStream xstream
                = new XStream();
        xstream.processAnnotations(OSCHullDataSpec.class);
        OSCHullDataSpec out = (OSCHullDataSpec) xstream.fromXML(new File(fname));
        return out;
    }

    public static OSCHullDataSpec loadFromXml(String objectName, String fname) {
        OSCHullDataSpec out = loadFromXml(fname);
        out.setName(objectName);
        return out;
    }

    public static OSCHullDataSpec findByNameInObjectList(String Name,
            ArrayList<Object> list) {
        for (Object o : list) {
            if (o instanceof OSCHullDataSpec) {
                if (((OSCHullDataSpec) o).getName().equals(Name)) {
                    return (OSCHullDataSpec) o;
                }
            }
        }
        return null;
    }

    /**
     * Set the name of the ship object used during simulation
     *
     * @param Name the of the ship object in the simulation
     */
    public void setName(String Name) {
        this.name = Name;
    }

    public String getName() {
        return name;
    }

    /**
     * Get the lateral area of the ship hull above the water plane
     *
     * @return the area in m**2
     */
    public String getWindLateralArea() {
        return windLateralArea_m2;
    }

    /**
     * Set the lateral area of the ship hull above the water plane
     *
     * @param windLateralArea new area in m**2
     */
    public void setWindLateralArea(String windLateralArea) {
        this.windLateralArea_m2 = windLateralArea;
    }

    /**
     * Get the lateral area of the ship hull above the water plane
     *
     * @return the area in m**2
     */
    public String getWindFrontalArea() {
        return windFrontalArea_m2;
    }

    /**
     * Set the lateral area of the ship hull above the water plane
     *
     * @param windFrontalArea the new area in m**2
     */
    public void setWindFrontalArea(String windFrontalArea) {
        this.windFrontalArea_m2 = windFrontalArea;
    }

    /**
     * Get the characteristic length of the ship wrt. the wind This is usually
     * the length overall
     *
     * @return the characteristic length in m
     */
    public String getWindCharacteristicLength() {
        return windCharacteristicLength_m;
    }

    /**
     * Set the characteristic length of the ship wrt. the wind This is usually
     * the length overall
     *
     * @param windCharacteristicLength the new length in m
     */
    public void setWindCharacteristicLength(String windCharacteristicLength) {
        this.windCharacteristicLength_m = windCharacteristicLength;
    }

    public int getNumberOfThrusters() {
        return numberOfThrusters;
    }

    /**
     * Number of thrusters used in Cr regression
     *
     * @param NumberOfThrusters the NumberOfThrusters to set
     */
    public void setNumberOfThrusters(int NumberOfThrusters) {
        this.numberOfThrusters = NumberOfThrusters;
    }

    public String getStabilityBookletCase() {
        return stabitlityBookletCase;
    }

    /**
     *
     * Name of the case in the stability booklet
     */
    public void setStabilityBookletCase(String stabilityBookletCase) {
        this.stabitlityBookletCase = stabilityBookletCase;
    }

    /**
     *
     * @param heel from Veres calculation
     */
    void setHeel_deg(String heel) {
        this.heel_deg = heel;
    }

    /**
     *
     * @return full name of the xml file to serialize to
     */
    public String getSpecFileName() {
        return FileNameRoot + "\\vessel_spec_" + stabitlityBookletCase + ".xml";
    }

    /**
     *
     * @return full name of the xml file to serialize to
     */
    public String getReportFileName() {
        return FileNameRoot + "\\report_spec_" + stabitlityBookletCase + ".xml";
    }

    /**
     *
     * @param root containing absolute path
     */
    public void setFileNameRoot(String root) {
        this.FileNameRoot = root;
    }

    /**
     *
     * @return true if the loading condition is for transit
     */
    public boolean getIsTransit() {
        return is_transit_load_condition;
    }

    public void setIsTransitLoadingCondition(boolean isTransit) {
        this.is_transit_load_condition = isTransit;
    }
}
