/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.osc.spec;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.File;

/**
 *
 * @author pierre
 */
@XStreamAlias("ReportSpec")
public class ReportSpec {

    private boolean runVeres = true;
    private boolean runStability = true;
    private boolean runDampingInfo = true;
    private boolean runDriftInfo = true;
    private boolean runPolar = true;
    private boolean runRAO = true;
    private boolean runWheelingHousePoster = true;
    private boolean runSensorInfo = true;
    private boolean runPropulsorInfo = true;
    private boolean runDPtest = true;
    private boolean runFullSpeed = true;
    private boolean runCrashStop = true;
    private boolean runHeelDecay = true;
    private boolean runExtraRAOPeriods = false;

    private double raoPeriodStep = 1.0;

    private double heelDecayStartAngleDegrees = 10.0;
    private double heelDecayEndTimeSeconds = 100;

    public static ReportSpec loadFromXml(String fname) {
        XStream xstream
                = new XStream();
        xstream.processAnnotations(ReportSpec.class);
        ReportSpec out = (ReportSpec) xstream.fromXML(new File(fname));
        return out;
    }

    public void setAllTrue() {
        runVeres = true;
        runStability = true;
        runDampingInfo = true;
        runDriftInfo = true;
        runPolar = true;
        runRAO = true;
        runWheelingHousePoster = true;
        runSensorInfo = true;
        runPropulsorInfo = true;
        runDPtest = true;
        runFullSpeed = true;
        runCrashStop = true;
        runHeelDecay = true;

    }

    public void setAllFalse() {
        runVeres = false;
        runStability = false;
        runDampingInfo = false;
        runDriftInfo = false;
        runPolar = false;
        runRAO = false;
        runWheelingHousePoster = false;
        runSensorInfo = false;
        runPropulsorInfo = false;
        runDPtest = false;
        runFullSpeed = false;
        runCrashStop = false;
        runHeelDecay = false;
        

    }

    public double getHeelDecayStartAngleDegrees() {
        return heelDecayStartAngleDegrees;
    }

    public void setHeelDecayStartAngleDegrees(double heelDecayStartAngleDegrees) {
        this.heelDecayStartAngleDegrees = heelDecayStartAngleDegrees;
    }

    public double getHeelDecayEndTimeSeconds() {
        return heelDecayEndTimeSeconds;
    }

    public void setHeelDecayEndTimeSeconds(double heelDecayEndTimeSeconds) {
        this.heelDecayEndTimeSeconds = heelDecayEndTimeSeconds;
    }

    public boolean isRunFullSpeed() {
        return runFullSpeed;
    }

    public void setRunFullSpeed(boolean runFullSpeed) {
        this.runFullSpeed = runFullSpeed;
    }

    public boolean isRunCrashStop() {
        return runCrashStop;
    }

    public void setRunCrashStop(boolean runCrashStop) {
        this.runCrashStop = runCrashStop;
    }

    public boolean isRunVeres() {
        return runVeres;
    }

    public void setRunVeres(boolean runVeres) {
        this.runVeres = runVeres;
    }

    public boolean isRunStability() {
        return runStability;
    }

    public void setRunStability(boolean runStability) {
        this.runStability = runStability;
    }

    public boolean isRunDampingInfo() {
        return runDampingInfo;
    }

    public void setRunDampingInfo(boolean runDampingInfo) {
        this.runDampingInfo = runDampingInfo;
    }

    public boolean isRunDriftInfo() {
        return runDriftInfo;
    }

    public void setRunDriftInfo(boolean runDriftInfo) {
        this.runDriftInfo = runDriftInfo;
    }

    public boolean isRunRAO() {
        return runRAO;
    }

    public void setRunRAO(boolean runRAO) {
        this.runRAO = runRAO;
    }

    public boolean isRunWheelingHousePoster() {
        return runWheelingHousePoster;
    }

    public void setRunWheelingHousePoster(boolean runWheelingHouse) {
        this.runWheelingHousePoster = runWheelingHouse;
    }

    public boolean isRunSensorInfo() {
        return runSensorInfo;
    }

    public void setRunSensorInfo(boolean runSensorInfo) {
        this.runSensorInfo = runSensorInfo;
    }

    public boolean isRunPropulsorInfo() {
        return runPropulsorInfo;
    }

    public void setRunPropulsorInfo(boolean runThruster) {
        this.runPropulsorInfo = runThruster;
    }

    public boolean isRunDPtest() {
        return runDPtest;
    }

    public void setRunDPtest(boolean runDPtest) {
        this.runDPtest = runDPtest;
    }

    public boolean isRunPolar() {
        return runPolar;
    }

    public void setRunPolar(boolean runPolar) {
        this.runPolar = runPolar;
    }

    public boolean isRunHeelDecay() {
        return runHeelDecay;
    }

    public void setRunHeelDecay(boolean runHeelDecay) {
        this.runHeelDecay = runHeelDecay;
    }

    public boolean isRunExtraRAOPeriods() {
        return runExtraRAOPeriods;
    }

    public void setRunExtraRAOPeriods(boolean runExtraRAOPeriods) {
        this.runExtraRAOPeriods = runExtraRAOPeriods;
    }

    public double getRaoPeriodStep() {
        return raoPeriodStep;
    }

    public void setRaoPeriodStep(double raoPeriodStep) {
        this.raoPeriodStep = raoPeriodStep;
    }

}
