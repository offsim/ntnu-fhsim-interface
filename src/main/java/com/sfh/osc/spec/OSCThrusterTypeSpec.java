/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.osc.spec;

/**
 *
 * @author pierre
 */
public class OSCThrusterTypeSpec {

    public String getDiameter_m() {
        return diameter_m;
    }

    public void setDiameter_m(String diameter_m) {
        this.diameter_m = diameter_m;
    }

    public boolean isFixPitch_deg() {
        return fixPitch_deg;
    }

    public void setFixPitch_deg(boolean fixPitch_deg) {
        this.fixPitch_deg = fixPitch_deg;
    }

    public boolean isFixRPM_rpm() {
        return fixRPM_rpm;
    }

    public void setFixRPM_rpm(boolean fixRPM_rpm) {
        this.fixRPM_rpm = fixRPM_rpm;
    }

    public String getTechnicalName() {
        return technicalName;
    }

    public void setTechnicalName(String technicalName) {
        this.technicalName = technicalName;
    }

    public String getVentilationType() {
        return ventilationType;
    }

    public void setVentilationType(String ventilationType) {
        this.ventilationType = ventilationType;
    }

    public String getMinRPM_rpm() {
        return minRPM_rpm;
    }

    public void setMinRPM_rpm(String minRPM_rpm) {
        this.minRPM_rpm = minRPM_rpm;
    }

    public String getMaxRPM_rpm() {
        return maxRPM_rpm;
    }

    public void setMaxRPM_rpm(String maxRPM_rpm) {
        this.maxRPM_rpm = maxRPM_rpm;
    }

    public String getRPMZeroToMax_s() {
        return RPMZeroToMax_s;
    }

    public void setRPMZeroToMax_s(String RPMZeroToMax_s) {
        this.RPMZeroToMax_s = RPMZeroToMax_s;
    }

    public String getRPMMaxChangeRate_rpm_s() {
        return RPMMaxChangeRate_rpm_s;
    }

    public void setRPMMaxChangeRate_rpm_s(String RPMMaxChangeRate_rpm_s) {
        this.RPMMaxChangeRate_rpm_s = RPMMaxChangeRate_rpm_s;
    }

    public String getGearRatio() {
        return gearRatio;
    }

    public void setGearRatio(String gearRatio) {
        this.gearRatio = gearRatio;
    }

    public String getMinPitch_deg() {
        return minPitch_deg;
    }

    public void setMinPitch_deg(String minPitch_deg) {
        this.minPitch_deg = minPitch_deg;
    }

    public String getMaxPitch_deg() {
        return maxPitch_deg;
    }

    public void setMaxPitch_deg(String maxPitch_deg) {
        this.maxPitch_deg = maxPitch_deg;
    }

    public String getPitchZeroToMax_s() {
        return pitchZeroToMax_s;
    }

    public void setPitchZeroToMax_s(String pitchZeroToMax_s) {
        this.pitchZeroToMax_s = pitchZeroToMax_s;
    }

    public String getPitchMaxChangeRate_deg_s() {
        return pitchMaxChangeRate_deg_s;
    }

    public void setPitchMaxChangeRate_deg_s(String pitchMaxChangeRate_deg_s) {
        this.pitchMaxChangeRate_deg_s = pitchMaxChangeRate_deg_s;
    }

    public String getMaxNegativePower_kW() {
        return maxNegativePower_kW;
    }

    public void setMaxNegativePower_kW(String maxNegativePower_kW) {
        this.maxNegativePower_kW = maxNegativePower_kW;
    }

    public String getMaxPositivePower_kW() {
        return maxPositivePower_kW;
    }

    public void setMaxPositivePower_kW(String maxPositivePower_kW) {
        this.maxPositivePower_kW = maxPositivePower_kW;
    }

    public String getMaxNegativeThrust_kN() {
        return maxNegativeThrust_kN;
    }

    public void setMaxNegativeThrust_kN(String maxNegativeThrust_kN) {
        this.maxNegativeThrust_kN = maxNegativeThrust_kN;
    }

    public String getMaxPositiveThrust_kN() {
        return maxPositiveThrust_kN;
    }

    public void setMaxPositiveThrust_kN(String maxPositiveThrust_kN) {
        this.maxPositiveThrust_kN = maxPositiveThrust_kN;
    }

    private String name;
    private String diameter_m;
    private boolean fixPitch_deg;
    private boolean fixRPM_rpm;
    private String technicalName;
    private String ventilationType;
    private String minRPM_rpm;
    private String maxRPM_rpm;
    private String RPMZeroToMax_s;
    private String RPMMaxChangeRate_rpm_s;
    private String gearRatio;
    private String minPitch_deg;
    private String maxPitch_deg;
    private String pitchZeroToMax_s;
    private String pitchMaxChangeRate_deg_s;
    private String maxNegativePower_kW;
    private String maxPositivePower_kW;
    private String maxNegativeThrust_kN;
    private String maxPositiveThrust_kN;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
