/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.osc.spec;

import com.thoughtworks.xstream.XStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author pierre
 */
public class TestSpec {

    public static void main(String[] args) throws FileNotFoundException {

        printSpec(addDWL(createGenericIslandCrown()));
       
        
    
    }

    public static void printReportSpec(OSCHullDataSpec spec) {

        XStream xstream = new XStream();

        ReportSpec reportSpec = new ReportSpec();
        reportSpec.setRunPolar(false);
        reportSpec.setRunRAO(false);
        reportSpec.setRunCrashStop(true);
        reportSpec.setRunWheelingHousePoster(false);
        reportSpec.setRunSensorInfo(false);
        reportSpec.setRunDPtest(false);
        reportSpec.setRaoPeriodStep(0.05);
        reportSpec.setRunExtraRAOPeriods(true);

        String xml = xstream.toXML(reportSpec);
        System.out.println(xml);

//        pwx = new PrintWriter(new File(spec.getReportFileName();));
//        pwx.append(xml);
//        pwx.close();
    }

    public static void printSpec(OSCHullDataSpec spec) throws FileNotFoundException {

        XStream xstream = new XStream();
        String xml = xstream.toXML(spec);

        PrintWriter pwx = new PrintWriter(new File(spec.getSpecFileName()));
        pwx.append(xml);
        pwx.close();

        OSCHullDataSpec newJoe = (OSCHullDataSpec) xstream.fromXML(xml);

        System.out.println(" number of thrusters " + newJoe.getThrusters().size());
        System.out.println(newJoe.getThrusters().get(0).getName());

        printReportSpec(spec);
    }

    /**
     * Add the specific requirements for Island Crown
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addDWL(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("DWL");
        spec.setDisplacement_t("7319");
        spec.setDraught_m("85.84");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("2.356");
        spec.setGRelBL_m("7.044");
        spec.setRollPeriod_s("xxx");
        spec.setIsTransitLoadingCondition(false);
        return spec;
    }
	
	 /**
     * Add the specific requirements for LC01B : VOYAGE 8.512m
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addLC01B(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC01B");
        spec.setDisplacement_t("74110");
        spec.setDraught_m("8.512");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("7.760");
        spec.setGRelBL_m("20.62");
        spec.setRollPeriod_s("17.7");
        spec.setIsTransitLoadingCondition(true);
        return spec;
    }

    /**
     * Add the specific requirements for LC10A : 2200T MONOPILE INSTALLATION
     * PREPARATION
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addLC10A(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC10A");
        spec.setDisplacement_t("96550");
        spec.setDraught_m("10.816");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("7.692");
        spec.setGRelBL_m("17.04");
        spec.setRollPeriod_s("14.8");
        return spec;
    }

    /**
     * Add the specific requirements for LC10A with free surface effect 2200T
     *
     * @param spec generic spec
     */
    public static OSCHullDataSpec addLC10A_reduced(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC10A_reduced");
        spec.setDisplacement_t("96550");
        spec.setDraught_m("10.816");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("6.942");
        spec.setGRelBL_m("17.79");
        spec.setRollPeriod_s("15.6");
        return spec;
    }

    /**
     * Add the specific requirements for LC10C monopile installation upending
     * finish
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addLC10C(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC10C");
        spec.setDisplacement_t("96550");
        spec.setDraught_m("10.772");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("3.074");
        spec.setGRelBL_m("21.64");
        spec.setRollPeriod_s("15");
        return spec;
    }

    /**
     * Add the specific requirements for LC10C monopile installation upending
     * finish reduced
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addLC10C_reduced(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC10C_reduced");
        spec.setDisplacement_t("96550");
        spec.setDraught_m("10.772");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("2.32");
        spec.setGRelBL_m("22.39");
        spec.setRollPeriod_s("17.3");
        return spec;
    }

    /**
     * Add the specific requirements for LC10D monopile installation resting on
     * sea floor reduced
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addLC10D(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC10D");
        spec.setDisplacement_t("94350");
        spec.setDraught_m("10.56");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("6.763");
        spec.setGRelBL_m("18.24");
        spec.setRollPeriod_s("15");
        return spec;
    }

    /**
     * Add the specific requirements for LC10D monopile installation resting on
     * sea floor reduced
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addLC10D_reduced(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC10D_reduced");
        spec.setDisplacement_t("94350");
        spec.setDraught_m("10.56");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("6.013");
        spec.setGRelBL_m("18.99");
        spec.setRollPeriod_s("15.9");
        return spec;
    }

    /**
     * Add the specific requirements for LC10E 2200T Max radius 73.4m forward
     * reduced
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addLC10E(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC10E");
        spec.setDisplacement_t("94350");
        spec.setDraught_m("10.582");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("4.443");
        spec.setGRelBL_m("20.57");
        spec.setRollPeriod_s("15");
        return spec;
    }

    /**
     * Add the specific requirements for LC10E 2200T Max radius 73.4m forward
     * reduced
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addLC10E_reduced(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC10E_reduced");
        spec.setDisplacement_t("94350");
        spec.setDraught_m("10.582");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("3.693");
        spec.setGRelBL_m("21.32");
        spec.setRollPeriod_s("15");
        return spec;
    }

    /**
     * Add the specific requirements for LC11A LIft max radius 73.4m for MP
     * foremost MONOPILE INSTALLATION PREPARATION
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addLC11A(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC11A");
        spec.setDisplacement_t("93848");
        spec.setDraught_m("10.601");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("3.971");
        spec.setGRelBL_m("21.25");
        spec.setRollPeriod_s("15");
        return spec;
    }

    /**
     * Add the specific requirements for LC11A LIft max radius 73.4m for MP
     * foremost
     *
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addLC11A_reduced(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC11A_reduced");
        spec.setDisplacement_t("93848");
        spec.setDraught_m("10.601");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("3.22");
        spec.setGRelBL_m("22");
        spec.setRollPeriod_s("16.6");
        return spec;
    }

    /**
     * Add the specific requirements for LC11C 2200T lift max radius 43m sb t =
     * 10.5m reduced
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addLC11C(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC11C");
        spec.setDisplacement_t("93848");
        spec.setDraught_m("10.516");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("3.339");
        spec.setGRelBL_m("21.87");
        spec.setRollPeriod_s("15");
        return spec;
    }

    /**
     * Add the specific requirements for LC11C 2200T lift max radius 43m sb t =
     * 10.5m MONOPILE INSTALLATION PREPARATION
     *
     * @param spec generic spec
     * @return OSCHullDataSpec
     */
    public static OSCHullDataSpec addLC11C_reduced(OSCHullDataSpec spec) {

        spec.setStabilityBookletCase("LC11C_reduced");
        spec.setDisplacement_t("93848");
        spec.setDraught_m("10.516");
        spec.setHeel_deg("0.0");
        spec.setMetacentricHeight_m("2.59");
        spec.setGRelBL_m("22.62");
        spec.setRollPeriod_s("17");
        return spec;
    }

    public static OSCHullDataSpec createGenericIslandCrown() {

       String root  = FhSimModelsFolder.getFolder();    
        
        
        OSCHullDataSpec spec = new OSCHullDataSpec();
        spec.setFileNameRoot(root + "\\OSCV_UT776CD_ISL\\dynmodel\\fhsim");
        spec.setLPP_m("80.8");
        spec.setLoa_m("98.6");
        spec.setBredth_m("19.978");
        spec.setBlockCoefficient("not known");

        OSCThrusterTypeSpec tunneltype = new OSCThrusterTypeSpec();
        tunneltype.setName("tunnel thruster");

        OSCThrusterTypeSpec retractabletype = new OSCThrusterTypeSpec();
        retractabletype.setName("retractable thruster");

        OSCThrusterTypeSpec maintype = new OSCThrusterTypeSpec();
        maintype.setName("azipod thruster");

        OSCThrusterSpec thruster1 = new OSCThrusterSpec();
        thruster1.setId(1);
        thruster1.setName("foreTT");
        thruster1.setType(tunneltype);

        OSCThrusterSpec thruster2 = new OSCThrusterSpec();
        thruster2.setId(2);
        thruster2.setName("aftTT");
        thruster2.setType(tunneltype);

        OSCThrusterSpec thruster3 = new OSCThrusterSpec();
        thruster3.setId(3);
        thruster3.setName("FF");
        thruster3.setType(retractabletype);

        OSCThrusterSpec thruster5 = new OSCThrusterSpec();
        thruster5.setId(5);
        thruster5.setName("PS");
        thruster5.setType(maintype);

        OSCThrusterSpec thruster8 = new OSCThrusterSpec();
        thruster8.setId(8);
        thruster8.setName("SB");
        thruster8.setType(maintype);

        ArrayList<OSCThrusterSpec> thrusters = new ArrayList<OSCThrusterSpec>();
        thrusters.add(thruster1);
        thrusters.add(thruster2);
        thrusters.add(thruster3);
        thrusters.add(thruster5);
        thrusters.add(thruster8);

        spec.setThrusters(thrusters);

        return spec;

    }



    public static OSCHullDataSpec createGenericOrion() {

        OSCHullDataSpec spec = new OSCHullDataSpec();
        spec.setFileNameRoot("C:\\Users\\pierre\\Documents\\fhsim_models_repo\\src\\main\\resources\\models\\fhsim_models\\HLIV_CJOB720_GEO_ORION\\dynmodel\\fhsim");
        spec.setLPP_m("203.13");
        spec.setLoa_m("222.46");
        spec.setBredth_m("49");
        spec.setBlockCoefficient("not known");

        OSCThrusterTypeSpec tunneltype = new OSCThrusterTypeSpec();
        tunneltype.setName("tunnel thruster");

        OSCThrusterTypeSpec retractabletype = new OSCThrusterTypeSpec();
        retractabletype.setName("retractable thruster");

        OSCThrusterTypeSpec maintype = new OSCThrusterTypeSpec();
        maintype.setName("azipod thruster");

        OSCThrusterSpec tunnelThruster1 = new OSCThrusterSpec();
        tunnelThruster1.setId(1);
        tunnelThruster1.setName("foreTT");
        tunnelThruster1.setType(tunneltype);

        OSCThrusterSpec tunnelThruster2 = new OSCThrusterSpec();
        tunnelThruster2.setId(2);
        tunnelThruster2.setName("aftTT");
        tunnelThruster2.setType(tunneltype);

        OSCThrusterSpec tunnelThruster3 = new OSCThrusterSpec();
        tunnelThruster3.setId(3);
        tunnelThruster3.setName("FF");
        tunnelThruster3.setType(retractabletype);

        OSCThrusterSpec tunnelThruster4 = new OSCThrusterSpec();
        tunnelThruster4.setId(4);
        tunnelThruster4.setName("FA");
        tunnelThruster4.setType(retractabletype);

        OSCThrusterSpec tunnelThruster5 = new OSCThrusterSpec();
        tunnelThruster5.setId(5);
        tunnelThruster5.setName("PS");
        tunnelThruster5.setType(maintype);

        OSCThrusterSpec tunnelThruster6 = new OSCThrusterSpec();
        tunnelThruster6.setId(6);
        tunnelThruster6.setName("PSM");
        tunnelThruster6.setType(maintype);

        OSCThrusterSpec tunnelThruster7 = new OSCThrusterSpec();
        tunnelThruster7.setId(7);
        tunnelThruster7.setName("SBM");
        tunnelThruster7.setType(maintype);

        OSCThrusterSpec tunnelThruster8 = new OSCThrusterSpec();
        tunnelThruster8.setId(8);
        tunnelThruster8.setName("SB");
        tunnelThruster8.setType(maintype);

        ArrayList<OSCThrusterSpec> thrusters = new ArrayList<OSCThrusterSpec>();
        thrusters.add(tunnelThruster1);
        thrusters.add(tunnelThruster2);
        thrusters.add(tunnelThruster3);
        thrusters.add(tunnelThruster4);
        thrusters.add(tunnelThruster5);
        thrusters.add(tunnelThruster6);
        thrusters.add(tunnelThruster7);
        thrusters.add(tunnelThruster8);

        spec.setThrusters(thrusters);

        return spec;

    }

}
