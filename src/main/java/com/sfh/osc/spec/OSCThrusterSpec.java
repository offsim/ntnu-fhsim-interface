/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfh.osc.spec;

/**
 *
 * @author pierre
 */
public class OSCThrusterSpec {

    private OSCThrusterTypeSpec type;
    private String[] positionAPCLBL_m;
    private String[] mountingAngle_deg;
    private String name;
    private int id;

    public String[] getPositionAPCLBL_m() {
        return positionAPCLBL_m;
    }

    public void setPositionAPCLBL_m(String[] positionAPCLBL_m) {
        this.positionAPCLBL_m = positionAPCLBL_m;
    }

    public String[] getMountingAngle_deg() {
        return mountingAngle_deg;
    }

    public void setMountingAngle_deg(String[] mountingAngle_deg) {
        this.mountingAngle_deg = mountingAngle_deg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public OSCThrusterTypeSpec getType() {
        return type;
    }

    public void setType(OSCThrusterTypeSpec type) {
        this.type = type;
    }

}
