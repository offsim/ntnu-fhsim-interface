/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.osc.wave;

import com.sfh.ship.FhDynamicModel;
import java.util.ArrayList;

/**
 *
 * @author aarsathe
 */
public class SingleWave implements IWaveModel{

    private double waveAmplitude;
    private double waveDirection;
    private double waveFrequency;
    private double wavePhase = 0; //For backward compatibility
    private final ArrayList<IWaveModelListener> listeners = new ArrayList<>();

    public SingleWave(){
            FhDynamicModel.instance().setNumberOfWavesInFhSim(1);
    }
    @Override
    public double[] getPhase() {
        double [] retval  = new double[1];
        retval[0] = wavePhase;
        return retval;
    }

    @Override
    public double[] getPsi() {
        double [] retval  = new double[1];
        retval[0] = waveDirection;
        return retval;        
    }

    @Override
    public double[] getOmega() {
        double [] retval  = new double[1];
        retval[0] = waveFrequency;
        return retval;
    }

    @Override
    public double[] getWavenum() {
        double [] retval  = new double[1];
        retval[0] = waveFrequency*waveFrequency/9.81;
        return retval;
    }

    @Override
    public double[] getZeta_a() {
        double [] retval  = new double[1];
        retval[0] = waveAmplitude;
        return retval;
    }

    @Override
    public void addListener(IWaveModelListener listener) {
        if( ! listeners.contains(listener) )
            listeners.add(listener);
       listener.waveModelUpdated(this, 1e3);
    }

    @Override
    public int getNumberOfWaves() {
        return 1;
    }

    /**
     * @return the waveAmplitude
     */
    public double getWaveAmplitude() {
        return waveAmplitude;
    }

    /**
     * @param waveAmplitude the waveAmplitude to set
     */
    public void setWaveAmplitude(double waveAmplitude) {
        this.waveAmplitude = waveAmplitude;
    }

    
      /**
     * @param wavePhase the wavePhase to set
     */
    public void setWavePhase(double wavePhase) {
        this.wavePhase = wavePhase;
    }
    
    /**
     * @return the waveDirection
     */
    public double getWaveDirection() {
        return waveDirection;
    }

    /**
     * @param waveDirection the waveDirection to set
     */
    public void setWaveDirection(double waveDirection) {
        this.waveDirection = waveDirection;
    }

    /**
     * @return the waveFrequency
     */
    public double getWaveFrequency() {
        return waveFrequency;
    }

    /**
     * @param waveFrequency the waveFrequency to set
     */
    public void setWaveFrequency(double waveFrequency) {
        this.waveFrequency = waveFrequency;
    }
    
}
