package com.osc.wave;

import com.sfh.ship.FhDynamicModel;
import java.util.ArrayList;

/**
 * Class to implement a flat sea (no waves) for use in reporting and tuning. 
 * @author Karl Gunnar Aarsæther
 */
public class FlatSea implements IWaveModel{

    public FlatSea(){
            FhDynamicModel.instance().setNumberOfWavesInFhSim(1);
    }
    
    private final ArrayList<IWaveModelListener> listeners = new ArrayList<>();
    
    private final double [] phase = {0};
    private final double [] psi = {0};
    private final double [] omg = {1};
    private final double [] k = {1};
    private final double [] z = {0};
    
    @Override
    public double[] getPhase() {
        return phase;
    }

    @Override
    public double[] getPsi() {
        return psi;
    }

    @Override
    public double[] getOmega() {
        return omg;
    }

    @Override
    public double[] getWavenum() {
        return k;
    }

    @Override
    public double[] getZeta_a() {
        return z;
    }

    @Override
    public void addListener(IWaveModelListener listener) {
                if( ! listeners.contains(listener) )
            listeners.add(listener);
    }

    public void toggleWaveFieldUpdate(){
    }

    @Override
    public int getNumberOfWaves() {
        return 1;
    }
}
