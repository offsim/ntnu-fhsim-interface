
package com.osc.wave;

/**
 * Interface to a wave model which implements a standard discrete realization of a wave spectrum 
 * 
 * The wave field is constructed by a summation of sine components zeta*sin(omega*t-k_x*x-k_y*y-phase)
 * where the direction information is contained in the wave numbers k_x and k_y
 * @author aarsathe
 */
public interface IWaveModel {

    /**
     * Get the random phase angle of the wave field components
     * @return vector of phase angles 
     */
    double[] getPhase();

    /**
     * Return the wave direction in degrees from north (0 degrees north propagation direction)
     * @return vector of wave directions 
     */
    double[] getPsi();

    /**
     * Return the wave frequency in rad/s: omega
     * @return vector of wave frequencies 
     */
    double[] getOmega();

    /**
     * Return the wave number in the direction of propagation. 
     * 
     * The wave number is the number of wavelengths pr meter and for deep sea waves is a direct function of wave frequency
     * @return
     */
    double[] getWavenum();

    /**
     *Return the wave amplitudes in meter
     * @return vector of wave amplitudes
     */
    double[] getZeta_a();

    /**
     *  Add a object to receive notifications when the internal representation of the wave field changes
     * @sa IWaveModelListener
     * @param listener Reference to object which implements IWaveModelListener
     */
    void  addListener(IWaveModelListener listener);
    
    /**
     * Get the number of waves in the wave field. Used in order to allow the wave field on both hull and environment to be controlled from OSC 
     * @return the number of waves in the wave field
     */
    int getNumberOfWaves();

}
