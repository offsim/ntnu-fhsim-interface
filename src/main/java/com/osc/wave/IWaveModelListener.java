package com.osc.wave;

/**
 * Interface between OSC wave model and other implementations of the wave field
 * @author aarsathe
 */
public interface IWaveModelListener {

    /**
     * Call back for OSC wave model which notifies object of updates to the wave pattern
     * @param model The model which is changed
     * @param transitionTimeInMilliseconds The time which should be used to transition to the new wave field
     */
    abstract void waveModelUpdated(IWaveModel model, double transitionTimeInMilliseconds);
}
